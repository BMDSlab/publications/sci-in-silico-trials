# *In silico* trials for spinal cord injury
This is the code accompanying the paper "Synthetic controls for traumatic spinal cord injury - a proof of principle by _in silico_ trial simulations".

### Background
Conducting clinical trials in spinal cord injury is challenging due to the heterogeneity between patients. One way to mitigate this is to use synthetic controls, 
i.e. a prediction of the patient state without treatment at the end of a trial using machine learning models. 

### Package info
This library uses data from the [EMSCI database](https://www.emsci.org/) and the [Sygen trial](https://doi.org/10.1097/00007632-200112151-00015) to train machine learning 
models to predict the full motor score sequence of the [ISNSCI examination](https://asia-spinalinjury.org/international-standards-neurological-classification-sci-isncsci-worksheet/). 
We evaluate the model using the RMSE below the level of injury, since levels above the injury are not affected in the same way and it allows for comparison across patients with 
different neurological levels of injury. To investigate the feature importance and what drives the prediction of the models, we use [SHAP values](https://shap.readthedocs.io/en/latest/). 
The machine learning models are then used to simulate a clinical trial, in which the models' predictions are used as synthetic controls.

Models evaluated as part of the study and included in this repository: 
- Ridge regression
- Elastic net
- Random forest
- eXtreme gradient boosting
- Convolutional neural network
- Transformer
- Graph neural network (*note additional details provided in the corresponding subdirectory*)

## Required packages
Install the requirements using requirements.txt.

Make sure you are in the directory of the requirements.txt file and run:
```bash
pip install -r requirements.txt
```

## Prediction model benchmark
1. Add paths of data to paths.py
2. run run_benchmark_training_eval.py (this will run the benchmark training, evaluation, and SHAP interpretability)

## In silico trial simulation framework
No further installations are required. We provide the code for the simulation framework and the evaluation of the simulation results to produce figures as shown in Figure 2 of the manuscript. This code requires model predictions (as synthetic controls to be provided in csv format for all included patients).
1. Run a specific trial simulation: Use run_trialSimulation.py 
    Specify the relevant paths (to reference data and synthetic controls) and trial settings (number of patients, included plegia and AIS grade distribtuion, inclusion criteria, time frames) at the end of the script as indicated. Rerun the script for a range of trial simulations. 
2. Evaluate the simulations: Use evaluateInSilicoTrials.py
    Indicate the relevant paths and naming convention of the trials you previously executed at the end of the script. Check different plot options as indicated.



