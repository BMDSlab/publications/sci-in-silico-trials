import pickle
from Miscellaneous.Misc.paths import *
from Run_collection.Run_prediction.SCIRecoveryPrediction_no_shape import run_training


def run(with_uncertainty=False, run_shap=False, plot_shap=False, sides_are_same=False, debug=False,
        run_model_training=True,
        models_to_run=basemodel_names, n_splits=5, parallelize_manually=False, augmented_evaluation=False,
        n_bootstrap=100, MS_are_same=False):
    if with_uncertainty:

        data = {}
        if debug:
            n_splits = 1
        for i in range(n_splits):

            data_path = open(get_data_dict_splits_path(i, n_bootstrap, scaled=True, augmented=True), 'rb')
            data_fold = pickle.load(data_path)

            data[i] = data_fold
    else:

        data = {}

        for i in range(n_splits):
            data_path = open(get_data_dict_splits_path(i, scaled=True, augmented=False), 'rb')
            data_fold = pickle.load(data_path)
            data[i] = data_fold

    for i in range(n_splits):

        try:
            data[i]['X_train_MS0'] = data[i]['X_train_MS0'].drop(['sc_X'], axis=1)
            data[i]['X_test_MS0'] = data[i]['X_test_MS0'].drop(['sc_X'], axis=1)
            data[i]['X_train_MS1_5'] = data[i]['X_train_MS1_5'].drop(['sc_X'], axis=1)
            data[i]['X_test_MS1_5'] = data[i]['X_test_MS1_5'].drop(['sc_X'], axis=1)

            print('Dropped sc_X fully')
        except:
            pass

    if debug:
        debug_length = 20000

        for i in range(len(data.keys())):
            data[i]['X_train_MS0'] = data[i]['X_train_MS0'].iloc[:debug_length]
            data[i]['X_test_MS0'] = data[i]['X_test_MS0'].iloc[:debug_length]
            data[i]['Y_train_MS0'] = data[i]['Y_train_MS0'].iloc[:debug_length]
            data[i]['Y_test_MS0'] = data[i]['Y_test_MS0'].iloc[:debug_length]

            data[i]['X_train_MS1_5'] = data[i]['X_train_MS1_5'].iloc[:debug_length]
            data[i]['X_test_MS1_5'] = data[i]['X_test_MS1_5'].iloc[:debug_length]
            data[i]['Y_train_MS1_5'] = data[i]['Y_train_MS1_5'].iloc[:debug_length]
            data[i]['Y_test_MS1_5'] = data[i]['Y_test_MS1_5'].iloc[:debug_length]

    print(os.getcwd())
    Path(euler_emsci_2023_path).mkdir(parents=True, exist_ok=True)

    for clf_choice in models_to_run:

        if sides_are_same:
            clf_choice_folder = clf_choice + '_sides_are_same'
        else:
            clf_choice_folder = clf_choice

        if MS_are_same:
            clf_choice_folder = clf_choice_folder + '_MS_are_same'

        output_clf = get_model_path(clf_choice_folder, debug=debug, with_uncertainty=with_uncertainty)
        print(clf_choice, output_clf)
        Path(output_clf).mkdir(parents=True, exist_ok=True)

        shap_path = os.path.join(output_clf, 'SHAP')
        Path(shap_path).mkdir(parents=True, exist_ok=True)

        feature_importance_path = os.path.join(output_clf, 'feature_importance')
        Path(feature_importance_path).mkdir(parents=True, exist_ok=True)

        run_training(data, clf_choice, output_clf, run_shap=run_shap, plot_shap=plot_shap,
                     sides_are_same=sides_are_same, debug=debug,
                     run_model_training=run_model_training, with_uncertainty=with_uncertainty,
                     parallelize_manually=parallelize_manually, augmented_evaluation=augmented_evaluation,
                     n_bootstrap=n_bootstrap,
                     MS_are_same=MS_are_same)

    print('Finished training all models')
