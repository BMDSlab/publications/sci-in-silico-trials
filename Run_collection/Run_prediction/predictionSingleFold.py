import os

import numpy as np
import pandas as pd

from sklearn.linear_model import LogisticRegression, Ridge, LinearRegression, ElasticNet, Lasso
from sklearn.model_selection import GridSearchCV
from sklearn.neural_network import MLPRegressor
from sklearn.ensemble import RandomForestRegressor
from sklearn.neighbors import KNeighborsRegressor
import xgboost as xgb
import multiprocessing as mp
import copy
from tqdm import tqdm

from Miscellaneous.Misc.columns import *
from Miscellaneous.Misc.fields_to_match import *
from Miscellaneous.Misc.paths import get_model_path


def getPredictionSingleFold(X_train, y_train, clf_choice, sides_are_same=True, debug=False,
                            parallelize=False, seed=1, with_uncertainty=False, fold_index=0, est=None, MS_are_same=False):
    n_jobs = -1
    folds = 5
    if debug:
        debug_var = -1
    else:
        debug_var = 0

    if clf_choice == MLP_str:
        clf = MLPRegressor(max_iter=1000)

        param_grid = {'solver': ['adam'],
                      'alpha': [0, 1e-04, 1e-02][debug_var:],
                      'activation': ['relu', 'logistic'][debug_var:],
                      'hidden_layer_sizes': [5, 10, 50][debug_var:],
                      'learning_rate_init': [0.001, 0.0001][debug_var:],
                      'early_stopping': [True],
                      'validation_fraction': [0.25]}
        clf = GridSearchCV(clf, param_grid, cv=folds, verbose=1, refit=True, n_jobs=n_jobs)

    elif clf_choice == LogR_str:
        clf = LogisticRegression(max_iter=30000, class_weight='balanced', random_state=seed, n_jobs=n_jobs)
        param_grid = {'penalty': ['elasticnet'],  #
                      'l1_ratio': np.logspace(-7, 0, 4)[debug_var:],
                      'C': np.logspace(-7, 4, 4)[debug_var:],
                      'solver': ['saga']}
        clf = GridSearchCV(clf, param_grid, cv=2, verbose=1, refit=True, n_jobs=n_jobs)

    elif clf_choice == LR_str:
        clf = LinearRegression(n_jobs=n_jobs, fit_intercept=False)

    elif clf_choice == RF_str:
        # Initialise random forest, optimise hyperparameters by random grid
        clf = RandomForestRegressor(random_state=seed, n_jobs=n_jobs)

        # Number of trees in random forest
        n_estimators = [200, 1000][debug_var:]

        # Number of features to consider at every split
        max_features = ['sqrt']  # , 'sqrt']

        # Maximum number of levels in tree
        max_depth = [5, 7, None][debug_var:]

        # Minimum number of samples required to split a node
        min_samples_split = [3, 5][debug_var:]

        # Minimum number of samples required at each leaf node
        min_samples_leaf = [3, 6][debug_var:]

        # Method of selecting samples for training each tree
        bootstrap = [True]

        # Create the random grid
        param_grid = {'n_estimators': n_estimators,
                      'max_features': max_features,
                      'max_depth': max_depth,
                      'min_samples_split': min_samples_split,
                      'min_samples_leaf': min_samples_leaf,
                      'bootstrap': bootstrap}

        folds = 5
        clf = GridSearchCV(clf, param_grid, cv=folds, verbose=1, scoring='neg_root_mean_squared_error', refit=True,
                           n_jobs=n_jobs)

    elif clf_choice == XGB_str:
        # Initialise model, optimise hyperparameters by random sampling
        clf = xgb.XGBRegressor(random_state=seed, n_jobs=n_jobs)

        # Maximum tree leaves for base learners


        max_depth = [3, 5, 10][debug_var:]

        # Boosting learning rate
        eta = [0.001, 0.01, 0.1][debug_var:]

        colsample_bytree = [0.5, 0.75, 1][debug_var:]

        # Number of boosted trees to fit
        if debug:
            n_estimators = [10]
        else:
            n_estimators = [500]

        # Create the random grid
        param_grid = {
            'eta': eta,
            'n_estimators': n_estimators,
            'colsample_bytree': colsample_bytree,
            'max_depth': max_depth,
            'subsample': [1]}

        # Could replace for random search here, too

        clf = GridSearchCV(clf, param_grid, cv=folds, scoring='neg_root_mean_squared_error', verbose=1,
                           n_jobs=n_jobs)  # set scoring='neg_root_mean_squared_error' for regression?


    elif clf_choice == KNN_str:
        clf = KNeighborsRegressor(n_jobs=-1)

        # Number of trees in random forest
        n_neighbors = [5, 10, 20][debug_var:]

        # Number of features to consider at every split
        weights = ['uniform', 'distance'][debug_var:]

        # Maximum number of levels in tree
        p = [1, 2][debug_var:]

        # Create the random grid
        param_grid = {'n_neighbors': n_neighbors,
                      'weights': weights,
                      'p': p}

        # Could replace for random search here, too
        folds = 5
        clf = GridSearchCV(clf, param_grid, cv=folds, verbose=1, scoring='neg_root_mean_squared_error', n_jobs=n_jobs)
    elif clf_choice == Ridge_str:
        clf = Ridge(random_state=seed, max_iter=100000)

        param_grid = {'alpha': [0, 1e-04, 1e-02][debug_var:],

                      }
        clf = GridSearchCV(clf, param_grid, cv=folds, verbose=1, scoring='neg_root_mean_squared_error', n_jobs=n_jobs)
    elif clf_choice == ElasticNet_str:
        clf = ElasticNet(random_state=seed, max_iter=100000)

        param_grid = {'alpha': [1e-04, 1e-02][debug_var:],
                      'l1_ratio': np.logspace(-7, 0, 4)[debug_var:]}
        clf = GridSearchCV(clf, param_grid, cv=folds, verbose=1, scoring='neg_root_mean_squared_error', n_jobs=n_jobs)
    elif clf_choice == Lasso_str:
        clf = Lasso(random_state=seed, max_iter=100000)

        param_grid = {'alpha': [1e-04, 1e-02][debug_var:]}
        clf = GridSearchCV(clf, param_grid, cv=folds, verbose=1, scoring='neg_root_mean_squared_error', n_jobs=n_jobs)
    else:
        raise ValueError('Invalid clf_choice', clf_choice)

    # Fit
    if sides_are_same == False:
        motorscore_models = ms_both_sides_fields_toMatch.copy()
    else:
        motorscore_models = ms_bothSide.copy()

    mp_input = []
    if parallelize:
        for m in motorscore_models:
            mp_input.append([copy.deepcopy(clf), copy.deepcopy(X_train), copy.deepcopy(y_train), m, sides_are_same])

        print('Starting training of motor score models')
        try:
            with mp.Pool(len(motorscore_models)) as pool:
                pool.daemon = False
                results = pool.starmap(get_trained_motor_scores_models, tqdm(mp_input))
        except AssertionError:
            results = []
            for pack in mp_input:
                best_est, ms = get_trained_motor_scores_models(*pack)
                results.append((best_est, ms))
    else:
        for m in motorscore_models:
            mp_input.append([clf, X_train, y_train, m, sides_are_same, with_uncertainty, est, fold_index, MS_are_same])

        results = []
        for pack in mp_input:
            best_est, ms = get_trained_motor_scores_models(*pack)
            results.append((best_est, ms))

    trained_motorscore_models = {}
    for best_est, ms in results:
        trained_motorscore_models[ms] = best_est

    return trained_motorscore_models


def get_trained_motor_scores_models(clf, X_train, y_train, motorscore_model, sides_are_same, with_uncertainty=False,
                                    est=None, fold_index=0, MS_are_same=False):
    X_train_ms, y_train_ms, _ = get_training_data_level(X_train, y_train, motorscore_model,
                                                        sides_are_same=sides_are_same)
    print('Training model for motor score: ', motorscore_model)
    print('Training data shape: ', X_train_ms.shape)

    clf.fit(X_train_ms, y_train_ms)

    # Save best estimator
    if hasattr(clf, 'best_estimator_'):
        best_est = clf.best_estimator_
    else:
        best_est = clf

    best_est.fit(X_train_ms, y_train_ms)

    return best_est, motorscore_model


def get_training_data_level(X_train, y_train, m, sides_are_same=False):
    if not sides_are_same:
        MS_level = m.split('_')[1]
        left_or_right = 1 if m[0] == 'L' else 0  # Left or right
        indices = (X_train['Left'] == left_or_right) & (X_train['level'] == dermatomes_withC1.index(MS_level))

    else:
        indices = X_train['level'] == dermatomes_withC1.index(m)
    X_train = X_train.drop(columns=['level'])
    y_train = y_train.drop(columns=['level'])
    return X_train[indices], y_train[indices], indices


def get_test_data_level(X_test, m, sides_are_same=False):
    if not sides_are_same:
        MS_level = m.split('_')[1]
        left_or_right = 1 if m[0] == 'L' else 0  # Left or right
        indices = (X_test['Left'] == left_or_right) & (X_test['level'] == dermatomes_withC1.index(MS_level))
    else:
        indices = X_test['level'] == dermatomes_withC1.index(m)
    X_test = X_test.drop(columns=['level'])
    return X_test[indices]
