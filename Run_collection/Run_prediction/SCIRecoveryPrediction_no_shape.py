import numbers

from sklearn.metrics import mean_squared_error

from Interpretability.shap_funcs import *
import warnings
from Run_collection.Run_prediction.predictionSingleFold import *
import pickle

warnings.filterwarnings("ignore")
from Miscellaneous.Misc.paths import *
from Miscellaneous.Misc.fields_to_match import *
from Miscellaneous.auxFunctions.math_utils import *


def get_description_dict_metrics(res_dict, metrics, output, save_file=True, augmented_evaluation=False,
                                 data_sequence_unscaled=None, dataset=emsci_str, date=standard_file_date):
    print('Dataset description metrics:', dataset)
    # load unscaled sequence data
    if data_sequence_unscaled is None:
        data_sequence_unscaled = pickle.load(
            open(get_data_dict_splits_full_sequences_path(scaled=False, augmented=False, test_set_augmented=False,
                                                          dataset=dataset, date=date),
                 'rb'))

    n_splits = res_dict.keys().__len__()
    desc_res_all_metrics = {m: {i: {} for i in range(n_splits)} for m in metrics}
    desc_dict = {}

    rmse_lists = []
    mae_lists = []
    mean_abs_lems_lists = []

    desc_df_rmse = pd.DataFrame(columns=np.arange(n_splits))
    desc_df_mae = pd.DataFrame(columns=np.arange(n_splits))
    desc_df_mean_abs_lems = pd.DataFrame(columns=np.arange(n_splits))
    predictions = []
    error_dfs = []

    for fold_index, res in res_dict.items():
        ids = res['id'].unique()
        fold_predictions = {}
        error_df = pd.DataFrame(index=ids, columns=[rmse_str, mae_str, mean_abs_lems_str])

        for i, index in enumerate(ids):
            patient_indices = res['id'] == index

            ind_NLI_patient = data_sequence_unscaled[fold_index][x_use_str].loc[str(index), ind_NLI_str]

            pat_res_below_NLI = res.loc[patient_indices][
                res.loc[patient_indices][motor_score_level_str] > ind_NLI_patient]

            real = pat_res_below_NLI['end value']
            pred = pat_res_below_NLI['pred']
            all_preds = res.loc[patient_indices, 'pred']
            all_preds.index = res.loc[patient_indices, motor_score_index_str]
            fold_predictions[index] = all_preds

            rmse_test_patient = mean_squared_error(real, pred, squared=False)

            mae_test_patient = np.abs(real - pred).mean()
            lems_patient = res[patient_indices][
                res[patient_indices]['motor score'].str.contains('|'.join(lems_ms))]
            mean_abs_lems_test_patient = np.abs(
                lems_patient['end value'] - lems_patient['pred'].astype(float).round(0)).mean()

            error_df.loc[index, rmse_str] = rmse_test_patient
            error_df.loc[index, mae_str] = mae_test_patient
            error_df.loc[index, mean_abs_lems_str] = mean_abs_lems_test_patient

        error_dfs.append(error_df)

        rmse_s = error_df.loc[:, rmse_str].to_list()
        mae_s = error_df.loc[:, mae_str].to_list()
        mean_abs_lems_s = error_df.loc[:, mean_abs_lems_str].to_list()

        rmse_lists.append(rmse_s)
        mae_lists.append(mae_s)
        mean_abs_lems_lists.append(mean_abs_lems_s)

        description_rmse = pd.DataFrame(rmse_s, columns=[fold_index]).describe()
        description_mae = pd.DataFrame(mae_s, columns=[fold_index]).describe()
        description_mean_abs_lems = pd.DataFrame(mean_abs_lems_s, columns=[fold_index]).describe()

        rmse_conf = pd.DataFrame(median_confidence_interval(rmse_s), index=['95%CI-', '95%CI+'],
                                 columns=[fold_index])
        mae_conf = pd.DataFrame(median_confidence_interval(mae_s), index=['95%CI-', '95%CI+'],
                                columns=[fold_index])
        mean_abs_lems_conf = pd.DataFrame(median_confidence_interval(mean_abs_lems_s),
                                          index=['95%CI-', '95%CI+'],
                                          columns=[fold_index])

        rmse_perc = pd.DataFrame(get_percentile(rmse_s), index=['95%-', '95%+'],
                                 columns=[fold_index])
        mae_perc = pd.DataFrame(get_percentile(mae_s), index=['95%-', '95%+'],
                                columns=[fold_index])
        mean_abs_lems_perc = pd.DataFrame(get_percentile(mean_abs_lems_s), index=['95%-', '95%+'],
                                          columns=[fold_index])

        desc_res_all_metrics[rmse_str][fold_index]['stats'] = pd.concat([description_rmse, rmse_conf, rmse_perc])
        desc_res_all_metrics[mae_str][fold_index]['stats'] = pd.concat([description_mae, mae_conf, mae_perc])
        desc_res_all_metrics[mean_abs_lems_str][fold_index]['stats'] = pd.concat(
            [description_mean_abs_lems, mean_abs_lems_conf, mean_abs_lems_perc])

        desc_df_rmse[fold_index] = desc_res_all_metrics[rmse_str][fold_index]['stats']
        desc_df_mae[fold_index] = desc_res_all_metrics[mae_str][fold_index]['stats']
        desc_df_mean_abs_lems[fold_index] = desc_res_all_metrics[mean_abs_lems_str][fold_index]['stats']

        predictions.append(fold_predictions)
    if save_file:
        rmse_path = get_error_desc_df_path(output, rmse_str, augmented_evaluation=augmented_evaluation, dataset=dataset)
        print('RMSE path:', rmse_path)
        desc_df_rmse.to_csv(rmse_path)
        desc_df_mae.to_csv(
            get_error_desc_df_path(output, mae_str, augmented_evaluation=augmented_evaluation, dataset=dataset))
        desc_df_mean_abs_lems.to_csv(
            get_error_desc_df_path(output, mean_abs_lems_str, augmented_evaluation=augmented_evaluation,
                                   dataset=dataset))

        error_df = pd.concat(error_dfs)
        error_df.to_csv(
            get_error_desc_df_path(output, 'error_df', augmented_evaluation=augmented_evaluation, dataset=dataset))

        if dataset == emsci_str:
            predictions_df = pd.DataFrame(predictions[0])
            predictions_df = predictions_df.reindex(ms_both_sides_fields_toMatch)
            predictions_df.to_csv(
                get_predictions_path(output, augmented_evaluation=augmented_evaluation, dataset=dataset))
        elif dataset == sygen_str:
            for i, fold_predictions in enumerate(predictions):
                predictions_df = pd.DataFrame(fold_predictions)
                predictions_df = predictions_df.reindex(ms_both_sides_fields_toMatch)
                predictions_df.to_csv(
                    get_predictions_path(output, augmented_evaluation=augmented_evaluation, dataset=dataset,
                                         fold_index=i))

        if augmented_evaluation:
            pickle.dump(res_dict, open(get_augmented_predictions_path(output), 'wb'))
            aug_res = res_dict[0]
            perc_2_5 = aug_res['Percentile_2_5'].to_frame().reset_index().drop('level_0', axis=1).pivot(
                index='motor score index', columns='id')
            perc_97_5 = aug_res['Percentile_97_5'].to_frame().reset_index().drop('level_0', axis=1).pivot(
                index='motor score index', columns='id')

            perc_2_5 = perc_2_5.reindex(ms_both_sides_fields_toMatch)
            perc_97_5 = perc_97_5.reindex(ms_both_sides_fields_toMatch)

            perc_2_5.to_csv(get_predictions_path(output, augmented_evaluation=True, lower_conf=True, dataset=dataset))
            perc_97_5.to_csv(get_predictions_path(output, augmented_evaluation=True, upper_conf=True, dataset=dataset))

    desc_dict[rmse_str] = rmse_lists
    desc_dict[mae_str] = mae_lists
    desc_dict[mean_abs_lems_str] = mean_abs_lems_lists
    return desc_dict


def get_motor_scores_estimators(datasets, i, clf_choice, output, name, run_model_training, sides_are_same=False,
                                debug=False, with_uncertainty=False, MS_are_same=False):
    # Data split
    X_train_MS0 = datasets['X_train_MS0']
    Y_train_MS0 = datasets['Y_train_MS0']

    X_train_MS1_5 = datasets['X_train_MS1_5']
    Y_train_MS1_5 = datasets['Y_train_MS1_5']

    try:
        X_train_MS0 = X_train_MS0.drop(columns=['Unnamed: 0'])
        X_train_MS1_5 = X_train_MS1_5.drop(columns=['Unnamed: 0'])
    except:
        pass
    # Prediction
    model_folder = os.path.join(output, 'models')
    if debug:
        debug_str = '_debug'
    else:
        debug_str = ''
    motor_scores_estimators_path = get_motor_scores_estimators_path(model_folder, name, debug_str, i)
    Path(model_folder).mkdir(parents=True, exist_ok=True)
    if run_model_training or not os.path.isfile(motor_scores_estimators_path):  # debug
        if not MS_are_same:
            best_est_cv_ms_MS0 = getPredictionSingleFold(X_train_MS0, Y_train_MS0, clf_choice, sides_are_same,
                                                         debug=debug, with_uncertainty=with_uncertainty, fold_index=i,
                                                         est='MS0', MS_are_same=MS_are_same)
            best_est_cv_ms_MS1_5 = getPredictionSingleFold(X_train_MS1_5, Y_train_MS1_5, clf_choice, sides_are_same,
                                                           debug=debug, with_uncertainty=with_uncertainty, fold_index=i,
                                                           est='MS1_5', MS_are_same=MS_are_same)

            motor_scores_estimators = {'MS0': best_est_cv_ms_MS0, 'MS1_5': best_est_cv_ms_MS1_5}

            pickle.dump(motor_scores_estimators, open(motor_scores_estimators_path, 'wb'))
        else:
            # merge MS0 and MS1_5
            X_train = pd.concat([X_train_MS0, X_train_MS1_5])
            Y_train = pd.concat([Y_train_MS0, Y_train_MS1_5])

            best_est_cv_ms_MS0_5 = getPredictionSingleFold(X_train, Y_train, clf_choice, sides_are_same,
                                                           debug=debug, with_uncertainty=with_uncertainty, fold_index=i,
                                                           est='MS0_5', MS_are_same=MS_are_same)
            motor_scores_estimators = {'MS0_5': best_est_cv_ms_MS0_5}
            pickle.dump(motor_scores_estimators, open(motor_scores_estimators_path, 'wb'))
    else:
        motor_scores_estimators = pickle.load(open(motor_scores_estimators_path, 'rb'))
        print('loaded stored models')
    return motor_scores_estimators, i


def to_motor_score_index(level_name, is_left):
    if is_left:
        side_str = 'L'
    else:
        side_str = 'R'
    if isinstance(level_name, numbers.Number):
        level_name = dermatomes_withC1[int(level_name)]

    return '{}MS_{}'.format(side_str, level_name)


def get_res(motor_scores_estimators, datasets, sides_are_same=True, augmented_evaluation=False):
    predictions_list = []
    for est_name, estimators in motor_scores_estimators.items():
        predictions, X_test, Y_test = get_test_data(datasets, est_name)

        for j, (ms, best_est_cv) in enumerate(estimators.items()):
            X_test_ms, Y_test_ms, indices = get_training_data_level(X_test, Y_test, ms,
                                                                    sides_are_same=sides_are_same)
            if X_test_ms.empty:
                continue

            if est_name == 'MS0' and 'sc_X' in X_test_ms.columns:
                X_test_ms = X_test_ms.drop(columns=['sc_X'])
            y_pred = best_est_cv.predict(X_test_ms)
            y_pred_df = pd.DataFrame(y_pred, index=X_test_ms.index, columns=[pred_str])

            predictions.loc[y_pred_df.index, pred_str] = y_pred_df[pred_str]

        # If pred is lower than 0, set it to 0
        predictions['pred'] = predictions['pred'].apply(lambda x: 0 if x < 0 else x)
        # If pred is higher than 5, set it to 5
        predictions['pred'] = predictions['pred'].apply(lambda x: 5 if x > 5 else x)
        predictions[motor_score_level_str] = predictions[motor_score_level_str].astype(int)
        predictions_list.append(predictions)
    fold_res = pd.concat(predictions_list)
    fold_res = fold_res.sort_values(by=[id_str, motor_score_str])
    if augmented_evaluation:
        fold_res = augmented_evaluation_res(fold_res)
    return fold_res


def augmented_evaluation_res(fold_res):
    fold_res[id_str] = fold_res[id_str].apply(lambda x: '_'.join(x.split('_')[:3]))
    aug_res = fold_res.groupby([id_str, motor_score_index_str]).agg(
        id=(id_str, 'first'),
        pred=(pred_str, np.median),
        Percentile_2_5=(pred_str, lambda x: np.percentile(x, 2.5)),
        Percentile_97_5=(pred_str, lambda x: np.percentile(x, 97.5)),
        Confidence_lower=(pred_str, lambda x: median_confidence_interval(x)[0]),
        Confidence_upper=(pred_str, lambda x: median_confidence_interval(x)[1]),
        Min=(pred_str, np.min),
        Max=(pred_str, np.max),
        EndValue=(end_value_str, 'first'),
        StartValue=(start_value_str, 'first'),
        PointPred=(pred_str, 'first'),
        ind_NLI_scaled=(ind_NLI_str, 'first'),
        MotorScore=(motor_score_str, 'first'),
        MotorScoreLevel=(motor_score_level_str, 'first'),
        MotorScoreIndex=(motor_score_index_str, 'first'),

    )
    aug_res.rename(columns={'EndValue': end_value_str,
                            'StartValue': start_value_str,
                            'MotorScore': motor_score_str,
                            'MotorScoreLevel': motor_score_level_str,
                            'MotorScoreIndex': motor_score_index_str,
                            }, inplace=True)

    return aug_res


def get_test_data(datasets, est_name):
    X_test_MS0 = datasets['X_test_MS0']
    Y_test_MS0 = datasets['Y_test_MS0']

    X_test_MS1_5 = datasets['X_test_MS1_5']
    Y_test_MS1_5 = datasets['Y_test_MS1_5']

    X_test_MS0_ids = X_test_MS0['Unnamed: 0']
    X_test_MS1_5_ids = X_test_MS1_5['Unnamed: 0']

    X_test_MS0 = X_test_MS0.drop(['Unnamed: 0'], axis=1)
    X_test_MS1_5 = X_test_MS1_5.drop(['Unnamed: 0'], axis=1)

    Y_test_MS0_preds = pd.DataFrame(index=X_test_MS0.index,
                                    columns=['id', 'ind_NLI', 'motor score', 'motor score level', 'start value',
                                             'end value', 'pred'])
    Y_test_MS1_5_preds = pd.DataFrame(index=X_test_MS1_5.index,
                                      columns=['id', 'ind_NLI', 'motor score', 'motor score level', 'start value',
                                               'end value', 'pred'])

    Y_test_MS0_preds['id'] = X_test_MS0_ids
    Y_test_MS1_5_preds['id'] = X_test_MS1_5_ids

    Y_test_MS0_preds['end value'] = Y_test_MS0
    Y_test_MS1_5_preds['end value'] = Y_test_MS1_5

    Y_test_MS0_preds['start value'] = 0
    Y_test_MS1_5_preds['start value'] = X_test_MS1_5.apply(lambda row: row[row.name[-6:]], axis=1)

    Y_test_MS0_preds['start value'] = 0
    Y_test_MS1_5_preds['start value'] = inverse_minmax_scaling(Y_test_MS1_5_preds['start value'], 0, 5)

    Y_test_MS0_preds['ind_NLI'] = X_test_MS0['ind_NLI']
    Y_test_MS1_5_preds['ind_NLI'] = X_test_MS1_5['ind_NLI']

    Y_test_MS0_preds[motor_score_index_str] = X_test_MS0.apply(
        lambda x: to_motor_score_index(x[level_str], x[left_str]), axis=1)
    Y_test_MS1_5_preds[motor_score_index_str] = X_test_MS1_5.apply(
        lambda x: to_motor_score_index(x[level_str], x[left_str]), axis=1)

    Y_test_MS0_preds[motor_score_str] = X_test_MS0[level_str].apply(lambda x: dermatomes_withC1[x])
    Y_test_MS1_5_preds[motor_score_str] = X_test_MS1_5[level_str].apply(lambda x: dermatomes_withC1[x])

    Y_test_MS0_preds[motor_score_level_str] = X_test_MS0[level_str]
    Y_test_MS1_5_preds[motor_score_level_str] = X_test_MS1_5[level_str]

    Y_test_MS0_preds[left_str] = X_test_MS0[left_str]
    Y_test_MS1_5_preds[left_str] = X_test_MS1_5[left_str]

    if est_name == 'MS0':
        X_test = X_test_MS0
        Y_test = Y_test_MS0

        preds = Y_test_MS0_preds

    elif est_name == 'MS1_5':
        X_test = X_test_MS1_5
        Y_test = Y_test_MS1_5

        preds = Y_test_MS1_5_preds
    elif est_name == 'MS0_5':
        X_test = pd.concat([X_test_MS0, X_test_MS1_5])
        Y_test = pd.concat([Y_test_MS0, Y_test_MS1_5])

        preds = pd.concat([Y_test_MS0_preds, Y_test_MS1_5_preds])

    return preds, X_test, Y_test


def run_training(data, clf_choice, output, sides_are_same=False, run_shap=False, plot_shap=False, debug=False,
                 run_model_training=True, with_uncertainty=False, parallelize_manually=True,
                 augmented_evaluation=False, n_bootstrap=10, MS_are_same=False):
    n_splits = data.keys().__len__()
    name = filesave_name

    inputs = []
    if parallelize_manually:
        for i, datasets in data.items():
            inputs.append((copy.deepcopy(datasets), i, clf_choice, output, name, run_model_training, sides_are_same,
                           debug, with_uncertainty, MS_are_same))

    else:
        for i, datasets in data.items():
            inputs.append(
                (datasets, i, clf_choice, output, name, run_model_training, sides_are_same, debug, with_uncertainty,
                 MS_are_same))
    print('Input data ready')
    motor_scores_estimators_all_folds = {}

    if parallelize_manually:
        try:
            with mp.Pool(len(inputs)) as pool:
                motor_scores_estimator = pool.starmap(get_motor_scores_estimators, tqdm(inputs))

            for ms_est in motor_scores_estimator:
                estimator, fold_index = ms_est
                motor_scores_estimators_all_folds[fold_index] = estimator

        except AssertionError:
            for pack in tqdm(inputs):
                motor_scores_estimator, i = get_motor_scores_estimators(*pack)
                motor_scores_estimators_all_folds[i] = motor_scores_estimator
    else:
        for pack in inputs:
            motor_scores_estimator, i = get_motor_scores_estimators(*pack)
            motor_scores_estimators_all_folds[i] = motor_scores_estimator

    res_dict = {}
    for fold_index, motor_scores_estimators in motor_scores_estimators_all_folds.items():
        res_dict[fold_index] = get_res(motor_scores_estimators, data[fold_index], sides_are_same,
                                       augmented_evaluation=False)
    res_dict = {0: pd.concat(res_dict)}

    data_sequence_unscaled = pickle.load(
        open(get_data_dict_splits_full_sequences_path(scaled=False, augmented=False, test_set_augmented=False),
             'rb'))
    desc_dict = get_description_dict_metrics(res_dict, metrics, output, save_file=True, augmented_evaluation=False,
                                             data_sequence_unscaled=data_sequence_unscaled)
    pickle.dump(desc_dict, open(get_desc_dict_path(output, augmented_evaluation=False), 'wb'))

    if augmented_evaluation:

        res_dict_aug = {}
        for fold_index in range(n_splits):

            data_path = open(get_data_dict_splits_path(fold_index, n_bootstrap=n_bootstrap, scaled=True, augmented=True,
                                                       test_set_augmented=True, test_set_only=True), 'rb')
            data_fold = pickle.load(data_path)
            if debug:
                debug_length = 200
                first_ids = data_fold['X_test_MS0']['Unnamed: 0'].unique()[:debug_length]
                # Get data of the first 200 patients where Unnamed :0 is the patient id
                indices_0 = data_fold['X_test_MS0']['Unnamed: 0'].isin(list(first_ids))
                data_fold['X_test_MS0'] = data_fold['X_test_MS0'][indices_0]
                data_fold['Y_test_MS0'] = data_fold['Y_test_MS0'][indices_0]

                indices_1_5 = data_fold['X_test_MS1_5']['Unnamed: 0'].isin(list(first_ids))
                data_fold['X_test_MS1_5'] = data_fold['X_test_MS1_5'][indices_1_5]
                data_fold['Y_test_MS1_5'] = data_fold['Y_test_MS1_5'][indices_1_5]

            try:
                data_fold['X_test_MS0'] = data_fold['X_test_MS0'].drop(['sc_X'], axis=1)
                data_fold['X_test_MS1_5'] = data_fold['X_test_MS1_5'].drop(['sc_X'], axis=1)
                print('Dropped sc_X')
            except:
                pass

            res_dict_aug[fold_index] = get_res(motor_scores_estimators_all_folds[fold_index], data_fold,
                                               augmented_evaluation=True)

        res_dict_aug = {0: pd.concat(res_dict_aug)}
        desc_dict_augmented = get_description_dict_metrics(res_dict_aug, metrics, output, augmented_evaluation=True,
                                                           data_sequence_unscaled=data_sequence_unscaled)
        pickle.dump(desc_dict_augmented, open(get_desc_dict_path(output, augmented_evaluation=True), 'wb'))

    if plot_shap:
        plot_path = os.path.join(output, 'plots')
        Path(plot_path).mkdir(parents=True, exist_ok=True)

        if os.path.isfile(get_shap_dict_path(output)) and not run_shap:
            all_shap_values_merge_folds = pickle.load(open(get_shap_dict_path(output), 'rb'))

        else:

            est_name = 'MS0'
            if est_name == 'MS0':
                X = data[0]['X_train_MS0']
                X_test = data[0]['X_test_MS0']

            elif est_name == 'MS1_5':
                X = data[0]['X_train_MS1_5']
                X_test = data[0]['X_test_MS1_5']

            all_shap_values_merge_folds = {est_names: {ms: {'shap': [], 'x': []} for ms in ms_bothSide} for est_names in
                                           motor_scores_estimators_all_folds[0].keys()}
            for fold_index, motor_scores_estimators in motor_scores_estimators_all_folds.items():
                for est_name, estimators in motor_scores_estimators.items():
                    if est_name == 'MS0':
                        X_test = data[fold_index]['X_test_MS0']

                    elif est_name == 'MS1_5':
                        X_test = data[fold_index]['X_test_MS1_5']
                    X_test = X_test.drop(columns=['Unnamed: 0'])

                    for ms, best_est_cv in estimators.items():
                        X_test_shap = get_test_data_level(X_test, ms, sides_are_same=sides_are_same)
                        shap_values, X_after_shap = get_shap(X_test_shap, best_est_cv, clf_choice)
                        all_shap_values_merge_folds[est_name][ms]['shap'].append(shap_values)
                        all_shap_values_merge_folds[est_name][ms]['x'].append(X_after_shap)

            pickle.dump(all_shap_values_merge_folds, open(get_shap_dict_path(output), 'wb'))
        if debug:
            debug_str = '_debug'
        else:
            debug_str = ''

        for est_name, ms_shap_values in all_shap_values_merge_folds.items():
            for ms, shap_values in ms_shap_values.items():

                if est_name == 'MS0':
                    X_train_MS0 = datasets['X_train_MS0']
                    X_test_MS0 = datasets['X_test_MS0']
                    X = pd.concat([X_train_MS0, X_test_MS0]).drop(columns=['Unnamed: 0'])
                elif est_name == 'MS1_5':
                    X_train_MS1_5 = datasets['X_train_MS1_5']
                    X_test_MS1_5 = datasets['X_test_MS1_5']
                    X = pd.concat([X_train_MS1_5, X_test_MS1_5]).drop(columns=['Unnamed: 0'])

                X_shap = X.drop(columns=['level'])
                plot_shap_figure(X_shap, all_shap_values_merge_folds[est_name][ms], output, debug_str, ms,
                                 est_name)  # X_test columns is the same for all folds
