import pandas as pd

from Miscellaneous.Misc.model_names import *
from Miscellaneous.Misc.paths import *


def full_performance_table(include_sygen_ais=False, sum_of_residuals=False, datasets=[emsci_str, sygen_str], with_uncertainty=True, augmented_evaluation=True, times=[None, times_interval_start_4_weeks]):
    value_metric = 'RMSE' if not sum_of_residuals else 'Sum of residuals'
    models = basemodel_names + [RF_MS_same_str] + custom_model_names
    conf_names = ['confidence_lower', 'confidence_higher']

    tables = {}
    for dataset in datasets:
        temp_tables = {}
        for time_interval in times:
            if time_interval is None:
                name = f'{value_metric} (all times) ' + dataset
            elif time_interval == times_interval_start_4_weeks:
                name = f'{value_metric} (4±1 weeks) ' + dataset
            else:
                raise ValueError('Invalid time interval')

            performance_table_path = get_rmse_table_path(with_uncertainty=with_uncertainty,
                                                         augmented_evaluation=augmented_evaluation,
                                                         dataset=dataset, times_interval_start=time_interval, sum_of_residuals=sum_of_residuals)
            full_performance_table_path = performance_table_path

            # load the tables
            performance_table = pd.read_csv(full_performance_table_path, index_col=0)


            performance_table = performance_table.drop(columns=conf_names)


            # round the values to 2 decimals
            performance_table = performance_table.round(2)


            # concatenate each row of the tables
            performance_table_formatted = performance_table.apply(
                lambda row: f"{row.iloc[0]:.2f} ({row.iloc[1]:.2f}, {row.iloc[2]:.2f})",
                axis=1)

            temp_tables[name] = performance_table_formatted
            # join performance_table and rmse on the index
        new_table = pd.concat(temp_tables, axis=1)
        tables[dataset] = new_table

    n_decimals = 2

    datasets_ais = [emsci_str]
    if include_sygen_ais:
        datasets_ais.append(sygen_str)
    ais_res_list = []
    for dataset in datasets_ais:
        ais_res = {}
        for model in models:

            if dataset == sygen_str:
                augmented_evaluation_temp = False
            else:
                augmented_evaluation_temp = augmented_evaluation

            ais_path = get_AIS_table_path(model, with_uncertainty=with_uncertainty,
                                          augment_test_set=augmented_evaluation_temp,
                                          dataset=dataset, sum_of_residuals=sum_of_residuals)
            full_ais_path = ais_path
            ais = pd.read_csv(full_ais_path, index_col=0)

            # round the values to 2 decimals
            ais = ais.round(n_decimals)

            ais_formatted = ais.apply(lambda row: f"{row.iloc[1]:.2f} ({row.iloc[0]:.2f}, {row.iloc[2]:.2f})", axis=1)
            # add name of dataset to all column names
            ais_formatted.index = ais_formatted.index + ' ' + dataset
            ais_res[model] = ais_formatted

        # concatenate ais_res
        ais_res = pd.concat(ais_res, axis=1).T
        ais_res_list.append(ais_res)

    ais_res = pd.concat(ais_res_list, axis=1)

    print(ais_res)


    # join new_table and ais_res on the index
    table_concatenation = [tables[emsci_str], ais_res]
    if include_sygen_ais:
        table_concatenation.append(tables[sygen_str])
    final_table = pd.concat(table_concatenation, axis=1)

    # set a multiindex for emsci and sygen (last 2 columns)

    # sort index by table_order
    final_table = final_table.reindex(table_order)

    # rename the index using rename_models
    final_table = final_table.rename(index=rename_models)

    cols_order = [f'{value_metric} (all times) ' + emsci_str]
    if times_interval_start_4_weeks in times:
        time_col = f'{value_metric} (4±1 weeks) ' + emsci_str
        cols_order = cols_order + [time_col]
    if include_sygen_ais:
        cols_order = cols_order + [f'{value_metric} (all times) ' + sygen_str, f'{value_metric} (4±1 weeks) ' + sygen_str]

    ais_cols = ['AIS A ' + emsci_str, 'AIS B ' + emsci_str, 'AIS C ' + emsci_str, 'AIS D ' + emsci_str]
    cols_order = cols_order + ais_cols

    if include_sygen_ais:
        cols_order = cols_order + ['AIS A ' + sygen_str, 'AIS B ' + sygen_str, 'AIS C ' + sygen_str, 'AIS D ' + sygen_str]
    final_table = final_table[cols_order]
    print(final_table)

    # save the final table
    include_sygen_ais_str = '_with_sygen_ais' if include_sygen_ais else ''
    sum_of_residuals_str = '_sum_of_residuals' if sum_of_residuals else ''
    table_path = os.path.join(get_project_root(), 'Tables/final_table/')
    Path(table_path).mkdir(parents=True, exist_ok=True)
    final_table.to_csv(table_path + f'full_performance_table{include_sygen_ais_str}{sum_of_residuals_str}.csv')

