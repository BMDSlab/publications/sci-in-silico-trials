from Run_collection.Run_files.eval_from_predict_file import *


def run_all_eval(augmented_evaluations=[True, False], with_uncertainties=[True, False], sum_of_residuals_alts=[True, False], times=[None, times_interval_start_4_weeks],
                 datasets=[emsci_str, sygen_str]):
    #times_interval_start = times_interval_start_4_weeks
    data_sequence_unscaled_emsci = pickle.load(
        open(get_data_dict_splits_full_sequences_path(scaled=False,
                                                      augmented=False,
                                                      test_set_augmented=False,
                                                      dataset=emsci_str), 'rb'))

    data_sequence_unscaled_sygen = pickle.load(
        open(get_data_dict_splits_full_sequences_path(scaled=False,
                                                      augmented=False,
                                                      test_set_augmented=False,
                                                      dataset=sygen_str), 'rb'))
    for dataset in datasets:
        for augmented_evaluation in augmented_evaluations:
            for with_uncertainty in with_uncertainties:
                for sum_of_residuals in sum_of_residuals_alts:
                    for time in times:
                        if dataset == sygen_str:
                            data_sequence_unscaled = data_sequence_unscaled_sygen
                        else:
                            data_sequence_unscaled = data_sequence_unscaled_emsci
                        run_eval(with_uncertainty=with_uncertainty, augmented_evaluation=augmented_evaluation,
                                 dataset=dataset, times_interval_start=time,
                                 data_sequence_unscaled=data_sequence_unscaled, sum_of_residuals=sum_of_residuals)


if __name__ == '__main__':
    run_all_eval()
