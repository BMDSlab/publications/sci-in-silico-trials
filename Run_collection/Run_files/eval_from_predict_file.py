import os
import pickle

import numpy as np
import pandas as pd
from Miscellaneous.Misc.fields_to_match import *
from Miscellaneous.Misc.paths import *
from Miscellaneous.auxFunctions.math_utils import get_percentile, median_confidence_interval



def root_mean_squared_error(y_true, y_pred):
    return np.sqrt(np.mean((y_true - y_pred) ** 2))


def eval_from_prediction_spec(clf_choice, with_uncertainty, dataset, augmented_evaluation, name=temp_name,
                              times_interval_start=None, sum_of_residuals=False, data_sequence_unscaled=None):
    errors = get_errors(clf_choice, with_uncertainty, dataset, augmented_evaluation, name, times_interval_start,
                        sum_of_residuals, data_sequence_unscaled)
    median = np.median(errors)
    percentiles = get_percentile(errors)
    median_confidence_interval(errors)

    value_metric_str = 'RMSE' if not sum_of_residuals else 'Sum of residuals'

    print(clf_choice, value_metric_str, ':', median, percentiles, median_confidence_interval(errors))
    # print round values
    median = round(median, 3)
    percentile_lower, percentile_higher = [round(p, 3) for p in percentiles]
    confidence_lower, confidence_higher = median_confidence_interval(errors)
    print(clf_choice, value_metric_str, 'median:', round(median, 3), '| percentiles:',
          [round(p, 3) for p in percentiles],
          '| confidence interval:', [round(p, 3) for p in median_confidence_interval(errors)])
    values = [median, percentile_lower, percentile_higher, confidence_lower, confidence_higher]
    return values


def get_errors(clf_choice, with_uncertainty, dataset, augmented_evaluation, name=temp_name,
               times_interval_start=None, sum_of_residuals=False, data_sequence_unscaled=None):
    if with_uncertainty:
        with_uncertainty_str = 'trained_with_augmentation'
    else:
        with_uncertainty_str = 'trained_without_augmentation'

    if dataset == emsci_str:
        dataset_path = get_base_path()
    elif dataset == sygen_str:
        dataset_path = get_sygen_path()
    else:
        raise ValueError('Invalid dataset')

    dst = os.path.join(dataset_path, 'predictions', name, clf_choice, with_uncertainty_str)
    predictions_list = []
    if dataset == sygen_str:
        fold_indices = [0, 1, 2, 3, 4]

        augmented_evaluation = False
    else:
        fold_indices = [None]

    for fold_index in fold_indices:
        pred_filename = predictions_filename(augmented_evaluation=augmented_evaluation, dataset=dataset,
                                             fold_index=fold_index)

        full_path = os.path.join(dst, pred_filename)
        predictions = pd.read_csv(full_path, index_col=0)
        predictions_list.append(predictions)

    rmse_folds = []
    for predictions in predictions_list:

        if times_interval_start is not None:
            times = [int(col.split('_')[1]) for col in predictions.columns]
            predictions_copy = predictions.copy()
            predictions_copy.loc['init_time'] = times
            # keep only the predictions from the time interval
            predictions = predictions_copy.loc[:,
                          predictions_copy.loc['init_time'].between(times_interval_start[0], times_interval_start[1])]
            predictions = predictions.drop(index=['init_time'])

        errors = eval_from_prediction_file(predictions, with_uncertainty, dataset, augmented_evaluation,
                                           sum_of_residuals=sum_of_residuals,
                                           data_sequence_unscaled=data_sequence_unscaled)
        rmse_folds.append(errors)
    if len(rmse_folds) > 1:
        mean_error = pd.concat(rmse_folds).groupby(level=0).mean()
    else:
        mean_error = rmse_folds[0]
    return mean_error


def eval_from_prediction_file(predictions, with_uncertainty=False, dataset=emsci_str, augmented_evaluation=False,
                              sum_of_residuals=False, data_sequence_unscaled=None):
    if data_sequence_unscaled is None:
        data_sequence_unscaled = pickle.load(
            open(get_data_dict_splits_full_sequences_path(scaled=False,
                                                          augmented=False,
                                                          test_set_augmented=False,
                                                          dataset=dataset), 'rb'))
    X_use_unscaled = data_sequence_unscaled[0][x_use_str]

    Y_use_unscaled = data_sequence_unscaled[0][y_use_str]

    nli_s = X_use_unscaled.loc[:, ind_NLI_str]

    motor_scores = Y_use_unscaled.loc[:, ms_both_sides_fields_toMatch]

    predictions = predictions.T
    # order prediction columns and index to match the order of the motor scores
    predictions = predictions[motor_scores.columns]

    # order prediction index to match the order of the motor scores
    motor_scores = motor_scores.loc[predictions.index]

    assert (motor_scores.index == predictions.index).all()
    # assert columns are the same
    assert (motor_scores.columns == predictions.columns).all()

    levels = [dermatomes_withC1.index(s[-2:]) for s in motor_scores.columns]

    ids = predictions.index

    rmses = {}
    for i, index in enumerate(ids):
        ind_NLI_patient = nli_s.loc[index]
        res_below_NLI = motor_scores.loc[index, ind_NLI_patient < levels]
        pred_below_NLI = predictions.loc[index, ind_NLI_patient < levels]
        if sum_of_residuals:
            rmse_patient = np.sum(res_below_NLI - pred_below_NLI) / len(res_below_NLI)
        else:
            rmse_patient = root_mean_squared_error(res_below_NLI, pred_below_NLI)
        rmses[index] = rmse_patient
    rmses = pd.Series(rmses)
    return rmses


def run_eval(with_uncertainty=True, augmented_evaluation=True, dataset=emsci_str, times_interval_start=None,
             sum_of_residuals=False, data_sequence_unscaled=None):
    name = 'Temp'

    print('Dataset:', dataset)
    print('With uncertainty:', with_uncertainty)
    print('Augmented evaluation:', augmented_evaluation)
    print('Name:', name)
    print('Sum of residuals:', sum_of_residuals)

    print('Using only predictions from time interval start:', times_interval_start)
    data_dict = {}
    if data_sequence_unscaled is None:
        data_sequence_unscaled = pickle.load(
            open(get_data_dict_splits_full_sequences_path(scaled=False,
                                                          augmented=False,
                                                          test_set_augmented=False,
                                                          dataset=dataset), 'rb'))


    for clf_choice in basemodel_names + [RF_MS_same_str] + [CNN_str, ensemble_str, transformer_str,
                                                            transformer_sequence_str,
                                                            CNN_sequence_str]:
        values = eval_from_prediction_spec(clf_choice, with_uncertainty, dataset, augmented_evaluation, name,
                                           times_interval_start=times_interval_start, sum_of_residuals=sum_of_residuals,
                                           data_sequence_unscaled=data_sequence_unscaled)
        data_dict[clf_choice] = values
    # create a dataframe with the results
    df = pd.DataFrame(data_dict, index=['median', 'percentile_lower', 'percentile_higher', 'confidence_lower',
                                        'confidence_higher']).T
    print(df)
    # save the results
    # set cwd to the path of the script
    os.chdir(os.path.dirname(os.path.abspath(__file__)))
    filename = get_rmse_table_path(with_uncertainty, augmented_evaluation, dataset, times_interval_start,
                                   sum_of_residuals=sum_of_residuals)
    print('Saving to:', filename, os.getcwd())
    df.to_csv(filename)


def run_sygen():
    name = temp_name
    with_uncertainty = False
    augmented_evaluation = False
    dataset = sygen_str
    for clf_choice in [CNN_str, ensemble_str, transformer_str, transformer_sequence_str,
                       CNN_sequence_str]:
        eval_from_prediction_spec(clf_choice, with_uncertainty, dataset, augmented_evaluation, name)
