'''
Model code for SCI in silico trial simulation using simple treatment effect models.
SBrueningk, July 2024
'''

import pickle
from tqdm import tqdm
import numpy as np
import seaborn as sns
from collections import Counter
from pathlib import Path
import os
import pandas as pd
import argparse
import warnings
from sklearn.metrics import mean_squared_error
import matplotlib.pyplot as plt

warnings.filterwarnings("ignore")
import random

#######################################################################################
# Auxillary functions and definitions
#######################################################################################

# Fixed settings
randomization = 'match'
useReducesEfficacyFor0 = False
n_bootstrap_trial = 1  # keep this at 1 to always generate a new draw for each motor score


# Structure of data
lems_MS     = ['LMS_L2', 'RMS_L2', 'LMS_L3', 'RMS_L3','LMS_L4', 'RMS_L4', 'LMS_L5', 'RMS_L5', 'LMS_S1', 'RMS_S1']
uems_MS     = ['LMS_C5', 'RMS_C5', 'LMS_C6', 'RMS_C6', 'LMS_C7', 'RMS_C7', 'LMS_C8','RMS_C8', 'LMS_T1', 'RMS_T1']
ms_bothSide = ['C5', 'C6', 'C7', 'C8', 'T1', 'L2', 'L3', 'L4', 'L5', 'S1']
dermatomes_withC1 = ['C1', 'C2', 'C3', 'C4', 'C5', 'C6', 'C7', 'C8', 'T1', 'T2', 'T3',
                     'T4', 'T5', 'T6', 'T7', 'T8', 'T9', 'T10', 'T11', 'T12',
                     'L1', 'L2', 'L3', 'L4', 'L5', 'S1', 'S2', 'S3', 'S45']
ms_fields_toMatch = []
for m in ms_bothSide:
    ms_fields_toMatch.append('LMS_' + m)
    ms_fields_toMatch.append('RMS_' + m)
ms_2 = ['C5_l', 'C5_r', 'C6_l', 'C6_r', 'C7_l', 'C7_r', 'C8_l', 'C8_r', 'T1_l',
       'T1_r', 'L2_l', 'L2_r', 'L3_l', 'L3_r', 'L4_l', 'L4_r', 'L5_l', 'L5_r',
       'S1_l', 'S1_r']
dicts_MS = {ms_2[i]: ms_fields_toMatch[i] for i in range(len(ms_2))}


# Evaluation metrics and plotting specifications
metrics = ['RMSE', 'MeanScImpr','LEMSImpr','UEMSImpr','LEMS','UEMS','MeanSc']
labels_metric = ['RMSE BL-Rec', 'MeanSc Improvement', 'LEMS Improvement', 'UEMS Improvement', 'LEMS', 'UEMS',
                     'MeanSc']
bins_dict = {
    'RMSE': np.linspace(-1, 2, 31),
    'MeanScImpr': np.linspace(-1, 5, 31),
    'LEMSImpr': np.linspace(-10, 30, 31),
    'UEMSImpr': np.linspace(-10, 30, 31),
    'LEMS': np.linspace(-30, 30, 31),
    'UEMS': np.linspace(-30, 30, 31),
    'MeanSc': np.linspace(-3, 5, 21)
}



    
# Randomization
def getTrialPats(X_add, matchby, plegia_trial, fracAISA, ais_trial, n_pat_trial, reusePats, randomization):

    # 2 cohorts to be identified
    pats_treat = []
    pats_control = []

    # Metadata used for randomization
    X_add_forMatching = X_add.copy()
    for i_ais, this_ais in enumerate(ais_trial):
        df_sub = X_add[(X_add['AIS'].isin([this_ais])) & (X_add['plegia'].isin(plegia_trial))]

        all_pats_criteria = list(df_sub.index)

        # randomly select pats and split 1:1 into treatment and control arms
        counter = 0
        while counter < int(0.5 * n_pat_trial * fracAISA[i_ais]):
            p = np.random.choice(all_pats_criteria, 1)[0]

            if not reusePats:
                all_pats_criteria.remove(p)

            # Now match the treated patients - exclude from matched population
            try:
                df_sub = X_add_forMatching.drop(p).copy()
            except:
                continue

            # Get characteristics
            this_ais    = X_add.loc[p, 'AIS']
            this_TMS_BL = X_add.loc[p, 'TMS']
            this_NLI    = X_add.loc[p, 'ind_NLI']
            this_age    = X_add.loc[p, 'age']
            this_sex    = X_add.loc[p, 'sex']
            this_lems   = X_add.loc[p, 'LEMS']

            # Subset data
            patient_excl = 0
            for m in matchby:
                if patient_excl == 0:
                    if m == 'LEMS':
                        df_sub_sub = df_sub[
                            (df_sub['LEMS'] <= this_TMS_BL + 5) & (df_sub['LEMS'] >= this_TMS_BL - 5)]
                    if m == 'TMS':
                        df_sub_sub = df_sub[(df_sub['TMS'] <= this_TMS_BL + 5) & (df_sub['TMS'] >= this_TMS_BL - 5)]
                    elif m == 'AIS':
                        df_sub_sub = df_sub[df_sub['AIS'] == this_ais]
                    elif m == 'sex':
                        df_sub_sub = df_sub[df_sub['sex'] == this_sex]
                    elif m == 'age':
                        df_sub_sub = df_sub[(df_sub['age'] <= this_age + 5) & (df_sub['age'] >= this_age - 5)]
                    elif m == 'NLI':
                        df_sub_sub = df_sub[
                            (df_sub['ind_NLI'] <= this_NLI + 2) & (df_sub['ind_NLI'] >= this_NLI - 2)]

                    if len(df_sub_sub) > 1:
                        df_sub = df_sub_sub.copy()
                    else:
                        print('No match for ' + m + ' for pat ' + str(p))

                        if m == 'NLI' or m == 'cause':
                            
                            # Patient needs to be excluded
                            patient_excl = 1
                            break

            # randomly select the matched patient and remove from list
            if patient_excl == 0:
                p_match = np.random.choice(df_sub.index, 1)[0]
                pats_control.append(p_match)
                pats_treat.append(p)

                if not reusePats:
                    X_add_forMatching.drop([p], inplace=True)
                    X_add_forMatching.drop([p_match], inplace=True)
                counter += 1
    return pats_treat, pats_control

# Bootstrap for treatment effect - not used here 
def generate_pat_bootPois(this_X_use, n_bootstrap, nli_ind, lambdaSc):
    ms_true = this_X_use.values

    # initialize dataframe
    df_new = pd.DataFrame()

    # draw samples for each of the scores below the nli
    for i, this_ms in enumerate(ms_true):

        if i < nli_ind:
            sc_boot = [this_ms for this_n_rand in range(0, n_bootstrap)]

        else:
            if this_ms > 0:

                # Draw random numbers for the improvement
                n_rands = np.random.poisson(lambdaSc, n_bootstrap) 
                
                # Assign score based on random numbers
                sc_boot = [int(this_ms + this_n_rand) if int(this_ms + this_n_rand) <= 5 else 5 for this_n_rand in
                           n_rands]
            else:
                if useReducesEfficacyFor0:
                    # Much lower probability of improvement for score of 0

                    # Draw random numbers for the improvement
                    n_rands = s = np.random.poisson(lambdaSc * 0.1, n_bootstrap)  
                else:
                    n_rands = s = np.random.poisson(lambdaSc , n_bootstrap)

                    # Assign score based on random numbers
                sc_boot = [int(this_ms + this_n_rand) if int(this_ms + this_n_rand) <= 5 else 5 for this_n_rand in
                           n_rands]

        # add to dataframe
        df_new.loc[:, this_X_use.index[i]] = sc_boot

    return df_new


# Main simulation function
def runTrialByAISA(n_pat_trial, ais_trial, fracAISA, plegia_trial, reusePats, matchby, randomization, n_runs,treatmentEffs, X_add, Y_MS, X_MS,simRecs,simnames, output_in,model_pred):
    
    # Naming
    trialname = str(n_pat_trial) + '_' + str(ais_trial) + str(fracAISA) + str(plegia_trial) + '_match' + str(matchby) +'_reusePats'+str(reusePats)
    
    output = os.path.join(output_in, trialname)
    Path(output).mkdir(parents=True, exist_ok=True)
    print(trialname)

    # Prepare output - this is the difference of the cohort medians of the specific metric between the intervention and control group where the control group either refers to the true recovery (ground truth), the simulated recovery (sim control), or the randomized controls
    all_ais = ['A', 'B', 'C', 'D']
    cols = []
    for ais in ['pop'] + all_ais:
        for m in metrics:
            for i_tr in range(0,len(treatmentEffs)):
                cols.append('tr'+str(i_tr)+'_'+m+'_ais'+ais)
    

    # Save differences at group level and individual group means 
    df_groundTruth = pd.DataFrame(index = np.arange(n_runs), columns = cols)
    df_randomContr = pd.DataFrame(index=np.arange(n_runs), columns=cols)
    df_controlRand = pd.DataFrame(index = np.arange(n_runs), columns = cols)
    df_interventionGT = pd.DataFrame(index = np.arange(n_runs), columns = cols)
    df_intervention   = pd.DataFrame(index = np.arange(n_runs), columns = cols)
    alldf_simContrs   = []
    alldf_simContrsABS = []
    for m in simnames:
        df_simContr = pd.DataFrame(index=np.arange(n_runs), columns=cols)
        alldf_simContrs.append(df_simContr)
        df_simContrABS = pd.DataFrame(index=np.arange(n_runs), columns=cols)
        alldf_simContrsABS.append(df_simContrABS)
    df_summaryTrialPop = pd.DataFrame()

    
    # Simulate trials - repeat this based on the indicated number of runs
    for counter in tqdm(np.arange(n_runs)):

        # Get treated patients
        pats_treat, pats_control = getTrialPats(X_add, matchby, plegia_trial, fracAISA, ais_trial, n_pat_trial,reusePats, randomization)

        # Split by AISA grades
        pats_treatAIS = [pats_treat]
        pats_contrAIS = [pats_control]
        for this_AIS in all_ais:
            this_pats_treatAIS = list(X_add.loc[pats_treat][X_add.loc[pats_treat, 'AIS'] == this_AIS].index)
            pats_treatAIS.append(this_pats_treatAIS)
            this_pats_controlAIS = list(X_add.loc[pats_control][X_add.loc[pats_control, 'AIS'] == this_AIS].index)
            pats_contrAIS.append(this_pats_controlAIS)

        # Summarize the patient population
        df_summaryTrialPop = getThisSummary(df_summaryTrialPop, counter,
                                            X_add, pats_treat,pats_control)

        # Simulate treatment effect in these patients
        Y_MS_Trial_lst = []
        for i_tr, lambdaSc in enumerate(treatmentEffs):

            Y_MS_Trial_lst.append(pd.DataFrame(columns=Y_MS.columns))
            for p in (pats_treat+pats_control):
                ms_true_TP2  = Y_MS.loc[p, :] # Baseline
                ms_BL        = X_MS.loc[p, :] # Recovery time point
                this_ind_nli = X_add.loc[p,'ind_NLI_MS']

                thisX_uncert = generate_pat_bootPois(ms_true_TP2, n_bootstrap_trial, this_ind_nli, lambdaSc)

                Y_MS_Trial_lst[i_tr].loc[p, :] = np.percentile(thisX_uncert.values, 50, axis=0).astype(float)


        # Score agreement and outcomes
        for i_metric, metric in enumerate(metrics):

            for this_ais in range(0, 5):

                if this_ais>0:
                    ais = all_ais[this_ais-1]
                else:
                    ais = 'pop'
                
                # Check if this AIS grade is used 
                if len(pats_treatAIS[this_ais]) ==0:
                    continue


                # True recovery
                true_sc = getEvalSc_recMatched(metric, pats_treatAIS[this_ais], X_add, Y_MS, X_MS)

                # Randomized control group - median/mean over the group
                rand_sc_med = getEvalSc_rec(metric, pats_contrAIS[this_ais], X_add, Y_MS, X_MS)
                rand_sc = getEvalSc_recMatched(metric, pats_contrAIS[this_ais], X_add, Y_MS, X_MS)


                # Trial patients receiving "treatment" - model the intervention cohort
                for i_tr in range(0, len(treatmentEffs)):

                    # Trial score
                    trial_sc_med = getEvalSc_rec(metric, pats_treatAIS[this_ais], X_add, Y_MS_Trial_lst[i_tr], X_MS)
                    trial_sc = getEvalSc_recMatched(metric, pats_treatAIS[this_ais], X_add, Y_MS_Trial_lst[i_tr], X_MS)

                    # Ground truth cohort difference
                    this_col = 'tr' + str(i_tr) + '_' + metric + '_ais' + ais
                    df_groundTruth.loc[counter, this_col] = summarize_trial(trial_sc - true_sc)

                     # Absolute scores
                    df_controlRand.loc[counter, this_col]           = summarize_trial(rand_sc)
                    df_controlRand.loc[counter, this_col+'_std']    = rand_sc.std().item()
                    df_interventionGT.loc[counter, this_col]        = summarize_trial(true_sc)
                    df_interventionGT.loc[counter, this_col+'_std'] = true_sc.std().item()
                    df_intervention.loc[counter, this_col]          = summarize_trial(trial_sc)
                    df_intervention.loc[counter, this_col+'_std']   = trial_sc.std().item()
                    
                    # Random control cohort difference
                    df_randomContr.loc[counter, this_col] = trial_sc_med - rand_sc_med
    
                    # Simulated control cohort difference
                    for i_sim, df_simContr in enumerate(alldf_simContrs):
    
                        # Simulated control group - no median yet - individual scores to have a difference at the level of the individuals
                        sim_sc = getEvalSc_recMatched(metric, pats_treatAIS[this_ais], X_add, simRecs[i_sim], X_MS)
    
                        df_simContr.loc[counter, this_col] = summarize_trial(trial_sc - sim_sc)
                        alldf_simContrs[i_sim] = df_simContr

                        df_simContrABS = alldf_simContrsABS[i_sim]
                        sim_sc = getEvalSc_recMatched(metric, pats_treatAIS[this_ais], X_add, simRecs[i_sim], X_MS)
                        df_simContrABS.loc[counter, this_col] = summarize_trial(sim_sc)
                        df_simContrABS.loc[counter, this_col+'_std'] = sim_sc.std().item()
                        alldf_simContrsABS[i_sim] = df_simContrABS


        # Plot single trials (n = 3)
        if counter < 3:
            this_ais = 0
            for metric in metrics:
                true_sc_treat = getEvalSc_recMatched(metric, pats_treatAIS[this_ais], X_add, Y_MS, X_MS)
                true_sc_contr = getEvalSc_recMatched(metric, pats_contrAIS[this_ais], X_add, Y_MS, X_MS)

                plt.figure(figsize=(3,3))
                plt.hist(true_sc_contr, alpha = 0.5, label = 'Control: %.1f' % (summarize_trial( true_sc_contr)),
                         color = 'r', bins = bins_dict[metric], edgecolor = 'k')
                plt.hist(true_sc_treat, alpha=0.5, label='Intervention: %.1f' % (summarize_trial( true_sc_treat)),
                         color = 'k', bins = bins_dict[metric], edgecolor = 'k')
                plt.xlabel(metric)
                plt.ylabel('Count')
                plt.legend()
                plt.tight_layout()
                plt.savefig(os.path.join(output, str(counter)+metric+'_NoTreat.png'))
                plt.savefig(os.path.join(output, str(counter) + metric + '_NoTreat.pdf'))

            # plot patient characteristics
            # AIS grades
            category_counts = X_add.loc[pats_contrAIS[this_ais],'AIS'].value_counts()
            category_counts_treat = X_add.loc[pats_treatAIS[this_ais], 'AIS'].value_counts()
            combined_counts = pd.DataFrame({'Control': category_counts, 'Intervention': category_counts_treat})

            # Create a grouped bar plot
            fig,ax = plt.subplots(figsize=(3,3))
            combined_counts.plot(kind='bar', width=0.5, color=['red', 'black'],
                                      alpha = 0.5, ax = ax)
            plt.xticks(rotation = 0)
            plt.xlabel('AIS grade')
            plt.ylabel('Count')
            plt.legend()
            plt.tight_layout()
            plt.savefig(os.path.join(output, str(counter)+'_AISgrades.png'))
            plt.savefig(os.path.join(output, str(counter) + '_AISgrades.pdf'))

            # LEMS
            lems_X_contr = X_MS.loc[pats_contrAIS[this_ais]].iloc[:,-10:].sum(axis = 1)
            lems_X_treat = X_MS.loc[pats_treatAIS[this_ais]].iloc[:, -10:].sum(axis=1)
            plt.figure(figsize=(3, 3))
            plt.hist(lems_X_contr, alpha=0.5, label='Control: %.1f' % (summarize_trial(lems_X_contr)),
                     color='r', bins=np.linspace(0, 50, 26), edgecolor='k')
            plt.hist(lems_X_treat, alpha=0.5, label='Intervention: %.1f' % (summarize_trial(lems_X_treat)),
                     color='k', bins=np.linspace(0, 50, 26), edgecolor='k')
            plt.xlabel('LEMS baseline')
            plt.ylabel('Count')
            plt.legend()
            plt.tight_layout()
            plt.savefig(os.path.join(output, str(counter) + '_lemsX.png'))
            plt.savefig(os.path.join(output, str(counter) + '_lemsX.pdf'))

            # Age
            age_contr = X_add.loc[pats_contrAIS[this_ais],'age']
            age_treat = X_add.loc[pats_treatAIS[this_ais],'age']
            plt.figure(figsize=(3, 3))
            plt.hist(age_contr, alpha=0.5, label='Control: %.1f' % (summarize_trial(age_contr)),
                     color='r', bins=np.linspace(15, 95, 41), edgecolor='k')
            plt.hist(age_treat, alpha=0.5, label='Intervention: %.1f' % (summarize_trial(age_treat)),
                     color='k', bins=np.linspace(15, 95, 41), edgecolor='k')
            plt.xlabel('Age [years]')
            plt.ylabel('Count')
            plt.legend()
            plt.tight_layout()
            plt.savefig(os.path.join(output, str(counter) + '_Age.png'))
            plt.savefig(os.path.join(output, str(counter) + '_Age.pdf'))

        else:
            continue

    return df_summaryTrialPop,df_groundTruth, alldf_simContrs, df_randomContr, output,df_controlRand ,df_interventionGT, df_intervention, alldf_simContrsABS

# Evaluation metrics
def get_RMSE(this_Y, this_pred, this_ind_nliMS):
    # RMSE below the NLI
    this_rmse = mean_squared_error(this_Y[int(this_ind_nliMS):], this_pred[int(this_ind_nliMS):], squared=False)
    return this_rmse
    
def summarize_trial(a):
    if metricsum == 'median':
        return np.median(a)
    elif metricsum == 'mean':
        return np.mean(a)
    else:
        raise ('Metric not supported')
        
def getEvalSc_rec(metric, pats_use, X_add, Y_MS, X_MS):

    if metric == 'LEMS':
        sc = summarize_trial(Y_MS.loc[pats_use, lems_MS].sum(axis = 1))
        
    elif metric == 'LEMSImpr':
        sc = summarize_trial((Y_MS.loc[pats_use, lems_MS].sum(axis = 1) - X_MS.loc[pats_use, lems_MS].sum(axis = 1)))

    elif metric == 'UEMS':
        sc = summarize_trial(Y_MS.loc[pats_use, uems_MS].sum(axis = 1))

    elif metric == 'UEMSImpr':
        sc = summarize_trial(Y_MS.loc[pats_use, uems_MS].sum(axis = 1) - X_MS.loc[pats_use, uems_MS].sum(axis = 1))

    elif metric == 'MeanSc':
        all_meanscblNLI = []
        for p in pats_use:
            all_meanscblNLI.append(np.mean(Y_MS.loc[p][X_add.loc[p,'ind_NLI_MS']:]))
        sc = summarize_trial(all_meanscblNLI)

    elif metric == 'RMSE':
        all_rmse = []
        for p in pats_use:
            this_ind_nli_MS = X_add.loc[p, 'ind_NLI_MS']
            this_rmse = get_RMSE(Y_MS.loc[p, :].values, X_MS.loc[p, :].values.astype(float), this_ind_nli_MS)
            all_rmse.append(this_rmse)
        sc = summarize_trial(all_rmse)

    elif metric == 'MeanScImpr':
        all_meanscblNLI_impr = []
        for p in pats_use:
            meanScY = np.mean(Y_MS.loc[p].iloc[int(X_add.loc[p, 'ind_NLI_MS']):])
            meanScX = np.mean(X_MS.loc[p].iloc[int(X_add.loc[p, 'ind_NLI_MS']):])
            all_meanscblNLI_impr.append(meanScY-meanScX)

        sc = summarize_trial(all_meanscblNLI_impr)
    elif metric == 'MeanSc':
        all_meanscblNLI_impr = []
        for p in pats_use:
            meanScY = np.mean(Y_MS.loc[p].iloc[int(X_add.loc[p, 'ind_NLI_MS']):])
            all_meanscblNLI_impr.append(meanScY)

        sc = summarize_trial(all_meanscblNLI_impr)

    return sc
    
def getEvalSc_recMatched(metric, pats_use, X_add, Y_MS, X_MS):

    if metric == 'LEMS':
        sc = Y_MS.loc[pats_use, lems_MS].sum(axis = 1)

    if metric == 'UEMS':
        sc = Y_MS.loc[pats_use, uems_MS].sum(axis = 1)

    elif metric == 'MeanSc':
        all_meanscblNLI = []
        for p in pats_use:
            all_meanscblNLI.append(np.mean(Y_MS.loc[p][X_add.loc[p,'ind_NLI_MS']:]))
        sc = pd.DataFrame(index=pats_use, data=all_meanscblNLI)

    elif metric == 'RMSE':
        all_rmse = []
        for p in pats_use:
            this_ind_nli_MS = X_add.loc[p, 'ind_NLI_MS']
            this_rmse = get_RMSE(Y_MS.loc[p, :].values, X_MS.loc[p, :].values.astype(float), this_ind_nli_MS)
            all_rmse.append(this_rmse)
        sc = pd.DataFrame(index = pats_use, data = all_rmse)

    elif metric == 'LEMSImpr':
        sc = Y_MS.loc[pats_use, lems_MS].sum(axis = 1) - X_MS.loc[pats_use, lems_MS].sum(axis = 1)

    elif metric == 'UEMSImpr':
        sc = Y_MS.loc[pats_use, uems_MS].sum(axis = 1) - X_MS.loc[pats_use, uems_MS].sum(axis = 1)

    elif metric == 'MeanScImpr':
        all_meanscblNLI_impr = []
        for p in pats_use:
            meanScY = np.mean(Y_MS.loc[p].iloc[int(X_add.loc[p, 'ind_NLI_MS']):])
            meanScX = np.mean(X_MS.loc[p].iloc[int(X_add.loc[p, 'ind_NLI_MS']):])
            all_meanscblNLI_impr.append(meanScY-meanScX)
        sc = pd.DataFrame(index = pats_use, data = all_meanscblNLI_impr)
    return sc
    
def getThisSummary(df_summaryTrialPop, counter,X_add, pats_treat,pats_control):
    for arm in ['control','intervention']:
        if arm == 'control':
            pats_arm = pats_control
        else:
            pats_arm = pats_treat
        for score in ['AIS','sex','NLI_coarse']:

            if score == 'sex':
                try:
                    df_summaryTrialPop.loc[counter, 'sex_male_' + arm ] = \
                    X_add.loc[pats_arm, 'sex'].value_counts()['m'] / len(pats_arm)
                except:
                    df_summaryTrialPop.loc[counter, 'sex_male_' + arm] =0

            if score == 'NLI_coarse':
                for this_nlic in ['cervical', np.nan, 'lumbar', 'sacral', 'thoracic']:
                    try:
                        df_summaryTrialPop.loc[counter,'NLIc_'+arm+'_'+this_nlic] = X_add.loc[pats_arm,'NLI_coarse'].value_counts()[this_nlic]/len(pats_arm)
                    except:
                        df_summaryTrialPop.loc[counter, 'NLIc_'+arm+'_'+str(this_nlic)] =0

            if score == 'AIS':
                for this_ais in ['A','B','C','D']:
                    try:
                        df_summaryTrialPop.loc[counter,'AIS_'+arm+'_'+this_ais] = X_add.loc[pats_arm,'AIS'].value_counts()[this_ais]/len(pats_arm)
                    except:
                        df_summaryTrialPop.loc[counter, 'AIS_' + arm + '_' + this_ais] = 0

        for score in ['age','LEMS','TMS','UEMS']:
            for perc in [2.5, 50,97.5]:
                df_summaryTrialPop.loc[counter, score +'_'+arm+'_perc'+str(perc)] = \
                    X_add.loc[pats_arm, score].astype(float).quantile(perc/100)
    return df_summaryTrialPop



# Get data
def load_EMSCI_fix(path, splitname):

    with open(os.path.join(path, splitname), 'rb') as f:
        data = pickle.load(f)
    X_use = data[0]['X_use']
    Y_use = data[0]['Y_use']

    # Get patients within 40 days of injury
    pats = np.unique([i.split('_')[0] for i in X_use.index])
    all_pats = [i.split('_')[0] for i in X_use.index]
    all_ids = X_use.index.to_list()
    ids = []
    for p in pats:
        ind_this_pat = [index for index, value in enumerate(all_pats) if value == p]
        ids_this_pat = [all_ids[i] for i in ind_this_pat]
        if len(ind_this_pat)>1:
            try:
                times_X = [float(id.split('_')[1]) for id in ids_this_pat]
                times_Y = [float(id.split('_')[2]) for id in ids_this_pat]
                id_y = np.where(times_Y == np.max(times_Y))[0]
                id_x = np.where(times_X == np.min(times_X))[0]
                id_keep = ids_this_pat[list(set(id_y )&set(id_x))[0]]
                ids.append(id_keep)
            except:
                try:
                    times_X = [float(id.split('_')[1]) for id in ids_this_pat]
                    times_Y = [float(id.split('_')[2]) for id in ids_this_pat]
                    id_y = np.where(times_Y == np.min(times_Y))[0]
                    id_x = np.where(times_X == np.min(times_X))[0]
                    id_keep = ids_this_pat[list(set(id_y) & set(id_x))[0]]
                    ids.append(id_keep)
                except:
                    print(p)
        else:
            ids.append(ids_this_pat[0])
    X_use = X_use.loc[ids]
    Y_use = Y_use.loc[ids]
    X_use['ID'] = pats
    X_use.set_index('ID', inplace = True)
    Y_use['ID'] = pats
    Y_use.set_index('ID', inplace = True)

    X_MS = X_use[ms_fields_toMatch]
    Y_MS = Y_use[ms_fields_toMatch]
    X_add = X_use[['age', 'ind_NLI', 'timeX', 'timeY',
                           'AIS_B', 'AIS_C', 'AIS_D', 'sex_m',
                           'VAC_Yes', 'DAP_Yes']]
    X_add['IDs_pred'] = ids
    pats = X_add[X_add.timeX<=time_max_baseline].index.to_list()
    X_MS = X_MS.loc[pats]
    X_add = X_add.loc[pats]
    Y_MS = Y_MS.loc[pats]
    ids = X_add['IDs_pred'].to_list()


    ais = []
    lems = []
    uems = []
    tms = []
    ind_nli_MS = []
    for s in X_add.index:

        # Get ind MS
        this_nliind = X_add.loc[s, 'ind_NLI']
        ms_blnli = [value for value in dermatomes_withC1[this_nliind:] if value in ms_bothSide]
        this_ind_nliMS = 2 * ms_bothSide.index(ms_blnli[0])
        ind_nli_MS.append(this_ind_nliMS)

        tms.append(X_use.loc[s,ms_fields_toMatch].sum())
        lems.append(X_use.loc[s,lems_MS].sum())
        uems.append(X_use.loc[s, uems_MS].sum())
        if X_add.loc[s, 'AIS_B']:
            ais.append('B')
        else:
            if X_add.loc[s, 'AIS_C']:
                ais.append('C')
            else:
                if X_add.loc[s, 'AIS_D']:
                    ais.append('D')
                else:
                    ais.append('A')
    X_add['AIS'] = ais
    X_add['LEMS'] = lems
    X_add['UEMS'] = uems
    X_add['TMS'] = tms
    X_add['ind_NLI_MS'] = ind_nli_MS
    X_add['plegia'] = X_add['ind_NLI_MS']<10
    X_add['plegia'] = X_add['plegia'].map({True: 'tetra',False:'para'})
    X_add['sex'] = X_add['sex_m']
    X_add.drop(['sex_m'],axis = 1, inplace = True)


    return X_MS, Y_MS, X_add, ids

#######################################################################################

if __name__ == '__main__':

    # Get the options - here model type for simulated controls
    simnames =['ElasticNet','Ridge','RF','RF_MS_same','XGB','CNN','CNN_seq','ensemble','TF','TF_seq','GNN']
                
               
    # Paths 
    path_model = 'mypath' # Path to trained models 
    path       = 'myEMSCIPath' # Path to emsci data 
    splitname  = 'myDatasplits.pkl' # Patient split to get all patients used
    output     = 'outputfoldername'
    
    # Create output folder 
    Path(output).mkdir(parents=True, exist_ok=True)


    # Loop over different prediction model outputs - all saved using the model name as .csv
    simpaths = []
    for model_pred in simnames:
        output_path_simPreds =os.path.join(path_model,model_pred+'.csv')
        simpaths.append(output_path_simPreds)

    
    # Define trial settings
    time_max_baseline = 40 # in days, Evaluation time is max time available
    metricsum = 'mean' # options 'median','mean'


    # Treatment effect simulation - currently not used here
    treatmentEffs = [0]


    # Trial settings
    n_runs = 500 # how often to repeat trial run, recommend 500

    
    # Specific trial inclusion criteria and size
    ais_trial     = ['A', 'B', 'C', 'D'] # Define order of below fractions
    fracAISA      = [0.4, 0.15, 0.15, 0.3] # Fractions by the above AIS grades
    n_pat_trial   = 200 # number of patients in trial
    plegia_trial  = ['tetra','para'] # Options: ['tetra'],['para'],['tetra','para']
    reusePats     = False # True or False

    
    # Matching criteria in order of importance:
    matchby = ['AIS', 'age','NLI','sex']  # Options: 'AIS', 'age','NLI','sex'

###########################################################################################
# END OF INPUT
###########################################################################################

    # Load EMSCI data 
    X_MS, Y_MS, X_add, ids = load_EMSCI_fix(path, splitname)
    pats = list(X_add.index)


    # Load the simulation predictions
    simRecs = []
    for i,output_path_simPreds in enumerate(simpaths):
        df_s2s = pd.read_csv(output_path_simPreds, index_col=0)
        df_s2s = df_s2s.T
        df_s2s = df_s2s.loc[ids]
        df_s2s['ID'] = X_MS.index.to_list()
        df_s2s.set_index('ID',inplace = True)
        df_s2s=  df_s2s.loc[:,ms_fields_toMatch]
        simRecs.append(df_s2s)


    # Check if some predictions are missing
    pats  = X_add.index.to_list()
    X_add = X_add.loc[pats]
    X_MS  = X_MS.loc[pats]
    Y_MS  = Y_MS.loc[pats]


    # Run the actual trial simulation
    df_summarypop, df_groundTruth, alldf_simContrs, df_randomContr, output_trial,df_controlRand ,df_interventionGT, df_intervention, alldf_simContrsABS = runTrialByAISA(n_pat_trial, ais_trial, fracAISA, plegia_trial, reusePats, matchby,
                                    randomization, n_runs, treatmentEffs, X_add, Y_MS, X_MS,simRecs,simnames, output,model_pred)

    # Save the results
    df_groundTruth.to_csv(os.path.join(output_trial,'groundTruth.csv'))
    df_randomContr.to_csv(os.path.join(output_trial, 'radomizedControl.csv'))
    df_summarypop.to_csv(os.path.join(output_trial, 'summary_population.csv'))
    df_intervention.to_csv(os.path.join(output_trial,'df_intervention.csv'))
    df_interventionGT.to_csv(os.path.join(output_trial, 'df_interventionGT.csv'))
    df_controlRand.to_csv(os.path.join(output_trial, 'df_controlRand.csv'))
    for i_sim, df_simContr in enumerate(alldf_simContrs):
        df_simContr.to_csv(os.path.join(output_trial, 'simulatedControl_' + simnames[i_sim] + '.csv'))
    for i_sim, df_simContrABS in enumerate(alldf_simContrsABS):  
        df_simContrABS.to_csv(os.path.join(output_trial, 'simulatedControlABS_' + simnames[i_sim] + '.csv'))

    plt.close('all')
    print('FINISHED')

