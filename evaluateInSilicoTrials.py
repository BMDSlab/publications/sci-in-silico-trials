# Evaluations and generated plots

# Imports
from tqdm import tqdm
import numpy as np
import seaborn as sns
from pathlib import Path
import os
import pandas as pd 
import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter





##########################################################################################################
# Fixed settings 
randomization = 'match'
i_reduce      = 0
usePoisson    = True
matchby       = ['AIS', 'age','NLI','sex']
treatmentEffs = [0]
metrics       = ['LEMSImpr']
labels_metric = ['LEMS Improvement']
metric_names  = {'LEMSImpr': r'$\Delta$ LEMS$_I$ -$\Delta$ LEMS$_C$'}
colors_models = {'Randomized\nControl':'darkred',
                 'Ground Truth':'red',  
                 'CNN':'orange',
                 'CNN \n(multi-modal)':'orange',
                 'CNN \n(block)':'darkorange',
                 'CNN \n(ensemble)':'gold',
                 'RF':'darkgrey',
                 'XGB':'lightgrey',
                 'Ridge':'m',
                 'Linear':'m',
                 'Elastic Net':'purple',
                 'Transformer':'blue',
                 'Transformer \n(multi-modal)':'blue',
                 'Transformer \n(block)':'cornflowerblue',
                 'TF':'cornflowerblue',
                 'GNN':'seagreen'
                }

##########################################################################################################
# Auxillary Functions
##########################################################################################################

# Get type I errors for a set of simulations
def getTypeIErrors(all_dfs_RC,eff,metric):
    percentiles = np.linspace(0,1,1000)
    col = ['tr' + str(0) + '_' + metric + '_aispop']
    p_rand = []
    for k in range(0,len(all_dfs_RC)): 
        df_randomContr = all_dfs_RC[k]

        # Two sided test
        p_pos = 1-percentiles[np.argmin(np.abs(np.array([df_randomContr[col].quantile(i).item()-eff for i in percentiles])))]
        p_neg = percentiles[np.argmin(np.abs(np.array([df_randomContr[col].quantile(i).item()+eff for i in percentiles])))]
        p_rand.append(p_neg+p_pos)
    return p_rand

# Load simulation results
def loadSimulation(simnames,output_in,n_pat_trial,ais_trial,fracAISA,plegia_trial,matchby,reusePats): 

        # Names
        trialname = str(n_pat_trial) + '_' + str(ais_trial) + str(fracAISA) + str(plegia_trial) + '_match' + str(matchby) + \
                        '_usePoissonTrue_randmomizationmatch_reduceLambda0_reusePats'+str(reusePats)
        trialname_short = str(n_pat_trial) + '_' +str(fracAISA) + str(plegia_trial) + str(matchby) +str(i_reduce)+str(reusePats)
        output_trial  = os.path.join(output_in, trialname)

        # Load data    
        df_groundTruth = pd.read_csv(os.path.join(output_trial,'groundTruth.csv'))
        df_randomContr = pd.read_csv(os.path.join(output_trial, 'radomizedControl.csv'))
        alldf_simContrs = []
        for i_sim, simname in enumerate(simnames):
            df_simContr = pd.read_csv(os.path.join(output_trial, 'simulatedControl_' + simname + '.csv'))
            alldf_simContrs.append(df_simContr)
            
        return trialname_short,df_groundTruth,df_randomContr,alldf_simContrs

# Plot functions - as a function of AIS grades, number of patients, or plegia
def plotAsFuncNPats(plegia_trial,n_pat_trials,fracAISA,output_in,reusePats = True, simnames = ['CNN'],
                  simnames_show = ['CNN \n(multi-modal)'],randomization = 'match',metrics       = ['LEMSImpr']):
    
    all_dfs_GT = []
    all_dfs_RC = []
    all_dfs_simCT = []
    for n_pat_trial in n_pat_trials:

        trialname_short,df_groundTruth,df_randomContr,alldf_simContrs = (simnames,output_in,n_pat_trial,ais_trial,fracAISA,plegia_trial,matchby,reusePats)

        # Save for later
        all_dfs_GT.append(df_groundTruth)
        all_dfs_RC.append(df_randomContr)
        all_dfs_simCT.append(alldf_simContrs)

    # Plot percentile as a function of patient number
    percentile = 0.95
    for i_m, m in enumerate(metrics): 
        col = ['tr' + str(0) + '_' + m + '_aispop']
        df_sub = pd.DataFrame(index = [f for f in n_pat_trials], columns = ['Randomized\nControl']+simnames_show)
        for i_n,n_pat_trial in enumerate(n_pat_trials):

            try:
                df_randomContr = all_dfs_RC[i_n]
                alldf_simContrs = all_dfs_simCT[i_n]
                df_sub.loc[n_pat_trial,'Randomized\nControl'] = df_randomContr[col].quantile(percentile).values[0]
                for i_sim, sim in enumerate(simnames_show): 
                    df_sub.loc[n_pat_trial,sim]   = alldf_simContrs[i_sim][col].quantile(percentile).values[0]
            except: 
                continue

        plt.figure(figsize = (3,3))
        plt.plot([f for f in n_pat_trials],df_sub['Randomized\nControl'], 'o',
                 label = 'Randomized\nControl', 
                 color = colors_models['Randomized\nControl'], linewidth = 2)

        for i_sim, sim in enumerate(simnames_show): 
            plt.plot([f for f in n_pat_trials],df_sub[sim], 'o',label = simnames_show[i_sim],
                     color = colors_models[sim])
        plt.legend()
        plt.ylabel(metric_names[m])
        plt.tick_params(axis='x', which='both', length=0)
        plt.xlabel('Number of Patients')
        plt.tight_layout()
        plt.savefig(os.path.join(plot_out,trialname_short+'_'+ m+'_asfunctionofNpats_'+str(percentile)+'.png'))

def plotAsFuncAIS(plegia_trial,n_pat_trial,fracAISAs,output_in,reusePats = True, simnames = ['CNN'],
                  simnames_show = ['CNN \n(multi-modal)'],randomization = 'match',metrics       = ['LEMSImpr']):
    
    all_dfs_GT = []
    all_dfs_RC = []
    all_dfs_simCT = []
    for fracAISA in fracAISAs:

        trialname_short,df_groundTruth,df_randomContr,alldf_simContrs = (simnames,output_in,n_pat_trial,ais_trial,fracAISA,plegia_trial,matchby,reusePats)

        # Save for later
        all_dfs_GT.append(df_groundTruth)
        all_dfs_RC.append(df_randomContr)
        all_dfs_simCT.append(alldf_simContrs)

    # Plot percentile as a function of patient number
    percentile = 0.95
    for i_m, m in enumerate(metrics): 
        col = ['tr' + str(0) + '_' + m + '_aispop']
        df_sub = pd.DataFrame(index = [str(f) for f in fracAISAs], columns = ['Randomized\nControl']+simnames_show)
        for i_n,fracAISA in enumerate(fracAISAs):

            try:
                df_randomContr = all_dfs_RC[i_n]
                alldf_simContrs = all_dfs_simCT[i_n]
                df_sub.loc[str(fracAISA),'Randomized\nControl'] = df_randomContr[col].quantile(percentile).values[0]
                for i_sim, sim in enumerate(simnames_show): 
                    df_sub.loc[str(fracAISA),sim]   = alldf_simContrs[i_sim][col].quantile(percentile).values[0]
            except: 
                continue

        plt.figure(figsize = (3,2.3))
        plt.plot([str(f) for f in fracAISAs],df_sub['Randomized\nControl'], 'o',
                 label = 'Randomized\nControl', 
                 color = colors_models['Randomized\nControl'], linewidth = 2)

        for i_sim, sim in enumerate(simnames_show): 
            plt.plot([str(f) for f in fracAISAs],df_sub[sim], 'o',label = simnames_show[i_sim],
                     color = colors_models[sim])
        #plt.legend()
        plt.ylabel(metric_names[m])
        plt.xticks(rotation=90, fontsize = 12)
        plt.xticks(ticks=[], labels=[])
        plt.tick_params(axis='x', which='both', length=0)
        plt.xlabel('')
        plt.tight_layout()
        plt.savefig(os.path.join(plot_out,trialname_short+'_'+ m+'_asfunctionofAIS_'+str(percentile)+'.png'))

def plotAsFuncPlegia(plegia_trials,n_pat_trial,fracAISA,output_in,reusePats = True, simnames = ['CNN'],
                  simnames_show = ['CNN \n(multi-modal)'],randomization = 'match',metrics       = ['LEMSImpr']):
    
    all_dfs_GT = []
    all_dfs_RC = []
    all_dfs_simCT = []
    for plegia_trial in plegia_trials:
        
        trialname_short,df_groundTruth,df_randomContr,alldf_simContrs = loadSimulation(simnames,output_in,n_pat_trial,ais_trial,fracAISA,plegia_trial,matchby,reusePats)
        
        # Save for later
        all_dfs_GT.append(df_groundTruth)
        all_dfs_RC.append(df_randomContr)
        all_dfs_simCT.append(alldf_simContrs)


    
    # Plot percentile as a function of patient number
    percentile = 0.95
    for i_m, m in enumerate(metrics): 
        col = ['tr' + str(0) + '_' + m + '_aispop']
        df_sub = pd.DataFrame(index = [str(f) for f in plegia_trials], columns = ['Randomized\nControl']+simnames_show)
        for i_n,fracAISA in enumerate(fracAISAs):

            try:
                df_randomContr = all_dfs_RC[i_n]
                alldf_simContrs = all_dfs_simCT[i_n]
                df_sub.loc[str(plegia_trial),'Randomized\nControl'] = df_randomContr[col].quantile(percentile).values[0]
                for i_sim, sim in enumerate(simnames_show): 
                    df_sub.loc[str(plegia_trial),sim]   = alldf_simContrs[i_sim][col].quantile(percentile).values[0]
            except: 
                continue

        plt.figure(figsize = (3,2.3))
        plt.plot([str(f) for f in plegia_trials],df_sub['Randomized\nControl'], 'o',
                 label = 'Randomized\nControl', 
                 color = colors_models['Randomized\nControl'], linewidth = 2)

        for i_sim, sim in enumerate(simnames_show): 
            plt.plot([str(f) for f in plegia_trials],df_sub[sim], 'o',label = simnames_show[i_sim],
                     color = colors_models[sim])
        #plt.legend()
        plt.ylabel(metric_names[m])
        plt.xticks(rotation=90, fontsize = 12)
        plt.xticks(ticks=[], labels=[])
        plt.tick_params(axis='x', which='both', length=0)
        plt.xlabel('')
        plt.tight_layout()
        plt.savefig(os.path.join(plot_out,trialname_short+'_'+ m+'_asfunctionofPlegia_'+str(percentile)+'.png'))

# For all models plot in the absence of treatment
def plotSimulationBoxPlot(trialname_short,df_groundTruth,df_randomContr,alldf_simContrs,metrics,simnames_show,plot_out)
    df_sub = pd.DataFrame(index = np.arange(500), columns = ['Randomized\nControl']+simnames_show)
    for i_m, m in enumerate(metrics): 
        
        col = ['tr' + str(0) + '_' + m + '_aispop']
        df_sub['Randomized\nControl'] = df_randomContr[col]
        for i_sim, sim in enumerate(simnames_show): 
            df_sub[sim]   = alldf_simContrs[i_sim][col]
        df_long = df_sub.melt(var_name='Column', value_name=metric_names[m])
    
        # Plot as boxplot
        plt.figure(figsize = (3,3))
        ax = sns.boxplot(x ='Column', y = metric_names[m], data = df_long,
                   palette = colors_models,
                   whiskerprops=dict(color='black'),
                   capprops=dict(color='black'),  
                   medianprops=dict(color='black', linewidth=2),
                   boxprops=dict(edgecolor='black'))
    
        plt.axhline(y=0, color='r', linestyle=':',linewidth = 3, label = 'Ground Truth')
        legend = ax.legend()
            #legend.set_title(None)
        plt.xticks(rotation=90)
        plt.xlabel('')
        plt.tight_layout()
        plt.savefig(os.path.join(plot_out,trialname_short+'_'+ m+'_boxplotNotreatmentAllmodels.png'))

def plotTypeIError(n_pat_trials,metric ,eff1,eff2,simnames,output_in,ais_trial,fracAISA,plegia_trial,matchby,reusePats)
    # Load data   
    all_dfs_RC = []
    all_dfs_simCT = []
    for n_pat_trial in n_pat_trials:
    
        # Get the specific simulation
        trialname_short,df_groundTruth,df_randomContr,alldf_simContrs = loadSimulation(simnames,output_in,n_pat_trial,ais_trial,fracAISA,plegia_trial,matchby,reusePats)
            
        # Save for later
        all_dfs_RC.append(df_randomContr)
        all_dfs_simCT.append(df_simContr)
    
    # Get the type 1 error for a fixed effect size
    p_rand  = getTypeIErrors(all_dfs_RC,eff1,metric)
    p_syn   = getTypeIErrors(all_dfs_simCT,eff1,metric)
    p_rand4 = getTypeIErrors(all_dfs_RC,eff2,metric)
    p_syn4  = getTypeIErrors(all_dfs_simCT,eff2,metric)
    
    
    # Create the plot 
    plt.figure(figsize=(3,3))
    plt.plot(n_pat_trials,p_syn,'o-', color = 'orange',label = 'Synthetic')
    plt.plot(n_pat_trials,p_rand,'o-', color = 'darkred',label = 'Random')
    plt.plot(n_pat_trials,p_syn4,'o--', color = 'orange')
    plt.plot(n_pat_trials,p_rand4,'o--', color = 'darkred')
    plt.hlines(xmin = 450, xmax = 500, y = 0.1,ls = '-', color = 'k',label = 'Effect Size '+str(eff1))
    plt.hlines(xmin = 450, xmax = 500, y = 0.1,ls = '--', color = 'k',label = 'Effect Size '+str(eff2))
    plt.legend()
    plt.xlim([0,420])
    plt.xlabel('Number of Patients')
    plt.ylabel('Probability of Type I Error')
   
##########################################################################################################

# Path to predictions
plot_out   = 'whereToSavePlots'
output_in  = 'outputfolderOfInSilicoTrialSimulationsToUse'

# Create overview plots

######################################################
## Boxplots for different synthethic control options


# Settings for the trial to show
n_pat_trial   = 200
ais_trial     = ['A', 'B', 'C', 'D']
fracAISA      = [0.4, 0.15, 0.15, 0.3]
plegia_trial  = ['tetra', 'para']
reusePats     = False

# All simulations to plot - saved names and names to shop in plot
simnames      = ['ElasticNet','XGB','RF','CNN','TF','GNN']
simnames_show = ['Elastic Net','XGB','RF','CNN','TF','GNN']


# Load data    
trialname_short,df_groundTruth,df_randomContr,alldf_simContrs = loadSimulation(simnames,output_in,n_pat_trial,ais_trial,fracAISA,plegia_trial,matchby,reusePats)

# plot
plotSimulationBoxPlot(trialname_short,df_groundTruth,df_randomContr,alldf_simContrs,metrics,simnames_show,plot_out)


######################################################
## Plot 95th percentile as a funciton of patient numbers, AIS grades, and plegia
simnames      = ['CNN']
simnames_show = ['CNN']
metrics       = ['LEMSImpr']

# Variable options
fracAISAs     = [[0.0, 0.5, 0.5, 0.0],[0.25, 0.25, 0.25, 0.25],[0.4, 0.15, 0.15, 0.3]]
plegia_trials = [['tetra','para'],['tetra'],['para']]
n_pat_trials  = [400,200,100,50]

# Fixed options
fracAISA      = [0.4, 0.15, 0.15, 0.3]
plegia_trial  = ['tetra','para']
n_pat_trial   = 200
reusePats     = False


plotAsFuncAIS(plegia_trial,n_pat_trial,fracAISAs,output_in,reusePats = reusePats, simnames = simnames,
                  simnames_show = simnames_show,randomization = randomization,metrics = metrics)
plotAsFuncPlegia(plegia_trials,n_pat_trial,fracAISA,output_in,reusePats = reusePats, simnames = simnames,
                  simnames_show = simnames_show,randomization = randomization,metrics = metrics)
plotAsFuncNPats(plegia_trial,n_pat_trials,fracAISA,output_in,reusePats = reusePats, simnames = simnames,
                  simnames_show = simnames_show,randomization = randomization,metrics = metrics)


######################################################
## Plot Type I error as a function of cohort size

# Settings - number of patients tested, observed group level differences
n_pat_trials  = [ 25,50,75,100,150,200,250,300,350,400]
metric        = metrics[0] # Metric used
eff1          = 2       
eff2          = 4

# plot
plotTypeIError(n_pat_trials,metric ,eff1,eff2,simnames,output_in,ais_trial,fracAISA,plegia_trial,matchby,reusePats)

