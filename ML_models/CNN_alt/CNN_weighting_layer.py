from tensorflow.keras.layers import Layer
import tensorflow.keras.backend as K


class WeightedAverageLayer(Layer):
    def __init__(self, **kwargs):
        super(WeightedAverageLayer, self).__init__(**kwargs)

    def build(self, input_shape):
        # Initialize weights for each input
        self.decision_weights = self.add_weight(name='decision_weights',
                                                shape=(len(input_shape),),
                                                initializer='uniform',
                                                trainable=True)
        super(WeightedAverageLayer, self).build(input_shape)

    def call(self, inputs):
        # Ensure inputs is a list of tensors
        if not isinstance(inputs, list):
            raise ValueError('A weighted average layer should be called on a list of inputs.')

        weighted_sum = K.zeros_like(inputs[0])
        for i, input_tensor in enumerate(inputs):
            weighted_sum += self.decision_weights[i] * input_tensor

        return weighted_sum

    def compute_output_shape(self, input_shape):
        return input_shape[0]
