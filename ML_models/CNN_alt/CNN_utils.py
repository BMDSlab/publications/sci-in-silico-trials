from keras.src.callbacks import Callback

from ML_models.CNN_alt.CNN_data_utils import *
from ML_models.CNN_alt.CNN_id_names import get_custom_model_name, get_model_ID_alt
from ML_models.CNN_alt.CNN_parameters import get_custom_model_parameters
from ML_models.CNN_alt.CNN_utils_model_path import load_custom_model_path
from ML_models.CNN_alt.model_name_from_parameters import get_model_from_parameters
from Miscellaneous.Misc.columns import *
from Miscellaneous.Misc.paths import get_file_path_custom_model, get_model_dir
from Miscellaneous.Misc.fields_to_match import dermatomes_withC1, ms_both_sides_fields_toMatch

np.random.seed(1234)
import pandas as pd


def get_ids_and_data_vectorized(X_train, Y_train, X_test, Y_test):
    train_and_val = X_train['Unnamed: 0'].unique()
    testIDs = X_test['Unnamed: 0'].unique()
    np.random.shuffle(train_and_val)
    split_index = int(len(train_and_val) / 4)
    trainIDs = train_and_val[split_index:]
    validationIDs = train_and_val[:split_index]

    X_use = pd.concat((X_train, X_test)).copy()
    Y_use = pd.concat((Y_train, Y_test))

    unique_ids = X_use['Unnamed: 0'].unique()
    Y_s = pd.DataFrame(5, index=unique_ids, columns=ms_both_sides_fields_toMatch)

    # Vectorized operations
    levels = X_use['level'].map(pd.Series(dermatomes_withC1))
    sides = X_use['Left'].map({1: 'LMS', 0: 'RMS'})

    combinations = sides + '_' + levels

    # Group by patient index and combine combinations
    grouped = pd.DataFrame({'combinations': combinations, 'patient_index': X_use['Unnamed: 0']})
    grouped = grouped.groupby('patient_index')['combinations'].agg('_'.join).reset_index()

    # Merge with Y_use
    result = pd.merge(grouped, Y_use, left_on='patient_index', right_index=True, how='left')
    # Y_s.loc[result['patient_index'], result['combinations']] = result.iloc[:, 2:].values

    for idx, row in result.iterrows():
        Y_s.loc[row['patient_index'], row['combinations']] = row.iloc[2:].values

    X_use = X_use.drop(columns=['level', 'Left', 'sc_X'], axis=1)
    X_use = X_use.drop_duplicates()

    return trainIDs, validationIDs, testIDs, X_use, Y_s


def get_parameters_from_args_old(debug=False, with_uncertainty=False, bs=200, n_layers=1,
                                 lr=0.0001, n_filters_1=16, do=0.2, kernel_size=3, l2=0.001, pat=200, TF=False,
                                 only_MS=False,
                                 n_epochs=10000, **kwargs):
    # TODO: should be able to pass in a dictionary of parameters
    parameters = get_custom_model_parameters(debug=debug)
    parameters['with_uncertainty'] = with_uncertainty
    parameters['bs'] = bs
    parameters['sample_shape'] = (bs, 20, 1)
    parameters['n_layers'] = n_layers
    parameters['n_filters_1'] = n_filters_1
    parameters['do'] = do
    parameters['lr'] = lr
    parameters['kernel_size'] = kernel_size
    parameters['l2'] = l2
    parameters['n_epochs'] = n_epochs
    # for key, value in kwargs.items():
    #     parameters[key] = value
    # parameters['l1_act'] = l2

    parameters['pat'] = pat

    if TF:
        parameters[transformer_str] = True
    if only_MS:
        parameters[only_MS_str] = True


    return parameters


def get_parameters_from_args(parameters, debug=False):
    new_parameters = get_custom_model_parameters(debug=debug)
    for key, value in parameters.items():
        if key == bs_str:
            new_parameters[sample_shape_str] = (parameters[bs_str], 20, 1)
        new_parameters[key] = value
    return new_parameters


def load_CNN_model(c=0, tsc='T1', debug=False, bootstrap=False, batch_size=100, n_layers=1,
                   learning_rate=0.0001, num_filters=16, dropout=0.2, kernel_size=3, l2=0.001, patience=200):
    # parameters = get_parameters_from_args(debug=debug, with_uncertainty=bootstrap, bs=batch_size, n_layers=n_layers,
    #                                       lr=learning_rate, n_filters_1=num_filters, do=dropout,
    #                                       kernel_size=kernel_size, l2=l2, pat=patience, )

    model = load_CNN_model_with_parameters(parameters, c, tsc, bootstrap=bootstrap)

    return model


def load_CNN_model_with_parameters(parameters, c, tsc='all'):
    bootstrap = parameters[with_uncertainty_str]
    model_type = get_model_from_parameters(parameters)
    is_ordinal = parameters[ordinal_str] if ordinal_str in parameters else False
    output = get_model_dir(bootstrap, model_type, ordinal=is_ordinal)
    modelID = get_model_ID_alt(parameters)

    model_name = get_custom_model_name(modelID, c, tsc)

    filepath_model = get_file_path_custom_model(output, model_name)
    #print(filepath_model)

    model = load_custom_model_path(filepath_model)
    return model


class NBatchLogger(Callback):
    """
    A Logger that log average performance per `display` steps.
    """

    def __init__(self, display):
        self.step = 0
        self.display = display
        self.metric_cache = {}

    def on_batch_end(self, batch, logs={}):
        self.step += 1
        for k in self.params['metrics']:
            if k in logs:
                self.metric_cache[k] = self.metric_cache.get(k, 0) + logs[k]
        if self.step % self.display == 0:
            metrics_log = ''
            for (k, v) in self.metric_cache.items():
                val = v / self.display
                if abs(val) > 1e-3:
                    metrics_log += ' - %s: %.4f' % (k, val)
                else:
                    metrics_log += ' - %s: %.4e' % (k, val)
            print('step: {}/{} ... {}'.format(self.step,
                                              self.params['steps'],
                                              metrics_log))
            self.metric_cache.clear()