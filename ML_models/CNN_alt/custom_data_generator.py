import pickle

from ML_models.CNN_alt.CNN_dataloader import DataGenerator
from ML_models.CNN_alt.sequence_dataloader import SequenceDataGenerator
from Miscellaneous.Misc.paths import *


def get_data_generators(parameters, dim, fold_index, dataset=emsci_str):
    scaled = True
    data_path = get_data_dict_splits_CNN_format_path(fold_index, bootstrap=parameters[with_uncertainty_str],
                                                     scaled=scaled, dataset=dataset)
    input_path = open(data_path, 'rb')
    print('Loading data from file')
    data = pickle.load(input_path)

    trainIDs, validationIDs, testIDs, X_use, Y_use = data['trainIDs'], data['validationIDs'], data[
        'testIDs'], data['X_use'], data['Y_use']
    print(len(trainIDs), len(validationIDs), len(testIDs))
    MS, LT, PP, Xadd = (data['X_use']['MS'], data['X_use']['LT'],
                        data['X_use']['PP'], data['X_use']['X_add'])

    training_generator, validation_generator, test_generator = get_dataloader(dim, parameters, trainIDs,
                                                                              validationIDs, testIDs, MS, LT,
                                                                              PP,
                                                                              Xadd,
                                                                              Y_use)
    return training_generator, validation_generator, test_generator


def get_dataloader(dim, parameters, trainIDs, validationIDs, testIDs, MS, LT, PP, Xadd, Y_use):
    params_dataloader = {'dim': dim,
                         'batch_size': parameters[bs_str],
                         'shuffle': True,
                         'MS': MS,
                         'LT': LT,
                         'PP': PP,
                         'X_add': Xadd,
                         'Y': Y_use,
                         ordinal_str: parameters[ordinal_str] if ordinal_str in parameters else False,
                         GNN_str: True if GNN_str in parameters or GNN2_str in parameters else False,
                         }
    params_dataloaderTest = {'dim': dim,
                             'batch_size': parameters['val_bs'],
                             'shuffle': False,
                             'MS': MS,
                             'LT': LT,
                             'PP': PP,
                             'X_add': Xadd,
                             'Y': Y_use,
                             ordinal_str: parameters[ordinal_str] if ordinal_str in parameters else False,
                             GNN_str: True if GNN_str in parameters or GNN2_str in parameters else False,
                             }

    training_generator = DataGenerator(trainIDs, **params_dataloader)
    validation_generator = DataGenerator(validationIDs, **params_dataloaderTest)
    test_generator = DataGenerator(testIDs, **params_dataloaderTest)

    return training_generator, validation_generator, test_generator


def get_dataloader_sequence(dim, parameters, trainIDs, validationIDs, testIDs, X, Y_use):
    params_dataloader = {'dim': dim,
                         'batch_size': parameters['bs'],
                         'shuffle': True,
                         'X': X,
                         'Y': Y_use,

                         }
    params_dataloaderTest = {'dim': dim,
                             'batch_size': parameters['val_bs'],
                             'shuffle': False,
                             'X': X,
                             'Y': Y_use,
                             }

    training_generator = SequenceDataGenerator(trainIDs, **params_dataloader)
    validation_generator = SequenceDataGenerator(validationIDs, **params_dataloaderTest)
    test_generator = SequenceDataGenerator(testIDs, **params_dataloaderTest)

    return training_generator, validation_generator, test_generator
