from Miscellaneous.Misc.columns import CNN_str, transformer_str, ensemble_str, only_MS_str, transformer_sequence_str, CNN_sequence_str


def get_model_from_parameters(parameters):
    if transformer_str in parameters and parameters[transformer_str]:
        model = transformer_str
    elif ensemble_str in parameters and parameters[ensemble_str]:
        model = ensemble_str
    elif only_MS_str in parameters and parameters[only_MS_str]:
        model = only_MS_str
    elif transformer_sequence_str in parameters and parameters[transformer_sequence_str]:
        model = transformer_sequence_str
    elif CNN_sequence_str in parameters and parameters[CNN_sequence_str]:
        model = CNN_sequence_str
    else:
        model = CNN_str
    return model
