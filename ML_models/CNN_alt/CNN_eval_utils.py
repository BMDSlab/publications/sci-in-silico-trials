from ML_models.CNN_alt.CNN_dataloader import DataGenerator
from ML_models.CNN_alt.CNN_parameters import get_custom_model_parameters
from ML_models.CNN_alt.CNN_plot_utils import plot_trainingProcess

import pandas as pd

from Miscellaneous.Misc.fields_to_match import *
from Miscellaneous.Misc.paths import *
from Run_collection.Run_prediction.SCIRecoveryPrediction_no_shape import augmented_evaluation_res


def get_res_custom_model(model, datasets, validation=False, augmented_evaluation=False,
                         ordinal=False, update_parameters=None):
    trainIDs, validationIDs, testIDs, X_use, Y_use = datasets[train_ids_str], datasets[validation_ids_str], datasets[
        test_ids_str], datasets[x_use_str], datasets[y_use_str]

    # X_use_unscaled = datasets_unscaled[x_use_str]
    IDs_to_use = validationIDs if validation else testIDs
    predictions = pd.DataFrame(index=IDs_to_use,
                               columns=['id', ind_NLI_str, motor_score_str, motor_score_level_str, 'start value',
                                        'end value', pred_str])

    predictions['id'] = IDs_to_use

    MS, LT, PP, Xadd = (X_use[ms_str], X_use[lt_str],
                        X_use[pp_str], X_use[x_add_str])
    # predictions['start value'] = 0

    # predictions[ind_NLI_str] = X_use_unscaled[x_add_str].loc[IDs_to_use][ind_NLI_str]
    predictions[ind_NLI_str] = (Xadd[ind_NLI_str] * max_indNLI).astype(int)
    # trainIDs, validationIDs, testIDs, X_use, Y_use, mask_use = get_ids_and_data(X_train, Y_train, X_test, Y_test)
    parameters = get_custom_model_parameters()
    if update_parameters is not None:
        parameters.update(update_parameters)

    dim = parameters['sample_shape'][1:]
    params_dataloaderTest = {
                             'batch_size': parameters['val_bs'],
                             'shuffle': False,
                             ms_str: MS,
                             lt_str: LT,
                             pp_str: PP,
                             x_add_str: Xadd,
                             'Y': Y_use,
                             ordinal_str: parameters[ordinal_str] if ordinal_str in parameters else False,
                             GNN_str: True if GNN_str in parameters or GNN2_str in parameters else False,
                             }

    test_generator = DataGenerator(IDs_to_use, **params_dataloaderTest)
    X_test_run, y_test_run = test_generator.__getall__()

    y_pred = model.predict(X_test_run, verbose=None)
    print('Creating dataframe')
    y_pred = pd.DataFrame(y_pred, index=IDs_to_use, columns=ms_both_sides_fields_toMatch)

    list_of_predictions = []
    for i, index in enumerate(IDs_to_use):
        patient_predictions = y_pred.loc[index]

        patient_preds = pd.DataFrame(index=patient_predictions.index,
                                     columns=['id', motor_score_str, 'start value', 'end value', pred_str])
        patient_preds['id'] = index
        patient_preds[pred_str] = patient_predictions.values
        patient_preds[motor_score_str] = patient_predictions.index
        patient_preds[motor_score_level_str] = patient_preds[motor_score_str].apply(
            lambda x: dermatomes_withC1.index(x[-2:]))
        patient_preds[motor_score_index_str] = patient_preds[motor_score_str]

        #patient_preds[end_value_str] = y_test_run.loc[index].values
        patient_preds[end_value_str] = y_test_run[i]
        patient_preds[start_value_str] = MS.loc[index].values.flatten() * 5
        # X_use_unscaled[ms_str].loc[index].values.flatten()

        patient_preds[ind_NLI_str] = (Xadd.loc[index][ind_NLI_str] * max_indNLI).astype(int)
        # X_use_unscaled[x_add_str].loc[index][ind_NLI_str]
        list_of_predictions.append(patient_preds)

    predictions = pd.concat(list_of_predictions, axis=0)

    if augmented_evaluation:
        predictions = augmented_evaluation_res(predictions)
    return predictions

def eval_CNN_model(model, filepath, output, model_name, validationIDs, testIDs, validation_generator, test_generator,
                   history):
    model.load_weights(filepath)

    # Plot training curves
    plot_trainingProcess(history, os.path.join(output, 'run_losses'), model_name, 0)

    X_val, y_val = validation_generator.__getall__()
    predict_val = model.predict(X_val)
    predict_val_df = pd.DataFrame(predict_val, index=validationIDs)

    X_test, y_test = test_generator.__getall__()
    predict_tst = model.predict(X_test)
    predict_tst_df = pd.DataFrame(predict_tst, index=testIDs)

    '''
    try:
        df_real_val.loc[validationIDs] = list(np.squeeze(predict_val))
    except:
        df_real_val.loc[:] = list(np.squeeze(predict_val))
        df_real_val['pat'] = validationIDs
        df_real_val.set_index('pat', drop=True, inplace=True)

    try:
        df_real.loc[testIDs] = list(np.squeeze(predict_tst))
    except:
        df_real.loc[:] = list(np.squeeze(predict_tst))
        df_real['pat'] = testIDs
        df_real.set_index('pat', drop=True, inplace=True)
    '''
    # return df_real, df_real_val
    return predict_tst_df, predict_val_df




