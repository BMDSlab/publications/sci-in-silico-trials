import argparse

from ML_models.CNN_alt.CNN_arg_parser import parent_parser
from Miscellaneous.Misc.columns import *


def get_custom_model_parameters(debug=False, run_mnist=False):


    main_parser = argparse.ArgumentParser()
    service_subparsers = main_parser.add_subparsers(title="service",
                                                    dest="service_command")
    parser = service_subparsers.add_parser("first", help="first",
                                                   parents=[parent_parser])


    parser.add_argument('--local', type=int, default=1, help='if local machine or cluster is used')
    parser.add_argument('--tp1', type=int, default=1, help='time point 1 - indicate number of weeks')
    parser.add_argument('--tp2', type=int, default=1, help='time point 2 - indicate number of weeks')
    parser.add_argument('--cv', type=int, default=1, help='cross validation fold')
    parser.add_argument('--addData', type=int, default=0, help='optional use of additional data')

    parser.add_argument('--num_layer', type=int, default=1)
    parser.add_argument('--do', type=float, default=0.25)
    parser.add_argument('--lr', type=float, default=0.0001)
    parser.add_argument('--bs', type=int, default=200)
    parser.add_argument('--pat', type=int, default=2000)
    parser.add_argument('--val_bs', type=int, default=20)
    parser.add_argument('--ks', type=int, default=3)
    parser.add_argument('--num_filters', type=int, default=16)
    parser.add_argument('--l2', type=float, default=0.001)
    parser.add_argument('--hiddendim', type=int, default=32)
    parser.add_argument('--clf', type=int, default=0)
    parser.add_argument('--uncert', action='store_true')
    # parser.add_argument('--tabular', action='store_true')
    parser.add_argument('--n_epochs', type=int, default=10000)
    parser.add_argument('--cnn', action='store_true')
    parser.add_argument('--transformer', action='store_true')
    parser.add_argument('--tf', action='store_true')
    parser.add_argument('--tf2', action='store_true')
    parser.add_argument('--only_MS', action='store_true')
    parser.add_argument('--ensemble', action='store_true')
    parser.add_argument('--tf_seq', action='store_true')
    parser.add_argument('--tf_seq2', action='store_true')
    parser.add_argument('--cnn_seq', action='store_true')
    parser.add_argument('--gnn', action='store_true')
    parser.add_argument('--gnn2', action='store_true')
    parser.add_argument('--ordinal', action='store_true')
    parser.add_argument('--new_hp', action='store_true')
    parser.add_argument('--skip_hp', action='store_true')




    args = parser.parse_args()

    parameters = dict()

    parameters['do'] = args.do
    parameters['bs'] = args.bs

    parameters['val_bs'] = args.val_bs
    parameters['lr'] = args.lr
    parameters['kernel_size'] = args.ks
    parameters['hiddendim'] = args.hiddendim
    parameters['pat'] = args.pat
    # parameters['sample_shape'] = (args.bs, 56, 2)
    parameters['sample_shape'] = (args.bs, 20, 1)
    if args.clf == 0:
        parameters['clf'] = 'gap'
    else:
        parameters['clf'] = 'dense'

    parameters['n_layers'] = args.num_layer
    parameters['n_filters_1'] = args.num_filters

    parameters['BNloc'] = 1
    parameters['useDO'] = True  # dropout may cause problems on euler
    parameters['l2'] = args.l2
    parameters['l1_act'] = args.l2
    parameters['n_epochs'] = args.n_epochs
    parameters['with_uncertainty'] = args.uncert
    # parameters['tabular'] = args.tabular

    # parameters['hiddendim'] = 8
    # parameters['bs'] = 100
    # parameters['n_filters_1'] = 4

    if debug:
        parameters['n_epochs'] = 50
        parameters['hiddendim'] = 8
        parameters['bs'] = 100
        parameters['n_filters_1'] = 4

    if run_mnist:
        parameters['sample_shape'] = (args.bs, 28, 28)

    if args.transformer or args.tf:
        parameters[transformer_str] = True
    if args.only_MS:
        parameters[only_MS_str] = True
    if args.ensemble:
        parameters[ensemble_str] = True
    if args.tf_seq:
        parameters[transformer_sequence_str] = True
    if args.cnn_seq:
        parameters[CNN_sequence_str] = True
    if args.gnn:
        parameters[GNN_str] = True
    if args.gnn2:
        parameters[GNN2_str] = True
    if args.tf2:
        parameters[TF2_str] = True
    if args.tf_seq2:
        parameters[TF_seq2_str] = True
    if args.ordinal:
        parameters[ordinal_str] = True

    if args.new_hp:
        parameters[new_hp_str] = True
    else:
        parameters[new_hp_str] = False
    if args.skip_hp:
        parameters[skip_hp_str] = True
    else:
        parameters[skip_hp_str] = False

    return parameters


def get_sweep_params(model, all_models=False, debug=False, ordinal=False):
    if debug:
        return get_sweep_params_validation_debug()
    if model == CNN_str:
        if all_models:
            return get_sweep_params_validation_CNN()
        else:
            return get_sweep_params_CNN()
    elif model == CNN_sequence_str:
        if all_models:
            return get_sweep_params_validation_CNN_seq(ordinal=ordinal)
        else:
            return get_sweep_params_CNN_seq(ordinal=ordinal)

    elif model == transformer_str:
        if all_models:
            return get_sweep_params_validation_transformer()
        else:
            return get_sweep_params_transformer()
    elif model == transformer_sequence_str:
        if all_models:
            return get_sweep_params_validation_transformer_seq(ordinal=ordinal)
        else:
            return get_sweep_params_transformer_seq(ordinal=ordinal)

    elif model == ensemble_str:

        return get_sweep_params_ensemble()



def get_sweep_params_CNN():
    params = {
        'lr': [0.0001],  # learning rate
        num_layers_str: [1, 2, 3],  # number of layers
        'do': [0.1, 0.2, 0.3],  # dropout
        num_filters_str: [8, 16, 32],  # number of filters
        'l2': [0.001, 0.01],  # l2 regularization
        'kernel_size': [3, 5],  # kernel size
        'with_uncertainty': [False],
        'bs': [200],
    }

    return params


def get_sweep_params_validation_CNN():
    params = {
        'lr': [0.00003, 0.0001, 0.001],  # learning rate
        num_layers_str: [1, 2, 3],  # number of layers
        'do': [0.1, 0.2, 0.25, 0.4],  # dropout
        num_filters_str: [8, 16, 32],  # number of filters
        'l2': [0.001, 0.01],  # l2 regularization
        'kernel_size': [3, 5, 7],  # kernel size
        'with_uncertainty': [False],
        pat_str: [300, 1000, 2000],
        bs_str: [32, 200],
        # only_MS_str: [False, True],
        # with_uncertainty_str: [False, True]

    }

    return params

def get_sweep_params_CNN_seq(ordinal=False):
    if not ordinal:
        params = {
            'lr': [0.0001],  # learning rate
            num_layers_str: [1, 2, 3],  # number of layers
            #'do': [0.1, 0.2, 0.3],  # dropout
            num_filters_str: [8, 16, 32],  # number of filters
            #'l2': [0.001, 0.01],  # l2 regularization
            'kernel_size': [3, 5],  # kernel size
            'with_uncertainty': [False],
            'bs': [128],
            CNN_sequence_str: [True]
        }

    else:
        params = {
            'lr': [0.0001],  # learning rate
            num_layers_str: [1, 2, 3],  # number of layers
            #'do': [0.1, 0.2, 0.3],  # dropout
            num_filters_str: [8, 16, 32],  # number of filters
            #'l2': [0.001, 0.01],  # l2 regularization
            'kernel_size': [3, 5],  # kernel size
            'with_uncertainty': [False],
            'bs': [128],
            CNN_sequence_str: [True],
            ordinal_str: [True]
        }
    return params


def get_sweep_params_validation_CNN_seq(ordinal=False):
    if not ordinal:
        params = {
            'lr': [0.0001],  # learning rate
            num_layers_str: [1, 2, 3],  # number of layers
            #'do': [0.1, 0.2, 0.25, 0.4],  # dropout
            num_filters_str: [8, 16, 32],  # number of filters
            #'l2': [0.001, 0.01],  # l2 regularization
            'kernel_size': [3, 5, 7],  # kernel size
            'with_uncertainty': [False],
            pat_str: [2000],
            bs_str: [32, 128, 200],
            CNN_sequence_str: [True]

        }
    else:
        params = {
            'lr': [0.0001],  # learning rate
            num_layers_str: [1, 2, 3],  # number of layers
            #'do': [0.1, 0.2, 0.25, 0.4],  # dropout
            num_filters_str: [8, 16, 32],  # number of filters
            #'l2': [0.001, 0.01],  # l2 regularization
            'kernel_size': [3, 5, 7],  # kernel size
            'with_uncertainty': [False],
            pat_str: [2000],
            bs_str: [32, 128, 200],
            CNN_sequence_str: [True],
            ordinal_str: [True]
        }

    return params

def get_sweep_params_transformer():
    params = {
        lr_str: [0.0001],  # learning rate
        num_layers_str: [1, 2, 3],  # num_transformer_blocks
        do_str: [0.1, 0.2, 0.3],  # dropout
        num_filters_str: [32, 64, 128],  # head_size
        l2_str: [0.001],  # l2 regularization
        kernel_size_str: [2, 4, 8],  # num_heads
        pat_str: [500],
        bs_str: [200],
        with_uncertainty_str: [False],
        transformer_str: [True]
    }

    return params

def get_sweep_params_validation_transformer():
    params = {
        lr_str: [0.0001],  # learning rate
        num_layers_str: [1, 2, 3],  # num_transformer_blocks
        do_str: [0.1, 0.2, 0.25, 0.3],  # dropout
        num_filters_str: [16, 32, 64, 128],  # head_size
        l2_str: [0.001],  # l2 regularization
        kernel_size_str: [2, 3, 4, 8],  # num_heads
        pat_str: [100, 500, 2000],
        bs_str: [32, 200],
        with_uncertainty_str: [False],
        transformer_str: [True],
        #only_MS_str: [False, True]
    }

    return params

def get_sweep_params_transformer_seq(ordinal=False):
    if ordinal is False:
        params = {
            lr_str: [0.0001],  # learning rate
            num_layers_str: [1, 2],  # num_transformer_blocks
            #do_str: [0.1, 0.2],  # dropout
            num_filters_str: [32, 64, 128],  # head_size
            #l2_str: [0.001],  # l2 regularization
            kernel_size_str: [6, 8, 12],  # num_heads
            pat_str: [1000],
            bs_str: [128],
            with_uncertainty_str: [False],
            transformer_sequence_str: [True]
        }
    else:
        params = {
            lr_str: [0.0001],  # learning rate
            num_layers_str: [1, 2],  # num_transformer_blocks
            #do_str: [0.1, 0.2],  # dropout
            num_filters_str: [32, 64, 128],  # head_size
            #l2_str: [0.001],  # l2 regularization
            kernel_size_str: [6, 8, 12],  # num_heads
            pat_str: [1000],
            bs_str: [128],
            with_uncertainty_str: [False],
            transformer_sequence_str: [True],
            ordinal_str: [True]
        }
    return params

def get_sweep_params_validation_transformer_seq(ordinal=False):
    if ordinal is False:
        params = {
            lr_str: [0.0001, 0.00001],  # learning rate
            num_layers_str: [1, 2],  # num_transformer_blocks
            do_str: [0.1, 0.25],  # dropout
            num_filters_str: [16, 32, 64, 128],  # head_size
            #l2_str: [0.001],  # l2 regularization
            kernel_size_str: [3, 4, 6, 8, 12],  # num_heads
            pat_str: [100, 200, 300, 400, 500, 1000, 2000],
            bs_str: [8, 16, 32, 64, 128, 200, 256],
            with_uncertainty_str: [False],
            transformer_sequence_str: [True],
            #only_MS_str: [False, True]
        }
    else:
        params = {
            # lr_str: [0.0001, 0.00001],  # learning rate
            num_layers_str: [1, 2],  # num_transformer_blocks
            # do_str: [0.1, 0.25],  # dropout
            num_filters_str: [16, 32, 64, 128],  # head_size
            # #l2_str: [0.001],  # l2 regularization
            kernel_size_str: [3, 4, 6, 8, 12],  # num_heads
            pat_str: [100, 1000, 2000],
            bs_str: [128, 200],
            #pat_str: [100],
            n_epochs_str: [2, 100, 10000],

            # bs_str: [8, 16, 32, 64, 128, 200, 256],
            with_uncertainty_str: [False],
            transformer_sequence_str: [True],
            ordinal_str: [True],
            # #only_MS_str: [False, True]
        }

    return params


def get_sweep_params_ensemble():
    params = {
        lr_str: [0.0001],  # learning rate
        num_layers_str: [1, 2, 3],  # num_transformer_blocks
        do_str: [0.2, 0.3],  # dropout
        num_filters_str: [16, 32],  # head_size
        l2_str: [0.001],  # l2 regularization
        kernel_size_str: [3, 5],  # num_heads
        pat_str: [1000],
        bs_str: [32],
        with_uncertainty_str: [False],
        ensemble_str: [True]
    }

    return params





def get_sweep_params_validation_debug():
    params = {
        lr_str: [0.0001],  # learning rate
        num_layers_str: [1],  # num_transformer_blocks
        do_str: [0.2],  # dropout
        num_filters_str: [16],  # head_size
        l2_str: [0.001],  # l2 regularization
        kernel_size_str: [5],  # num_heads
        pat_str: [2000],
        bs_str: [32],
        with_uncertainty_str: [False],
        only_MS_str: [False, True],
        ensemble_str: [False, True],
    }

    return params
