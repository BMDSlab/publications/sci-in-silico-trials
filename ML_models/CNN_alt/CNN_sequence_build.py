from keras import Input, Model
from keras.src.layers import Flatten, Conv2D, MaxPooling2D, Dense, concatenate, BatchNormalization, Dropout, \
    AveragePooling2D

from tensorflow.keras import layers

from ML_models.CNN_alt.CNN_args import *
from ML_models.CNN_alt.architecture_blocks import get_output_layer
from ML_models.CNN_alt.block_input_preprocessing import *
from ML_models.CNN_alt.custom_models_hyperparameters import cnn_sequence_hyperparameters
from ML_models.CNN_alt.loss import *
from Miscellaneous.Misc.columns import *
from tensorflow.keras.regularizers import l2


def build_CNN_seq_model(
        num_conv_layers=2,
        conv_filters=32,
        kernel_size=(3, 3),
        pool_size=(2, 2),
        num_tabular_dense_layers=1,
        tabular_dense_units=64,
        num_merged_dense_layers=2,
        merged_dense_units=128,
        lr=0.0001,
        minimal=False, loss_use=rmse_str):
    matrix_input1 = layers.Input(shape=ms_dims)
    matrix_input2 = layers.Input(shape=lt_pp_dims)
    tabular_input = Input(shape=tabular_dim)
    if minimal:
        # Wrong place atm
        matrix_input2_shortened = matrix_input2[:, 9:19, :]
        matrix_input1_expanded = tf.expand_dims(matrix_input1, axis=-1)
        input_3d = tf.concat([matrix_input1_expanded, matrix_input2_shortened], axis=-1)
    else:
        nli = tabular_input[:, 1]
        input_3d = get_3D_input_full_score_impute(matrix_input1, matrix_input2, scaled_nli=nli)

    x1 = input_3d
    for _ in range(num_conv_layers):
        x1 = Conv2D(conv_filters, kernel_size, activation='relu', padding='same')(x1)
        if pool_size:
            x1 = MaxPooling2D(pool_size, padding='same')(x1)
    x1 = Flatten()(x1)

    # Tabular input pathway
    x2 = tabular_input
    for _ in range(num_tabular_dense_layers):
        x2 = Dense(tabular_dense_units, activation='relu')(x2)

    # Merge pathways
    concatenated = concatenate([x1, x2])

    # Further processing
    x = concatenated
    for _ in range(num_merged_dense_layers):
        x = Dense(merged_dense_units, activation='relu')(x)

    outputs = get_output_layer(concatenated, loss_use)

    model = keras.Model(inputs=[matrix_input1, matrix_input2, tabular_input], outputs=outputs)

    loss_func = get_loss_function(loss_use)

    model.compile(loss=loss_func,
                  optimizer=keras.optimizers.Adam(learning_rate=lr),
                  metrics=[tf.keras.metrics.MeanSquaredError(name='mse')])  # Add ordinal metrics if it will be used
    return model


def build_CNN_seq_model_hp(hp, loss_use=rmse_str):
    (num_conv_layers, conv_filters, kernel_size, pool_size, num_tabular_dense_layers, tabular_dense_units,
     num_merged_dense_layers, merged_dense_units, lr, do, l2_reg_factor) = cnn_sequence_hyperparameters(hp)

    ms_input = layers.Input(shape=ms_dims)
    lt_pp_input = layers.Input(shape=lt_pp_dims)
    tabular_input = Input(shape=tabular_dim)
    nli = tabular_input[:, 1]
    #input_3d = get_3D_input_full_score_impute(ms_input, lt_pp_input, scaled_nli=nli)
    input_3d = ImputeLayer3D()([ms_input, lt_pp_input, nli])

    x1 = input_3d
    for _ in range(num_conv_layers):
        x1 = Conv2D(filters=conv_filters, kernel_size=kernel_size, padding='same',
                    kernel_regularizer=l2(l2_reg_factor))(x1)
        x1 = BatchNormalization()(x1)
        x1 = tf.keras.activations.relu(x1)
        if pool_size:
            x1 = AveragePooling2D(pool_size, padding='same')(x1)

    x1 = Flatten()(x1)

    # Tabular input pathway
    x2 = tabular_input
    for _ in range(num_tabular_dense_layers):
        x2 = Dense(tabular_dense_units, kernel_regularizer=l2(l2_reg_factor))(x2)
        x2 = BatchNormalization()(x2)
        x2 = tf.keras.activations.relu(x2)
        x2 = Dropout(do)(x2)

    # Merge pathways
    concatenated = concatenate([x1, x2])

    # Further processing
    x = concatenated
    for _ in range(num_merged_dense_layers):
        x = Dense(merged_dense_units, kernel_regularizer=l2(l2_reg_factor))(x)
        x = BatchNormalization()(x)
        x = tf.keras.activations.relu(x)

    # Final output layer adjustments based on task, e.g., classification or regression
    predictions = get_output_layer(x, loss_use)  # Ensure get_output_layer is appropriately defined

    nli_mask_layer = ExpandedMaskLayer()
    nli_output_masks = nli_mask_layer(nli)

    y_true = Input(shape=(output_size,))

    model = Model(inputs=[ms_input, lt_pp_input, tabular_input, y_true], outputs=predictions)

    RMSEMaskedNLILossLayer()([y_true, predictions, nli_output_masks])

    #model.add_loss(rmse_masked_nli(y_true, predictions, nli_output_masks))  # Fix to be able to use ordinal loss
    model.compile(loss=None,
                  optimizer=keras.optimizers.Adam(learning_rate=lr),
                  metrics=[tf.keras.metrics.MeanSquaredError(name='mse')])  # Add ordinal metrics if it will be used
    return model
