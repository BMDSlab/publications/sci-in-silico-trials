import copy

from tensorflow import keras
import numpy as np

from Miscellaneous.Misc.fields_to_match import *


class DataGenerator(keras.utils.Sequence):
    # In part adapted from https://stanford.edu/~shervine/blog/keras-how-to-generate-data-on-the-fly 20200630

    def __init__(self, list_IDs=None, Y=None, MS=None, LT=None, PP=None, X_add=None, dim=(56, 2), batch_size=10,
                 shuffle=True, transformer_data=False, ord=False, GNN=False):  # output_masks=None,
        'Initialization'
        self.dim = dim
        self.batch_size = batch_size
        self.labels = Y
        self.MS = MS

        self.LT = LT
        self.PP = PP
        self.X_add = X_add
        self.ordinal = ord
        if list_IDs is None:
            self.list_IDs = None
        else:
            self.list_IDs = list(list_IDs)
        self._all_ids = list(self.MS.index)
        self.shuffle = shuffle
        self.on_epoch_end()

    def on_epoch_end(self):

        'Updates indexes after each epoch'
        self.indexes = np.arange(len(self.list_IDs))
        if self.shuffle == True:
            np.random.shuffle(self.indexes)

    def __data_generation(self, list_IDs_temp):
        """Generates data containing batch_size samples"""  # X : (n_samples, *dim, n_channels)
        ms_batch = np.stack(self.MS.loc[list_IDs_temp].to_numpy())
        lt_batch = np.stack(self.LT.loc[list_IDs_temp].to_numpy())
        pp_batch = np.stack(self.PP.loc[list_IDs_temp].to_numpy())
        lt_pp_batch = np.stack((lt_batch, pp_batch), axis=3)

        X_add_batch = np.stack(self.X_add.loc[list_IDs_temp].to_numpy())
        y_batch = self.labels.loc[list_IDs_temp].to_numpy()

        return (ms_batch, lt_pp_batch, X_add_batch, y_batch), y_batch

    def __len__(self):
        'Denotes the number of batches per epoch'
        return int(np.floor(len(self.list_IDs) / self.batch_size))

    def __getitem__(self, index):
        'Generate one batch of data'

        # Generate indexes of the batch
        indexes = self.indexes[index * self.batch_size:(index + 1) * self.batch_size]

        # Find list of IDs
        try:
            list_IDs_temp = [self.list_IDs[k] for k in indexes]
        except:
            print(indexes)

        # Generate data
        X, y = self.__data_generation(list_IDs_temp)

        return X, y

    def __getY__(self, index):
        'Generate one batch of data'

        # Generate indexes of the batch
        indexes = self.indexes[index * self.batch_size:(index + 1) * self.batch_size]

        # Find list of IDs
        list_IDs_temp = [self.list_IDs[k] for k in indexes]

        # Generate data
        y = self.__data_generationY(list_IDs_temp)

        return y

    def __data_generationY(self, list_IDs_temp):
        'Generates data containing batch_size samples'  # X : (n_samples, *dim, n_channels)

        # Initialization
        y = np.empty((self.batch_size), dtype=int)

        # Generate data
        for i, ID in enumerate(list_IDs_temp):
            # Store class
            y[i] = self.labels.loc[ID]

        return y

    def __getall__(self):
        'Generate one batch of data'

        # Find list of IDs
        list_IDs_temp = self.list_IDs

        # Generate data
        X, y = self.__data_generation(list_IDs_temp)

        return X, y

    def get_N_samples_all(self, N=None):
        'Generate one batch of data'

        temp_all_ids = self._all_ids
        # scramble the order
        np.random.shuffle(temp_all_ids)

        if N is not None:
            ids = temp_all_ids[:N]
        else:
            ids = temp_all_ids
            # Generate data
        X, y = self.__data_generation(ids)

        return X, y

    def get_N_samples(self, N):
        'Generate one batch of data'

        # Find list of IDs
        list_IDs_temp = self.list_IDs[:N]

        # Generate data
        X, y = self.__data_generation(list_IDs_temp)

        return X, y

    def __data_generation_Y(self, list_IDs_temp):
        'Generates data containing batch_size samples'  # X : (n_samples, *dim, n_channels)

        # Initialization
        y = np.empty((len(list_IDs_temp)), dtype=int)
        print(len(list_IDs_temp))

        # Generate data
        for i, ID in enumerate(list_IDs_temp):
            # Store class
            y[i] = self.labels.loc[ID]

        return y

    def __getall_Y__(self):
        'Generate one batch of data'

        # Find list of IDs
        list_IDs_temp = self.list_IDs

        # Generate data
        y = self.__data_generation_Y(list_IDs_temp)

        return y

    def get_individual_feature_names(self):
        return ms_both_sides_fields_toMatch, LTS_scores, PPS_scores, list(self.X_add.columns)

    def get_feature_names(self):
        return list(ms_both_sides_fields_toMatch) + list(LTS_scores) + list(PPS_scores) + list(self.X_add.columns)

    def __copy__(self):
        return copy.deepcopy(self)