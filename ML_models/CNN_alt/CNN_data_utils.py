import numpy as np
np.random.seed(271)
import pandas as pd
from tqdm import tqdm

from Miscellaneous.Misc.fields_to_match import *

def get_ids_and_data_already_sequence(X_train, Y_train, X_test, Y_test):
    train_and_val = X_train['Unnamed: 0'].unique()
    testIDs = X_test['Unnamed: 0'].unique()
    np.random.shuffle(train_and_val)
    split_index = int(len(train_and_val) / 5)
    trainIDs = train_and_val[split_index:]
    validationIDs = train_and_val[:split_index]

    X_use = pd.concat((X_train, X_test))
    Y_use = pd.concat((Y_train, Y_test))

    return trainIDs, validationIDs, testIDs, X_use, Y_use

def get_ids_and_data(X_train, Y_train, X_test, Y_test):

    train_and_val = list(X_train['Unnamed: 0'].unique().astype(str))
    testIDs = list(X_test['Unnamed: 0'].unique().astype(str))

    original_id_list = [s for s in train_and_val if '_' not in s]

    np.random.shuffle(original_id_list)

    split_index = int(len(original_id_list) / 5)
    trainIDs_original = original_id_list[split_index:]
    trainIDs = [string_a for string_a in train_and_val if any(string_b in string_a for string_b in trainIDs_original)]

    validationIDs = original_id_list[:split_index]

    X_use = pd.concat((X_train, X_test))
    #X_use.index = X_use['Unnamed: 0']
    # TODO: may need to set index on non-bootstrapped data
    Y_use = pd.concat((Y_train, Y_test))

    unique_ids = X_use['Unnamed: 0'].unique().astype(str)
    X_use['Unnamed: 0'] = X_use['Unnamed: 0'].astype(str)

    #mask_df = pd.DataFrame(False, index=unique_ids, columns=ms_both_sides_fields_toMatch) # Unused for now
    Y_s = pd.DataFrame(5, index=unique_ids, columns=ms_both_sides_fields_toMatch)

    for patient_index in tqdm(unique_ids):
        pat = X_use[X_use['Unnamed: 0'] == patient_index][['level', 'Left']]
        pat['level'] = pat['level'].apply(lambda x: dermatomes_withC1[x])
        pat['Left'] = pat['Left'].apply(lambda x: 'LMS' if x == 1 else 'RMS')

        combinations = pat[['Left', 'level']].agg('_'.join, axis=1)

        patient_Y = Y_use.loc[X_use['Unnamed: 0'] == patient_index]
        #for combination in combinations:
        #    mask_df.loc[patient_index, combination] = True
        Y_s.loc[patient_index, combinations] = patient_Y.values

    X_use = X_use.drop(columns=['level', 'Left', 'sc_X'], axis=1)
    X_use = X_use.drop_duplicates()
    X_use = X_use.drop_duplicates(subset=['Unnamed: 0'])
    X_use.index = list(X_use['Unnamed: 0'])
    #X_use = X_use.drop(columns=['Unnamed: 0'])  # TODO: make sure this doesn't happen earlier
    # X_use['sc_X'] = X_use['sc_X'].fillna(0)

    return trainIDs, validationIDs, testIDs, X_use, Y_s


def get_test_data(datasets, est_name):
    X_test_MS0 = datasets['X_test_MS0']
    Y_test_MS0 = datasets['Y_test_MS0']

    X_test_MS1_5 = datasets['X_test_MS1_5']
    Y_test_MS1_5 = datasets['Y_test_MS1_5']

    X_test_MS0_ids = X_test_MS0['Unnamed: 0']
    X_test_MS1_5_ids = X_test_MS1_5['Unnamed: 0']

    X_test_MS0 = X_test_MS0.drop(['Unnamed: 0'], axis=1)
    X_test_MS1_5 = X_test_MS1_5.drop(['Unnamed: 0'], axis=1)

    Y_test_MS0_preds = pd.DataFrame(index=X_test_MS0.index,
                                    columns=['id', 'ind_NLI', 'motor score', 'motor score level', 'start value',
                                             'end value', 'pred'])
    Y_test_MS1_5_preds = pd.DataFrame(index=X_test_MS1_5.index,
                                      columns=['id', 'ind_NLI', 'motor score', 'motor score level', 'start value',
                                               'end value', 'pred'])

    Y_test_MS0_preds['id'] = X_test_MS0_ids
    Y_test_MS1_5_preds['id'] = X_test_MS1_5_ids

    Y_test_MS0_preds['end value'] = Y_test_MS0
    Y_test_MS1_5_preds['end value'] = Y_test_MS1_5

    Y_test_MS0_preds['start value'] = 0
    Y_test_MS1_5_preds['start value'] = X_test_MS1_5['sc_X']

    Y_test_MS0_preds['ind_NLI'] = X_test_MS0['ind_NLI']
    Y_test_MS1_5_preds['ind_NLI'] = X_test_MS1_5['ind_NLI']

    if est_name == 'MS0':
        X_test = X_test_MS0
        Y_test = Y_test_MS0

        preds = Y_test_MS0_preds

    elif est_name == 'MS1_5':
        X_test = X_test_MS1_5
        Y_test = Y_test_MS1_5

        preds = Y_test_MS1_5_preds

    return preds, X_test, Y_test

def get_data_level(X_train, y_train, m):
    indices = X_train['level'] == dermatomes_withC1.index(m)
    X_train = X_train.drop(columns=['level'])
    y_train = y_train.drop(columns=['level'])
    return X_train[indices], y_train[indices], indices

