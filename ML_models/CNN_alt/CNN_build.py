from keras import Layer
from keras.src.layers import Conv2D, MaxPooling2D
from keras.models import Model
from keras.layers import Dense, Flatten, Dropout, BatchNormalization, Input, Activation
from keras.regularizers import l2, l1
from keras.layers import Conv1D, MaxPooling1D, GlobalAveragePooling1D, concatenate, LeakyReLU

from ML_models.CNN_alt.CNN_args import *
from ML_models.CNN_alt.CNN_weighting_layer import WeightedAverageLayer
from ML_models.CNN_alt.custom_models_hyperparameters import *
from ML_models.CNN_alt.loss import *
from Miscellaneous.Misc.columns import *
from Miscellaneous.Misc.fields_to_match import ms_bothSide

# Clear all previously registered custom objects
keras.utils.get_custom_objects().clear()


def build_CNNflexi_Modular1D(input):
    """Simple CNN with n Conv layers and either globalAveragePooling or fully connected layers for classification"""

    params = input['params']
    sample_shape = params['sample_shape']
    inputA = Input(shape=sample_shape[1:])
    clf = params['clf']

    # Model head
    for i_layers in range(0, params['n_layers']):
        if i_layers == 0:
            x = conv1D_block(params, inputA)
        else:
            x = conv1D_block(params, x)

    # Classifiers
    if clf == 'dense':
        x = Flatten()(x)
        n_dim = params['n_dim_dense']
        for i_layer in range(0, params['n_layers_dense']):
            x = Dense(n_dim / (i_layer + 1), activation='relu', kernel_initializer='he_uniform',
                      activity_regularizer=l1(params['l1_dense']))(x)
    elif clf == 'gap':
        x = GlobalAveragePooling1D()(x)
    else:
        raise ('Invalid classifier choice!')

    # Output layer
    x = Dense(1)(x)

    model = Model(inputs=inputA, outputs=x)
    return model


def build_CNNflexi_Modular1D_alt_ensemble(params):
    output_shape = len(ms_bothSide) * 2
    # Define inputs
    ms_input = Input(shape=ms_dims, name='ms_input')
    lt_pp_input = Input(shape=lt_pp_dims, name='lt_pp_input')  # Adjust the shape as necessary
    tabular_input = Input(shape=tabular_dim, name='tabular_input')

    # Define branches (assuming these functions are defined and return the last layer of each branch)
    ms_branch = conv1D_block(params, ms_input)
    for n_layers in range(1, params['n_layers']):
        ms_branch = conv1D_block(params, ms_branch)
    ms_branch = Flatten()(ms_branch)
    ms_branch = Dense(32, name='ms_branch')(ms_branch)

    lt_pp_branch = conv2D_block(params, lt_pp_input)
    for n_layers in range(1, params['n_layers']):
        lt_pp_branch = conv2D_block(params, lt_pp_branch)

    lt_pp_branch = Flatten()(lt_pp_branch)
    lt_pp_branch = Dense(32, name='lt_pp_branch')(lt_pp_branch)

    tabular_branch = Dense(32, name='tabular_branch')(tabular_input)
    tabular_branch = LeakyReLU(alpha=0.3)(tabular_branch)
    for n_layers in range(1, params['n_layers']):
        tabular_branch = Dense(32)(tabular_branch)
        tabular_branch = LeakyReLU(alpha=0.3)(tabular_branch)

    ms_branch = Dense(output_shape)(ms_branch)
    lt_pp_branch = Dense(output_shape)(lt_pp_branch)
    tabular_branch = Dense(output_shape)(tabular_branch)

    # Use the WeightedAverageLayer to combine the branches

    weighted_average = WeightedAverageLayer(name='weighted_average')([ms_branch, lt_pp_branch, tabular_branch])
    predictions = Activation(mapping_to_target_range)(weighted_average)
    # Create model
    model = Model(inputs=[ms_input, lt_pp_input, tabular_input], outputs=predictions)
    print(model.summary())
    return model


def build_CNNflexi_Modular1D_alt_ensemble_hp(hp):
    params = ensemble_hyperparameters(hp)

    output_shape = len(ms_bothSide) * 2
    # Define inputs
    ms_input = Input(shape=ms_dims, name='ms_input')
    lt_pp_input = Input(shape=lt_pp_dims, name='lt_pp_input')  # Adjust the shape as necessary
    tabular_input = Input(shape=tabular_dim, name='tabular_input')

    # Define branches (assuming these functions are defined and return the last layer of each branch)
    ms_branch = conv1D_block(params, ms_input)
    for n_layers in range(1, params['n_layers']):
        ms_branch = conv1D_block(params, ms_branch)
    ms_branch = Flatten()(ms_branch)
    ms_branch = Dense(32, name='ms_branch')(ms_branch)

    lt_pp_branch = conv2D_block(params, lt_pp_input)
    for n_layers in range(1, params['n_layers']):
        lt_pp_branch = conv2D_block(params, lt_pp_branch)

    lt_pp_branch = Flatten()(lt_pp_branch)
    lt_pp_branch = Dense(32, name='lt_pp_branch')(lt_pp_branch)

    tabular_branch = Dense(32, name='tabular_branch')(tabular_input)
    tabular_branch = LeakyReLU(alpha=0.3)(tabular_branch)
    for n_layers in range(1, params['n_layers']):
        tabular_branch = Dense(32)(tabular_branch)
        tabular_branch = LeakyReLU(alpha=0.3)(tabular_branch)

    ms_branch = Dense(output_shape)(ms_branch)
    lt_pp_branch = Dense(output_shape)(lt_pp_branch)
    tabular_branch = Dense(output_shape)(tabular_branch)

    # Use the WeightedAverageLayer to combine the branches

    weighted_average = WeightedAverageLayer(name='weighted_average')([ms_branch, lt_pp_branch, tabular_branch])
    predictions = Activation(mapping_to_target_range)(weighted_average)

    nli = tabular_input[:, 1]
    nli_mask_layer = ExpandedMaskLayer()
    nli_output_masks = nli_mask_layer(nli)

    y_true = Input(shape=(output_size,))
    # Create model

    model = Model(inputs=[ms_input, lt_pp_input, tabular_input, y_true], outputs=predictions)
    RMSEMaskedNLILossLayer()([y_true, predictions, nli_output_masks])


    model.compile(loss=None,
                  optimizer=keras.optimizers.Adam(learning_rate=params['lr']),
                  metrics=[tf.keras.metrics.MeanSquaredError(name='mse')])
    return model


def weighted_ensemble_predictions(models, input_data, weights):
    # Ensure the weights list is in the same order as the models and input_data
    weighted_predictions = np.zeros_like(
        models[0].predict(input_data[0]))  # Initialize to zeros with shape matching the output

    for model, data, (model_name, weight) in zip(models, input_data, weights.items()):
        predictions = model.predict(data)
        weighted_predictions += weight * predictions  # Add weighted predictions

    # Optionally, you can normalize the final predictions if it makes sense for your application
    # For example, in classification tasks where predictions sum to 1
    # weighted_predictions /= np.sum(list(weights.values()))

    return weighted_predictions


def build_CNNflexi_Modular1D_alt(params):
    l2_param = params['l2']
    l1_act = params['l1_act']
    n_layers = params['n_layers']

    if ensemble_str in params and params[ensemble_str]:
        return build_CNNflexi_Modular1D_alt_ensemble(params)

    output_shape = len(ms_bothSide) * 2
    if only_MS_str in params and params[only_MS_str]:
        ms_input = Input(shape=ms_dims, name='ms_input')
        ms_branch = conv1D_block(params, ms_input)
        for n_layers in range(1, n_layers):
            ms_branch = conv1D_block(params, ms_branch)
        ms_branch = Flatten()(ms_branch)
        ms_branch = Dense(32, name='ms_branch')(ms_branch)
        x = ms_branch

        x = Dense(64, activity_regularizer=(l2(l2_param)),
                  bias_regularizer=l2(l2_param), kernel_regularizer=l1(l1_act))(
            x)  # L1 and L2 are the same atm
        x = Dense(32, activity_regularizer=(l2(l2_param)),
                  bias_regularizer=l2(l2_param), kernel_regularizer=l1(l1_act))(x)

        predictions = Dense(output_shape, activation=mapping_to_target_range, name='main_output')(x)
        # Disconnected for now

        lt_pp_input = Input(shape=lt_pp_dims, name='lt_pp_input')
        tabular_input = Input(shape=tabular_dim, name='tabular_input')

        model = Model(inputs=[ms_input, lt_pp_input, tabular_input], outputs=predictions)
    else:
        ms_input = Input(shape=ms_dims, name='ms_input')
        ms_branch = conv1D_block(params, ms_input)
        for n_layers in range(1, n_layers):
            ms_branch = conv1D_block(params, ms_branch)
        ms_branch = Flatten()(ms_branch)
        ms_branch = Dense(32, name='ms_branch')(ms_branch)

        lt_pp_input = Input(shape=lt_pp_dims, name='lt_pp_input')
        lt_pp_branch = conv2D_block(params, lt_pp_input)
        for n_layers in range(1, n_layers):
            lt_pp_branch = conv2D_block(params, lt_pp_branch)

        lt_pp_branch = Flatten()(lt_pp_branch)
        lt_pp_branch = Dense(32, name='lt_pp_branch')(lt_pp_branch)

        tabular_input = Input(shape=tabular_dim, name='tabular_input')

        tabular_branch = Dense(32, name='tabular_branch')(tabular_input)
        tabular_branch = LeakyReLU(alpha=0.3)(tabular_branch)
        for n_layers in range(1, n_layers):
            tabular_branch = Dense(32)(tabular_branch)
            tabular_branch = LeakyReLU(alpha=0.3)(tabular_branch)

        tabular_branch = Dense(16)(tabular_branch)

        x = concatenate([ms_branch, lt_pp_branch, tabular_branch])

        x = Dense(64, activity_regularizer=(l2(l2_param)),
                  bias_regularizer=l2(l2_param), kernel_regularizer=l1(l1_act))(
            x)  # L1 and L2 are the same atm
        x = Dense(32, activity_regularizer=(l2(l2_param)),
                  bias_regularizer=l2(l2_param), kernel_regularizer=l1(l1_act))(x)

        predictions = Dense(output_shape, activation=mapping_to_target_range, name='main_output')(x)

        model = Model(inputs=[ms_input, lt_pp_input, tabular_input], outputs=predictions)

    return model


def build_CNNflexi_Modular1D_alt_hp(hp):
    params = cnn_hyperparameters(hp)

    l2_param = params['l2']
    l1_act = params['l1_act']
    n_layers = params['n_layers']

    if ensemble_str in params and params[ensemble_str]:
        return build_CNNflexi_Modular1D_alt_ensemble(params)

    ms_input = Input(shape=ms_dims, name='ms_input')
    ms_branch = conv1D_block(params, ms_input)
    for n_layers in range(1, n_layers):
        ms_branch = conv1D_block(params, ms_branch)
    ms_branch = Flatten()(ms_branch)
    ms_branch = Dense(32, name='ms_branch')(ms_branch)

    lt_pp_input = Input(shape=lt_pp_dims, name='lt_pp_input')
    lt_pp_branch = conv2D_block(params, lt_pp_input)
    for n_layers in range(1, n_layers):
        lt_pp_branch = conv2D_block(params, lt_pp_branch)

    lt_pp_branch = Flatten()(lt_pp_branch)
    lt_pp_branch = Dense(32, name='lt_pp_branch')(lt_pp_branch)

    tabular_input = Input(shape=tabular_dim, name='tabular_input')

    tabular_branch = Dense(32, name='tabular_branch')(tabular_input)
    tabular_branch = LeakyReLU(alpha=0.3)(tabular_branch)
    for n_layers in range(1, n_layers):
        tabular_branch = Dense(32)(tabular_branch)
        tabular_branch = LeakyReLU(alpha=0.3)(tabular_branch)

    tabular_branch = Dense(16)(tabular_branch)

    x = concatenate([ms_branch, lt_pp_branch, tabular_branch])

    x = Dense(64, activity_regularizer=(l2(l2_param)),
              bias_regularizer=l2(l2_param), kernel_regularizer=l1(l1_act))(
        x)  # L1 and L2 are the same atm
    x = Dense(32, activity_regularizer=(l2(l2_param)),
              bias_regularizer=l2(l2_param), kernel_regularizer=l1(l1_act))(x)

    predictions = Dense(output_size, activation=mapping_to_target_range, name='main_output')(x)

    nli = tabular_input[:, 1]
    nli_mask_layer = ExpandedMaskLayer()
    nli_output_masks = nli_mask_layer(nli)

    y_true = Input(shape=(output_size,))


    model = Model(inputs=[ms_input, lt_pp_input, tabular_input, y_true], outputs=predictions)
    RMSEMaskedNLILossLayer()([y_true, predictions, nli_output_masks])

    model.compile(
                  optimizer=keras.optimizers.Adam(learning_rate=params['lr']),
                  metrics=[tf.keras.metrics.MeanSquaredError(name='mse')]
                  )

    return model


def conv1D_block(params, input):
    if params['BNloc'] == 0:
        x = Conv1D(
            params['n_filters_1'],
            kernel_size=(params['kernel_size']),
            activation=None,
            kernel_initializer='he_uniform',
            input_shape=params['sample_shape'],
            padding='SAME',
            data_format='channels_last',
            kernel_regularizer=l2(params['l2']),
            bias_regularizer=l2(params['l2'])
        )(input)

        x = BatchNormalization(center=True, scale=True, trainable=True)(x)
        x = LeakyReLU(alpha=0.3, activity_regularizer=l1(params['l1_act']))(x)

    else:
        x = Conv1D(
            filters=params['n_filters_1'],
            kernel_size=(params['kernel_size']),
            kernel_initializer='he_uniform',
            padding='SAME',
            kernel_regularizer=l2(params['l2']),
            bias_regularizer=l2(params['l2']),
            activity_regularizer=l1(params['l1_act']),
            groups=1
        )(input)
        x = LeakyReLU(alpha=0.3)(x)

    if params['BNloc'] == 1:
        x = BatchNormalization(center=True, scale=True, trainable=True)(x)
    try:
        x = MaxPooling1D(pool_size=(2))(x)
    except:
        pass

    if params['BNloc'] == 2:
        x = BatchNormalization(center=True, scale=True, trainable=True)(x)

    if params['useDO']:
        x = Dropout(params['do'])(x)
    return x


def conv2D_block(params, input):
    if params['BNloc'] == 0:
        x = Conv2D(
            params['n_filters_1'],
            kernel_size=(params['kernel_size']),
            activation=None,
            kernel_initializer='he_uniform',
            input_shape=params['sample_shape'],
            padding='SAME',
            data_format='channels_last',
            kernel_regularizer=l2(params['l2']),
            bias_regularizer=l2(params['l2'],
                                )
        )(input)

        x = BatchNormalization(center=True, scale=True, trainable=True)(x)
        x = LeakyReLU(alpha=0.3, activity_regularizer=l1(params['l1_act']))(x)

    else:
        x = Conv2D(
            params['n_filters_1'],
            kernel_size=(params['kernel_size']),
            kernel_initializer='he_uniform',
            padding='SAME',
            kernel_regularizer=l2(params['l2']),
            bias_regularizer=l2(params['l2']),
            activity_regularizer=l1(params['l1_act'])
        )(input)
        x = LeakyReLU(alpha=0.3)(x)

    if params['BNloc'] == 1:
        x = BatchNormalization(center=True, scale=True, trainable=True)(x)

    x = MaxPooling2D(pool_size=(2, 2), padding='same')(x)

    if params['BNloc'] == 2:
        x = BatchNormalization(center=True, scale=True, trainable=True)(x)

    if params['useDO']:
        x = Dropout(params['do'])(x)
    return x


class CustomMaskedLayer(Layer):
    def __init__(self, output_dim, **kwargs):
        self.output_dim = output_dim
        super(CustomMaskedLayer, self).__init__(**kwargs)

    def build(self, input_shape):
        super(CustomMaskedLayer, self).build(input_shape)  # Be sure to call this at the end

    def call(self, inputs, mask=None, **kwargs):
        # Assuming inputs is a tuple (regular_input, mask_input)
        regular_input, mask_input = inputs

        # Element-wise multiplication to zero out selected neurons
        masked_output = regular_input * mask_input

        return masked_output

    def compute_output_shape(self, input_shape):
        # Output shape is the same as the regular input shape
        return input_shape[0]