import numpy as np
import tensorflow as tf
from keras import backend as BK
from keras.src.layers import Lambda
from tensorflow import keras
from tensorflow.keras.layers import Layer

from Miscellaneous.Misc.columns import *

from Miscellaneous.Misc.fields_to_match import max_indNLI


@keras.utils.register_keras_serializable(name="ExpandedMaskLayer")
class ExpandedMaskLayer(tf.keras.layers.Layer):
    def __init__(self, max_length=10, **kwargs):
        super(ExpandedMaskLayer, self).__init__(**kwargs)
        self.max_length = max_length

    def call(self, inputs_scaled):
        inputs = inputs_scaled * max_indNLI
        # Initialize outputs to zeros
        outputs = tf.zeros_like(inputs)

        # Apply the conditional logic using nested tf.where
        outputs = tf.where(inputs < 4.0, 0.0, outputs)
        outputs = tf.where((inputs >= 4.0) & (inputs < 9.0), inputs - 3.0, outputs)
        outputs = tf.where((inputs >= 9.0) & (inputs < 20.0), 5.0, outputs)
        outputs = tf.where(inputs >= 20.0, inputs - 15.0, outputs)

        # Generate the mask based on the transformed inputs
        indices = tf.range(self.max_length, dtype=tf.float32)
        expanded_outputs = tf.expand_dims(outputs, -1)
        mask = indices >= expanded_outputs

        # Duplicate the mask
        duplicated = tf.reshape(tf.repeat(mask, repeats=2, axis=-1), shape=(-1, self.max_length * 2))
        duplicated = tf.cast(duplicated, dtype=tf.float32)
        return duplicated


@keras.utils.register_keras_serializable(name="mapping_to_target_range")
def mapping_to_target_range(x, target_min=0, target_max=5):
    return tf.keras.backend.clip(x, target_min, target_max)


@keras.utils.register_keras_serializable(name="rmse")
def rmse(y_true, y_pred):
    y_true = tf.cast(y_true, tf.float32)
    y_pred = tf.cast(y_pred, tf.float32)
    return keras.backend.sqrt(keras.backend.mean(keras.backend.square(y_pred - y_true)))


def compute_median(tensor):
    tensor = tf.reshape(tensor, [-1])  # Flatten the tensor
    sorted_tensor = tf.sort(tensor)  # Sort the tensor values

    def median_from_sorted(sorted_t):
        num_elements = tf.size(sorted_t, out_type=tf.int32)
        middle_idx = num_elements // 2
        return tf.cond(
            pred=tf.equal(num_elements % 2, 1),
            true_fn=lambda: sorted_t[middle_idx],
            false_fn=lambda: (sorted_t[middle_idx - 1] + sorted_t[middle_idx]) / 2.0
        )

    return median_from_sorted(sorted_tensor)


def median_tensor(x):
    # Ensuring the tensor is 1-D.
    x = tf.reshape(x, [-1])

    # Sorting the tensor.
    values = tf.sort(x, axis=-1)

    # Getting the middle index.
    middle = tf.shape(values)[0] // 2

    # Handling even and odd cases
    median_val = tf.cond(
        tf.equal(tf.math.mod(tf.shape(values)[0], 2), 0),
        lambda: (values[middle - 1] + values[middle]) / 2,
        lambda: values[middle]
    )

    return median_val


@keras.utils.register_keras_serializable(name="rmse_masked_nli")
def rmse_masked_nli(y_true, y_pred, nli_mask):

    y_true = tf.cast(y_true, tf.float32)
    y_pred = tf.cast(y_pred, tf.float32)

    y_true_masked = y_true * nli_mask
    y_pred_masked = y_pred * nli_mask

    sub = (y_pred_masked - y_true_masked)

    sq = keras.backend.square(sub)
    s = keras.backend.sum(sq, axis=1)

    mask_sum = keras.backend.sum(nli_mask, axis=1)
    mse = s / mask_sum

    root_per_sample = tf.where(mse > 0, tf.sqrt(mse), tf.zeros_like(mse))
    return keras.backend.mean(root_per_sample)


def rmse_below_nli(nli_mask):
    def c_loss(y_true, y_pred):
        return rmse_masked_nli(y_true, y_pred, nli_mask)

    return c_loss


class RMSEMaskedNLILossLayer(Layer):
    def __init__(self, **kwargs):
        super(RMSEMaskedNLILossLayer, self).__init__(**kwargs)

    def call(self, inputs):
        y_true, y_pred, nli_mask = inputs
        loss = rmse_masked_nli(y_true, y_pred, nli_mask)
        self.add_loss(loss)
        return loss

    #def compute_output_shape(self, input_shape):
    #    return (input_shape[0],)  # Return shape with batch dimension


def test_rmse_masked_nli():
    # tensor with random values of shape (3, 20)
    y_true = tf.random.normal(shape=(3, 20))
    y_pred = tf.random.normal(shape=(3, 20))

    mask_layer = ExpandedMaskLayer()
    inputs = tf.constant([[1 / 24, 21 / 24, 24 / 24]], dtype=tf.float32)
    nli_mask = mask_layer(inputs).numpy()
    print(nli_mask)

    print(rmse_masked_nli(y_true, y_pred, nli_mask).numpy())

    # calculate rmse
    print(rmse(y_true, y_pred).numpy())


@keras.utils.register_keras_serializable(name="rmse_masked")
def rmse_masked(y_true, y_pred_nli):
    y_true = tf.cast(y_true, tf.float32)
    y_pred_nli = tf.cast(y_pred_nli, tf.float32)

    # get the nli mask which is the back 20 values of the y_pred
    nli_mask = y_pred_nli[:, -20:]
    # y_pred is the first 20 values of the y_pred
    y_pred = y_pred_nli[:, :20]

    y_true_masked = y_true * nli_mask
    y_pred_masked = y_pred * nli_mask

    sub = y_pred_masked - y_true_masked
    sq = keras.backend.square(sub)
    s = keras.backend.sum(sq)
    mask_sum = keras.backend.sum(nli_mask)
    mse = s / mask_sum
    rmse = keras.backend.sqrt(mse)
    return rmse


@keras.utils.register_keras_serializable(name="MaskedRMSELoss")
class MaskedRMSELoss(keras.layers.Layer):
    def __init__(self, nli_mask, **kwargs):
        super().__init__(**kwargs)
        self.nli_mask = tf.cast(nli_mask, dtype=tf.float32)

    def call(self, y_true, y_pred):
        y_true_masked = y_true * self.nli_mask
        y_pred_masked = y_pred * self.nli_mask

        sub = y_pred_masked - y_true_masked
        sq = keras.backend.square(sub)
        s = keras.backend.sum(sq)

        mask_sum = keras.backend.sum(self.nli_mask)
        mse = s / mask_sum
        rmse = keras.backend.sqrt(mse)
        return rmse


def test_expanded_mask_layer():
    # Example of using the layer
    mask_layer = ExpandedMaskLayer()
    inputs = tf.constant(np.array([0, 4, 5, 6, 7, 8, 10, 20, 23, 24]) / 24, dtype=tf.float32)
    out = mask_layer(inputs).numpy()
    for i in range(10):
        print(inputs[i].numpy())
        print(out[i])


def get_loss_function(loss_str, num_outputs_ordinal=20):  # for extensibility
    if loss_str == rmse_str:
        return rmse
    else:
        print('Loss function not found, using RMSE')
        return rmse
