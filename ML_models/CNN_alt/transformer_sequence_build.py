from keras import layers
from keras.src import regularizers
from keras.src.layers import MultiHeadAttention, Input

from ML_models.CNN_alt.CNN_args import *
from ML_models.CNN_alt.architecture_blocks import get_output_layer
from ML_models.CNN_alt.block_input_preprocessing import *
from ML_models.CNN_alt.custom_models_hyperparameters import transformer_sequence_hyperparameters
from ML_models.CNN_alt.loss import *
from Miscellaneous.Misc.columns import *


# Custom 3D Positional Encoding
def get_positional_encoding(seq_length, d_model):
    position = np.arange(seq_length)[:, np.newaxis]
    div_term = np.exp(np.arange(0, d_model, 2) * -(np.log(10000.0) / d_model))
    pos_encoding = np.zeros((seq_length, d_model))
    pos_encoding[:, 0::2] = np.sin(position * div_term)
    pos_encoding[:, 1::2] = np.cos(position * div_term)
    return tf.constant(pos_encoding, dtype=tf.float32)


# Custom Embedding Layer for 2x3 matrices
@keras.utils.register_keras_serializable(name="MatrixEmbeddingLayer")
class MatrixEmbeddingLayer(layers.Layer):
    def __init__(self, embed_dim, **kwargs):
        super(MatrixEmbeddingLayer, self).__init__(**kwargs)
        self.embed_dim = embed_dim

    def build(self, input_shape):
        self.kernel = self.add_weight(
            shape=(input_shape[-2], input_shape[-1], self.embed_dim),
            initializer='uniform',
            name='kernel',
            trainable=True,
        )

    def call(self, inputs):
        # Embedding each 2x3 matrix
        return tf.tensordot(inputs, self.kernel, axes=[[2, 3], [0, 1]])


class CustomMultiHeadAttention(MultiHeadAttention):
    def call(self, query, value, **kwargs):
        outputs = super().call(query, value, **kwargs)
        attention_scores = self._attention_scores  # Accessing the attention scores from the base class
        return outputs, attention_scores


def transformer_block(inputs, embed_dim, num_heads, ff_dim, dropout=0, l2=0):
    attention_output = layers.MultiHeadAttention(num_heads=num_heads, key_dim=embed_dim)(inputs, inputs)
    attention_output = layers.LayerNormalization(epsilon=1e-6)(attention_output + inputs)
    ff_output = layers.Dense(ff_dim, activation="relu")(attention_output)
    ff_output = layers.Dense(embed_dim, activation='relu', kernel_regularizer=regularizers.L2(l2))(ff_output)

    return layers.LayerNormalization(epsilon=1e-6)(ff_output + attention_output)


def transformer_block_small(inputs, embed_dim, num_heads, dropout=0):
    attention_output = layers.MultiHeadAttention(num_heads=num_heads, key_dim=embed_dim)(inputs, inputs)
    attention_output = layers.Dropout(dropout)(attention_output)
    attention_output = layers.LayerNormalization(epsilon=1e-6)(attention_output + inputs)
    return attention_output


# Building the Model
def build_seq_transformer_model(embed_dim, num_heads, ff_dim, lr=1e-4, dropout=0, l2=0,
                                n_layers=1, minimal=False, loss_use=rmse_str, hp=None):
    if hp is not None:
        num_heads = hp.Int('num_heads', min_value=4, max_value=8, step=1)
        embed_dim = hp.Int('embed_dim', min_value=32, max_value=64, step=16)
        ff_dim = hp.Int('ff_dim', min_value=128, max_value=256, step=64)
        dropout = hp.Float('dropout', min_value=0.1, max_value=0.3, step=0.1)
        l2 = hp.Float('l2', min_value=0.001, max_value=0.01, step=0.001)
        n_layers = hp.Int('n_layers', min_value=1, max_value=3, step=1)

    ms_input = Input(shape=ms_dims)
    lt_pp_input = Input(shape=lt_pp_dims)
    tabular_input = Input(shape=tabular_dim)

    nli = tabular_input[:, 1]

    input_3d = get_3D_input_full_score_impute(ms_input, lt_pp_input, scaled_nli=nli)

    tabular = layers.Dense(64, activation="relu")(tabular_input)

    # Matrix Embedding
    x = MatrixEmbeddingLayer(embed_dim)(input_3d)
    seq_length = 28
    # Positional Encoding
    pos_encoding = get_positional_encoding(seq_length, embed_dim)
    x += pos_encoding

    # Transformer Block
    for n in range(n_layers):
        x = transformer_block(x, embed_dim, num_heads, ff_dim, dropout, l2)

    # Concatenation in Latent Space
    x = layers.GlobalAveragePooling1D()(x)
    concatenated = layers.Concatenate()([x, tabular])

    # Output Layer

    predictions = get_output_layer(concatenated, loss_use)

    nli_mask_layer = ExpandedMaskLayer()
    nli_output_masks = nli_mask_layer(nli)

    y_true = Input(shape=(output_size,))

    model = keras.Model(inputs=[ms_input, lt_pp_input, tabular_input, y_true], outputs=predictions)

    model.add_loss(rmse_masked_nli(y_true, predictions, nli_output_masks))
    model.compile(loss=None,
                  optimizer=keras.optimizers.Adam(learning_rate=lr),
                  metrics=[tf.keras.metrics.MeanSquaredError(name='mse')])
    return model


def build_seq_transformer_model_hp(hp, loss_use=rmse_str):
    num_heads, embed_dim, ff_dim, dropout, l2, n_layers, lr = transformer_sequence_hyperparameters(hp)

    ms_input = Input(shape=ms_dims)
    lt_pp_input = Input(shape=lt_pp_dims)
    tabular_input = Input(shape=tabular_dim)
    nli = tabular_input[:, 1]

    #input_3d = get_3D_input_full_score_impute(ms_input, lt_pp_input, scaled_nli=nli)
    input_3d = ImputeLayer3D()([ms_input, lt_pp_input, nli])

    tabular = layers.Dense(64, activation="relu")(tabular_input)

    # Matrix Embedding
    x = MatrixEmbeddingLayer(embed_dim)(input_3d)

    # Positional Encoding
    seq_length = 28
    pos_encoding = get_positional_encoding(seq_length, embed_dim)
    x += pos_encoding

    # Transformer Block

    for n in range(n_layers):
        x = transformer_block(x, embed_dim, num_heads, ff_dim, dropout, l2)

    # Concatenation in Latent Space
    x = layers.GlobalAveragePooling1D()(x)
    concatenated = layers.Concatenate()([x, tabular])

    # Output Layer

    predictions = get_output_layer(concatenated, loss_use)

    y_true = Input(shape=(output_size,))


    model = keras.Model(inputs=[ms_input, lt_pp_input, tabular_input, y_true], outputs=predictions)

    nli_mask_layer = ExpandedMaskLayer()
    nli_output_masks = nli_mask_layer(nli)

    RMSEMaskedNLILossLayer()([y_true, predictions, nli_output_masks])

    #model.add_loss(rmse_masked_nli(y_true, predictions, nli_output_masks))
    model.compile(loss=None,
                  optimizer=keras.optimizers.Adam(learning_rate=lr),
                  metrics=[tf.keras.metrics.MeanSquaredError(name='mse')],
                  )
    return model


def build_seq_transformer_model_v2_hp(hp, loss_use=rmse_str):
    num_heads, embed_dim, ff_dim, dropout, l2, n_layers, lr = transformer_sequence_hyperparameters(hp)

    ms_input = Input(shape=ms_dims)
    lt_pp_input = Input(shape=lt_pp_dims)
    tabular_input = Input(shape=tabular_dim)
    nli = tabular_input[:, 1]

    input_3d = get_3D_input_full_score_impute(ms_input, lt_pp_input, scaled_nli=nli)

    tabular = layers.Dense(64, activation="relu")(tabular_input)

    # Matrix Embedding
    x = MatrixEmbeddingLayer(embed_dim)(input_3d)

    # Positional Encoding
    seq_length = 28
    pos_encoding = get_positional_encoding(seq_length, embed_dim)
    x += pos_encoding

    # Transformer Block

    for n in range(n_layers):
        x = transformer_block_small(x, embed_dim, num_heads, dropout)
        # x = layers.MultiHeadAttention(num_heads=num_heads, key_dim=embed_dim)(x, x)

        # x = transformer_block(x, embed_dim, num_heads, ff_dim, dropout, l2)

    # Concatenation in Latent Space
    x = layers.GlobalAveragePooling1D()(x)

    concatenated = layers.Concatenate()([x, tabular])

    predictions = get_output_layer(concatenated, loss_use, l2=l2)

    y_true = Input(shape=(output_size,))

    model = keras.Model(inputs=[ms_input, lt_pp_input, tabular_input, y_true], outputs=predictions)

    nli_mask_layer = ExpandedMaskLayer()
    nli_output_masks = nli_mask_layer(nli)

    model.add_loss(rmse_masked_nli(y_true, predictions, nli_output_masks))
    model.compile(loss=None,
                  optimizer=keras.optimizers.Adam(learning_rate=lr),
                  metrics=tf.keras.metrics.MeanSquaredError(name='mse'),

                  )
    # model.summary()
    return model
