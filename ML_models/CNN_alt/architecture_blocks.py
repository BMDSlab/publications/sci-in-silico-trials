from keras import layers
from keras.src import regularizers

from ML_models.CNN_alt.loss import mapping_to_target_range


def get_output_layer(in_layer, loss_use, l2=None, name='output'):

    activation = mapping_to_target_range
    output_dim = 20
    if l2 is not None:
        outputs = layers.Dense(output_dim, activation=activation, kernel_regularizer=regularizers.l2(l2))(in_layer)
    else:
        outputs = layers.Dense(output_dim, activation=activation, name=name)(in_layer)

    return outputs
