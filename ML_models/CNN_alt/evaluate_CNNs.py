import gc
import pickle

from ML_models.CNN_alt.CNN_eval_utils import get_res_custom_model
from ML_models.CNN_alt.CNN_utils import *

from Miscellaneous.Misc.paths import *
from Miscellaneous.Misc.splits import n_splits
from Run_collection.Run_prediction.SCIRecoveryPrediction_no_shape import get_description_dict_metrics


def evaluate_custom_model_by_path(model_type, parameters, with_uncertainty=False,
                                  run_folder=hp_run_folder,
                                  augmented=True, dataset=emsci_str, augmented_model=False, clip_output=True,
                                  date=standard_file_date):
    print('Dataset custom model by path:', dataset)
    models = {}
    modelID = get_model_ID_alt(parameters)

    output = get_model_dir(model=model_type, with_uncertainty=augmented_model,
                           clip_output=clip_output)
    for fold_index in range(n_splits):
        model_name = get_custom_model_name(modelID, fold_index, 'all')
        filepath_model = get_file_path_custom_model(output, model_name, run_folder)
        print(filepath_model)
        models[fold_index] = load_custom_model_path(filepath_model)
    print('Loaded models')

    evaluate_custom_model_mean_per_fold(models, output, bootstrap=with_uncertainty,
                                        parameters=parameters, augmented=augmented, dataset=dataset, date=date)


def evaluate_custom_model_mean_per_fold(models, output, bootstrap=False, parameters=None, augmented=True,
                                        dataset=emsci_str, date=standard_file_date):
    print('Dataset per fold:', dataset)
    res_dict = {}
    res_dict_augmented = {}

    for fold_index in tqdm(range(n_splits)):
        print('Evaluation', fold_index)

        data_path = get_data_dict_splits_CNN_format_path(fold_index, bootstrap=bootstrap, scaled=True, dataset=dataset,
                                                         date=date)
        input_path = open(data_path, 'rb')
        datasets = pickle.load(input_path)

        predictions = get_res_custom_model(models[fold_index], datasets, update_parameters=parameters)
        res_dict[fold_index] = predictions

        del datasets
        gc.collect()
        if augmented:
            print('Augmented evaluation', fold_index)

            data_path_augmented = get_data_dict_splits_CNN_format_path(fold_index, bootstrap=True, scaled=True,
                                                                       augmented_test_set=True)
            datasets_augmented = pickle.load(open(data_path_augmented, 'rb'))

            predictions_augmented = get_res_custom_model(models[fold_index], datasets_augmented,

                                                         augmented_evaluation=True, update_parameters=parameters)
            res_dict_augmented[fold_index] = predictions_augmented

    if dataset == emsci_str:
        res_dict = {0: pd.concat(res_dict)}

    desc_dict = get_description_dict_metrics(res_dict, metrics, output, dataset=dataset, date=date)
    path = get_desc_dict_path(output, dataset)
    print(path)
    pickle.dump(desc_dict, open(path, 'wb'))

    if augmented:
        res_dict_augmented = {0: pd.concat(res_dict_augmented)}
        desc_dict_augmented = get_description_dict_metrics(res_dict_augmented, metrics, output,
                                                           augmented_evaluation=True, dataset=dataset, date=date)
        pickle.dump(desc_dict_augmented, open(get_desc_dict_path(output, augmented_evaluation=True), 'wb'))

    median_res = []
    for i in range(len(desc_dict['rmse'])):
        rmse_res = desc_dict['rmse'][i]
        median_res.append(np.median(rmse_res))
        print('Fold', i, 'results:', np.median(rmse_res), np.std(rmse_res))
    print('Mean:', np.mean(median_res))
