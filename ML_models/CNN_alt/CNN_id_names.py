from ML_models.CNN_alt.model_name_from_parameters import get_model_from_parameters
from Miscellaneous.Misc.columns import *
from Miscellaneous.Misc.model_names import custom_model_names



def get_custom_model_name(modelID, c, tsc='all'):
    return modelID + '_cv' + str(c) + '_run' + str(0) + '_' + tsc


def get_model_ID_alt(parameters, skip_keys=[]):
    model_type = get_model_from_parameters(parameters)
    modelID = model_type
    for k in parameters.keys():
        if k in skip_keys:
            continue
        if k in custom_model_names:
            modelID += '_' + k
        if k in [with_uncertainty_str] and parameters[k] is True:
            modelID += '_with_uncertainty'
    return ''

