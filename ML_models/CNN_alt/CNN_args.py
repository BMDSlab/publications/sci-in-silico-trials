
ms_dims = (10, 2)
lt_pp_dims = (28, 2, 2)
tabular_dim = (10, )
output_size = 20
lt_dims = (28, 2)
pp_dims = (28, 2)

usemultiTP = False
tp1b = 4
tp1 = 2
tp2 = 26
if usemultiTP:
    dim = (28, 6)
else:
    dim = (28, 3)

useAugmentation=False

n_bootstrap = 1  # 50

targetscores = ['T1']  # 'C5', 'C6', 'C7', 'C8', ,'L2', 'L3', 'L4', 'L5', 'S1']

partition_name = 'partition_KMeans_withCC_tp1_' + str(tp1) + '_tp2_' + str(tp2) + '_cv'
name_pre = 'CNN_'

