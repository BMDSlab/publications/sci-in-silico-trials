import pickle

from keras_tuner import Hyperband
import keras_tuner as kt

from ML_models.CNN_alt.custom_data_generator import get_dataloader
from Miscellaneous.Misc.columns import *
from Miscellaneous.Misc.paths import get_data_dict_splits_CNN_format_path


class MyHyperModel(kt.HyperModel):

    def __init__(self, build_model, parameters, dim):
        super().__init__()
        self.build_model = build_model
        self.parameters = parameters
        self.dim = dim

        scaled = True
        data_path = get_data_dict_splits_CNN_format_path(0, bootstrap=parameters[with_uncertainty_str],
                                                         scaled=scaled)
        input_path = open(data_path, 'rb')
        print('Loading data from file')
        data = pickle.load(input_path)

        self.trainIDs, self.validationIDs, self.testIDs, self.X_use, self.Y_use = data['trainIDs'], data['validationIDs'], data[
            'testIDs'], data['X_use'], data['Y_use']

        self.MS, self.LT, self.PP, self.Xadd = (data['X_use']['MS'], data['X_use']['LT'],
                            data['X_use']['PP'], data['X_use']['X_add'])


    def build(self, hp):
        return self.build_model(hp)

    def fit(self, hp, model, *args, **kwargs):
        batch_size = hp.Choice(bs_str, values=[32]) #, 64, 128, 200, 256]) set to 32 for testing, change back for real runs
        self.parameters[bs_str] = batch_size
        training_generator, validation_generator, _ = get_dataloader(self.dim, self.parameters, self.trainIDs,
                                                                    self.validationIDs, self.testIDs, self.MS, self.LT,
                                                                    self.PP,
                                                                    self.Xadd,
                                                                    self.Y_use)

        return model.fit(x=training_generator, validation_data=validation_generator, *args, **kwargs)


def get_hyperband_tuner(hypermodel, max_epochs, debug, parameters, modelID, hyperband_dir):
    if debug:
        max_epochs = 1
        hyperband_iterations = 1
        tuner_id = 'debug'
    else:
        hyperband_iterations = 2
        tuner_id = modelID

    tuner = Hyperband(
        hypermodel,
        objective='val_loss',
        max_epochs=max_epochs,
        factor=3,
        hyperband_iterations=hyperband_iterations,
        seed=54321,
        directory=hyperband_dir,
        project_name='EMSCI',
        tuner_id=tuner_id,
        overwrite=parameters[new_hp_str]
    )
    return tuner
