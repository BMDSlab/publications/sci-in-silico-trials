import tensorflow as tf

import numpy as np
from Miscellaneous.Misc.fields_to_match import max_indNLI, dermatomes_withC1


def get_3D_input_full_zero_impute(matrix_input1, matrix_input2):
    padded_matrix_input1 = tf.pad(matrix_input1, [[0, 0], [0, 2], [0, 0]])

    # Create tensors for zeros
    zeros_before = tf.zeros_like(matrix_input1[:, :3, :])
    zeros_middle = tf.zeros_like(padded_matrix_input1[:, :12, :])
    zeros_after = tf.zeros_like(matrix_input1[:, :3, :])

    # Concatenate all tensors
    matrix_input1_padded = tf.concat(
        [zeros_before, matrix_input1[:, :5, :], zeros_middle, matrix_input1[:, 5:, :], zeros_after], axis=1)

    matrix_input1_padded = tf.expand_dims(matrix_input1_padded, axis=-1)

    # Put matrix_input1_padded on top of matrix_input2
    input_3d = tf.concat([matrix_input1_padded, matrix_input2], axis=-1)
    return input_3d


def pad_motor_score(motor_scores, scaled_nli):
    nli = scaled_nli * max_indNLI

    fives_before = tf.ones_like(motor_scores[:, :3, :])
    fives_before_middle_above_nli = tf.ones_like(motor_scores[:, 4, :])

    impute_grade_middle_below_nli = motor_scores[:, 5, :]
    impute_grade_end = motor_scores[:, -1, :]

    nli_adjusted = nli - 8

    length_above = tf.clip_by_value(nli_adjusted, 0, 12)

    fives_before_middle_above_nli = tf.expand_dims(fives_before_middle_above_nli, axis=1)
    impute_grade_middle_below_nli = tf.expand_dims(impute_grade_middle_below_nli, axis=1)

    all_mid_fills = create_combined_tensor(length_above, fives_before_middle_above_nli, impute_grade_middle_below_nli)

    impute_grade_end = tf.expand_dims(impute_grade_end, axis=1)
    same_after = tf.tile(impute_grade_end, multiples=[1, 3, 1])

    # Concatenate all tensors
    matrix_input1_padded = tf.concat(
        [fives_before,
         motor_scores[:, :5, :],
         all_mid_fills,
         motor_scores[:, 5:, :],
         same_after], axis=1)

    matrix_input1_padded = tf.reshape(matrix_input1_padded, (-1, 56, 1))
    return matrix_input1_padded

class ImputeLayer3D(tf.keras.layers.Layer):
    def __init__(self, **kwargs):
        super(ImputeLayer3D, self).__init__(**kwargs)

    def call(self, inputs):
        ms_input, lt_pp_input, scaled_nli = inputs

        return get_3D_input_full_score_impute(ms_input, lt_pp_input, scaled_nli)


def get_3D_input_full_score_impute(ms_input, lt_pp_input, scaled_nli):
    nli = scaled_nli * max_indNLI

    fives_before = tf.ones_like(ms_input[:, :3, :])
    fives_before_middle_above_nli = tf.ones_like(ms_input[:, 4, :])

    impute_grade_middle_below_nli = ms_input[:, 5, :]
    impute_grade_end = ms_input[:, -1, :]

    nli_adjusted = nli - 8

    length_above = tf.clip_by_value(nli_adjusted, 0, 12)

    fives_before_middle_above_nli = tf.expand_dims(fives_before_middle_above_nli, axis=1)
    impute_grade_middle_below_nli = tf.expand_dims(impute_grade_middle_below_nli, axis=1)

    all_mid_fills = create_combined_tensor(length_above, fives_before_middle_above_nli, impute_grade_middle_below_nli)

    impute_grade_end = tf.expand_dims(impute_grade_end, axis=1)
    same_after = tf.tile(impute_grade_end, multiples=[1, 3, 1])

    # Concatenate all tensors
    matrix_input1_padded = tf.concat(
        [fives_before,
         ms_input[:, :5, :],
         all_mid_fills,
         ms_input[:, 5:, :],
         same_after], axis=1)

    matrix_input1_padded = tf.expand_dims(matrix_input1_padded, axis=-1)

    # Put matrix_input1_padded on top of matrix_input2
    input_3d = tf.concat([matrix_input1_padded, lt_pp_input], axis=-1)
    return input_3d


def create_combined_tensor(length_above, impute_grade_middle_above_nli, impute_grade_middle_below_nli):
    max_length = 12

    # Ensure length_above is an integer tensor
    length_above = tf.cast(length_above, tf.int32)

    # Create range tensor and masks
    range_tensor = tf.range(max_length, dtype=tf.int32)
    length_above_matrix = tf.tile(tf.expand_dims(length_above, -1), [1, max_length])
    mask_above = tf.less(range_tensor, length_above_matrix)
    mask_below = tf.logical_not(mask_above)

    # Replicate tensors
    # Ensure the shape is correct for tf.tile by specifying the multiples correctly
    replicated_above = tf.tile(impute_grade_middle_above_nli, [1, max_length, 1])
    replicated_below = tf.tile(impute_grade_middle_below_nli, [1, max_length, 1])

    # Apply masks
    selected_above = tf.where(tf.expand_dims(mask_above, -1), replicated_above, tf.zeros_like(replicated_above))
    selected_below = tf.where(tf.expand_dims(mask_below, -1), replicated_below, tf.zeros_like(replicated_below))

    # Combine the above and below parts
    combined = selected_above + selected_below

    return combined


def test_full_score_impute():
    tf.executing_eagerly()
    # Matrix input 1 should be 10, 2 and matrix input 2 should be 28, 2, 2 for this test, values should be from 9 to 0 on the first matrix and 27 to 0 on the second matrix
    batch_size = 3
    m1 = tf.constant(np.arange(20).reshape(10, 2), dtype=tf.float32)
    m1 = tf.expand_dims(m1, axis=0)

    m2 = tf.constant(np.arange(112).reshape(28, 2, 2), dtype=tf.float32)
    m2 = tf.expand_dims(m2, axis=0)

    matrix_input1 = tf.tile(m1, multiples=[batch_size, 1, 1])
    matrix_input2 = tf.tile(m2, multiples=[batch_size, 1, 1, 1])
    # nli is batch size with values 0.5

    nlis = [0.50, 0.75, 0.3]
    print([dermatomes_withC1[round(nli * max_indNLI)] for nli in nlis])
    print(np.array(nlis) * max_indNLI)
    output = get_3D_input_full_score_impute(matrix_input1, matrix_input2,
                                            scaled_nli=tf.constant(nlis, dtype=tf.float32))
    print(output)



