from ML_models.CNN_alt.CNN_build import build_CNNflexi_Modular1D_alt_hp, build_CNNflexi_Modular1D_alt_ensemble_hp
from ML_models.CNN_alt.CNN_sequence_build import build_CNN_seq_model_hp
from ML_models.CNN_alt.transformer_build import build_transformer_model_hp
from ML_models.CNN_alt.transformer_sequence_build import build_seq_transformer_model_hp
from Miscellaneous.Misc.columns import *

import tensorflow as tf

from functools import partial


def get_training_conditions(parameters):
    model_type = CNN_str
    if transformer_str in parameters and parameters[transformer_str]:
        model_type = transformer_str

    if ensemble_str in parameters and parameters[ensemble_str]:
        model_type = ensemble_str

    if only_MS_str in parameters and parameters[only_MS_str]:
        run_only_MS = True
    else:
        run_only_MS = False

    if transformer_sequence_str in parameters and parameters[transformer_sequence_str]:
        model_type = transformer_sequence_str

    if CNN_sequence_str in parameters and parameters[CNN_sequence_str]:
        model_type = CNN_sequence_str

    if GNN_str in parameters and parameters[GNN_str]:
        model_type = GNN_str

    if GNN2_str in parameters and parameters[GNN2_str]:
        model_type = GNN2_str

    if TF2_str in parameters and parameters[TF2_str]:
        model_type = TF2_str

    if TF_seq2_str in parameters and parameters[TF_seq2_str]:
        model_type = TF_seq2_str

    if ordinal_str in parameters and parameters[ordinal_str]:
        loss_use = ordinal_str
        is_ordinal = True

        # Move out of ordinal statement eventually
    else:
        loss_use = rmse_str
        is_ordinal = False

    return model_type, run_only_MS, loss_use, is_ordinal


def get_build_model_function(model_type, loss_use=rmse_str):
    if model_type == CNN_str:
        build_model = build_CNNflexi_Modular1D_alt_hp
    elif model_type == transformer_str:
        build_model = build_transformer_model_hp
    elif model_type == transformer_sequence_str:
        build_model = partial(build_seq_transformer_model_hp, loss_use=loss_use)

    elif model_type == CNN_sequence_str:
        build_model = partial(build_CNN_seq_model_hp, loss_use=loss_use)
    elif model_type == ensemble_str:
        build_model = build_CNNflexi_Modular1D_alt_ensemble_hp
    else:
        build_model = build_CNNflexi_Modular1D_alt_hp
    return build_model

# def tensorflow_config():
#     config = tf.compat.v1.ConfigProto()
#     config.gpu_options.allow_growth = True
#     tf.compat.v1.keras.backend.set_session(tf.compat.v1.Session(config=config))

def tensorflow_config():
    gpus = tf.config.experimental.list_physical_devices('GPU')
    if gpus:
        try:
            for gpu in gpus:
                tf.config.experimental.set_memory_growth(gpu, True)
            logical_gpus = tf.config.experimental.list_logical_devices('GPU')
            print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
        except RuntimeError as e:
            # Memory growth must be set before GPUs have been initialized
            print(e)