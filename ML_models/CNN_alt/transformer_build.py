from tensorflow.keras import layers, models, regularizers, Input
import keras

from ML_models.CNN_alt.CNN_args import *
from ML_models.CNN_alt.custom_models_hyperparameters import transformer_hyperparameters
from ML_models.CNN_alt.loss import *


def transformer_encoder(inputs, head_size, num_heads, ff_dim, dropout=0, l2=0):
    # Attention and Normalization
    x = layers.MultiHeadAttention(key_dim=head_size, num_heads=num_heads, dropout=dropout)(inputs, inputs)
    x = layers.Dropout(dropout)(x)

    # Feed Forward Part
    ff = layers.Dense(ff_dim, activation="relu")(x)
    ff = layers.Dropout(dropout)(ff)
    ff = layers.Dense(inputs.shape[-1], kernel_regularizer=regularizers.l2(l2))(ff)
    return ff


def build_transformer_model(head_size, num_heads, ff_dim, num_transformer_blocks, learning_rate, dropout=0, l2=0,
                            only_MS=False):
    if only_MS:
        matrix_input1 = layers.Input(shape=ms_dims)
        matrix_input2 = layers.Input(shape=lt_pp_dims)
        tabular_input = layers.Input(shape=tabular_dim)
        x1 = matrix_input1
        for _ in range(num_transformer_blocks):
            x1 = transformer_encoder(x1, head_size, num_heads, ff_dim, dropout, l2)
        x1 = layers.Flatten()(x1)
        output = layers.Dense(output_size, activation=mapping_to_target_range)(x1)
        model = models.Model(inputs=[matrix_input1, matrix_input2, tabular_input], outputs=output)
        model.compile(loss=rmse,
                      optimizer=keras.optimizers.Adam(learning_rate=learning_rate),
                      metrics=[tf.keras.metrics.MeanSquaredError(name='mse')])
        return model

    # Input Layers
    matrix_input1 = layers.Input(shape=ms_dims)
    matrix_input2 = layers.Input(shape=lt_pp_dims)
    tabular_input = layers.Input(shape=tabular_dim)

    # Matrix Pathways
    x1 = matrix_input1
    x2 = matrix_input2
    for _ in range(num_transformer_blocks):
        x1 = transformer_encoder(x1, head_size, num_heads, ff_dim, dropout, l2)
        x2 = transformer_encoder(x2, head_size, num_heads, ff_dim, dropout, l2)

    x1 = layers.Flatten()(x1)
    x2 = layers.Flatten()(x2)

    x1 = layers.Dense(ff_dim, activation="relu", kernel_regularizer=regularizers.l2(l2))(x1)
    x1 = layers.Dropout(dropout)(x1)

    x2 = layers.Dense(ff_dim, activation="relu", kernel_regularizer=regularizers.l2(l2))(x2)
    x2 = layers.Dropout(dropout)(x2)

    # Tabular Pathway
    tabular_x = layers.Dense(ff_dim, activation="relu", kernel_regularizer=regularizers.l2(l2))(tabular_input)
    tabular_x = layers.Dropout(dropout)(tabular_x)

    # Combine Pathways
    combined = layers.concatenate([x1, x2, tabular_x])

    # Additional Processing
    combined = layers.Dense(ff_dim, activation="relu", kernel_regularizer=regularizers.l2(l2))(combined)
    combined = layers.Dropout(dropout)(combined)

    # Output Layer - Modify according to your task
    output = layers.Dense(output_size, activation=mapping_to_target_range)(combined)

    # Build and Compile Model
    model = models.Model(inputs=[matrix_input1, matrix_input2, tabular_input], outputs=output)

    model.compile(loss=rmse,
                  optimizer=keras.optimizers.Adam(learning_rate=learning_rate),
                  metrics=[tf.keras.metrics.MeanSquaredError(name='mse')])
    return model


def build_transformer_model_hp(hp):
    head_size, num_heads, ff_dim, num_transformer_blocks, learning_rate, dropout, l2 = transformer_hyperparameters(hp)
    # Input Layers
    ms_input = Input(shape=ms_dims)
    lt_pp_input = Input(shape=lt_pp_dims)
    tabular_input = Input(shape=tabular_dim)

    # Matrix Pathways
    x1 = ms_input
    x2 = lt_pp_input
    for _ in range(num_transformer_blocks):
        x1 = transformer_encoder(x1, head_size, num_heads, ff_dim, dropout, l2)
        x2 = transformer_encoder(x2, head_size, num_heads, ff_dim, dropout, l2)

    x1 = layers.Flatten()(x1)
    x2 = layers.Flatten()(x2)

    x1 = layers.Dense(ff_dim, activation="relu", kernel_regularizer=regularizers.l2(l2))(x1)
    x1 = layers.Dropout(dropout)(x1)

    x2 = layers.Dense(ff_dim, activation="relu", kernel_regularizer=regularizers.l2(l2))(x2)
    x2 = layers.Dropout(dropout)(x2)

    # Tabular Pathway
    tabular_x = layers.Dense(ff_dim, activation="relu", kernel_regularizer=regularizers.l2(l2))(tabular_input)
    tabular_x = layers.Dropout(dropout)(tabular_x)

    # Combine Pathways
    combined = layers.concatenate([x1, x2, tabular_x])

    # Additional Processing
    combined = layers.Dense(ff_dim, activation="relu", kernel_regularizer=regularizers.l2(l2))(combined)
    combined = layers.Dropout(dropout)(combined)

    # Output Layer - Modify according to your task
    predictions = layers.Dense(output_size, activation=mapping_to_target_range)(combined)

    nli = tabular_input[:, 1]
    nli_mask_layer = ExpandedMaskLayer()
    nli_output_masks = nli_mask_layer(nli)

    y_true = Input(shape=(output_size,))
    # Build and Compile Model

    model = models.Model(inputs=[ms_input, lt_pp_input, tabular_input, y_true], outputs=predictions)

    RMSEMaskedNLILossLayer()([y_true, predictions, nli_output_masks])
    #model.add_loss(rmse_masked_nli(y_true, predictions, nli_output_masks))

    model.compile(loss=None,
                  optimizer=keras.optimizers.Adam(learning_rate=learning_rate),
                  metrics=[tf.keras.metrics.MeanSquaredError(name='mse')])

    return model


def build_transformer_model_v2_hp(hp):
    head_size, num_heads, ff_dim, num_transformer_blocks, learning_rate, dropout, l2 = transformer_hyperparameters(hp)
    # Input Layers
    ms_input = Input(shape=ms_dims)
    lt_pp_input = Input(shape=lt_pp_dims)
    tabular_input = Input(shape=tabular_dim)

    # Matrix Pathways
    x1 = ms_input
    x2 = lt_pp_input
    for _ in range(num_transformer_blocks):
        x1 = layers.MultiHeadAttention(key_dim=head_size, num_heads=num_heads, dropout=dropout)(x1, x1)
        x1 = layers.Dropout(dropout)(x1)
        x2 = layers.MultiHeadAttention(key_dim=head_size, num_heads=num_heads, dropout=dropout)(x2, x2)
        x2 = layers.Dropout(dropout)(x2)

    x1 = layers.Flatten()(x1)
    x2 = layers.Flatten()(x2)

    # Tabular Pathway
    tabular_x = layers.Dense(ff_dim, activation="relu", kernel_regularizer=regularizers.l2(l2))(tabular_input)
    tabular_x = layers.Dropout(dropout)(tabular_x)

    # Combine Pathways
    combined = layers.concatenate([x1, x2, tabular_x])

    # Additional Processing
    combined = layers.Dense(ff_dim, activation="relu", kernel_regularizer=regularizers.l2(l2))(combined)
    combined = layers.Dropout(dropout)(combined)

    # Output Layer - Modify according to your task
    predictions = layers.Dense(output_size, activation=mapping_to_target_range)(combined)

    nli = tabular_input[:, 1]
    nli_mask_layer = ExpandedMaskLayer()
    nli_output_masks = nli_mask_layer(nli)

    y_true = Input(shape=(output_size,))
    # Build and Compile Model
    model = models.Model(inputs=[ms_input, lt_pp_input, tabular_input, y_true], outputs=predictions)

    model.add_loss(rmse_masked_nli(y_true, predictions, nli_output_masks))
    model.compile(loss=None,
                  optimizer=keras.optimizers.Adam(learning_rate=learning_rate),
                  metrics=[tf.keras.metrics.MeanSquaredError(name='mse')])

    return model
