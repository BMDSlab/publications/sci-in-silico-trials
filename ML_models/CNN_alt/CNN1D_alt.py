# import pickle

# from keras_tuner import Hyperband
# import keras_tuner as kt

# from keras.src.callbacks import ReduceLROnPlateau

from ML_models.CNN_alt.CNN_eval_utils import eval_CNN_model
# from tqdm.keras import TqdmCallback

from ML_models.CNN_alt.CNN_id_names import get_custom_model_name, get_model_ID_alt

from ML_models.CNN_alt.custom_data_generator import get_data_generators
from ML_models.CNN_alt.custom_model_preconditions import get_training_conditions, get_build_model_function, \
    tensorflow_config
from ML_models.CNN_alt.hyperband_optimization import *

from ML_models.CNN_alt.transformer_sequence_build import *
from Miscellaneous.Misc.paths import *

from Miscellaneous.Plotting.printing import TQDMProgressBar

gpu_available = tf.config.list_physical_devices('GPU')

print("Available GPU's", gpu_available)


def train_custom_model(parameters, cvs=tuple(range(0, 5)), run_folder=hp_run_folder, debug=False):
    np.random.seed(0)
    tensorflow_config()
    objective = 'val_loss'

    model_type, run_only_MS, loss_use, is_ordinal = get_training_conditions(parameters)

    dim = parameters['sample_shape'][1:]

    modelID = get_model_ID_alt(parameters)

    output = get_model_dir(parameters[with_uncertainty_str], model=model_type, ordinal=is_ordinal)
    print(output)
    skip_hp = parameters[skip_hp_str]

    model_n = get_model_name(model=model_type, with_uncertainty=False, ordinal=is_ordinal) # Don't run hyperparameter optimization for the uncertainty model
    print(model_n)

    hyperband_dir = 'hyperbands/hyperband_' + model_n
    print(hyperband_dir)

    build_model = get_build_model_function(model_type, loss_use)

    hypermodel = MyHyperModel(build_model, parameters, dim)

    max_epochs = 2000

    tuner = get_hyperband_tuner(hypermodel, max_epochs, debug, parameters, modelID, hyperband_dir)
    #
    if not skip_hp:
        tuner.search_space_summary()
        tuner.search(
            epochs=max_epochs,
            callbacks=[tf.keras.callbacks.EarlyStopping('val_loss', patience=200)],
        )

    # Get the best hyperparameters
    best_hps = tuner.get_best_hyperparameters(num_trials=1)[0]
    print('Best hyperparameters: ')
    print(best_hps.values)
    parameters[bs_str] = best_hps.values[bs_str]

    # Create folders if needed
    Path(output).mkdir(parents=True, exist_ok=True)

    for fold_index in cvs:
        # if skip_hp:
        #     model = get_model_non_hp(model_type, parameters, loss_use)
        # else:
        #     model = tuner.hypermodel.build(best_hps)
        model = tuner.hypermodel.build(best_hps)
        model.summary()
        model_name = get_custom_model_name(modelID, fold_index, 'all')
        print('Working on model ' + model_name, 'all')
        filepath_model = get_file_path_custom_model(output, model_name, run_folder)
        print(filepath_model)

        training_generator, validation_generator, test_generator = get_data_generators(parameters, dim, fold_index)

        filepath = os.path.join(os.path.join(output, run_folder), model_name + '_run.keras')

        folders = ['run_distance', 'run_eval', 'run_hdf5', 'run_losses', 'run_overviews', 'run_parameters',
                   'run_predictions', 'run_testtube', 'run_partitions', 'figures']
        for f in folders:
            Path(os.path.join(output, f)).mkdir(parents=True, exist_ok=True)
        Path(os.path.join(output, run_folder)).mkdir(parents=True, exist_ok=True)

        Path.mkdir(Path(os.path.join(output, 'model_plots')), parents=True, exist_ok=True)

        #  Early stopping and check points
        check = tf.keras.callbacks.ModelCheckpoint(filepath, monitor='val_loss', save_best_only=True, mode='auto')
        earlyStopping = tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=parameters['pat'], mode='auto')
        # out_batch = NBatchLogger(display=20)
        # reduce_lr = ReduceLROnPlateau(monitor='val_loss', factor=0.1,
        #                              patience=5, min_lr=0.000001)
        tqdm_callback = TQDMProgressBar(total_epochs=parameters['n_epochs'])
        callbacks = [check, earlyStopping, tqdm_callback]
        # Fit
        history = model.fit(training_generator,
                            validation_data=validation_generator,
                            batch_size=parameters['bs'],
                            epochs=parameters['n_epochs'],
                            verbose=0,
                            callbacks=callbacks,
                            # class_weight=class_weight_dict,
                            shuffle=True
                            )

        print('Lowest validation loss: ', np.min(history.history['val_loss']))
        # Save best model

        model.save(filepath_model)

        validationIDs = validation_generator.list_IDs
        testIDs = test_generator.list_IDs
        df_real, df_real_val = eval_CNN_model(model, filepath_model, output, model_name, validationIDs, testIDs,
                                              validation_generator, test_generator, history)

        df_real.to_csv(
            os.path.join(output, 'run_predictions',
                         modelID + '_cv' + str(fold_index) + '_run' + str(0) + '_real.csv'))
        df_real_val.to_csv(
            os.path.join(output, 'run_predictions',
                         modelID + '_cv' + str(fold_index) + '_run' + str(0) + '_real_val.csv'))



    print("FINISHED!!")