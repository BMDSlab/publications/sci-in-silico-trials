import os

import numpy as np
from matplotlib import pyplot as plt


def plot_trainingProcess(history, output, name, counter):
    nameUse = 'loss_epoch_' + name + '.png'
    filename = os.path.join(output, nameUse)
    plt.figure()
    plt.plot(history.history['loss'])
    try:
        plt.plot(history.history['val_loss'])
    except KeyError:
        pass
    plt.grid()
    y = history.history['loss']
    plt.yticks(np.arange(min(y), max(y) + 1, 1.0))

    plt.title('Model loss')
    plt.ylabel('Loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'validation'], loc='upper left')
    plt.savefig(filename)
    print('Saved loss plot', filename)
    plt.close('all')

    plt.close('all')

    try:
        nameUse = 'acc_epoch_' + name + '.png'
        filename = os.path.join(output, nameUse)
        plt.figure()
        plt.plot(history.history['accuracy'])
        plt.plot(history.history['val_accuracy'])
        plt.title('Model accuracy')
        plt.ylabel('Accuracy')
        plt.xlabel('epoch')
        plt.legend(['train', 'validation'], loc='upper left')
        plt.savefig(filename)
        plt.close('all')
    except:
        pass
        print('No accuracy')

    try:
        nameUse = 'auc_epoch_' + name + '.png'
        filename = os.path.join(output, nameUse)
        plt.figure()
        try:
            plt.plot(history.history['auc'])
            plt.plot(history.history['val_auc'])
        except:
            plt.plot(history.history['auc_' + str(counter)])
            plt.plot(history.history['val_auc_' + str(counter)])
        plt.title('Model auc')
        plt.ylabel('AUC')
        plt.xlabel('epoch')
        plt.legend(['train', 'validation'], loc='upper left')
        plt.savefig(filename)

        plt.close('all')
    except:
        pass
        print('No auc')

    return 0