from ML_models.CNN_alt.CNN_build import CustomMaskedLayer

from ML_models.CNN_alt.loss import *
from ML_models.CNN_alt.CNN_weighting_layer import WeightedAverageLayer
from ML_models.CNN_alt.transformer_sequence_build import MatrixEmbeddingLayer


def load_custom_model_path(filepath_model, compile_model=True):
    with keras.utils.custom_object_scope(
            {'CustomMaskedLayer': CustomMaskedLayer, 'Custom>mapping_to_target_range': mapping_to_target_range,
             'rmse': rmse, 'Custom>rmse_masked_nli': rmse_masked_nli, 'Custom>ExpandedMaskLayer': ExpandedMaskLayer,
             'WeightedAverageLayer': WeightedAverageLayer, 'MatrixEmbeddingLayer': MatrixEmbeddingLayer,
             }
    ):
        model = keras.models.load_model(filepath_model, compile=compile_model)
    return model
