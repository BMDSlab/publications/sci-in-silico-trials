def cnn_hyperparameters(hp):
    params = {}
    params['l2'] = hp.Float('l2', min_value=0.0, max_value=0.002, step=0.001)
    params['l1_act'] = params['l2']
    params['n_layers'] = hp.Int('n_layers', min_value=1, max_value=2, step=1)
    params['n_filters_1'] = hp.Int('n_filters_1', min_value=4, max_value=16, step=4)
    params['kernel_size'] = hp.Int('kernel_size', min_value=3, max_value=7, step=2)
    params['do'] = hp.Float('do', min_value=0.0, max_value=0.2, step=0.1)
    params['useDO'] = True
    params['BNloc'] = 1
    params['sample_shape'] = (20, 2)
    params['clf'] = 'gap'

    params['lr'] = hp.Choice('lr', values=[1e-5, 1e-4, 1e-3])

    return params


def ensemble_hyperparameters(hp):
    params = {}
    params['l2'] = hp.Float('l2', min_value=0.0, max_value=0.002, step=0.001)
    params['l1_act'] = params['l2']
    params['n_layers'] = hp.Int('n_layers', min_value=1, max_value=2, step=1)
    params['n_filters_1'] = hp.Int('n_filters_1', min_value=4, max_value=16, step=4)
    params['kernel_size'] = hp.Int('kernel_size', min_value=3, max_value=7,
                                   step=2)
    params['useDO'] = True
    params['do'] = hp.Float('do', min_value=0.1, max_value=0.5, step=0.1)
    params['BNloc'] = 1
    params['sample_shape'] = (20, 1)
    params['clf'] = 'gap'
    params['lr'] = hp.Choice('lr', values=[1e-5, 1e-4, 1e-3])
    return params


def transformer_hyperparameters(hp):

    head_size = hp.Choice('head_size', values=[64, 128, 256, 512], ordered=True)
    num_heads = hp.Int('num_heads', min_value=1, max_value=3, step=1)
    ff_dim = hp.Choice('ff_dim', values=[64, 128, 256], ordered=True)
    num_transformer_blocks = hp.Int('num_transformer_blocks', min_value=1, max_value=3, step=1)
    learning_rate = hp.Choice('learning_rate', values=[1e-2, 1e-3, 1e-4])
    dropout = hp.Choice('dropout', values=[0.0, 0.1, 0.2], ordered=True)
    l2 = hp.Choice('l2', values=[0.0, 0.0001, 0.001], ordered=True)
    return head_size, num_heads, ff_dim, num_transformer_blocks, learning_rate, dropout, l2


def transformer_sequence_hyperparameters(hp):
    num_heads = hp.Int('num_heads', min_value=4, max_value=8, step=1)
    embed_dim = hp.Int('embed_dim', min_value=32, max_value=64, step=16)
    ff_dim = hp.Choice('ff_dim', values=[128, 256, 512], ordered=True)
    dropout = hp.Float('dropout', min_value=0.0, max_value=0.3, step=0.1)
    l2 = hp.Float('l2', min_value=0.000, max_value=0.006, step=0.002)
    n_layers = hp.Int('n_layers', min_value=1, max_value=2, step=1)
    lr = hp.Choice('lr', values=[1e-5, 1e-4, 1e-3])
    return num_heads, embed_dim, ff_dim, dropout, l2, n_layers, lr


def cnn_sequence_hyperparameters(hp):
    num_conv_layers = hp.Int('n_layers', min_value=1, max_value=2, step=1)
    conv_filters = hp.Int('n_filters_1', min_value=4, max_value=16, step=4)
    kernel_size = hp.Int('kernel_size', min_value=3, max_value=7, step=2)
    pool_size = hp.Int('pool_size', min_value=2, max_value=4, step=1)
    num_tabular_dense_layers = hp.Int('n_tabular_layers', min_value=1, max_value=2, step=1)
    tabular_dense_units = hp.Int('tabular_units', min_value=32, max_value=64, step=32)
    num_merged_dense_layers = hp.Int('n_merged_layers', min_value=1, max_value=2, step=1)
    merged_dense_units = hp.Int('merged_units', min_value=32, max_value=64, step=32)
    lr = hp.Choice('lr', values=[1e-5, 1e-4, 1e-3], ordered=True)
    do = hp.Float('do', min_value=0.0, max_value=0.2, step=0.1)
    l2_reg_factor = hp.Float('l2', min_value=0.0, max_value=0.002, step=0.001)
    return num_conv_layers, conv_filters, kernel_size, pool_size, num_tabular_dense_layers, tabular_dense_units, num_merged_dense_layers, merged_dense_units, lr, do, l2_reg_factor
