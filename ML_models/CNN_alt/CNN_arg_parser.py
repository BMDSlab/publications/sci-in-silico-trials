import argparse

from Miscellaneous.Misc.columns import *

parent_parser = argparse.ArgumentParser(add_help=False)


def model_arg_interpreter(model):
    model_lower = model.lower()
    if model_lower in ['transformer', 'tf']:
        model = transformer_str
    if model_lower in ['ens', 'ensemble']:
        model = ensemble_str
    if model_lower in ['cnn']:
        model = CNN_str
    if model_lower in ['only_ms']:
        model = only_MS_str
    if model_lower in ['tf_seq', 'transformer_sequence']:
        model = transformer_sequence_str
    if model_lower in ['cnn_seq']:
        model = CNN_sequence_str
    if model_lower in ['gnn']:
        model = GNN_str
    if model_lower in ['gnn2']:
        model = GNN2_str
    if model_lower in ['tf2']:
        model = TF2_str
    if model_lower in ['tf_seq2']:
        model = TF_seq2_str
    return model
