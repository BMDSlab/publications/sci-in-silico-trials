# Graph Neural Network for motor score sequence prediction

Graph Neural Networks (GNNs) are one approach to represent existing knowledge about the relationship between motor and sensory scores as assessed by the ISNSCI exam. GNNs trained to predict motor score sequences at recovery for patient-level graphs containing motor and sensory scores as assessed at baseline togehter with basic clinical information were evaluated as part of the model benchmark.

## requirements

Note that GNNs were implemented using [pytorch geometric](https://pytorch-geometric.readthedocs.io/en/latest/) and therefore require additional libraries to be installed. Refer to `pyproject.toml` for details.

## data

ISNCSCI assessment results and clinical information have to be converted to data structures which can be processed by a GNN. The `data` directory contains dummy data for five patients to illustrate the format based on an adjacency lists, graph and node labels as well as further attributes. These files can be processed as part of the `MotorSensoryScoreGraphsFlexible` class resulting in a dataset holding graph representations of individual patients. 