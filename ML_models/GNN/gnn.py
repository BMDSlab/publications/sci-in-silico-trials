"""Run GNN as used for model benchmark.

Author: Louis P Lukas
"""

import itertools
import json
import pathlib
import numpy as np
import pandas as pd
import torch
import torch_geometric

from torch.nn import functional as F
from torch.nn import ModuleList
from torch.utils.data import Subset
from torch_geometric.data import Data, InMemoryDataset
from torch_geometric.loader import DataLoader
from torch_geometric.nn import BatchNorm, Linear

# ######################################################################################
# parameters to be updated from dummy for actual use case
data_path = pathlib.Path("./data")
parameters_path = pathlib.Path("./parameters")
emsci_ver = 1
# ######################################################################################


# --------------------------------------------------------------------------------------
def _convert_architecture_descriptor(descriptor):
    """Convert descriptor of model architecture containing strings from .json file to
    list of tuples containing matching function references required to instantiate a
    pytorch geometric model.

    Args
    ----
    descriptor : dict
        Dictionary containing information on the hidden layers, size of
        hidden layers and activation functions to be used.
    """
    architecture = [
        (
            getattr(torch_geometric.nn, layer_type),
            hidden_size,
            getattr(torch.nn, activation_fct),
        )
        for layer_type, hidden_size, activation_fct in zip(
            descriptor["hidden_layers"],
            descriptor["hidden_channels"],
            descriptor["activations"],
        )
    ]

    return architecture


def _get_motor_levels(nli: str = None):
    """Return valid motor levels according to ASIA ISNCSCI worksheet.

    If a neurological level of injury (NLI) is provided, only motor levels
    *below* the NLI will be returned.

    Args
    ----
    nli: None or str
        Valid NLI as string

    Returns
    -------
    list
        List containing motor levels in anatomical order
    """
    if nli is None:
        return ["C5", "C6", "C7", "C8", "T1", "L2", "L3", "L4", "L5", "S1"]

    if nli in [f"C{lvl}" for lvl in range(1, 5)]:
        return [
            "C5",
            "C6",
            "C7",
            "C8",
            "T1",
            "L2",
            "L3",
            "L4",
            "L5",
            "S1",
        ]
    if nli == "C5":
        return ["C6", "C7", "C8", "T1", "L2", "L3", "L4", "L5", "S1"]
    if nli == "C6":
        return ["C7", "C8", "T1", "L2", "L3", "L4", "L5", "S1"]
    if nli == "C7":
        return ["C8", "T1", "L2", "L3", "L4", "L5", "S1"]
    if nli == "C8":
        return ["T1", "L2", "L3", "L4", "L5", "S1"]
    if nli in [f"T{lvl}" for lvl in range(1, 13)] + ["L1"]:
        return ["L2", "L3", "L4", "L5", "S1"]
    if nli == "L2":
        return ["L3", "L4", "L5", "S1"]
    if nli == "L3":
        return ["L4", "L5", "S1"]
    if nli == "L4":
        return ["L5", "S1"]
    if nli == "L5":
        return ["S1"]
    if nli.startswith("S"):
        return []


def _idx_below_nli(nli):
    """Calculate indices for motorscores below a particular NLI."""
    nli_to_start_idx = {
        "C1": 0,
        "C2": 0,
        "C3": 0,
        "C4": 0,
        "C5": 2,
        "C6": 4,
        "C7": 6,
        "C8": 8,
        "T1": 10,
        "T2": 10,
        "T3": 10,
        "T4": 10,
        "T5": 10,
        "T6": 10,
        "T7": 10,
        "T8": 10,
        "T9": 10,
        "T10": 10,
        "T11": 10,
        "T12": 10,
        "L1": 10,
        "L2": 12,
        "L3": 14,
        "L4": 16,
        "L5": 18,
        # not possible to calculate RMSE below NLI for sacral level of injury
    }

    return list(range(nli_to_start_idx[nli], 20))


def _motorscore_vector_to_dataframe(
    motorscores: np.array, patient_id: int
) -> pd.DataFrame:
    """Convert vector w/ predictions for motor scores to a pd.DataFrame.

    Args
    ----
    motorscores: np.array
        array (len=20) w/ predicted motor scores in the order (C5 left, C5 right,
        C6 left, C6 right, ...)
    patient_id: int
        patient identifier; will be used in index of dataframe

    Returns
    -------
    dataframe w/ MultiIndex on patient ID and side (left, right), motor lvls as columns
    """
    predicted_motorscores = pd.DataFrame(
        index=pd.MultiIndex.from_product(
            [[patient_id], ["L", "R"]], names=["patient", "side"]
        ),
        columns=_get_motor_levels(nli=None),
    )
    for vec_idx, (motor_level, side) in enumerate(
        itertools.product(_get_motor_levels(), ["L", "R"])
    ):
        predicted_motorscores.loc[pd.IndexSlice[patient_id, side], motor_level] = (
            motorscores[vec_idx]
        )

    return predicted_motorscores


def _rmse_below_nli(true, predicted, patients, nlis, check_NAs=False):
    """Compute the RMSE loss only below the NLI.

    Args
    ----
    true:
        ...values of motor score.
    predicted:
        ...values of motor score.
    patient: list
        List of patient IDs in batch for which loss should be calculated.
    nlis: pd.Series
        ...of patients for whom loss should be calculated.

    Returns
    -------
    RMSE below NLI if single patient, sum of RMSEs across set of patients.
    """
    assert true.shape[0] % 20 == 0
    assert predicted.shape[0] % 20 == 0

    losses = torch.zeros(len(patients), dtype=torch.float32)
    for i, patient in enumerate(patients):
        start_idx, stop_idx = (i * 20, i * 20 + 20)
        true_patient = true[start_idx:stop_idx]
        predicted_patient = predicted[start_idx:stop_idx]
        idxs_below_nli = _idx_below_nli(nlis[patient])
        true_below_nli = true_patient[idxs_below_nli]
        if check_NAs:
            assert torch.isnan(true_below_nli).sum() == 0
        predicted_below_nli = predicted_patient[idxs_below_nli]
        if check_NAs:
            assert torch.isnan(predicted_below_nli).sum() == 0
        delta = true_below_nli.sub(predicted_below_nli)
        if check_NAs:
            assert torch.isnan(delta).sum() == 0
        delta_squared = delta**2
        if check_NAs:
            assert torch.isnan(delta_squared).sum() == 0
        mean = torch.mean(delta_squared)
        if check_NAs:
            assert torch.isnan(mean).sum() == 0
        root = torch.sqrt(mean)
        if check_NAs:
            assert torch.isnan(root).sum() == 0
        losses[i] = root

    return torch.sum(losses)


# --------------------------------------------------------------------------------------
class MotorSensoryScoreGraphsFlexible(InMemoryDataset):
    """Custom dataset of patient level graphs w/ spinal levels for motor score
    assessment only. Both motor and sensory scores are represented as node features.
    Additional graph-level features required to enable flexible use of assessment time
    points, both baseline (features) and follow-up (labels).

    Files required for instantiating DataSet need to be generated separately. Refer to
    example files included for structure.
    """

    def __init__(
        self,
        root: str,
        emsci_version: int = 2023,
        transform=None,
        pre_transform=None,
        pre_filter=None,
    ):
        """
        Args
        ----
        root: str
            Directory within which to look for raw and processed.
        emsci_version: int
            Which version of EMSCI data to use. Example data labelled as "1"; o/w year
            corresponding to when data was shared is used.
        """
        self.emsci_version = emsci_version
        self.fname_base = f"EMSCI-{emsci_version}-MS-SS-nodelvl"
        self.base_dir = root + "/" + self.fname_base
        super(MotorSensoryScoreGraphsFlexible, self).__init__(
            self.base_dir, transform, pre_transform, pre_filter
        )
        self.data, self.slices = torch.load(self.processed_paths[0])

    @property
    def raw_file_names(self):
        return [
            (self.fname_base + "_A.txt"),
            (self.fname_base + "_graph_indicators.txt"),
            (self.fname_base + "_graph_labels.txt"),
            (self.fname_base + "_node_labels.txt"),
            (self.fname_base + "_node_attributes.txt"),
            # additional features have to be present as these are added to patient
            # graphs in process()
            ("additional_features_baseline.csv"),
        ]

    @property
    def processed_file_names(self):
        return ["data.pt"]

    def download(self):
        raise NotImplementedError(
            "EMSCI/Sygen data cannot be made available for download. In case you are"
            " interested in working with the data, please contact bmds@hest.ethz.ch."
        )

    def process(self):
        """Generate individual graphs from aggregate information."""
        path = pathlib.Path(self.raw_dir) / (self.fname_base + "_A.txt")
        adj = pd.read_csv(
            path,
            header=None,
            converters={0: lambda x: int(x.strip("(")), 1: lambda x: int(x.strip(")"))},
            names=["source", "target"],
        )
        adj.index += 1
        path = pathlib.Path(self.raw_dir) / (self.fname_base + "_graph_indicators.txt")
        g_indicator = pd.read_csv(path, header=None, names=["id"])
        g_indicator.index += 1
        path = pathlib.Path(self.raw_dir) / (self.fname_base + "_node_labels.txt")
        v_labels = pd.read_csv(path, header=None, names=["motor_score"])
        v_labels.index += 1
        path = pathlib.Path(self.raw_dir) / (self.fname_base + "_node_attributes.txt")
        v_attrs = pd.read_csv(
            path,
            header=None,
            names=[
                "motorscore",
                "lighttouch",
                "pinprick",
            ],
        )
        v_attrs.index += 1
        path = pathlib.Path(self.raw_dir) / "additional_features_baseline.csv"
        additional_features = pd.read_csv(path, header=0, index_col=0)
        graph_ids = g_indicator["id"].unique()
        assert additional_features.shape[0] == len(graph_ids)
        assert additional_features.index.equals(pd.Index(graph_ids))
        # create a graph for each patient
        data = list()
        for i, graph_id in enumerate(graph_ids):
            i += 1
            # nodes belonging to particular graph
            g_node_ids = g_indicator.loc[g_indicator["id"] == graph_id].index
            # labels of those nodes and ...
            # (labels are only one dimension)
            g_v_labels = v_labels.loc[g_node_ids]
            # ... attributes of those nodes
            # (attributes can have multiple dimensions)
            g_v_attrs = v_attrs.loc[g_node_ids, :]
            # edges belonging to the graph
            g_edges = adj.loc[adj["source"].isin(g_node_ids)]
            # 'normalise' edge indices (nodes in individual patient graph extracted from
            # aggregate information for full dataset should be labelled starting at 1)
            g_edges = g_edges.map(self._normalise_edge_indices)
            # convert all graph properties to tensors
            g = Data(
                # node features
                x=torch.tensor(g_v_attrs.to_numpy(), dtype=torch.float32),
                # edges
                edge_index=torch.tensor(
                    g_edges.to_numpy().transpose(), dtype=torch.long
                ),
                # graph/node label(s)
                y=torch.tensor(g_v_labels.to_numpy(), dtype=torch.float32),
            )
            g["pat_id"] = graph_id
            g["baseline_assmt_days"] = additional_features.loc[graph_id, "timeX"]
            g["followup_assmt_days"] = additional_features.loc[graph_id, "timeY"]
            g["sex_m"] = additional_features.loc[graph_id, "sex_m"]
            g["NLI_ind"] = additional_features.loc[graph_id, "ind_NLI"]
            g["NLI_str"] = additional_features.loc[graph_id, "str_NLI"]
            g["AIS_B"] = additional_features.loc[graph_id, "AIS_B"]
            g["AIS_C"] = additional_features.loc[graph_id, "AIS_C"]
            g["AIS_D"] = additional_features.loc[graph_id, "AIS_D"]

            data.append(g)

        data, slices = self.collate(data)

        torch.save((data, slices), self.processed_paths[0])

    def _normalise_edge_indices(self, i):
        return (i % 20) - 1 if i % 20 != 0 else 19


class GNNForISNCSCIGraphAndClinicalData(torch.nn.Module):
    """Simple class for making node-level predictions on patient graphs representing
    ISNCSCI exam results in combination with additional clinical and demogrpahic data.

    Allow for flexible initialisation of hidden layers in GNN (type, size, activation).

    Uses additional clinical data by concatenating features to embedding obtained from
    GNN applied on graph representation of ISNCSI examination results.
    """

    def __init__(
        self,
        hidden_layers: list,
        num_node_features: int,
        num_add_features: int,
        use_batch_norm: bool,
        seed: int,
        task: str = "nodelevel",
    ):
        """
        Args
        ----
        hidden_layers : list[(int, layer type, activation function)]
            List of tuples containing specification of hidden layers; first
            element of tuple specifies size of hidden layer, second element of
            tuple specifies the type of graph convolutional layer to be used,
            and the third element specifies the activation function.
        num_node_features : int
            Number of node features.
        num_add_features : int
            Number of additional features to use.
        use_batch_norm: bool
            Use batch_norm on concatenated feature vector.
        seed : int
            Random seed for initialising torch.
        task : str
            Indicate if model architecture (readout layer) should be initialised
            for node or graph level prediction; must be "nodelevel" or "graphlevel".
            Node level corresponds to motor score sequence, graph level corresponds
            to walking ability.
        """
        super().__init__()
        torch.manual_seed(seed)
        self.use_batch_norm = use_batch_norm
        self.n_layers = len(hidden_layers)
        self.layers = ModuleList()
        self.activations = ModuleList()
        self.task = task

        # GNN
        input_size = num_node_features
        for layer, hidden_size, activation in hidden_layers:
            self.layers.append(
                layer(input_size, hidden_size),
            )
            self.activations.append(activation())
            input_size = hidden_size
        # linear layers for graph embedding + clinical features
        input_size += num_add_features
        if self.use_batch_norm:
            self.batchnorm = BatchNorm(input_size)
        self.fc_1 = Linear(input_size, input_size)
        self.fc_2 = Linear(input_size, input_size)
        # final layer to project to relevant output
        if self.task == "nodelevel":
            self.layers.append(Linear(input_size, 1))
        elif self.task == "graphlevel":
            self.layers.append(Linear(input_size, 2))

    def forward(self, x, edge_index, add_features, batch=None):
        """Simple forward pass constructed from ModuleList instances holding
        hidden layer and activation function instances.

        Args
        ----
        x :
            Feature tensor.
        edge_index :
            Tensor (?) with edge information
        """
        # GNN
        for i in range(self.n_layers):
            x = self.layers[i](x, edge_index)
            x = self.activations[i](x)
        # concatenate output + clinical features
        x = torch.cat((x, add_features), dim=1)
        if self.use_batch_norm:
            x = self.batchnorm(x)
        x = self.fc_1(x)
        x = F.leaky_relu(x)
        x = self.fc_2(x)
        x = F.leaky_relu(x)
        if self.task == "graphlevel":
            raise NotImplementedError(
                "Graph-level prediction task not implemented"
                + " for expanded design using clinical data."
            )
        else:
            x = self.layers[-1](x)

        return x


# --------------------------------------------------------------------------------------
def _predict_extended(model, device, data, nlis=None):
    """Predict outcome for graph(s) in dataset containing additional clinical features
    (assessment time, sex, NLI, AIS grade).

    Args
    ----
    model:
        (trained) pytorch geometric model
    device:
        torch "flag" indicating which device (cpu, cuda) to use
    data:
        pytorch geometric dataset containing patient graphs with motor and sensory
        scores, and additional clinical data

    Returns
    -------
    pandas dataframe with predictions (MultiIndex on patient ID and side [left, right],
    motor lvls. as columns)
    """
    model.eval()
    predictions = pd.DataFrame(
        index=pd.MultiIndex(levels=[[], []], codes=[[], []], names=["patient", "side"]),
        columns=_get_motor_levels(),
    )
    for patient_graph in data:
        patient = patient_graph.pat_id[0]
        additional_features = torch.Tensor(
            [
                patient_graph.baseline_assmt_days,
                patient_graph.followup_assmt_days,
                patient_graph.sex_m,
                patient_graph.NLI_ind,
                patient_graph.AIS_B,
                patient_graph.AIS_C,
                patient_graph.AIS_D,
            ]
        ).reshape(1, -1)
        additional_features = additional_features.repeat_interleave(20, 0)
        out = model(
            patient_graph.x.to(device),
            patient_graph.edge_index.to(device),
            additional_features.to(device),
        )
        if nlis is None:
            predictions = pd.concat(
                [
                    predictions,
                    _motorscore_vector_to_dataframe(
                        out.cpu().detach().squeeze().numpy(), patient
                    ),
                ]
            )
        else:
            prediction_patient = _motorscore_vector_to_dataframe(
                out.cpu().detach().squeeze().numpy(), patient
            )
            observed_patient = _motorscore_vector_to_dataframe(
                patient_graph.y.cpu().detach().squeeze().numpy(), patient
            )
            nli = nlis[patient]
            if nli in ["C1", "C2", "C3", "C4"]:  # all motor scores below NLI
                predictions = pd.concat([predictions, prediction_patient])
            else:  # only update motor scores below NLI with predictions
                observed_patient.loc[:, _get_motor_levels(nli=nli)] = (
                    prediction_patient.loc[:, _get_motor_levels(nli=nli)]
                )
                predictions = pd.concat([predictions, observed_patient])

    return predictions


def _train_loop_extended(
    model,
    device,
    learning_rate,
    optimiser,
    train_loader,
    val_loader,
    test_loader,
    nlis,
    n_epochs,
):
    """Training routine for GNN architecture using clinical features."""
    # set up
    if optimiser == "adam":
        optimiser = torch.optim.Adam(model.parameters(), lr=learning_rate)
    elif optimiser == "sgd":
        optimiser = torch.optim.SGD(model.parameters(), lr=learning_rate)
    else:
        raise AttributeError(f"Invalid optimiser {optimiser} specified.")
    train_losses = []
    val_losses = []
    test_perform = []

    for _ in range(n_epochs):
        model.train()
        train_loss_epoch = []
        for train_data in train_loader:
            optimiser.zero_grad()
            # data = data.to_device(device)
            if len(train_data) > 1:
                additional_features = torch.Tensor(
                    [
                        train_data.baseline_assmt_days,
                        train_data.followup_assmt_days,
                        train_data.sex_m,
                        train_data.NLI_ind,
                        train_data.AIS_B,
                        train_data.AIS_C,
                        train_data.AIS_D,
                    ]
                ).T
                additional_features = additional_features.repeat_interleave(20, 0)
            else:
                additional_features = torch.Tensor(
                    [
                        train_data.baseline_assmt_days,
                        train_data.followup_assmt_days,
                        train_data.sex_m,
                        train_data.NLI_ind,
                        train_data.AIS_B,
                        train_data.AIS_C,
                        train_data.AIS_D,
                    ]
                ).reshape(1, -1)
                additional_features = additional_features.repeat_interleave(20, 0)
            out = model(
                train_data.x.to(device),
                train_data.edge_index.to(device),
                additional_features.to(device),
            )
            # loss = criterion(out, train_data.y.to(device))
            loss = _rmse_below_nli(
                train_data.y.to(device), out, train_data["pat_id"], nlis
            )
            loss.backward()
            train_loss_epoch.append(loss.cpu().detach().numpy())
            optimiser.step()
        train_losses.append(np.sum(train_loss_epoch) / len(train_loader.dataset))

        model.eval()
        val_loss_epoch = []
        for val_data in val_loader:
            if len(train_data) > 1:
                additional_features = torch.Tensor(
                    [
                        val_data.baseline_assmt_days,
                        val_data.followup_assmt_days,
                        val_data.sex_m,
                        val_data.NLI_ind,
                        val_data.AIS_B,
                        val_data.AIS_C,
                        val_data.AIS_D,
                    ]
                ).T
                additional_features = additional_features.repeat_interleave(20, 0)
            else:
                additional_features = torch.Tensor(
                    [
                        val_data.baseline_assmt_days,
                        val_data.followup_assmt_days,
                        val_data.sex_m,
                        val_data.NLI_ind,
                        val_data.AIS_B,
                        val_data.AIS_C,
                        val_data.AIS_D,
                    ]
                ).reshape(1, -1)
                additional_features = additional_features.repeat_interleave(20, 0)
            out = model(
                val_data.x.to(device),
                val_data.edge_index.to(device),
                additional_features.to(device),
            )
            loss = _rmse_below_nli(val_data.y.to(device), out, val_data["pat_id"], nlis)
            val_loss_epoch.append(loss.cpu().detach().numpy())
        val_losses.append(np.sum(val_loss_epoch) / len(val_loader.dataset))
        test_perform_epoch = []
        for test_data in test_loader:
            if len(train_data) > 1:
                additional_features = torch.Tensor(
                    [
                        test_data.baseline_assmt_days,
                        test_data.followup_assmt_days,
                        test_data.sex_m,
                        test_data.NLI_ind,
                        test_data.AIS_B,
                        test_data.AIS_C,
                        test_data.AIS_D,
                    ]
                ).T
                additional_features = additional_features.repeat_interleave(20, 0)
            else:
                additional_features = torch.Tensor(
                    [
                        test_data.baseline_assmt_days,
                        test_data.followup_assmt_days,
                        test_data.sex_m,
                        test_data.NLI_ind,
                        test_data.AIS_B,
                        test_data.AIS_C,
                        test_data.AIS_D,
                    ]
                ).reshape(1, -1)
                additional_features = additional_features.repeat_interleave(20, 0)
            out = model(
                test_data.x.to(device),
                test_data.edge_index.to(device),
                additional_features.to(device),
            )
            rmse = _rmse_below_nli(
                test_data.y.to(device), out, test_data["pat_id"], nlis
            )
            test_perform_epoch.append(rmse.cpu().detach().numpy())
        test_perform.append(np.sum(test_perform_epoch) / len(test_loader.dataset))

    return model, train_losses, val_losses, test_perform


def evaluate_model_extended(parameters, dataset, data_split, nlis):
    """Evaluate model architecture including clinical features.

    Args
    ----
    parameters: dict
        Dictionary with model and training parameters.

    dataset: torch_geometric.InMemoryDataset

    data_split: dict
        Nested ictionary w/ fold IDs as top-level keys, and ["train", "validation",
        "test"] as second-level keys; values are lists w/ patient IDs to use for
        training, validating, and testing.

    Returns
    -------
    model_instances: dictionary with trained model instances for each fold
    train_loss_df, val_loss_df, test_perform_df: dataframes with loss values
    val_pred_df, test_pred_df: dataframes with predictions
    """
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    print(f"\tUsing {str(device).upper()} for training.")
    rng = np.random.default_rng(seed=parameters["model"]["seed"])
    indices = np.arange(len(dataset))
    rng.shuffle(indices)
    patient_ids = dataset.pat_id

    # store losses and predictions
    model_instances = {}
    train_loss_df = pd.DataFrame(
        columns=pd.Index(range(1, parameters["training"]["n_epochs"] + 1)),
    )
    val_loss_df = pd.DataFrame(
        columns=pd.Index(range(1, parameters["training"]["n_epochs"] + 1)),
    )
    val_pred_df = pd.DataFrame(
        index=pd.MultiIndex(levels=[[], []], codes=[[], []], names=["patient", "side"]),
        columns=_get_motor_levels(),
    )
    test_perform_df = pd.DataFrame(
        columns=pd.Index(range(1, parameters["training"]["n_epochs"] + 1)),
    )
    test_pred_df = pd.DataFrame(
        index=pd.MultiIndex(levels=[[], []], codes=[[], []], names=["patient", "side"]),
        columns=_get_motor_levels(),
    )

    # model training
    for fold in sorted(data_split.keys()):
        patients_train = data_split[fold]["train"]
        patients_val = data_split[fold]["validation"]
        patients_test = data_split[fold]["test"]
        train_idx = np.in1d(patient_ids, patients_train).nonzero()[0]
        val_idx = np.in1d(patient_ids, patients_val).nonzero()[0]
        test_idx = np.in1d(patient_ids, patients_test).nonzero()[0]
        assert len(set(train_idx).intersection(set(val_idx))) == 0
        assert len(set(train_idx).intersection(set(test_idx))) == 0
        assert len(set(val_idx).intersection(set(test_idx))) == 0
        train_loader = DataLoader(
            Subset(dataset, train_idx),
            batch_size=parameters["training"]["batch_size"],
            shuffle=True,
        )
        val_loader = DataLoader(
            Subset(dataset, val_idx),
            batch_size=1,
            shuffle=False,
        )
        test_loader = DataLoader(
            Subset(dataset, test_idx),
            batch_size=1,
            shuffle=False,
        )

        architecture = _convert_architecture_descriptor(parameters["model"])
        model_instance = GNNForISNCSCIGraphAndClinicalData(
            hidden_layers=architecture,
            num_node_features=dataset.num_features,
            num_add_features=7,
            use_batch_norm=bool(parameters["model"]["use_batch_norm"]),
            seed=parameters["model"]["seed"],
            task="nodelevel",
        )
        model_instance.to(device)
        (
            model_instance,
            train_losses,
            val_losses,
            test_perform,
        ) = _train_loop_extended(
            model_instance,
            device,
            learning_rate=parameters["training"]["learning_rate"],
            optimiser=parameters["training"]["optimiser"],
            train_loader=train_loader,
            val_loader=val_loader,
            test_loader=test_loader,
            nlis=nlis,
            n_epochs=parameters["training"]["n_epochs"],
        )
        model_instances[f"fold_{fold}_state_dict"] = model_instance.state_dict()
        train_loss_df.loc[fold, :] = train_losses
        val_loss_df.loc[fold, :] = val_losses
        val_pred_df = pd.concat(
            [val_pred_df, _predict_extended(model_instance, device, val_loader)]
        )
        test_perform_df.loc[fold, :] = test_perform
        test_pred_df = pd.concat(
            [test_pred_df, _predict_extended(model_instance, device, test_loader)]
        )

    return (
        model_instances,
        train_loss_df,
        val_loss_df,
        val_pred_df,
        test_perform_df,
        test_pred_df,
    )


if __name__ == "__main__":
    with open(parameters_path / "parameters.json", "r", encoding="utf-8") as f:
        parameters = json.load(f)
    with open(data_path / "splits.json", "r", encoding="utf-8") as f:
        data_split = json.load(f)
    graph_data = MotorSensoryScoreGraphsFlexible(
        root=str(data_path / "graph-representations"),
        emsci_version=emsci_ver,
    )

    nlis_baseline = {}
    for g in graph_data:
        nlis_baseline[g["pat_id"]] = g["NLI_str"]
    nlis_baseline = pd.Series(nlis_baseline)

    (
        model_instances,
        train_losses,
        val_losses,
        val_predictions,
        test_perf,
        test_predictions,
    ) = evaluate_model_extended(
        parameters,
        graph_data,
        data_split,
        nlis_baseline,
    )
