import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error
from sklearn.metrics import pairwise_distances
import tqdm


#######################################################################################
# Auxillary functions
#######################################################################################

# Data loading
def getSygen(path_sygen, dermatomes_toMatch, sc_TMS_toMatch):
    # Load Sygen data
    X_addfile = os.path.join(path_sygen)
    df_data = pd.read_csv(X_addfile)
    df_data.set_index('ptid', drop=True, inplace=True)
    sygenSc = ['elbfl', 'wrext', 'elbex', 'finfl', 'finab', 'hipfl', 'kneex', 'ankdo', 'greto', 'ankpl']
    sygenScr = ['elbfl', 'wrext', 'elbex', 'finfl', 'finab', 'hipfl', 'kneet', 'ankdo', 'greto', 'ankpl']
    dermatomes = ['C2', 'C3', 'C4', 'C5', 'C6', 'C7', 'C8', 'T1', 'T2', 'T3',
                  'T4', 'T5', 'T6', 'T7', 'T8', 'T9', 'T10', 'T11', 'T12',
                  'L1', 'L2', 'L3', 'L4', 'L5', 'S1', 'S2', 'S3', 'S45']
    derm_fields_lr = ['C2_l', 'C3_l', 'C4_l', 'C5_l', 'C6_l', 'C7_l', 'C8_l', 'T1_l', 'T2_l',
                      'T3_l', 'T4_l', 'T5_l', 'T6_l', 'T7_l', 'T8_l', 'T9_l', 'T10_l',
                      'T11_l', 'T12_l', 'L1_l', 'L2_l', 'L3_l', 'L4_l', 'L5_l', 'S1_l',
                      'S2_l', 'S3_l', 'S45_l', 'C2_r', 'C3_r', 'C4_r', 'C5_r', 'C6_r', 'C7_r',
                      'C8_r', 'T1_r', 'T2_r', 'T3_r', 'T4_r', 'T5_r', 'T6_r', 'T7_r', 'T8_r',
                      'T9_r', 'T10_r', 'T11_r', 'T12_r', 'L1_r', 'L2_r', 'L3_r', 'L4_r',
                      'L5_r', 'S1_r', 'S2_r', 'S3_r', 'S45_r']
    ms_fields_lr = ['C5_l', 'C6_l', 'C7_l', 'C8_l', 'T1_l', 'L2_l', 'L3_l', 'L4_l', 'L5_l', 'S1_l',
                    'C5_r', 'C6_r', 'C7_r', 'C8_r', 'T1_r', 'L2_r', 'L3_r', 'L4_r', 'L5_r', 'S1_r']
    ms_fields = ['C5_l', 'C5_r', 'C6_l', 'C6_r', 'C7_l', 'C7_r', 'C8_l', 'C8_r', 'T1_l', 'T1_r', 'L2_l', 'L2_r', 'L3_l',
                 'L3_r',
                 'L4_l', 'L4_r', 'L5_l', 'L5_r', 'S1_l', 'S1_r']

    ltl_sygen = []
    ltr_sygen = []
    ppl_sygen = []
    ppr_sygen = []
    for sc in dermatomes:
        if sc[0] == 'S':
            sc = 's' + sc[1:]
        elif sc[0] == 'C':
            sc = 'c' + sc[1:]
        elif sc[0] == 'T':
            sc = 't' + sc[1:]
        elif sc[0] == 'L':
            sc = 'l' + sc[1:]

        ltl_sygen.append(sc + 'ltl')
        ltr_sygen.append(sc + 'ltr')
        ppl_sygen.append(sc + 'ppl')
        ppr_sygen.append(sc + 'ppr')

    msl_sygen = [s + 'l' for s in sygenSc]
    msr_sygen = [s + 'r' for s in sygenScr]

    all_scSygen = msl_sygen + msr_sygen + ltl_sygen + ltr_sygen + ppl_sygen + ppr_sygen
    all_scSygen_tp1 = [s + '01' for s in all_scSygen]
    all_scSygen_tp2 = [s + '26' for s in all_scSygen]

    # Load the scores
    sygenX = df_data[all_scSygen_tp1]
    sygenY = df_data[all_scSygen_tp2]
    sygenX.dropna(inplace=True)
    sygenX.columns = all_scSygen
    sygenY.dropna(inplace=True)
    sygenY.columns = all_scSygen
    sygenpats = list(set(sygenX.index) & set(sygenY.index))

    # Additional data
    sygenX_add_lr = df_data[['age', 'sexcd', 'tx1_r', 'ais1', 'splvl', 'lower01', 'vaccd01', 'anyana01']]
    sygenX_add_lr = sygenX_add_lr.loc[sygenpats]
    sygenX_add_lr.columns = ['age', 'sex', 'treat', 'aisa', 'nli', 'LEMS', 'VAC', 'DAP']

    sygenY_add_lr = df_data[['age', 'sexcd', 'tx1_r', 'ais26', 'splvl', 'lower26', 'vaccd26', 'anyana26']]
    sygenY_add_lr = sygenY_add_lr.loc[sygenpats]
    sygenY_add_lr.columns = ['age', 'sex', 'treat', 'aisa', 'nli', 'LEMS', 'VAC', 'DAP']

    plegia = []
    TMS = []
    aisa = []
    nli_ind = []
    nli = []
    nli_coarse = []
    ind_nli_MS = []
    sex = []

    TMS_Y = []
    aisa_Y = []
    for p in tqdm.tqdm(sygenpats):

        this_nli = sygenX_add_lr.loc[p, 'nli'][0] + str(int(sygenX_add_lr.loc[p, 'nli'][-2:]))
        nli.append(this_nli)
        if this_nli[0] == 'C':
            nli_coarse.append('cervical')
        elif this_nli[0] == 'T':
            nli_coarse.append('thoracic')
        elif this_nli[0] == 'L':
            nli_coarse.append('lumbar')
        elif this_nli[0] == 'S':
            nli_coarse.append('sacral')

        this_nli = sygenX_add_lr.loc[p, 'nli'][0] + str(int(sygenX_add_lr.loc[p, 'nli'][-2:]))
        if this_nli == 'C1':
            this_nliind = 0
        else:
            this_nliind = dermatomes.index(this_nli)
        nli_ind.append(this_nliind)

        ms_blnli = [value for value in dermatomes_toMatch[this_nliind:] if value in sc_TMS_toMatch]
        this_ind_nliMS = 2 * sc_TMS_toMatch.index(ms_blnli[0])
        ind_nli_MS.append(this_ind_nliMS)

        if this_nli[0] in ['T', 'L', 'S']:
            plegia.append('para')
        else:
            plegia.append('tetra')

        this_aislong = sygenX_add_lr.loc[p, 'aisa']
        try:
            aisa.append(this_aislong[-1])
        except:
            aisa.append(np.nan)

        this_aislongY = sygenY_add_lr.loc[p, 'aisa']
        try:
            aisa_Y.append(this_aislongY[-1])
        except:
            aisa_Y.append(np.nan)

        TMS.append(sygenX.loc[p, msl_sygen + msr_sygen].sum())
        TMS_Y.append(sygenY.loc[p, msl_sygen + msr_sygen].sum())

        if sygenX_add_lr.loc[p, 'sex'] == 1:
            sex.append('F')
        else:
            sex.append('M')

    sygenX_add_lr['plegia'] = plegia
    sygenX_add_lr['TMS'] = TMS
    sygenX_add_lr['nli_ind'] = nli_ind
    sygenX_add_lr['aisa'] = aisa
    sygenX_add_lr['nli'] = nli
    sygenX_add_lr['sex'] = sex
    sygenX_add_lr['nli_coarse'] = nli_coarse
    sygenX_add_lr['nli_ind_MS'] = ind_nli_MS

    sygenY_add_lr['plegia'] = plegia
    sygenY_add_lr['TMS'] = TMS_Y
    sygenY_add_lr['nli_ind'] = nli_ind
    sygenY_add_lr['aisa'] = aisa_Y
    sygenY_add_lr['nli'] = nli
    sygenY_add_lr['sex'] = sex
    sygenY_add_lr['nli_coarse'] = nli_coarse
    sygenY_add_lr['nli_ind_MS'] = ind_nli_MS

    sygenX_MS = pd.DataFrame(columns=ms_fields_lr)
    sygenX_LTS = pd.DataFrame(columns=derm_fields_lr)
    sygenX_PPS = pd.DataFrame(columns=derm_fields_lr)
    sygenX_add = pd.DataFrame(columns=sygenX_add_lr.columns)

    sygenY_MStrue = pd.DataFrame(columns=ms_fields_lr)
    sygenY_LTS = pd.DataFrame(columns=derm_fields_lr)
    sygenY_PPS = pd.DataFrame(columns=derm_fields_lr)
    sygenY_add = pd.DataFrame(columns=sygenX_add_lr.columns)

    ms_use = msl_sygen + msr_sygen
    lts_use = ltl_sygen + ltr_sygen
    pps_use = ppl_sygen + ppr_sygen
    meanSc = []
    for p in tqdm.tqdm(sygenpats):
        sygenX_MS.loc[p, :] = list(sygenX.loc[p, ms_use])
        sygenX_LTS.loc[p, :] = list(sygenX.loc[p, lts_use])
        sygenX_PPS.loc[p, :] = list(sygenX.loc[p, pps_use])
        sygenX_add.loc[p, :] = sygenX_add_lr.loc[p, :]

        sygenY_MStrue.loc[p, :] = list(sygenY.loc[p, ms_use])
        sygenY_LTS.loc[p, :] = list(sygenY.loc[p, lts_use])
        sygenY_PPS.loc[p, :] = list(sygenY.loc[p, pps_use])
        sygenY_add.loc[p, :] = sygenY_add_lr.loc[p, :]

        this_ind_nliMS = sygenX_add_lr.loc[p, 'nli_ind_MS']
        this_meanSc = sygenX_MS.loc[p, ms_fields[this_ind_nliMS:]].mean()
        meanSc.append(this_meanSc)
    sygenX_add['MeanScBlNLI'] = meanSc

    # drop patients missing an AISA grade
    pats_drop = list(sygenX_add[sygenX_add['aisa'].isnull()].index)
    sygenX_add.drop(pats_drop, inplace=True)
    sygenX_MS.drop(pats_drop, inplace=True)
    sygenX_LTS.drop(pats_drop, inplace=True)
    sygenX_PPS.drop(pats_drop, inplace=True)

    sygenY_MStrue.drop(pats_drop, inplace=True)
    sygenY_add.drop(pats_drop, inplace=True)
    sygenY_LTS.drop(pats_drop, inplace=True)
    sygenY_PPS.drop(pats_drop, inplace=True)

    sygenX_add = defineNLI(sygenX_add, sygenX_MS, sygenX_LTS, sygenX_PPS, sc_TMS_toMatch, dermatomes_toMatch, ms_fields)
    sygenY_add = defineNLI(sygenY_add, sygenY_MStrue, sygenY_LTS, sygenY_PPS, sc_TMS_toMatch, dermatomes_toMatch,
                           ms_fields)

    return sygenX_MS, sygenX_LTS, sygenX_PPS, sygenX_add, \
        sygenY_MStrue, sygenY_LTS, sygenY_PPS, sygenY_add


def getEMSCI(pats_include_file, outcome_file_toMatch, inputMS_file_toMatch, inputLTS_file_toMatch,
             inputPPS_file_toMatch,
             X_addfile_toMatch, aisa_grade_field_toMatch, plegia_field_toMatch, nli_field_toMatch, age_field_toMatch,
             sex_field_toMatch, cause_field_toMatch, nlicoarse_field_toMatch, ms_fields_toMatch,
             dermatomes_toMatch, sc_TMS_toMatch, vac_field, dap_field):
    # Load baseline and recovery MS for the patients to be matched (true results)
    Y_toMatch = pd.read_csv(outcome_file_toMatch, index_col=0)
    X_MS_toMatch = pd.read_csv(inputMS_file_toMatch, index_col=0)
    X_LTS_toMatch = pd.read_csv(inputLTS_file_toMatch, index_col=0)
    X_PPS_toMatch = pd.read_csv(inputPPS_file_toMatch, index_col=0)
    X_add_toMatch = getaddData(X_addfile_toMatch, aisa_grade_field_toMatch, plegia_field_toMatch, nli_field_toMatch,
                               age_field_toMatch, sex_field_toMatch, cause_field_toMatch, nlicoarse_field_toMatch,
                               ['aisa', 'nli', 'age', 'plegia', 'LEMS', 'MeanScBlNLI'], ms_fields_toMatch, X_MS_toMatch,
                               X_LTS_toMatch, X_PPS_toMatch, Y_toMatch.index, dermatomes_toMatch, sc_TMS_toMatch,
                               Vac_field=vac_field,
                               DAP_field=dap_field)

    # Optional load patient subset
    if len(pats_include_file) > 0:
        pats_use = list(pd.read_csv(pats_include_file, index_col=0).index)
        Y_toMatch = Y_toMatch.loc[pats_use]
        X_MS_toMatch = X_MS_toMatch.loc[pats_use]
        X_LTS_toMatch = X_LTS_toMatch.loc[pats_use]
        X_PPS_toMatch = X_PPS_toMatch.loc[pats_use]
        X_add_toMatch = X_add_toMatch.loc[pats_use]

    return X_MS_toMatch, X_LTS_toMatch, X_PPS_toMatch, Y_toMatch, X_add_toMatch

def getNLI(X_MS, X_LTS, X_PPS, dermatomes_noC1):

    # Get index for sensory scores
    ind_LTS = np.where(X_LTS.astype(float) < 2)[0][0]
    if ind_LTS >0:
        ind_LTS = ind_LTS-1

    ind_PPS = np.where(X_PPS.astype(float) < 2)[0][0]
    if ind_PPS > 0:
        ind_PPS = ind_PPS - 1

    MS_notintakt = X_MS[X_MS.astype(float) < 3].index[0].split('_')[0]
    ind_MS = dermatomes_noC1.index(MS_notintakt)

    # convert to dermatome level
    if ind_MS > 0:
        ind_MS = ind_MS - 1

    ind_NLI = np.min([ind_LTS,ind_PPS,ind_MS])
    nli = dermatomes_noC1[ind_NLI]
    return nli
def getOutcomeData_EMSCI(emsci_data_file, time_point, dermatomes_toMatch, pats):
    emsci_data = pd.read_csv(emsci_data_file, index_col=0)
    emsci_data = emsci_data[emsci_data['ExamStage_weeks'] == time_point]

    lts_cols = ['LLT_' + s for s in dermatomes_toMatch] + ['RLT_' + s for s in dermatomes_toMatch]
    pps_cols = ['LPP_' + s for s in dermatomes_toMatch] + ['RPP_' + s for s in dermatomes_toMatch]
    Y_LTS = emsci_data[lts_cols]
    Y_PPS = emsci_data[pps_cols]
    Y_LTS.columns = [s + '_l' for s in dermatomes_toMatch] + [s + '_r' for s in dermatomes_toMatch]
    Y_PPS.columns = [s + '_l' for s in dermatomes_toMatch] + [s + '_r' for s in dermatomes_toMatch]

    emsci_data = emsci_data.loc[:, ['AIS', 'SCIM3_TotalScore', 'SCIM2_TotalScore', 'SCIM23_TotalScore', 'WISCI',
                                    '6min', '10m', 'TUG', 'SCIM2_12', 'SCIM3_12', 'VAC', 'DAP', 'SCIM23_SubScore1']]
    emsci_data = emsci_data.loc[pats]
    emsci_data['SCIM_12'] = emsci_data.loc[:, ['SCIM2_12', 'SCIM3_12']].mean(axis=1)
    emsci_data['Walker'] = emsci_data['SCIM_12'] > 3
    emsci_data.loc[emsci_data['SCIM_12'].isna(), 'Walker'] = np.nan

    emsci_data['Eater'] = emsci_data['SCIM23_SubScore1'] > 15
    emsci_data.loc[emsci_data['SCIM23_SubScore1'].isna(), 'Eater'] = np.nan

    return emsci_data, Y_LTS, Y_PPS

def getAbsoluteTime6months(emsci_data_file):
    emsci_data = pd.read_csv(emsci_data_file, index_col=0)
    emsci_data_26_weeks = emsci_data[emsci_data["ExamStage_weeks"] == 26]
    doi = emsci_data_26_weeks['DOI']
    test_date_mot = emsci_data_26_weeks['TestDate_Mot']
    doi_formatted = pd.to_datetime(doi, format=None)
    test_date_mot_formatted = pd.to_datetime(test_date_mot, format='%d.%m.%y')
    absolute_time = test_date_mot_formatted - doi_formatted
    absolute_time = absolute_time.dropna()
    return absolute_time

def get_MeanScBlNLI(p, X_add, ms_fields, X):
    this_ind_nliMS = X_add.loc[p, 'nli_ind_MS']
    meanScBlNLI = X.loc[p, ms_fields[int(this_ind_nliMS):]].mean()

    return meanScBlNLI


def getName(useToSubset, sc_tol, useKNN, n_NN, useClosestTwin, allowScoreDoubling, useMean, metric_knn):
    name = 'Twin_'

    # all subsets
    for sub in useToSubset:
        name += sub + '_'

    # score tolerance
    if np.any(['LEMS' in useToSubset, 'MeanScBlNLI' in useToSubset, 'RMSE' in useToSubset]):
        name += 'SCtol' + str(sc_tol) + '_' + 'scDouble' + str(allowScoreDoubling)

    # KNN
    if ('NN_MS_SS' in useToSubset) or ('NN_MS' in useToSubset):
        if useKNN:
            name += 'KNN_' + str(n_NN) + '_'
            if useClosestTwin:
                name += 'ClosestTwin_'
        if metric_knn != 'hamming':
            name += '_eucl'

    if useMean:
        name += '_mean'
    else:
        name += '_median'
    return name


def getaddData(X_addfile_ref, aisa_grade_field, plegia_field, nli_field, age_field, sex_field, cause_field,
               nlicoarse_field,
               useToSubset, ms_fields, X, X_LTS, X_PPS, pats, dermatomes_toMatch, sc_TMS_toMatch, Vac_field=[],
               DAP_field=[]):
    # X_addAll = pd.read_csv('/Users/sbrueningk/PycharmProjects/SCI/SCI/EMSCI/emsci_data_2020.csv', index_col=0)
    # X_addAll = pd.read_csv('/Users/sbrueningk/PycharmProjects/SCI/SCI/EMSCI/emsci_data_2020.csv', index_col=0)

    X_add_load = pd.read_csv(X_addfile_ref, index_col=0)
    # X_addAll = X_addAll[X_addAll['ExamStage_weeks'] == 26]

    if type(pats) == str:
        pats_use = X_add_load.index
    else:
        pats_use = pats.copy()

    # X_addrec = X_addAll.loc[pats_use]
    X_add = pd.DataFrame(columns=useToSubset, index=pats_use)

    # Get date of SCI
    import calendar
    from datetime import datetime
    month_dict = {month: index for index, month in enumerate(calendar.month_name) if month}
    dates_raw = X_add_load.loc[pats_use, 'DOI']
    all_DOIs = []
    for d in dates_raw:
        month = month_dict[d.split(',')[1].split(' ')[1]]
        day = int(d.split(',')[1].split(' ')[2])
        year = int(d.split(',')[2].split(' ')[1])
        all_DOIs.append(datetime(year=year, month=month, day=day))
    X_add.loc[pats_use, 'DOI'] = all_DOIs

    X_add.loc[pats_use, 'TestDate_Mot'] = pd.to_datetime(X_add_load['TestDate_Mot'], format='%d.%m.%y')

    X_add.loc[pats_use, 'Days_to_Mot'] = (X_add.loc[pats_use, 'TestDate_Mot'] - X_add.loc[pats_use, 'DOI']).dt.days
    #X_add.loc[pats_use, 'Days_to_Mot'] = pd.to_numeric(X_add.loc[pats_use, 'Days_to_Mot'], downcast='integer')


    if len(plegia_field) > 0:
        X_add.loc[pats_use, 'plegia'] = X_add_load.loc[pats_use, plegia_field]
    else:
        raise ('plegia_field needs to be given!')

    if len(aisa_grade_field) > 0:
        X_add.loc[pats_use, 'aisa'] = X_add_load.loc[pats_use, aisa_grade_field]
        # X_add.loc[pats_use, 'aisa_rec'] = X_addrec.loc[pats_use, aisa_grade_field]
    else:
        raise ('aisa_grade_field needs to be given!')

    if len(nli_field) > 0:
        # Calculate own NLI - level of first non intact myotome or dermatome

        X_add.loc[pats_use, 'nli'] = X_add_load.loc[pats_use, nli_field]



    else:
        raise ('nli_field needs to be given!')
    if len(age_field) > 0:
        X_add.loc[pats_use, 'age'] = X_add_load.loc[pats_use, age_field]
    if len(sex_field) > 0:
        X_add.loc[pats_use, 'sex'] = X_add_load.loc[pats_use, sex_field]
    if len(cause_field) > 0:
        X_add.loc[pats_use, 'cause'] = X_add_load.loc[pats_use, cause_field]
    if len(nlicoarse_field) > 0:
        X_add.loc[pats_use, 'nli_coarse'] = X_add_load.loc[pats_use, nlicoarse_field]
    if len(Vac_field) > 0:
        X_add.loc[pats_use, 'VAC'] = X_add_load.loc[pats_use, Vac_field].map(dict(Yes=1, No=0))
    if len(DAP_field) > 0:
        X_add.loc[pats_use, 'DAP'] = X_add_load.loc[pats_use, DAP_field].map(dict(Yes=1, No=0))

    # Get nli indices for MS (in order as anticipated!)
    nli_ind_all = []
    for p in pats_use:
        try:
            this_ind_nli = dermatomes_toMatch.index(X_add_load.loc[p, nli_field])
        except:
            try:
                if np.isnan(X_add_load.loc[p, nli_field]):
                    this_ind_nli = 0
            except:
                if X_add_load.loc[p, nli_field] == 'C1' or X_add_load.loc[p, nli_field] == 'INT':
                    this_ind_nli = 0

        ms_blnli = [value for value in dermatomes_toMatch[this_ind_nli:] if value in sc_TMS_toMatch]
        this_ind_nliMS = 2 * sc_TMS_toMatch.index(ms_blnli[0])

        nli_ind_all.append(this_ind_nliMS)
    X_add.loc[pats_use, 'nli_ind_MS'] = nli_ind_all

    X_add = defineNLI(X_add, X, X_LTS, X_PPS, sc_TMS_toMatch, dermatomes_toMatch, ms_fields)

    for sub in useToSubset:
        # Summary scores
        if sub == 'LEMS':
            X_add.loc[pats_use, 'LEMS'] = X.loc[pats_use, ms_fields[10:]].sum(axis=1)

        if sub == 'MeanScBlNLI':
            for p in pats_use:
                X_add.loc[p, 'MeanScBlNLI'] = get_MeanScBlNLI(p, X_add, ms_fields, X)

        if sub == 'PropSc':
            X_add.loc[pats_use, 'PropSc'] = X.loc[pats_use, ms_fields[10:]].sum(axis=1)

    return X_add


def defineNLI(X_add_toMatch, X_MS_toMatch, X_LTS_toMatch, X_PPS_toMatch, sc_TMS_toMatch, dermatomes_toMatch,
              ms_fields_toMatch):
    # get new NLI
    all_indMS_nli = []
    nli_score = []
    for p in tqdm.tqdm(X_add_toMatch.index):
        inds = np.where(np.logical_or((X_LTS_toMatch.loc[p, :] < 2), (X_PPS_toMatch.loc[p, :] < 2)))[0]
        if len(inds) > 0:
            derm_notInt = X_LTS_toMatch.columns[inds[0]]
            this_ind_nli = dermatomes_toMatch.index(derm_notInt[:-2])
            if this_ind_nli > 24:
                this_ind_nliMS = len(sc_TMS_toMatch) + 1
            else:
                ms_blnli = [value for value in dermatomes_toMatch[this_ind_nli:] if value in sc_TMS_toMatch]
                this_ind_nliMS = 2 * sc_TMS_toMatch.index(ms_blnli[0])
        else:
            this_ind_nliMS = len(sc_TMS_toMatch) + 1
            this_ind_nli = len(dermatomes_toMatch) + 1

        inds_MS = np.where(X_MS_toMatch.loc[p, ms_fields_toMatch] < 5)[0]
        if len(inds_MS) > 0:
            myonot_notInt_ind = inds_MS[0]
        else:
            myonot_notInt_ind = len(sc_TMS_toMatch) + 1
        ind_nli_notInt = np.min([this_ind_nliMS, myonot_notInt_ind])

        derm_ind_newnli = dermatomes_toMatch.index(sc_TMS_toMatch[int(ind_nli_notInt / 2)])
        derm_nli = dermatomes_toMatch[np.min([derm_ind_newnli, this_ind_nli])]
        nli_score.append(derm_nli)

        # convert derm to myotom
        all_indMS_nli.append(ind_nli_notInt)

    X_add_toMatch['nli_ind_MS'] = all_indMS_nli
    X_add_toMatch['nli'] = nli_score
    return X_add_toMatch


# For Twin matching
def generate_pat_uncert(df_uncert, this_X_use, n_bootstrap, nli_ind):
    ms_true = this_X_use.values

    # initialize dataframe and random number generator - ensure the same seed for all patients
    rng = np.random.default_rng(seed=0)
    df_new = pd.DataFrame()

    # draw samples for each of the scores below the nli
    for i, this_ms in enumerate(ms_true):

        if i < nli_ind:
            sc_boot = [this_ms for this_n_rand in range(0, n_bootstrap)]

        else:

            # Draw random numbers
            n_rands = rng.random(n_bootstrap)  # np.random.uniform(0, 1, n_bootstrap)

            # get score pdf
            sc_prop = df_uncert.loc[this_ms, :].cumsum()

            # Assign score based on random numbers
            sc_boot = [int(sc_prop.index[(sc_prop > this_n_rand).values][0]) for this_n_rand in n_rands]

        # add to dataframe
        df_new.loc[:, this_X_use.index[i]] = sc_boot

    return df_new


def getPatChar(useToSubset, this_X_add):
    # get the patient characterisitces
    this_char = []
    for sub in useToSubset:
        try:
            this_char.append(this_X_add.loc[sub])
        except:
            this_char.append(np.nan)

    return this_char


def getTwin(useToSubset, data_Knn_use, this_X_use, X_add_ref_use, this_char, useKNN, n_NN, useClosestTwin, useMean,
            Y_MS_ref, X_MS_ref, allowScoreDoubling, sc_tol, this_ind_nliMS, addMatch_tol, metric_knn='hamming'):
    # First generate patient subset
    addMatchPos = 1
    data_sub = X_add_ref_use.copy()
    for i_sc, sub in enumerate(useToSubset):
        this_sc = this_char[i_sc]

        if sub in ['RMSEblNLI']:
            this_X_MS = this_X_use[:20]
            this_rmse = [mean_squared_error(X_MS_ref.loc[i, :][int(this_ind_nliMS):].values,
                                            this_X_MS[int(this_ind_nliMS):].values * 5,
                                            squared=False) for i in list(data_sub.index)]
            inds_keep = np.where(np.array(this_rmse) <= sc_tol)[0]
            data_sub_sc = data_sub.iloc[inds_keep]
            if data_sub_sc.shape[0] >= 1:
                data_sub = data_sub_sc
            else:
                print(sub + ' match not possible')
                return [], None, None, [], addMatchPos

        if sub in ['LEMS', 'MeanScBlNLI']:

            data_sub_sc = data_sub[data_sub[sub] >= this_sc - sc_tol]
            data_sub_sc = data_sub_sc[data_sub_sc[sub] <= this_sc + sc_tol]

            if data_sub_sc.shape[0] >= 1:
                data_sub = data_sub_sc
            else:
                if allowScoreDoubling:
                    print('Doubled score tolerance!')
                    data_sub_sc = data_sub[data_sub[sub] >= this_sc - sc_tol * 2]
                    data_sub_sc = data_sub_sc[data_sub_sc[sub] <= this_sc + sc_tol * 2]
                    if data_sub_sc.shape[0] >= 1:
                        data_sub = data_sub_sc
                    else:
                        print(sub + ' match not possible')
                        return [], None, None, []
                else:
                    print(sub + ' match not possible')
                    return [], None, None, [], addMatchPos

        elif sub == 'age':
            if addMatch_tol > 0:
                data_sub_age = data_sub[data_sub['age'] >= this_sc - (10 + addMatch_tol * 5)]
                data_sub_age = data_sub_age[data_sub_age['age'] <= this_sc + (10 + addMatch_tol * 5)]
            else:
                data_sub_age = data_sub[data_sub['age'] >= this_sc - (10 + addMatch_tol * 5)]
                data_sub_age = data_sub_age[data_sub_age['age'] <= this_sc + (10 + addMatch_tol * 5)]

            if data_sub_age.shape[0] >= 1:
                data_sub = data_sub_age.copy()
            else:
                print('Age match not possible but I continue')

        elif sub == 'sex':
            data_sub_sex = data_sub[data_sub['sex'] == this_sc]

            if data_sub_sex.shape[0] >= 1:
                data_sub = data_sub_sex.copy()
            else:
                print('Sex match not possible but I continue')

        elif sub in ['aisa', 'nli_coarse', 'cause', 'nli']:

            if addMatch_tol > 0:

                if sub == 'nli':
                    nli_pos = ['C1', 'C2', 'C3', 'C4', 'C5', 'C6', 'C7', 'C8', 'T1', 'T2', 'T3',
                               'T4', 'T5', 'T6', 'T7', 'T8', 'T9', 'T10', 'T11', 'T12',
                               'L1', 'L2', 'L3', 'L4', 'L5', 'S1', 'S2', 'S3', 'S45']
                    ind_start = np.max([0, nli_pos.index(this_sc) - addMatch_tol])
                    ind_end = np.min([len(nli_pos), nli_pos.index(this_sc) + addMatch_tol + 1])
                    nlis_use = nli_pos[ind_start: ind_end]

                    inds = [(n in nlis_use) for n in data_sub['nli']]
                    data_sub_ais = data_sub[inds]

                if sub in ['cause', 'aisa', 'nli_coarse']:
                    data_sub_ais = data_sub
            else:
                data_sub_ais = data_sub[data_sub[sub] == this_sc]

            if data_sub_ais.shape[0] > 1:
                data_sub = data_sub_ais
            else:
                print(sub + ' match not possible!')
                addMatchPos = 0
                return [], None, None, [], addMatchPos
        else:
            continue

    # Check if any possible matches remain
    if data_sub.shape[0] > 0:

        if metric_knn == 'hamming':
            thisdist = (pairwise_distances(data_Knn_use.loc[data_sub.index],
                                           np.expand_dims(np.array(this_X_use), axis=0), metric='hamming') *
                        data_Knn_use.loc[data_sub.index].shape[1])  # .astype(int)
        else:
            thisdist = (pairwise_distances(data_Knn_use.loc[data_sub.index],
                                           np.expand_dims(np.array(this_X_use), axis=0), metric='euclidean') *
                        data_Knn_use.loc[data_sub.index].shape[1])  # .astype(int)

        # Find all 'neighbors'
        if useKNN:

            n_inds_all = sorted(range(len(thisdist)), key=lambda k: thisdist[k])[:n_NN]

            if useClosestTwin:
                inds_minDist = list(np.where(thisdist == np.min(thisdist))[0])
                min_dist = np.mean([thisdist[j][0] for j in inds_minDist])
                pats_neigh = list(data_Knn_use.loc[data_sub.index].index[inds_minDist].values)
            else:

                # use k nearest neighbours
                if n_NN > len(n_inds_all):
                    this_inds_use = n_inds_all[:n_NN]
                else:
                    this_inds_use = n_inds_all

                min_dist = np.mean([thisdist[j][0] for j in this_inds_use])
                pats_neigh = list(data_Knn_use.loc[data_sub.index].index[this_inds_use].values)
        else:
            pats_neigh = data_sub.index
            min_dist = np.mean(thisdist)

        # Calcualte the mean from them
        y_neigh = Y_MS_ref.loc[pats_neigh, :]
        x_neigh = X_MS_ref.loc[pats_neigh, :]

        if useMean:
            twin_mean = y_neigh.mean().values
            twin_mean_X = x_neigh.mean().values
        else:
            twin_mean = y_neigh.median().values
            twin_mean_X = x_neigh.median().values

        return twin_mean, min_dist, pats_neigh, twin_mean_X, addMatchPos

    else:
        addMatchPos = 1
        return [], None, None, [], addMatchPos


def getTwin_scoresOnly(data_Knn_use, this_X_use,  useKNN, n_NN, useClosestTwin, useMean,
            Y_MS_ref, X_MS_ref, metric_knn='hamming'):

    # First generate patient subset
    data_sub = X_MS_ref.copy()
    addMatchPos = 1
    # for i_sc, sub in enumerate(useToSubset):
    #
    #     this_sc = this_char[i_sc]
    #     if sub in ['RMSEblNLI']:
    #         this_X_MS = this_X_use[:20]
    #         this_rmse = [mean_squared_error(X_MS_ref.loc[i, :][int(this_ind_nliMS):].values,
    #                                         this_X_MS[int(this_ind_nliMS):].values * 5,
    #                                         squared=False) for i in pats_ref]
    #         inds_keep = np.where(np.array(this_rmse) <= sc_tol)[0]
    #         data_sub_sc = data_sub.iloc[inds_keep]
    #         if data_sub_sc.shape[0] >= 1:
    #             data_sub = data_sub_sc
    #         else:
    #             print(sub + ' match not possible')
    #             return [], None, None, [],addMatchPos
    #     if sub in ['LEMS', 'MeanScBlNLI']:
    #
    #         data_sub_sc = data_sub[data_sub[sub] >= this_sc - sc_tol]
    #         data_sub_sc = data_sub_sc[data_sub_sc[sub] <= this_sc + sc_tol]
    #
    #         if data_sub_sc.shape[0] >= 1:
    #             data_sub = data_sub_sc
    #         else:
    #             if allowScoreDoubling:
    #                 print('Doubled score tolerance!')
    #                 data_sub_sc = data_sub[data_sub[sub] >= this_sc - sc_tol * 2]
    #                 data_sub_sc = data_sub_sc[data_sub_sc[sub] <= this_sc + sc_tol * 2]
    #                 if data_sub_sc.shape[0] >= 1:
    #                     data_sub = data_sub_sc
    #                 else:
    #                     print(sub + ' match not possible')
    #                     return [], None, None, []
    #             else:
    #                 print(sub + ' match not possible')
    #                 return [], None, None, [], addMatchPos

    # Check if any possible matches remain
    if data_sub.shape[0] > 0:

        if metric_knn == 'hamming':
            thisdist = (pairwise_distances(data_Knn_use.loc[data_sub.index],
                                           np.expand_dims(np.array(this_X_use), axis=0), metric='hamming') *
                        data_Knn_use.loc[data_sub.index].shape[1])  # .astype(int)
        elif metric_knn == 'euclidean':
            thisdist = (pairwise_distances(data_Knn_use.loc[data_sub.index],
                                           np.expand_dims(np.array(this_X_use), axis=0), metric='euclidean') *
                        data_Knn_use.loc[data_sub.index].shape[1])  # .astype(int)
        else:
            raise('Invalid distance metric - has to be either hamming or euclidean')

        # Find all 'neighbors'
        if useKNN:

            n_inds_all = sorted(range(len(thisdist)), key=lambda k: thisdist[k])[:n_NN]

            if useClosestTwin:
                inds_minDist = list(np.where(thisdist == np.min(thisdist))[0])
                min_dist = np.mean([thisdist[j][0] for j in inds_minDist])
                pats_neigh = list(data_Knn_use.loc[data_sub.index].index[inds_minDist].values)
            else:

                # use k nearest neighbours
                if n_NN > len(n_inds_all):
                    this_inds_use = n_inds_all[:n_NN]
                else:
                    this_inds_use = n_inds_all

                min_dist = np.mean([thisdist[j][0] for j in this_inds_use])
                pats_neigh = list(data_Knn_use.loc[data_sub.index].index[this_inds_use].values)
        else:
            pats_neigh = data_sub.index
            min_dist = np.mean(thisdist)

        # Calcualte the mean from them
        y_neigh = Y_MS_ref.loc[pats_neigh, :]
        x_neigh = X_MS_ref.loc[pats_neigh, :]

        if useMean:
            twin_mean = y_neigh.mean().values
            twin_mean_X = x_neigh.mean().values
        else:
            twin_mean = y_neigh.median().values
            twin_mean_X = x_neigh.median().values

        return twin_mean, min_dist, pats_neigh, twin_mean_X, addMatchPos

    else:
        addMatchPos = 1
        return [], None, None, [], addMatchPos

# Evaluation
def getAISgrade(X_MS, X_LTS, X_PPS, X_add, dermatomes_toMatch, ms_fields_toMatch, emsci_data=[], ais_assigned=[]):
    # Emsci TP 2W: 201 changed!
    ais_grades = []
    changedPats = []  # 201
    ais_grades_newDef = []
    changedPats_newDef = []  # 701
    for p in tqdm.tqdm(X_MS.index):

        try:
            this_vac = emsci_data.loc[:, 'VAC'].map({'No': 0, 'Yes': 1}).loc[p]
            this_dap = emsci_data.loc[:, 'DAP'].map({'No': 0, 'Yes': 1}).loc[p]
        except:
            this_vac = X_add.loc[p, 'VAC']
            this_dap = X_add.loc[p, 'DAP']

        ind_NLI_MS = X_add.loc[p, 'nli_ind_MS']

        derms_blNLI = [d + '_l' for d in dermatomes_toMatch[dermatomes_toMatch.index(X_add.loc[p, 'nli']):]] + \
                      [d + '_r' for d in dermatomes_toMatch[dermatomes_toMatch.index(X_add.loc[p, 'nli']):]]

        this_ms = X_MS.loc[p, ms_fields_toMatch].values
        ms_bl_nli = this_ms[ind_NLI_MS:].astype(float)
        ss_bl_nli = list(X_LTS.loc[p, derms_blNLI].astype(float)) + list(X_PPS.loc[p, derms_blNLI].astype(float))

        # ss_bl_nli_vac = ss_bl_nli+[this_vac,this_dap]
        ss_S45_bl_nli = list(X_LTS.loc[p, ['S45_r', 'S45_l']].astype(float)) + list(
            X_PPS.loc[p, ['S45_r', 'S45_l']].astype(float))

        # My definition
        # Ais A: No sensory and no motor function below the NLI
        this_ais = np.nan
        if np.sum([this_vac, this_dap]) == 0:
            if np.sum(ss_S45_bl_nli) > 0:
                this_ais = 'B'
            else:
                this_ais = 'A'
        else:

            # Ais B: sensory but no motor function below the NLI
            if (np.sum(ms_bl_nli) == 0):
                this_ais = 'B'

            # AIS C: in more than half of the MS below the NLI the MS is <3
            elif (np.sum(ms_bl_nli >= 3) < 0.5 * len(ms_bl_nli)):
                this_ais = 'C'

            # AIS D: in more than half of the MS below the NLI the MS is >=3
            elif (np.sum(ms_bl_nli >= 3) >= 0.5 * len(ms_bl_nli)):

                # AIS E: normal motor and sensory function
                if (np.sum(ss_bl_nli) == 2 * len(ss_bl_nli)) & (np.sum(ms_bl_nli) == 5 * len(ms_bl_nli)):
                    this_ais = 'E'
                else:
                    this_ais = 'D'

        if ais_assigned.loc[p] != this_ais:
            changedPats_newDef.append(p)
        ais_grades_newDef.append(this_ais)

        # Try to implement ISNCSCI
        this_ais = np.nan
        if np.isnan(this_vac + this_dap) or (this_vac + this_dap) > 0:

            # Ais B: sensory but no motor function below the NLI
            if (np.sum(ms_bl_nli) == 0):
                this_ais = 'B'

            # AIS C: in more than half of the MS below the NLI the MS is <3
            elif (np.sum(ms_bl_nli >= 3) <= 0.5 * len(ms_bl_nli)):
                this_ais = 'C'

            # AIS D: in more than half of the MS below the NLI the MS is >=3
            elif (np.sum(ms_bl_nli >= 3) > 0.5 * len(ms_bl_nli)):

                # AIS E: normal motor and sensory function
                if (np.sum(ss_bl_nli) == 2 * len(ss_bl_nli)) & (np.sum(ms_bl_nli) == 5 * len(ms_bl_nli)):
                    this_ais = 'E'
                else:
                    this_ais = 'D'

        else:
            # no DAP/VAC
            # Check S4/5
            if (np.sum(ss_S45_bl_nli) > 0):
                this_ais = 'B'
            else:
                this_ais = 'A'

        if ((np.sum(ss_bl_nli) == 0) & (np.sum(ms_bl_nli) == 0)) | ((this_dap == 0) & (this_vac == 0)):
            this_ais = 'A'

        else:

            # Ais B: sensory but no motor function below the NLI
            if (np.sum(ss_bl_nli) > 0) & (np.sum(ms_bl_nli) == 0):
                this_ais = 'B'

            # AIS C: in more than half of the MS below the NLI the MS is <3
            elif (np.sum(ss_bl_nli) > 0) & (np.sum(ms_bl_nli >= 3) <= 0.5 * len(ms_bl_nli)):
                this_ais = 'C'

            # AIS D: in more than half of the MS below the NLI the MS is >=3
            elif (np.sum(ss_bl_nli) > 0) & (np.sum(ms_bl_nli >= 3) > 0.5 * len(ms_bl_nli)):
                this_ais = 'D'

            # AIS E: normal motor and sensory function
            elif (np.sum(ss_bl_nli) == 2 * len(ss_bl_nli)) & (np.sum(ms_bl_nli) == 5 * len(ms_bl_nli)):
                this_ais = 'E'
            else:
                print('AIS grade failed for ' + str(p))
                this_ais = np.nan

        # Check with assigned AIS grade
        if ais_assigned.loc[p] != this_ais:
            changedPats.append(p)
            # print('Missmatch for pat '+str(p))
        ais_grades.append(this_ais)

    X_add['aisa_corr'] = ais_grades
    X_add['aisa_corrSarah'] = ais_grades_newDef

    print(len(changedPats))
    print(len(changedPats_newDef))
    return X_add, changedPats, changedPats_newDef


def get_eval_scores(this_Y, this_pred, this_ind_nliMS):
    # RMSE
    this_rmse = mean_squared_error(this_Y[this_ind_nliMS:], this_pred[this_ind_nliMS:], squared=False)

    # LEMS
    lems_true = this_Y[10:].sum()
    lems_pred = this_pred[10:].sum()
    this_deltaLEMS = (lems_true - lems_pred)

    # non linear score
    # this_nonlin = nonlineScore(this_pred[this_ind_nliMS:], this_Y[this_ind_nliMS:])
    this_nonlin = np.nan

    return this_rmse, this_deltaLEMS, this_nonlin


def softplus(x):
    y = np.log(1 + np.exp(x))
    return y


def linSc(sc):
    b = 0.4
    a = 99.63
    y = a * sc / (sc + b)
    y[sc < 0] = 0
    return y


def nonlineScore(y_pred, y_true):
    target_low = y_true
    delta_top = linSc(y_pred) - linSc(target_low + 0.5)
    delta_bot = linSc((target_low - 0.5) * np.heaviside(target_low - 0.5, 0.5)) - linSc(y_pred)
    nonlinSc = np.sum((softplus(delta_top) + softplus(delta_bot)))

    return nonlinSc


# For plotting
def plotIndividualPatTraj_2sidesManyModels(p, ms_use, Y_MS, X_MS,
                                           df_med_lst, df_medX_lst, use_thick,
                                           labels, savepath, name='x', doSave=True):
    if name == 'x':
        name = p

    ms_use_l = [m for m in ms_use if m[-1] == 'l']
    ms_use_r = [m for m in ms_use if m[-1] == 'r']
    colors = ['red', 'black', 'blue', 'orange', 'green', 'yellow']

    plt.figure(figsize=(8, 8))
    plt.subplot(2, 2, 1)
    plt.plot(ms_use_l, X_MS.loc[p, ms_use_l], 'ko-', label='True')
    for i in range(0, len(df_medX_lst)):
        plt.plot(ms_use_l, df_medX_lst[i].loc[p, ms_use_l], '-', color=colors[use_thick[i]], alpha=0.25)

    plt.ylim((0, 6))
    plt.ylabel('MS left - acute')

    plt.subplot(2, 2, 2)
    plt.plot(ms_use_r, X_MS.loc[p, ms_use_r], 'ko-', label='True')
    for i in range(0, len(df_medX_lst)):
        plt.plot(ms_use_r, df_medX_lst[i].loc[p, ms_use_r], '-', color=colors[use_thick[i]], alpha=0.25)

    plt.ylim((0, 6))
    plt.ylabel('MS right - acute')

    plt.subplot(2, 2, 3)
    plt.plot(ms_use_l, Y_MS.loc[p, ms_use_l], 'ko-', label='True')
    for i in range(0, len(df_med_lst)):
        plt.plot(ms_use_l, df_med_lst[i].loc[p, ms_use_l], '-', color=colors[use_thick[i]], alpha=0.25)

    plt.ylim((0, 6))
    plt.ylabel('MS left - recovered')

    plt.subplot(2, 2, 4)
    plt.plot(ms_use_r, Y_MS.loc[p, ms_use_r], 'ko-', label='True')
    for i in range(0, len(df_med_lst)):
        plt.plot(ms_use_r, df_med_lst[i].loc[p, ms_use_r], '-', color=colors[use_thick[i]], alpha=0.25)

    plt.ylim((0, 6))
    plt.ylabel('MS right - recovered')
    plt.legend()

    plt.suptitle(p, fontsize=14)
    plt.tight_layout()
    if doSave:
        plt.savefig(os.path.join(savepath, name + '_MS.png'))


def plotIndividualPatTraj_1side(p, ms_use, Y_MS, X_MS,
                                df_med_lst, df_low_lst, df_top_lst,
                                df_medX_lst, df_lowX_lst, df_topX_lst,
                                labels, savepath, name='x', doSave=True, color='k'):
    if name == 'x':
        name = p
    colors = [color, 'green', 'blue', 'yellow', 'orange', 'purple', 'lightblue', 'coral']
    ms_use_l = [m for m in ms_use if m[-1] == 'l']
    ms_use_r = [m for m in ms_use if m[-1] == 'r']
    ms_plt_label = [m[:-2] for m in ms_use if m[-1] == 'l']

    fig = plt.figure(figsize=(3.5, 2))
    plt.subplot(1, 2, 1)
    plt.plot(ms_plt_label, X_MS.loc[p, ms_use_l], 'o-', color='darkred', label='True', alpha=0.8)
    for i in range(0, len(df_medX_lst)):
        plt.plot(ms_plt_label, df_medX_lst[i].loc[p, ms_use_l], 'o-', color=colors[i], label=labels[i], alpha=0.5)

        try:
            plt.fill_between(ms_plt_label, df_lowX_lst[i].loc[p, ms_use_l].values.astype(float),
                             df_topX_lst[i].loc[p, ms_use_l].values.astype(float), alpha=0.25, linewidth=0,
                             zorder=3,
                             color=colors[i])
        except:
            continue

    plt.ylim((-0.2, 5.8))
    plt.ylabel('MS left')
    plt.xticks(np.arange(0, 10, step=2), [ms_plt_label[im] for im in np.arange(0, 10, step=2)], rotation=0)

    # plt.setp(cbar.ax.get_xticklabels()[::2], visible=False)
    plt.title('acute')

    # plt.subplot(2, 2, 2)
    # plt.plot(ms_use_r, X_MS.loc[p, ms_use_r], 'ko-', label='True')
    # for i in range(0, len(df_medX_lst)):
    #     plt.plot(ms_use_r, df_medX_lst[i].loc[p, ms_use_r], 'o-', color=colors[i], label=labels[i], alpha=0.5)
    #
    #     try:
    #         plt.fill_between(ms_use_r, df_lowX_lst[i].loc[p, ms_use_r].values.astype(float),
    #                          df_topX_lst[i].loc[p, ms_use_r].values.astype(float), alpha=0.25, linewidth=0,
    #                          zorder=3,
    #                          color=colors[i])
    #     except:
    #         continue
    #
    # plt.ylim((0, 6))
    # plt.ylabel('MS right - acute')

    plt.subplot(1, 2, 2)
    plt.plot(ms_plt_label, Y_MS.loc[p, ms_use_l], 'o-', color='darkred', label='True', alpha=0.8)
    for i in range(0, len(df_med_lst)):
        plt.plot(ms_plt_label, df_med_lst[i].loc[p, ms_use_l], 'o-', color=colors[i], label=labels[i], alpha=0.5)

        try:
            plt.fill_between(ms_plt_label, df_low_lst[i].loc[p, ms_use_l].values.astype(float),
                             df_top_lst[i].loc[p, ms_use_l].values.astype(float), alpha=0.25, linewidth=0,
                             zorder=3,
                             color=colors[i])
        except:
            continue

    plt.ylim((-0.2, 5.8))
    # plt.ylabel('MS left')
    plt.title('recovered')
    plt.xticks(np.arange(0, 10, step=2), [ms_plt_label[im] for im in np.arange(0, 10, step=2)], rotation=0)

    # plt.subplot(2, 2, 4)
    # plt.plot(ms_use_r, Y_MS.loc[p, ms_use_r], 'ko-', label='True')
    # for i in range(0, len(df_med_lst)):
    #     plt.plot(ms_use_r, df_med_lst[i].loc[p, ms_use_r], 'o-', color=colors[i], label=labels[i], alpha=0.5)
    #
    #     try:
    #         plt.fill_between(ms_use_r, df_low_lst[i].loc[p, ms_use_r].values.astype(float),
    #                          df_top_lst[i].loc[p, ms_use_r].values.astype(float), alpha=0.25, linewidth=0,
    #                          zorder=3,
    #                          color=colors[i])
    #     except:
    #         continue
    #
    # plt.ylim((0, 6))
    # plt.ylabel('MS right - recovered')
    # plt.legend()

    # plt.suptitle(p, fontsize=14)
    plt.tight_layout()
    if doSave:
        plt.savefig(os.path.join(savepath, name + '_MS.png'))


def plotIndividualPatTraj_1side_sep(p, ms_use, Y_MS, X_MS,
                                    df_med_lst, df_low_lst, df_top_lst,
                                    df_medX_lst, df_lowX_lst, df_topX_lst,
                                    labels, savepath, name='x', doSave=True, color='k'):
    if name == 'x':
        name = p
    colors = [color, 'green', 'blue', 'yellow', 'orange', 'purple', 'lightblue', 'coral']
    ms_use_l = [m for m in ms_use if m[-1] == 'l']
    ms_use_r = [m for m in ms_use if m[-1] == 'r']
    ms_plt_label = [m[:-2] for m in ms_use if m[-1] == 'l']

    plt.figure(figsize=(2, 2))
    plt.plot(ms_plt_label, X_MS.loc[p, ms_use_l], 'o-', color='darkred', label='True', alpha=0.8)
    for i in range(0, len(df_medX_lst)):
        plt.plot(ms_plt_label, df_medX_lst[i].loc[p, ms_use_l], 'o-', color=colors[i], label=labels[i], alpha=0.5)

        try:
            plt.fill_between(ms_plt_label, df_lowX_lst[i].loc[p, ms_use_l].values.astype(float),
                             df_topX_lst[i].loc[p, ms_use_l].values.astype(float), alpha=0.25, linewidth=0,
                             zorder=3,
                             color=colors[i])
        except:
            continue

    plt.ylim((-0.2, 5.8))
    plt.ylabel('MS left')
    plt.yticks(np.arange(0, 6), [im for im in np.arange(0, 6)], rotation=0)
    plt.xticks(np.arange(0, 10, step=2), [ms_plt_label[im] for im in np.arange(0, 10, step=2)], rotation=0)
    plt.title('acute')
    plt.tight_layout()
    if doSave:
        plt.savefig(os.path.join(savepath, name + '_MS_acute_left.png'))

    plt.figure(figsize=(2, 2))
    plt.plot(ms_plt_label, X_MS.loc[p, ms_use_r], 'o-', color='darkred', label='True', alpha=0.8)
    for i in range(0, len(df_medX_lst)):
        plt.plot(ms_plt_label, df_medX_lst[i].loc[p, ms_use_r], 'o-', color=colors[i], label=labels[i], alpha=0.5)

        try:
            plt.fill_between(ms_plt_label, df_lowX_lst[i].loc[p, ms_use_r].values.astype(float),
                             df_topX_lst[i].loc[p, ms_use_r].values.astype(float), alpha=0.25, linewidth=0,
                             zorder=3,
                             color=colors[i])
        except:
            continue
    plt.ylim((-0.2, 5.8))
    plt.ylabel('MS right')
    plt.yticks(np.arange(0, 6), [im for im in np.arange(0, 6)], rotation=0)
    plt.xticks(np.arange(0, 10, step=2), [ms_plt_label[im] for im in np.arange(0, 10, step=2)], rotation=0)
    plt.title('acute')
    plt.tight_layout()
    if doSave:
        plt.savefig(os.path.join(savepath, name + '_MS_acute_right.png'))

    plt.figure(figsize=(2, 2))
    plt.plot(ms_plt_label, Y_MS.loc[p, ms_use_l], 'o-', color='darkred', label='True', alpha=0.8)
    for i in range(0, len(df_med_lst)):
        plt.plot(ms_plt_label, df_med_lst[i].loc[p, ms_use_l], 'o-', color=colors[i], label=labels[i], alpha=0.5)

        try:
            plt.fill_between(ms_plt_label, df_low_lst[i].loc[p, ms_use_l].values.astype(float),
                             df_top_lst[i].loc[p, ms_use_l].values.astype(float), alpha=0.25, linewidth=0,
                             zorder=3,
                             color=colors[i])
        except:
            continue
    plt.ylim((-0.2, 5.8))
    plt.title('recovered')
    plt.ylabel('MS left')
    plt.xticks(np.arange(0, 10, step=2), [ms_plt_label[im] for im in np.arange(0, 10, step=2)], rotation=0)
    plt.yticks(np.arange(0, 6), [im for im in np.arange(0, 6)], rotation=0)
    plt.tight_layout()
    if doSave:
        plt.savefig(os.path.join(savepath, name + '_MS_recovered_left.png'))

    plt.figure(figsize=(2, 2))
    plt.plot(ms_plt_label, Y_MS.loc[p, ms_use_r], 'o-', color='darkred', label='True', alpha=0.8)
    for i in range(0, len(df_med_lst)):
        plt.plot(ms_plt_label, df_med_lst[i].loc[p, ms_use_r], 'o-', color=colors[i], label=labels[i], alpha=0.5)

        try:
            plt.fill_between(ms_plt_label, df_low_lst[i].loc[p, ms_use_r].values.astype(float),
                             df_top_lst[i].loc[p, ms_use_r].values.astype(float), alpha=0.25, linewidth=0,
                             zorder=3,
                             color=colors[i])
        except:
            continue
    plt.ylim((-0.2, 5.8))
    plt.title('recovered')
    plt.ylabel('MS right')
    plt.xticks(np.arange(0, 10, step=2), [ms_plt_label[im] for im in np.arange(0, 10, step=2)], rotation=0)
    plt.yticks(np.arange(0, 6), [im for im in np.arange(0, 6)], rotation=0)
    plt.tight_layout()
    if doSave:
        plt.savefig(os.path.join(savepath, name + '_MS_recovered_right.png'))


def plotIndividualPatTraj_2sides(p, ms_use, Y_MS, X_MS,
                                 df_med_lst, df_low_lst, df_top_lst,
                                 df_medX_lst, df_lowX_lst, df_topX_lst,
                                 labels, savepath, name='x', doSave=True):
    if name == 'x':
        name = p
    colors = ['red', 'green', 'blue', 'yellow', 'orange', 'purple', 'lightblue', 'coral']
    ms_use_l = [m for m in ms_use if m[-1] == 'l']
    ms_use_r = [m for m in ms_use if m[-1] == 'r']

    plt.figure(figsize=(8, 8))
    plt.subplot(2, 2, 1)
    plt.plot(ms_use_l, X_MS.loc[p, ms_use_l], 'ko-', label='True')
    for i in range(0, len(df_medX_lst)):
        plt.plot(ms_use_l, df_medX_lst[i].loc[p, ms_use_l], 'o-', color=colors[i], label=labels[i], alpha=0.5)

        try:
            plt.fill_between(ms_use_l, df_lowX_lst[i].loc[p, ms_use_l].values.astype(float),
                             df_topX_lst[i].loc[p, ms_use_l].values.astype(float), alpha=0.25, linewidth=0,
                             zorder=3,
                             color=colors[i])
        except:
            continue

    plt.ylim((0, 6))
    plt.ylabel('MS left - acute')

    plt.subplot(2, 2, 2)
    plt.plot(ms_use_r, X_MS.loc[p, ms_use_r], 'ko-', label='True')
    for i in range(0, len(df_medX_lst)):
        plt.plot(ms_use_r, df_medX_lst[i].loc[p, ms_use_r], 'o-', color=colors[i], label=labels[i], alpha=0.5)

        try:
            plt.fill_between(ms_use_r, df_lowX_lst[i].loc[p, ms_use_r].values.astype(float),
                             df_topX_lst[i].loc[p, ms_use_r].values.astype(float), alpha=0.25, linewidth=0,
                             zorder=3,
                             color=colors[i])
        except:
            continue

    plt.ylim((0, 6))
    plt.ylabel('MS right - acute')

    plt.subplot(2, 2, 3)
    plt.plot(ms_use_l, Y_MS.loc[p, ms_use_l], 'ko-', label='True')
    for i in range(0, len(df_med_lst)):
        plt.plot(ms_use_l, df_med_lst[i].loc[p, ms_use_l], 'o-', color=colors[i], label=labels[i], alpha=0.5)

        try:
            plt.fill_between(ms_use_l, df_low_lst[i].loc[p, ms_use_l].values.astype(float),
                             df_top_lst[i].loc[p, ms_use_l].values.astype(float), alpha=0.25, linewidth=0,
                             zorder=3,
                             color=colors[i])
        except:
            continue

    plt.ylim((0, 6))
    plt.ylabel('MS left - recovered')

    plt.subplot(2, 2, 4)
    plt.plot(ms_use_r, Y_MS.loc[p, ms_use_r], 'ko-', label='True')
    for i in range(0, len(df_med_lst)):
        plt.plot(ms_use_r, df_med_lst[i].loc[p, ms_use_r], 'o-', color=colors[i], label=labels[i], alpha=0.5)

        try:
            plt.fill_between(ms_use_r, df_low_lst[i].loc[p, ms_use_r].values.astype(float),
                             df_top_lst[i].loc[p, ms_use_r].values.astype(float), alpha=0.25, linewidth=0,
                             zorder=3,
                             color=colors[i])
        except:
            continue

    plt.ylim((0, 6))
    plt.ylabel('MS right - recovered')
    plt.legend()

    plt.suptitle(p, fontsize=14)
    plt.tight_layout()
    if doSave:
        plt.savefig(os.path.join(savepath, name + '_MS.png'))

def plotIndividualPatTraj_MS(p, Y_MS, X_MS,
                                 df_med_lst, df_low_lst, df_top_lst,
                                 df_medX_lst, df_lowX_lst, df_topX_lst,
                                 labels, savepath, name='x', doSave=True, timepoint = 0,
                                    timepoint_rec = 26 ):
    if name == 'x':
        name = p
    colors = ['red', 'green', 'blue', 'yellow', 'orange', 'purple', 'lightblue', 'coral']
    ms_use_l = []
    ms_use_r = []
    ms_label = []
    for m in X_MS.columns:
        if m[0] =='L':
            ms_use_l.append(m)
            ms_label.append(m.split('_')[1])
        else:
            ms_use_r.append(m)


    plt.figure(figsize=( 6,2.5))
    plt.subplot(1, 2, 1)
    plt.plot(ms_label, X_MS.loc[p, ms_use_l], 'ko-', label='True')
    for i in range(0, len(df_medX_lst)):
        plt.plot(ms_label, df_medX_lst[i].loc[p, ms_use_l], 'o-', color=colors[i], label=labels[i], alpha=0.5)

        try:
            plt.fill_between(ms_label, df_lowX_lst[i].loc[p, ms_use_l].values.astype(float),
                             df_topX_lst[i].loc[p, ms_use_l].values.astype(float), alpha=0.25, linewidth=0,
                             zorder=3,
                             color=colors[i])
        except:
            continue

    plt.ylim((0, 6))
    plt.ylabel('MS left - acute')


    plt.subplot(1, 2, 2)
    plt.plot(ms_label, X_MS.loc[p, ms_use_r], 'ko-', label='True')
    for i in range(0, len(df_medX_lst)):
        plt.plot(ms_label, df_medX_lst[i].loc[p, ms_use_r], 'o-', color=colors[i], label=labels[i], alpha=0.5)

        try:
            plt.fill_between(ms_label, df_lowX_lst[i].loc[p, ms_use_r].values.astype(float),
                             df_topX_lst[i].loc[p, ms_use_r].values.astype(float), alpha=0.25, linewidth=0,
                             zorder=3,
                             color=colors[i])
        except:
            continue
    plt.ylim((0, 6))
    plt.ylabel('MS right - match time')
    plt.legend()
    plt.tight_layout()
    if doSave:
        plt.savefig(os.path.join(savepath, name +str(timepoint)+'weeks'+ '_matchtimeMS.png'))

    plt.figure(figsize=(6,2.5))
    plt.subplot(1, 2, 1)
    try:
        plt.plot(ms_label, Y_MS.loc[p, ms_use_l], 'ko-', label='True')
    except:
        print('No validation')
    for i in range(0, len(df_med_lst)):
        plt.plot(ms_label, df_med_lst[i].loc[p, ms_use_l], 'o-', color=colors[i], label=labels[i], alpha=0.5)

        try:
            plt.fill_between(ms_label, df_low_lst[i].loc[p, ms_use_l].values.astype(float),
                             df_top_lst[i].loc[p, ms_use_l].values.astype(float), alpha=0.25, linewidth=0,
                             zorder=3,
                             color=colors[i])
        except:
            continue

    plt.ylim((0, 6))
    plt.ylabel('MS left - '+str(timepoint)+'weeks')

    plt.subplot(1, 2, 2)
    try:
        plt.plot(ms_label, Y_MS.loc[p, ms_use_r], 'ko-', label='True')
    except:
        print('No validation')
    for i in range(0, len(df_med_lst)):
        plt.plot(ms_label, df_med_lst[i].loc[p, ms_use_r], 'o-', color=colors[i], label=labels[i], alpha=0.5)

        try:
            plt.fill_between(ms_label, df_low_lst[i].loc[p, ms_use_r].values.astype(float),
                             df_top_lst[i].loc[p, ms_use_r].values.astype(float), alpha=0.25, linewidth=0,
                             zorder=3,
                             color=colors[i])
        except:
            continue

    plt.ylim((0, 6))
    plt.ylabel('MS right - '+str(timepoint)+'weeks')
    plt.legend()
    plt.tight_layout()
    if doSave:
        plt.savefig(os.path.join(savepath, name +str(timepoint_rec)+'weeks' '_recoveryMS.png'))


def plotAllScores_Compare3New(all_df, model_names, title, output_folder,
                              scores=['RMSEblNLI', 'LEMS_delta', 'nonlinSc_blNLI'],
                              titles=['RMSE bl.NLI', 'LEMS difference', 'linSc bl.NLI'],
                              bins_use=[np.linspace(0, 6, 13), np.linspace(-50, 50, 26), np.linspace(0, 500, 21)],
                              this_ais_str=['A', 'B', 'C', 'D'],
                              plegia_use=['tetra', 'para']):
    colors = ['red', 'green', 'blue', 'yellow', 'black', 'orange']
    for this_plegia in plegia_use:

        for isc, sc in enumerate(scores):

            ais_dict = dict(zip(['A', 'B', 'C', 'D'], ['1', '2', '3', '4']))

            this_ais_use = this_ais_str
            fig, axs, = plt.subplots(1, len(this_ais_use), figsize=(2.5 * len(this_ais_use), 3))
            for i, this_ais in enumerate(this_ais_use):
                # try:
                if len(this_ais_use) > 1:
                    axs_use = axs[i]
                else:
                    axs_use = axs

                for idf in range(0, len(all_df)):

                    try:
                        this_df = all_df[idf]
                        df_sub_pl = this_df[this_df.plegia == this_plegia]

                        df_sub = df_sub_pl[df_sub_pl.AIS == this_ais]
                        if len(df_sub) < 1:
                            df_sub = df_sub_pl[df_sub_pl.AIS == ais_dict[this_ais]]

                        if sc == 'RMSEblNLI':
                            axs_use.hist(df_sub[sc], alpha=0.5,
                                         label=model_names[idf] + " %.1f(%.1f,%.1f)" % (np.percentile(df_sub[sc], 50),
                                                                                        np.percentile(df_sub[sc], 2.5),
                                                                                        np.percentile(df_sub[sc],
                                                                                                      97.5)),
                                         density=True, color=colors[idf], bins=bins_use[isc])
                        else:
                            axs_use.hist(df_sub[sc], alpha=0.5,
                                         label=model_names[idf] + " %.0f(%.0f,%.0f)" % (np.percentile(df_sub[sc], 50),
                                                                                        np.percentile(df_sub[sc], 2.5),
                                                                                        np.percentile(df_sub[sc],
                                                                                                      97.5)),
                                         density=True, color=colors[idf], bins=bins_use[isc])
                    except:
                        continue

                # if i == 0:
                #     axs[i].set_title(titles[isc])
                axs_use.set_title(this_ais_str[i])
                axs_use.set_xlabel(titles[isc])
                axs_use.legend(fontsize=8)  # loc="lower center", bbox_to_anchor=(0.5, -0.5)

            plt.suptitle('Plegia: ' + this_plegia + '   Score: ' + sc, fontsize=14)
            plt.tight_layout()
            plt.savefig(os.path.join(output_folder, title + this_plegia + sc + '.png'))
