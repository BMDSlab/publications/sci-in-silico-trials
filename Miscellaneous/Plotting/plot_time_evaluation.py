import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
font_size = 16
plt.rcParams.update({'font.size':
                         font_size})
import seaborn as sns

from Miscellaneous.Data_postprocessing.calc_rmse_from_file import get_error_df, get_error_df_from_predictions
from Miscellaneous.Misc.paths import *


def split_dataframe(df, column_name, threshold=10):
    # Ensure the DataFrame is sorted by the time column
    df = df.sort_values(by=[column_name])

    # Calculate the differences between consecutive time values
    df['diff'] = df[column_name].diff().fillna(0)  # Fill NA for the first element

    # Find indices where the difference is 10 or more
    split_indices = df[df['diff'] >= threshold].index

    # Split DataFrame at identified indices and print split points
    list_of_dfs = []
    start_idx = 0
    for idx in split_indices:
        if start_idx != idx:  # Ensure we don't add empty DataFrames
            list_of_dfs.append(df.iloc[start_idx:idx])
            print(f"Splitting at index {idx}, value {df.at[idx, column_name]}")
        start_idx = idx

    list_of_dfs.append(df.iloc[start_idx:])  # Append the last segment

    return list_of_dfs


def plot_time_eval_2(model, augmented=False, read_plot_data=True, predictions=None, window=7, gap_allowance=4,
                     dataset=emsci_str, color='blue', debug=False):
    if read_plot_data:
        plot_data = pd.read_csv(get_time_eval_plot_path(model, with_uncertainty=augmented, dataset=dataset),
                                index_col=0)
    else:
        if predictions is not None:
            plot_data = get_error_df_from_predictions(predictions, dataset=dataset)
        else:
            plot_data = get_error_df(model, with_uncertainty=augmented, dataset=dataset, debug=debug)

    plot_data = plot_data.sort_values(by=timeX_str)
    df = plot_data[[timeX_str, rmse_str]]


    stats_method_1 = 'median'
    stats_method_2 = 'mean'

    daily_stats = df.groupby(timeX_str)[rmse_str].agg([
        stats_method_1,
        lambda x: np.percentile(x, 2.5),
        lambda x: np.percentile(x, 97.5)
    ])

    # Compute 7-day rolling statistics for median, 2.5th, and 97.5th percentiles
    aggregate = [
        stats_method_2,
    ]

    rolling_stats = daily_stats.rolling(window=window, min_periods=1).agg(aggregate)

    rolling_stats.columns = [stats_method_2, '2.5%', '97.5%']

    # add the 2.5th and 97.5th percentiles to the rolling_stats DataFrame

    # Clean up the DataFrame

    rolling_stats.reset_index(inplace=True)

    # split rolling_stats into groups (a list) where there is a gap of 10 days or more

    rolling_stats_list = split_dataframe(rolling_stats, timeX_str, threshold=gap_allowance)

    g = sns.JointGrid()
    # Plot the median, 2.5th, and 97.5th percentiles
    for x in rolling_stats_list:
        g.ax_joint.plot(x[timeX_str], x[stats_method_2], label=f'Rolling {window}-day average', c=color)
        # add a dashed line at the start value of the median
        g.ax_joint.fill_between(x[timeX_str], x['2.5%'], x['97.5%'],
                                alpha=0.2, color=color)

        # Plot the rolling statistics line plot on the joint axis

    g.ax_joint.set_xlabel('Days after injury')

    g.ax_joint.set_ylabel(r'$RMSE_{bl.NLI}$')

    # Create the histogram on the marginal axis
    sns.histplot(df[timeX_str], ax=g.ax_marg_x, color=color, kde=True)

    g.ax_joint.axhline(y=df['rmse'].median(), color='black', linestyle='--')
    # plot horizontal line of the mean


    g.ax_marg_x.set_ylabel('N')
    g.ax_marg_x.yaxis.label.set_visible(True)
    g.ax_marg_x.yaxis.set_major_locator(plt.MaxNLocator(3))  # This will set a fixed number of ticks
    g.ax_marg_x.yaxis.set_tick_params(labelleft=True)  # Explicitly turn on tick labels on the left

    # remove the line plot from the marginal axis
    g.ax_marg_x.lines[0].remove()

    # Keep only the first label in the legend

    # Adjust the plot to ensure it's not clipped
    g.fig.tight_layout()
    g.ax_joint.legend()
    handles, labels = g.ax_joint.get_legend_handles_labels()
    g.ax_joint.legend(handles[:1], labels[:1])

    # limit the y-axis to 0-3.5
    g.ax_joint.set_ylim(0, 3.5)
    # limit the x-axis to 0-100
    g.ax_joint.set_xlim(0, 100)

    # increase the font size of the plot

    # increase the font size of the x-axis labels
    plt.xticks(fontsize=font_size)
    # increase the font size of the y-axis labels
    plt.yticks(fontsize=font_size)
    folder = os.path.join(get_project_root(), 'Plots')
    Path(folder).mkdir(parents=True, exist_ok=True)
    plt.savefig(f'{folder}/{model}_augmented-{augmented}_time_eval_dataset-{dataset}.pdf')

    plt.show()


def plot_time_eval(model):
    plot_data = pd.read_csv(get_time_eval_plot_path(model), index_col=0)
    # Separate the data into two groups based on timeY_str at 250
    x1 = plot_data[plot_data[timeY_str] < 250]
    x2 = plot_data[plot_data[timeY_str] >= 250]
    fig, ax = plt.subplots(1, 2, figsize=(10, 5))
    ax1, ax2 = ax
    # Group and take median for each timeX_str

    n_bins = 10
    # Bin x1 and x2 based on timeX_str and take the median of each bin
    x1_binned = x1.groupby(pd.cut(x1[timeX_str], bins=n_bins)).median()
    x2_binned = x2.groupby(pd.cut(x2[timeX_str], bins=n_bins)).median()

    # Store the 95% confidence interval for each bin
    lower_confidence = 0.025
    upper_confidence = 0.975
    x1_lower = x1.groupby(pd.cut(x1[timeX_str], bins=n_bins))[rmse_str].quantile(lower_confidence)
    x1_upper = x1.groupby(pd.cut(x1[timeX_str], bins=n_bins))[rmse_str].quantile(upper_confidence)

    x2_lower = x2.groupby(pd.cut(x2[timeX_str], bins=n_bins))[rmse_str].quantile(lower_confidence)
    x2_upper = x2.groupby(pd.cut(x2[timeX_str], bins=n_bins))[rmse_str].quantile(upper_confidence)

    ax1.plot(x1_binned[timeX_str], x1_binned[rmse_str], 'k-', lw=2)
    ax2.plot(x2_binned[timeX_str], x2_binned[rmse_str], 'k-', lw=2)

    ax1.fill_between(x1_binned[timeX_str], x1_lower, x1_upper, alpha=0.5)
    ax2.fill_between(x2_binned[timeX_str], x2_lower, x2_upper, alpha=0.5)

    # The plots should have the same y-axis based on the one with the highest range

    y_max = max(x1_upper.max(), x2_upper.max())
    y_min = min(x1_lower.min(), x2_lower.min())
    ax1.set_ylim(y_min, y_max)
    ax2.set_ylim(y_min, y_max)

    # Add titles and labels
    ax1.set_title('Time < 250 (acute III)')
    ax1.set_xlabel(timeX_str)
    ax1.set_ylabel(rmse_str)
    ax2.set_title('Time >= 250 (chronic)')
    ax2.set_xlabel(timeX_str)
    ax2.set_ylabel(rmse_str)

    # store the plot
    plt.savefig(f'plots/{model}_time_eval.pdf')

    plt.show()

    timeX_plot_joint = sns.jointplot(data=x1, x=timeX_str, y=rmse_str, kind="reg", marker="+",
                                     line_kws={'color': 'red'})
    timeY_plot_joint = sns.jointplot(data=x1, x=timeY_str, y=rmse_str, kind="reg", marker="+",
                                     line_kws={'color': 'red'})
    # Store the plots
    timeX_plot_joint.savefig(f'plots/{model}_jointplot_timeX.pdf')
    timeY_plot_joint.savefig(f'plots/{model}_jointplot_timeY.pdf')

    timeX_plot_joint = sns.jointplot(data=x2, x=timeX_str, y=rmse_str, kind="reg", marker="+",
                                     line_kws={'color': 'red'})
    timeY_plot_joint = sns.jointplot(data=x2, x=timeY_str, y=rmse_str, kind="reg", marker="+",
                                     line_kws={'color': 'red'})
    # Store the plots
    timeX_plot_joint.savefig(f'plots/{model}_jointplot_timeX_2.pdf')
    timeY_plot_joint.savefig(f'plots/{model}_jointplot_timeY_2.pdf')
    # Show the plot
    plt.show()


def plot_time_eval_all():
    for model in [XGB_str, RF_str, KNN_str, LR_str, Ridge_str, CNN_str, transformer_str, ensemble_str,
                  transformer_sequence_str,
                  CNN_sequence_str]:

        plot_time_eval_2(model, read_plot_data=False, augmented=False)

# best models
window = 7
