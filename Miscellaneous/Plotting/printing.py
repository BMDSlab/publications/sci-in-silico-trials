from keras.callbacks import Callback
from tqdm import tqdm

class TQDMProgressBar(Callback):
    def __init__(self, total_epochs):
        super(TQDMProgressBar, self).__init__()
        self.total_epochs = total_epochs
        self.pbar = None

    def on_train_begin(self, logs=None):
        self.epochs = 0
        self.eta = tqdm(total=self.total_epochs, desc='Training', position=0, ncols=150)

    def on_epoch_end(self, epoch, logs=None):
        self.epochs += 1
        self.eta.update(1)
        if logs is not None:
            log_message = f"Epoch {self.epochs}/{self.total_epochs}"
            for key, value in logs.items():
                if key in ['loss', 'val_loss']:
                    log_message += f", {key}: {value:.4f}"
            self.eta.set_postfix_str(log_message)
        if self.epochs == self.total_epochs:
            self.eta.close()