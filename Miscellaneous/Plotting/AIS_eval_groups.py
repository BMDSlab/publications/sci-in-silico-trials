import os
import pickle

import numpy as np
from Miscellaneous.Misc.columns import *
from Miscellaneous.Misc.model_names import custom_model_names, basemodel_names
from Miscellaneous.Misc.paths import get_data_dict_splits_full_sequences_path, get_AIS_table_path
from Run_collection.Run_files.eval_from_predict_file import get_errors


def calc_ais_bins_vars(model, with_uncertainty=False, augment_test_set=False, dataset=emsci_str,
                       sequences_unscaled=None, sum_of_residuals=False):
    if sequences_unscaled is None:
        path_unscaled = open(
            get_data_dict_splits_full_sequences_path(scaled=False, augmented=augment_test_set,
                                                     # because of available datasets
                                                     test_set_augmented=augment_test_set, dataset=dataset),
            'rb')
        sequences_unscaled = pickle.load(path_unscaled)

    rmse_errors = get_errors(model, with_uncertainty, dataset, augmented_evaluation=augment_test_set,
                             times_interval_start=None,
                             sum_of_residuals=sum_of_residuals, data_sequence_unscaled=sequences_unscaled)

    X_unscaled = sequences_unscaled[0]['X_use']
    X_unscaled = X_unscaled.loc[rmse_errors.index]
    X_unscaled[rmse_str] = rmse_errors

    # Create bins of 0.5 for RMSE
    df = X_unscaled.groupby(['AIS_B', 'AIS_C', 'AIS_D']).agg(
        percentile_2_5=(rmse_str, lambda x: np.percentile(x, 2.5)),
        percentile_50=(rmse_str, lambda x: np.percentile(x, 50)),
        percentile_97_5=(rmse_str, lambda x: np.percentile(x, 97.5))
    )
    df = df.round(2)
    df = df.reset_index(drop=True)
    df.index = ['AIS A', 'AIS D', 'AIS C', 'AIS B']
    df = df.sort_index()
    print('Model:', model)
    print(df)
    filename = get_AIS_table_path(model, with_uncertainty, augment_test_set, dataset, sum_of_residuals)
    print('Saving to', filename)
    df.to_csv(filename)


def run_AIS_eval(dataset=emsci_str, augmented_test_set=True):
    models = basemodel_names + [RF_MS_same_str] + custom_model_names
    if dataset == emsci_str:
        # EMSCI
        augment_test_set = True
        path = get_data_dict_splits_full_sequences_path(scaled=False, augmented=augment_test_set,
                                                     # because of available datasets
                                                     test_set_augmented=augment_test_set, dataset=dataset)
        print(path, os.getcwd())
        path_unscaled_augmented = open(path,
            'rb')
        sequences_unscaled_augmented = pickle.load(path_unscaled_augmented)
        for sum_of_residuals in [True, False]:
            for model in models:
                calc_ais_bins_vars(model, with_uncertainty=True, augment_test_set=augmented_test_set, dataset=dataset,
                                   sequences_unscaled=sequences_unscaled_augmented, sum_of_residuals=sum_of_residuals)

    elif dataset == sygen_str:
        # Sygen

        augment_test_set = False
        path_unscaled_sygen = open(
            get_data_dict_splits_full_sequences_path(scaled=False, augmented=augment_test_set,
                                                     # because of available datasets
                                                     test_set_augmented=augment_test_set, dataset=dataset),
            'rb')
        sequences_unscaled_sygen = pickle.load(path_unscaled_sygen)
        for sum_of_residuals in [True, False]:
            for model in models:
                calc_ais_bins_vars(model, with_uncertainty=True, augment_test_set=False, dataset=dataset,
                                   sequences_unscaled=sequences_unscaled_sygen, sum_of_residuals=sum_of_residuals)






