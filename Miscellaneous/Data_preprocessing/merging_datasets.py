import pandas as pd


def merge_datasets(datasets):
    X_train_MS0 = datasets['X_train_MS0']
    Y_train_MS0 = datasets['Y_train_MS0']

    X_train_MS1_5 = datasets['X_train_MS1_5']
    Y_train_MS1_5 = datasets['Y_train_MS1_5']

    X_train = pd.concat((X_train_MS0, X_train_MS1_5))
    Y_train = pd.concat((Y_train_MS0, Y_train_MS1_5))

    X_test_MS0 = datasets['X_test_MS0']
    Y_test_MS0 = datasets['Y_test_MS0']

    X_test_MS1_5 = datasets['X_test_MS1_5']
    Y_test_MS1_5 = datasets['Y_test_MS1_5']

    X_test = pd.concat((X_test_MS0, X_test_MS1_5))
    Y_test = pd.concat((Y_test_MS0, Y_test_MS1_5))

    return X_train, Y_train, X_test, Y_test


def merge_left_and_right(df):
    # Assuming df has a multi-index with levels 'level_0' and 'level_1'
    left_df = df.xs('left', axis=1, level='level_1')
    right_df = df.xs('right', axis=1, level='level_1')

    merged_df = pd.concat([left_df, right_df], axis=1, keys=['left', 'right'], names=['level_1'])
    merged_df = merged_df.stack('level_1').unstack('level_0')

    return merged_df


def reshape_left_and_right(df):
    # every other value is left and right but keep the index as a multiindex
    result_df = df.apply(split_columns, axis=1)
    return result_df


def split_columns(df):
    return pd.DataFrame({'left': df.T.iloc[::2].values.flatten(), 'right': df.T.iloc[1::2].values.flatten()})
