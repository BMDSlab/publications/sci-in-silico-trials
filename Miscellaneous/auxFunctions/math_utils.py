import numpy as np

rng = np.random.default_rng()
np.random.seed(271)

from scipy.stats import bootstrap


def median_confidence_interval(data, confidence=0.95):
    data = (data,)
    res = bootstrap(data, np.median, confidence_level=confidence,
                    n_resamples=1000, random_state=rng)
    ci_l, ci_u = res.confidence_interval
    return ci_l, ci_u


def get_percentile(data):
    perc_lower = np.percentile(data, 2.5)
    perc_upper = np.percentile(data, 97.5)

    return perc_lower, perc_upper


def inverse_minmax_scaling(scaled_value, original_min, original_max):
    """
    Inverse of min-max scaling to convert a scaled value back to the original range.

    Parameters:
    - scaled_value: The scaled value to be converted.
    - original_min: The minimum value in the original range.
    - original_max: The maximum value in the original range.

    Returns:
    - original_value: The value in the original range.
    """
    original_value = (scaled_value) * (original_max - original_min) + original_min
    return original_value


def round_up_to_divisible(number, divisor):
    result = np.ceil(number / divisor) * divisor
    return result
