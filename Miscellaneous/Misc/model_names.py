from Miscellaneous.Misc.columns import *

basemodel_names = [XGB_str, RF_str, Ridge_str, ElasticNet_str]
custom_model_names = [CNN_str, ensemble_str, transformer_str, transformer_sequence_str, CNN_sequence_str]

name_change_dict = {
    XGB_str: 'XGBoost',
    RF_str: 'Random Forest',
    KNN_str: 'KNN',
    MLP_str: 'MLP',
    LR_str: 'Linear Regression',
    Ridge_str: 'Ridge Regression',
    CNN_str: 'CNN ("multi-modal")',
    ensemble_str: 'CNN ("ensemble")',
    transformer_str: 'Transformer ("multi-modal")',
    transformer_sequence_str: 'Transformer ("block input")',
    CNN_sequence_str: 'CNN ("block input")',
    GNN_str: 'GNN'
}

