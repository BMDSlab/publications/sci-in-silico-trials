# Patient Data to be matched - as described above for reference data

ms_fields_toMatch = ['C5_l', 'C5_r', 'C6_l', 'C6_r', 'C7_l', 'C7_r', 'C8_l', 'C8_r', 'T1_l', 'T1_r', 'L2_l', 'L2_r',
                     'L3_l', 'L3_r',
                     'L4_l', 'L4_r', 'L5_l', 'L5_r', 'S1_l', 'S1_r']
dermatomes_toMatch = ['C2', 'C3', 'C4', 'C5', 'C6', 'C7', 'C8', 'T1', 'T2', 'T3',
                      'T4', 'T5', 'T6', 'T7', 'T8', 'T9', 'T10', 'T11', 'T12',
                      'L1', 'L2', 'L3', 'L4', 'L5', 'S1', 'S2', 'S3', 'S45']
sc_TMS_toMatch = ['C5', 'C6', 'C7', 'C8', 'T1', 'L2', 'L3', 'L4', 'L5', 'S1']
derm_fields_toMatch = ['C2_l', 'C3_l', 'C4_l', 'C5_l', 'C6_l', 'C7_l', 'C8_l', 'T1_l', 'T2_l',
                       'T3_l', 'T4_l', 'T5_l', 'T6_l', 'T7_l', 'T8_l', 'T9_l', 'T10_l',
                       'T11_l', 'T12_l', 'L1_l', 'L2_l', 'L3_l', 'L4_l', 'L5_l', 'S1_l',
                       'S2_l', 'S3_l', 'S45_l', 'C2_r', 'C3_r', 'C4_r', 'C5_r', 'C6_r', 'C7_r',
                       'C8_r', 'T1_r', 'T2_r', 'T3_r', 'T4_r', 'T5_r', 'T6_r', 'T7_r', 'T8_r',
                       'T9_r', 'T10_r', 'T11_r', 'T12_r', 'L1_r', 'L2_r', 'L3_r', 'L4_r',
                       'L5_r', 'S1_r', 'S2_r', 'S3_r', 'S45_r']

lems_ms = ['L2', 'L3', 'L4', 'L5', 'S1']

aisa_grade_field_toMatch = 'AIS'
plegia_field_toMatch = 'plegia'
nli_field_toMatch = 'NLI'
age_field_toMatch = 'AgeAtDOI'
sex_field_toMatch = 'Sex'
cause_field_toMatch = 'Cause'
nlicoarse_field_toMatch = 'NLI_level'
vac_field = 'VAC'
dap_field = 'DAP'

uems_field_toMatch = 'UEMS'
lems_field_toMatch = 'LEMS'
nodeDM_field_toMatch = 'nodeDM'
tlt_field_toMatch = 'TLT'

## EMSCI_indMSprediction


ms_bothSide = ['C5', 'C6', 'C7', 'C8', 'T1', 'L2', 'L3', 'L4', 'L5', 'S1']

ms_both_sides_fields_toMatch = []
for m in ms_bothSide:
    ms_both_sides_fields_toMatch.append('LMS_' + m)
    ms_both_sides_fields_toMatch.append('RMS_' + m)

dermatomes_withC1 = ['C1', 'C2', 'C3', 'C4', 'C5', 'C6', 'C7', 'C8', 'T1', 'T2', 'T3',
                     'T4', 'T5', 'T6', 'T7', 'T8', 'T9', 'T10', 'T11', 'T12',
                     'L1', 'L2', 'L3', 'L4', 'L5', 'S1', 'S2', 'S3', 'S45']


dermatomes_noC1 = ['C2', 'C3', 'C4', 'C5', 'C6', 'C7', 'C8', 'T1', 'T2', 'T3',
                   'T4', 'T5', 'T6', 'T7', 'T8', 'T9', 'T10', 'T11', 'T12',
                   'L1', 'L2', 'L3', 'L4', 'L5', 'S1', 'S2', 'S3', 'S45']

dermatomes_index = {value: index for index, value in enumerate(dermatomes_withC1)}

# Fill the nli
nli_dermatomes = {'C1': -1, 'C2': -1, 'C3': -1, 'C4': -1, 'C5': 0, 'C6': 1, 'C7': 2, 'C8': 3,
                  'T1': 4, 'T2': 4, 'T3': 4, 'T4': 4, 'T5': 4, 'T6': 4,
                  'T7': 4, 'T8': 4, 'T9': 4, 'T10': 4, 'T11': 4, 'T12': 4,
                  'L1': 4, 'L2': 5, 'L3': 6, 'L4': 7, 'L5': 8,
                  'S1': 9, 'S2': 10, 'S3': 10, 'S45': 10}

# nli_dermatomes but double the values
nli_dermatomes_double = {}
for key in nli_dermatomes:
    nli_dermatomes_double[key] = nli_dermatomes[key] * 2


LTS_scores = []
PPS_scores = []
for d in dermatomes_noC1:
    LTS_scores.append('LLT_' + d)
    LTS_scores.append('RLT_' + d)
    PPS_scores.append('LPP_' + d)
    PPS_scores.append('RPP_' + d)

max_indNLI = 24

sygenSc = ['elbfl', 'wrext', 'elbex', 'finfl', 'finab', 'hipfl', 'kneex', 'ankdo', 'greto', 'ankpl']
sygenScr = ['elbfl', 'wrext', 'elbex', 'finfl', 'finab', 'hipfl', 'kneet', 'ankdo', 'greto', 'ankpl']

derm_fields_lr = ['C2_l', 'C3_l', 'C4_l', 'C5_l', 'C6_l', 'C7_l', 'C8_l', 'T1_l', 'T2_l',
                  'T3_l', 'T4_l', 'T5_l', 'T6_l', 'T7_l', 'T8_l', 'T9_l', 'T10_l',
                  'T11_l', 'T12_l', 'L1_l', 'L2_l', 'L3_l', 'L4_l', 'L5_l', 'S1_l',
                  'S2_l', 'S3_l', 'S45_l', 'C2_r', 'C3_r', 'C4_r', 'C5_r', 'C6_r', 'C7_r',
                  'C8_r', 'T1_r', 'T2_r', 'T3_r', 'T4_r', 'T5_r', 'T6_r', 'T7_r', 'T8_r',
                  'T9_r', 'T10_r', 'T11_r', 'T12_r', 'L1_r', 'L2_r', 'L3_r', 'L4_r',
                  'L5_r', 'S1_r', 'S2_r', 'S3_r', 'S45_r']
ms_fields_lr = ['C5_l', 'C6_l', 'C7_l', 'C8_l', 'T1_l', 'L2_l', 'L3_l', 'L4_l', 'L5_l', 'S1_l',
                'C5_r', 'C6_r', 'C7_r', 'C8_r', 'T1_r', 'L2_r', 'L3_r', 'L4_r', 'L5_r', 'S1_r']
ms_fields = ['C5_l', 'C5_r', 'C6_l', 'C6_r', 'C7_l', 'C7_r', 'C8_l', 'C8_r', 'T1_l', 'T1_r', 'L2_l', 'L2_r', 'L3_l',
             'L3_r',
             'L4_l', 'L4_r', 'L5_l', 'L5_r', 'S1_l', 'S1_r']

# a dict to convert from ms_fields_lr to ms_fields
temp_name = 'Temp'
