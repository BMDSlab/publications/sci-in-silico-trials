

cols_num = ['sc_X', 'Left', 'level', 'age', 'timeX', 'timeY', 'ind_NLI']  # 'distNLI',

orig_col = ['node_5.0', 'node_8.0', 'node_9.0', 'node_10.0', 'node_13.0',
            'node_16.0', 'node_17', 'node_18', 'node_20_21']
new_col = ['node_a', 'node_b', 'node_c', 'node_d', 'node_e',
           'node_f', 'node_g', 'node_h', 'node_j']



cols_exclude_feat = ['pat', 'score', 'sc_Y']

current = ['shape_below_flat', 'shape_below_up', 'AIS_C', 'AIS_B', 'AIS_D',
           'distNLI_far', 'timeX', 'timeY', 'age', 'level',
           'sex_m', 'sc_X']

new_featurename = ['shape\n(caudal: flat)',
                   'shape\n(caudal: up)',
                   'AIS C', 'AIS B', 'AIS D',
                   'far from NLI', 'absolute time\n acute',
                   'absolute time\n recovery', 'Age',
                   'MS level',
                   'Sex (male)', 'MS']

cols_lmef_usenode = ['Intercept', 'C(node_a, Treatment(True))[T.False]',
                 'C(node_b, Treatment(True))[T.False]',
                 'C(node_c, Treatment(True))[T.False]',
                 'C(node_d, Treatment(True))[T.False]',
                 'C(node_e, Treatment(True))[T.False]',
                 'C(node_f, Treatment(True))[T.False]',
                 'C(node_g, Treatment(True))[T.False]',
                 'C(node_h, Treatment(True))[T.False]',
                 'C(node_j, Treatment(True))[T.False]',
                 'C(sex_m, Treatment(True))[T.False]',
                 'C(distNLI_close, Treatment(True))[T.False]',
                 'level', 'age',
                 'timeX', 'timeY', 'sc_X', 'pat Var']  # 'distNLI',

cols_lmef_dont_use_node = ['Intercept', 'C(node_a, Treatment(True))[T.False]',
             'C(AIS_B, Treatment(True))[T.False]',
             'C(AIS_C, Treatment(True))[T.False]',
             'C(AIS_D, Treatment(True))[T.False]',
             'C(node_j, Treatment(True))[T.False]',
             'C(sex_m, Treatment(True))[T.False]',
             'C(distNLI_far, Treatment(True))[T.False]',
             'level', 'age',
             'timeX', 'timeY', 'sc_X', 'pat Var']  # 'distNLI',



metrics = ['rmse', 'mae', 'mean_abs_lems']

est_names = ['MS0', 'MS1_5']
level_str = 'level'
left_str = 'Left'
sc_X_str = 'sc_X'
sc_Y_str = 'sc_Y'
pat_str = 'pat'
score_str = 'score'
timeX_str = 'timeX'
timeY_str = 'timeY'
ind_NLI_str = 'ind_NLI'
distNLI_str = 'distNLI'

sex_str = 'sex'
sex_m_str = 'sex_m'
AIS_str = 'AIS'
age_str = 'age'

vac_str = 'VAC'
dap_str = 'DAP'

cause_str = 'Cause'

traumatic_str = 'traumatic'
ichemic_str = 'ichemic'  # Misspelled on purpose to match the data
haemorragic_str = 'haemorragic'
other_str = 'other'
causes_strs = [traumatic_str, ichemic_str, haemorragic_str, other_str]

cause_traumatic_str = cause_str + '_' + traumatic_str
cause_ichemic_str = cause_str + '_' + ichemic_str
cause_haemorragic_str = cause_str + '_' + haemorragic_str
cause_other_str = cause_str + '_' + other_str

causes_true_strs = [cause_traumatic_str, cause_ichemic_str, cause_haemorragic_str, cause_other_str]

vac_yes_str = 'VAC_Yes'
dap_yes_str = 'DAP_Yes'

plegia_str = 'plegia'
tetraplegia_str = 'tetraplegia'
paraplegia_str = 'paraplegia'

lems_str = 'LEMS'
uems_str = 'UEMS'
tms_str = 'TMS'


ais_a_str = 'AIS_A'
ais_b_str = 'AIS_B'
ais_c_str = 'AIS_C'
ais_d_str = 'AIS_D'

ms_str = 'MS'
lt_str = 'LT'
pp_str = 'PP'
x_add_str = 'X_add'

patient_id_unnamed_str = 'Unnamed: 0'
id_str = 'id'

train_ids_str = 'trainIDs'
validation_ids_str = 'validationIDs'
test_ids_str = 'testIDs'

x_use_str = 'X_use'
y_use_str = 'Y_use'

x_train_str = 'X_train'
y_train_str = 'Y_train'
x_test_str = 'X_test'
y_test_str = 'Y_test'

x_train_ms0_str = 'X_train_MS0'
y_train_ms0_str = 'Y_train_MS0'
x_train_ms1_5_str = 'X_train_MS1_5'
y_train_ms1_5_str = 'Y_train_MS1_5'

x_test_ms0_str = 'X_test_MS0'
y_test_ms0_str = 'Y_test_MS0'
x_test_ms1_5_str = 'X_test_MS1_5'
y_test_ms1_5_str = 'Y_test_MS1_5'

pred_str = 'pred'

motor_score_str = 'motor score'
motor_score_level_str = 'motor score level'
motor_score_index_str = 'motor score index'

start_value_str = 'start value'
end_value_str = 'end value'

sides_are_same_str = '_sides_are_same'

rmse_str = 'rmse'
mae_str = 'mae'

mean_abs_lems_str = 'mean_abs_lems'

ordinal_str = 'ord'
ordinal_versions = ['ordinal', ordinal_str]
ordinal_alt_str = 'ordinal_alt'

error_losses = [rmse_str, mae_str, mean_abs_lems_str]
ordinal_losses = [ordinal_str, ordinal_alt_str, 'ordinal']

new_hp_str = 'new_hp'
skip_hp_str = 'skip_hp'
all_timepoints_str = 'all_timepoints'
date_str = 'date'
outcome_str = 'outcome'
clip_output_str = 'clip_output'

XGB_str = 'XGB'
RF_str = 'RF'
LR_str = 'LinR'
LogR_str = 'LogR'
MLP_str = 'MLP'
CNN_str = 'CNN'
KNN_str = 'KNN'
Ridge_str = 'Ridge'
Catboost_str = 'Catboost'
ElasticNet_str = 'ElasticNet'
Lasso_str = 'Lasso'

GNN_louis_str = 'GNN_louis'

RF_MS_same_str = 'RF_MS_same'
CNN_no_clip_str = 'CNN_no_clip'


transformer_str = 'TF'
transformer_sequence_str = 'TF_seq'
CNN_sequence_str = 'CNN_seq'
GNN_str = 'GNN'
GNN2_str = 'GNN2'
TF2_str = 'TF2'
TF_seq2_str = 'TF_seq2'

kernel_size_str = 'kernel_size'
num_filters_str = 'n_filters_1'
num_layers_str = 'n_layers'
do_str = 'do'
l2_str = 'l2'
lr_str = 'lr'
sample_shape_str = 'sample_shape'
bs_str = 'bs'
hidden_dim_str = 'hiddendim'
n_epochs_str = 'n_epochs'

only_MS_str = 'only_MS'
ensemble_str = 'ensemble'

with_uncertainty_str = 'with_uncertainty'
augmented_evaluation_str = 'augmented_evaluation'

model_parameters_str = 'model_parameters'


intervalRMSE_lower_str = 'intervalRMSE_lower'
intervalRMSE_upper_str = 'intervalRMSE_upper'
median_rmse_str = 'median_rmse'

X_use_str = 'X_use'
Y_use_str = 'Y_use'

cols_add = ['age', 'sex', 'ind_NLI', 'AIS', 'timeX', 'timeY', 'VAC', 'DAP', plegia_str]

sygen_str = 'sygen'
emsci_str = 'emsci'
nisci_str = 'nisci'
emsci_in_silico_str = 'emsci_in_silico'
murnau_str = 'murnau'

times_interval_start_4_weeks = (28 - 7, 28 + 7)

rename_models = {
    XGB_str: 'XGBoost',
    RF_str: 'Random Forest',
    LR_str: 'Linear Regression',
    MLP_str: 'MLP',
    CNN_str: 'CNN multi-modal',
    KNN_str: 'KNN',
    Ridge_str: 'Ridge',
    Catboost_str: 'Catboost',
    ElasticNet_str: 'ElasticNet',
    Lasso_str: 'Lasso',
    RF_MS_same_str: 'Random Forest (MS same)',
    transformer_str: 'Transformer',
    transformer_sequence_str: 'Transformer (block)',
    CNN_sequence_str: 'CNN (block)',
    GNN_str: 'GNN',
    GNN2_str: 'GNN2',
    TF2_str: 'TF2',
    TF_seq2_str: 'TF_seq2',
    ensemble_str: 'CNN (ensemble)',

}

table_order = [XGB_str, RF_str, RF_MS_same_str,
               Ridge_str,
               ElasticNet_str,
               CNN_str, ensemble_str, CNN_sequence_str,
               transformer_str, transformer_sequence_str,
               ]

mean_abs_lems_str = 'mean_abs_lems'

standard_file_date = '20240319'
later_file_date = '20240612'
nisci_file_date = '20240615'
emsci_in_silico_date = '20240617'
murnau_date = '20240618'

# outcomes
ms_sequence_str = 'ms_sequence'
bowel_str = 'bowel'
bladder_str = 'bladder'
bowel_bladder_outcomes = [bowel_str, bladder_str]
