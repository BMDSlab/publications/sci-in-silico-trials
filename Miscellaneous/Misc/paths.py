import os

from pathlib import Path

from Miscellaneous.Misc.columns import *
from Miscellaneous.Misc.model_names import basemodel_names


def get_project_root() -> Path:
    return Path(__file__).parent.parent.parent


def get_base_path():
    # root then Fake_data
    return os.path.join(get_project_root(), 'Fake_data')


base_path = get_base_path()

data_path = None


def get_path_refdata2023():
    return


def get_path_refdata_sygen():
    return


path_refdata2023 = get_path_refdata2023()
path_refdata_sygen = get_path_refdata_sygen()


def get_sygen_path():
    return


time_point = 26  # Evaluation endpoint (6 months)
time_point_acute = 2  # Matching phase (2 weeks)


def get_murnau2023_data_file_path():
    return


output_heatmaps = os.path.join(os.getcwd(), '../output_heatmaps')
output_heatmaps_EMSCI = os.path.join(os.getcwd(), '../output_heatmaps_EMSCI')
output_shap = os.path.join(os.getcwd(), '../output_shap')

# Patient Data to be matched - as described above for reference data


x_ms_prefix = 'X_MS_'
y_ms_prefix = 'Y_MS_'
x_lts_prefix = 'X_LTS_'
x_pps_prefix = 'X_PPS_'
x_add_prefix = 'X_add_'


def get_pat_file_name(prefix, time_point_init, time_point_end, outcome_type=ms_sequence_str):
    if outcome_type == ms_sequence_str:
        outcome_str = ''
    else:
        outcome_str = outcome_type + '_'
    path_file_name_XY = outcome_str + str(time_point_init) + 'weeks_to_' + str(time_point_end) + 'weeks.csv'
    return prefix + path_file_name_XY


def get_pat_file_name_time_points(prefix, time_point_init, time_point_end, outcome_type=ms_sequence_str):
    if outcome_type == ms_sequence_str:
        outcome_str = ''
    else:
        outcome_str = outcome_type + '_'
    path_file_name_XY = outcome_str + str(time_point_init) + '_' + str(time_point_end) + '.csv'
    # Add _ instead of space
    path_file_name_XY = path_file_name_XY.replace(' ', '_')
    return prefix + path_file_name_XY


def get_model_path(clf_choice_folder, debug=False, with_uncertainty=False):
    output_clf = os.path.join(get_output_path(), clf_choice_folder, ('debug' if debug else 'full_model') +
                              ('_with_uncertainty' if with_uncertainty else '_without_uncertainty'))
    return output_clf


def get_motor_scores_estimators_path(model_folder, name, debug_str, i):
    motor_scores_estimators_path = os.path.join(model_folder, name + debug_str + '_cv_' + str(i) + '.pkl')
    return motor_scores_estimators_path


old_run_folder = 'run_hd5'
hp_run_folder = 'hp_hd5'


def get_data_dict_splits_full_sequences_path(scaled=False, augmented=False, test_set_augmented=False,
                                             test_set_only=False,
                                             dataset=emsci_str, date=standard_file_date, include_early_timepoints=False,
                                             outcome_type=ms_sequence_str):
    return get_fake_data_path(type_data='sequence')
    # if scaled:
    #     scale_str = '_scaled'
    # else:
    #     scale_str = ''
    # if augmented:
    #     augmented_str = '_augmented'
    # else:
    #     augmented_str = ''
    # if test_set_augmented:
    #     test_set_augmented_str = '_test_set_augmented'
    # else:
    #     test_set_augmented_str = ''
    # if test_set_only:
    #     only_test_set_str = '_only_test_set'
    # else:
    #     only_test_set_str = ''
    # if date is not None:
    #     date_str = '_' + date
    # else:
    #     date_str = ''
    # if include_early_timepoints:
    #     early_timepoints_str = '_early_timepoints'
    # else:
    #     early_timepoints_str = ''
    # if outcome_type != ms_sequence_str:
    #     outcome_type_str = '_{}'.format(outcome_type)
    # else:
    #     outcome_type_str = ''
    #
    # filepath = 'data_dict_splits_full_sequences{}{}{}{}{}{}{}.pkl'.format(scale_str, augmented_str,
    #                                                                       test_set_augmented_str,
    #                                                                       only_test_set_str,
    #                                                                       early_timepoints_str,
    #                                                                       outcome_type_str,
    #                                                                       date_str)
    # if dataset == sygen_str:
    #     data_set_path = get_path_refdata_sygen()
    # elif dataset == emsci_str:  # EMSCI
    #     data_set_path = get_path_refdata2023()
    # else:
    #     raise ValueError('Dataset not recognized')
    # return os.path.join(data_set_path, filepath)


def get_data_dict_splits_CNN_format_path_old(bootstrap=True, scaled=False, augmented_test_set=False):
    if scaled:
        scaled_str = '_scaled'
    else:
        scaled_str = '_unscaled'
    if bootstrap:
        bootstrap_str = '_uncert'
    else:
        bootstrap_str = ''

    if augmented_test_set:
        augmented_test_set_str = '_augmented_test_set'
    else:
        augmented_test_set_str = ''
    return os.path.join(path_refdata2023, 'data_dict_splits{}_MS_LT_PP_Xadd{}{}.pkl'.format(bootstrap_str, scaled_str,
                                                                                            augmented_test_set_str))


def get_fake_data_path(type_data='deep_learning', fold_index=None):
    # type_data: deep_learning, baseline
    if fold_index is None:
        fold_index = ''
    else:
        fold_index = '_{}'.format(fold_index)
    path = Path(os.path.join(get_project_root(), 'Fake_data', type_data))
    return os.path.join(path, f'fake_data{fold_index}.pkl')


def get_data_dict_splits_CNN_format_path(fold_index, bootstrap=True, scaled=False, augmented_test_set=False,
                                         dataset=emsci_str, include_early_timepoints=False,
                                         outcome_type=ms_sequence_str,
                                         date=standard_file_date):
    return get_fake_data_path(type_data='deep_learning', fold_index=fold_index)
    # if scaled:
    #     scaled_str = '_scaled'
    # else:
    #     scaled_str = '_unscaled'
    # if dataset == sygen_str:
    #     data_set_path = get_path_refdata_sygen()
    # else:
    #     raise ValueError('Dataset not recognized')
    #
    # if bootstrap:
    #     folder = Path(os.path.join(data_set_path, 'bootstrap_CNN_format' + scaled_str))
    # else:
    #     folder = Path(os.path.join(data_set_path, 'non_bootstrap_CNN_format' + scaled_str))
    # Path.mkdir(folder, exist_ok=True)
    # if augmented_test_set:
    #     augmented_test_set_str = '_augmented_test_set'
    # else:
    #     augmented_test_set_str = ''
    # if include_early_timepoints:
    #     early_timepoints_str = '_early_timepoints'
    # else:
    #     early_timepoints_str = ''
    # if date is None or date == standard_file_date:
    #     date_str = ''
    # else:
    #     date_str = '_' + date
    #
    # if outcome_type != ms_sequence_str:
    #     outcome_type_str = '_{}'.format(outcome_type)
    # else:
    #     outcome_type_str = ''
    # return os.path.join(folder,
    #                     'data_dict_splits_MS_LT_PP_Xadd_fold_{}{}{}{}{}.pkl'.format(fold_index, augmented_test_set_str,
    #                                                                                 early_timepoints_str,
    #                                                                                 outcome_type_str,
    #                                                                                 date_str))


def get_data_dict_splits_sequence_format_path(fold_index, bootstrap=True, scaled=False):
    if scaled:
        scaled_str = '_scaled'
    else:
        scaled_str = '_unscaled'
    if bootstrap:
        folder = Path(os.path.join(path_refdata2023, 'bootstrap_sequence_format' + scaled_str))
    else:
        folder = Path(os.path.join(path_refdata2023, 'non_bootstrap_sequence_format' + scaled_str))
    Path.mkdir(folder, exist_ok=True, parents=True)
    return os.path.join(folder, 'data_dict_splits_sequence_fold_{}.pkl'.format(fold_index))


def get_data_dict_splits_path(fold_index, n_bootstrap=100, scaled=False, augmented=True, test_set_augmented=False,
                              test_set_only=False, dataset=emsci_str, include_early_timepoints=False,
                              outcome_type=ms_sequence_str,
                              date=standard_file_date):
    return get_fake_data_path(type_data='baseline', fold_index=fold_index)
    # if augmented:
    #     augmented_str = '_with_uncertainty'
    # else:
    #     augmented_str = '_without_uncertainty'
    #     n_bootstrap = ''  # No bootstrap if not augmented
    #
    # if scaled:
    #     scaled_str = '_scaled'
    # else:
    #     scaled_str = '_unscaled'
    # if test_set_augmented:
    #     test_str = '_test_set_augmented'
    # else:
    #     test_str = ''
    # if test_set_only:
    #     only_test_set_str = '_only_test_set'
    # else:
    #     only_test_set_str = ''
    # if dataset == sygen_str:
    #     data_set_path = get_path_refdata_sygen()
    # elif dataset == emsci_str:  # EMSCI
    #     data_set_path = get_path_refdata2023()
    # else:
    #     raise ValueError('Dataset not recognized')
    # if include_early_timepoints:
    #     early_timepoints_str = '_early_timepoints'
    # else:
    #     early_timepoints_str = ''
    # if date is not None:
    #     date_str = '_' + date
    # else:
    #     date_str = ''
    # if outcome_type != ms_sequence_str:
    #     outcome_type_str = '_{}'.format(outcome_type)
    # else:
    #     outcome_type_str = ''
    #
    # folder = Path(os.path.join(data_set_path, 'bootstrap{}'.format(scaled_str)))
    # Path.mkdir(folder, exist_ok=True, parents=True)
    # return os.path.join(folder,
    #                     'data_dict_splits{}{}_fold_{}{}{}{}{}{}.pkl'.format(augmented_str, n_bootstrap, fold_index,
    #                                                                         test_str,
    #                                                                         only_test_set_str,
    #                                                                         early_timepoints_str,
    #                                                                         outcome_type_str,
    #                                                                         date_str))


def set_output_path():
    return os.path.join(get_project_root(), 'out')


euler_emsci_2023_path = set_output_path()


def get_output_path():
    global euler_emsci_2023_path
    return euler_emsci_2023_path


def set_shap_output_path():
    global output_shap
    output_shap = os.path.join(get_project_root(), 'output_shap')
    return output_shap


shap_path = set_shap_output_path()


def set_model_dir(model):
    return os.path.join(get_output_path(), model)


global CNN_str, transformer_str, ensemble_str, only_MS_str, transformer_sequence_str, CNN_sequence_str, GNN_str, GNN2_str
CNN_dir = set_model_dir("CNN1D")
transformer_dir = set_model_dir(transformer_str)
ensemble_dir = set_model_dir('Ensemble')
only_MS_dir = os.path.join(euler_emsci_2023_path, "Only_MS_CNN1D")
transformer_sequence_dir = set_model_dir(transformer_sequence_str)
CNN_sequence_dir = set_model_dir(CNN_sequence_str)
GNN_dir = set_model_dir(GNN_str)
GNN2_dir = set_model_dir(GNN2_str)
TF2_dir = set_model_dir(TF2_str)
TF_seq2_dir = set_model_dir(TF_seq2_str)

CNN_hyperparameter_sweep_dir = os.path.join(euler_emsci_2023_path, "CNN_hyperparameter_sweep")
# Path(CNN_hyperparameter_sweep_dir).mkdir(parents=True, exist_ok=True)

CNN_hyperparameter_sweep_file_path = os.path.join(CNN_hyperparameter_sweep_dir, 'CNN_hyperparameter_sweep.csv')
CNN_hyperparameter_sweep_file_path_params = os.path.join(CNN_hyperparameter_sweep_dir,
                                                         'CNN_hyperparameter_sweep_params.csv')


def get_hyperparameter_sweep_dir(model, ordinal=False):
    path = os.path.join(euler_emsci_2023_path, '{}{}_hyperparameter_sweep'.format(model, '_ord' if ordinal else ''))
    Path(path).mkdir(parents=True, exist_ok=True)
    return path


def get_hyperparameter_sweep_file_path(model, all_models=False, ordinal=False):
    return os.path.join(get_hyperparameter_sweep_dir(model, ordinal=ordinal),
                        '{}_hyperparameter_sweep{}.csv'.format(model, '_all' if all_models else ''))


def get_hyperparameter_sweep_file_path_params(model, all_models=False, ordinal=False):
    return os.path.join(get_hyperparameter_sweep_dir(model, ordinal=ordinal),
                        '{}{}_hyperparameter_sweep_params{}.csv'.format(model,
                                                                        '_ordinal' if ordinal else '',
                                                                        '_all' if all_models else ''))


def get_model_output_folder(modelID, model=CNN_str, ordinal=False):
    directory = get_model_dir(model=model, ordinal=ordinal, with_uncertainty=False)
    output = os.path.join(directory + '_all', modelID)
    return output


def get_desc_dict_path(output, augmented_evaluation=False, dataset=emsci_str):
    if augmented_evaluation:
        augmented_str = '_augmented'
    else:
        augmented_str = ''
    if dataset == sygen_str:
        dataset_str = '_sygen'
    else:
        dataset_str = ''
    return os.path.join(output, filesave_name + '_desc_dict{}{}.pkl'.format(augmented_str, dataset_str))


def get_umap_embedding_path(model, augmented=False):
    if augmented:
        augmented_str = '_augmented'
    else:
        augmented_str = ''
    path = 'Plotting/umap_embeddings/umap_{}{}.csv'.format(model, augmented_str)
    return os.path.join(get_project_root(), path)


def get_umap_X_path(model, augmented=False):
    if augmented:
        augmented_str = '_augmented'
    else:
        augmented_str = ''
    path = 'Plotting/umap_embeddings/umap_X_{}{}.csv'.format(model, augmented_str)
    return os.path.join(get_project_root(), path)


def get_shap_dict_path(output):
    return os.path.join(output, filesave_name + '_shap_dict.pkl')


def get_shap_values_path(output, extra=''):
    return os.path.join(output, filesave_name + '_shap_{}.pkl'.format(extra))


def get_predictions_path(output, augmented_evaluation=False, upper_conf=False, lower_conf=False, extra='',
                         pickle=False, dataset=emsci_str, fold_index=None):
    filename = predictions_filename(augmented_evaluation=augmented_evaluation, upper_conf=upper_conf,
                                    lower_conf=lower_conf, extra=extra, pickle=pickle, dataset=dataset,
                                    fold_index=fold_index)
    return os.path.join(output, filename)


def predictions_filename(augmented_evaluation=False, upper_conf=False, lower_conf=False, extra='',
                         pickle=False, dataset=emsci_str, fold_index=None):
    if augmented_evaluation:
        augmented_test_set_str = '_augmented_test_set'
    else:
        augmented_test_set_str = ''
    if upper_conf:
        extra += '_upper_conf'
    elif lower_conf:
        extra += '_lower_conf'
    if pickle:
        file_type = 'pkl'
    else:
        file_type = 'csv'
    if dataset == sygen_str:
        dataset_str = '_sygen'
    else:
        dataset_str = ''
    if fold_index is not None:
        fold_index_str = '_fold_{}'.format(fold_index)
    else:
        fold_index_str = ''

    filename = filesave_name + '_predictions{}{}{}{}.{}'.format(augmented_test_set_str, extra, dataset_str,
                                                                fold_index_str, file_type)
    return filename


def get_augmented_predictions_path(output, extra=''):
    return os.path.join(output, filesave_name + '_augmented_predictions_dict{}.pkl'.format(extra))


def get_error_desc_df_path(output, error_type, augmented_evaluation=False, extra='', dataset=emsci_str):
    if augmented_evaluation:
        augmented_evaluation_str = '_augmented_evaluation'
    else:
        augmented_evaluation_str = ''
    if dataset == sygen_str:
        dataset_str = '_sygen'
    else:
        dataset_str = ''
    return os.path.join(output,
                        filesave_name + '_desc_df_{}{}{}{}.csv'.format(error_type, augmented_evaluation_str, extra,
                                                                       dataset_str))


def get_pred_df_path(output, extra=''):
    return os.path.join(output, filesave_name + '_pred_df_{}.csv'.format(extra))


def get_shap_plot_path(output, ms, est_name, debug_str, filetype='pdf'):
    file_name = "SHAP_{}_".format(ms) + debug_str + '_cv_' + ms + est_name
    return os.path.join(output, 'SHAP', file_name + '_SHAP_all.' + filetype)


def get_shap_plot_path_CNN(output, filetype='pdf', extra=''):
    file_name = "SHAP_CNN_{}".format(extra)
    return os.path.join(output, 'SHAP', file_name + '_SHAP_all.' + filetype)


def get_file_path_custom_model(output, model_name, folder=hp_run_folder, include_early_timepoints=False):
    if include_early_timepoints:
        early_timepoints_str = '_AT'
    else:
        early_timepoints_str = ''
    filepath_custom_model = os.path.join(os.path.join(output, folder),
                                         'MODEL_' + model_name + early_timepoints_str + '_nnet_run_test.keras')

    return filepath_custom_model


def get_file_path_CNN_model_new(output, model_name, folder=hp_run_folder):
    filepath_CNN_model = os.path.join(os.path.join(output, folder),
                                      model_name + '_run.keras')
    return filepath_CNN_model


def shap_file_name(model, subset='', dataset=emsci_str, dataset_part=False, with_uncertainty=False, num_examples=5000,
                   motor_score=None, motor_score_set=None, fold_index=None):
    if dataset == sygen_str:
        dataset_str = '_sygen'
    else:
        dataset_str = ''
    if dataset_part is not False:
        training_set_str = '_{}'.format(dataset_part)
    else:
        training_set_str = ''

    if with_uncertainty:
        with_uncertainty_str = '_with_uncertainty'
    else:
        with_uncertainty_str = ''
    num_examples_str = '_{}'.format(num_examples)
    if model in basemodel_names:
        motor_score_add_str = '_{}'.format(motor_score)
        motor_score_set_str = '_{}'.format(motor_score_set)
    else:
        motor_score_add_str = ''
        motor_score_set_str = ''
    if fold_index is not None:
        fold_index_str = '_fold_{}'.format(fold_index)
    else:
        fold_index_str = ''

    return os.path.join(shap_path,
                        'SHAP_values_{}{}{}{}{}{}{}{}{}.pkl'.format(model, dataset_str, subset, training_set_str,
                                                                    with_uncertainty_str, num_examples_str,
                                                                    motor_score_add_str, motor_score_set_str,
                                                                    fold_index_str))


def extract_output_model_name_from_filepath(filepath):
    # Split the filepath into parts
    parts = filepath.split(os.path.sep)

    # Find the 'run_hdf5' directory
    try:
        index = parts.index('run_hdf5')
    except ValueError:
        # 'run_hdf5' not found in the filepath
        return None, None

    # Extract output and model_name from the parts
    if index < len(parts) - 3:
        output = parts[index - 1]
        model_name = parts[index + 1].replace('MODEL_', '').replace('_nnet_run_test.keras',
                                                                    '')  # Update the 'MODEL_' and '_nnet_run_test.keras' parts if used
        return output, model_name
    else:
        return None, None


def get_model_name(model, with_uncertainty, ordinal=False, outcome_type=ms_sequence_str, clip_output=True):
    model_name = model
    if with_uncertainty:
        model_name += '_with_uncertainty'
    if ordinal:
        model_name += '_' + ordinal_str
    if outcome_type != ms_sequence_str:
        model_name += '_{}'.format(outcome_type)
    if not clip_output:
        model_name += '_no_clip_output'
    return model_name


def get_model_dir(with_uncertainty, model, ordinal=False, include_early_timepoints=False, outcome_type=ms_sequence_str,
                  clip_output=True):
    if model == CNN_str:
        directory = CNN_dir
    elif model == transformer_str:
        directory = transformer_dir
    elif model == ensemble_str:
        directory = ensemble_dir
    elif model == only_MS_str:
        directory = only_MS_dir
    elif model == transformer_sequence_str:
        directory = transformer_sequence_dir
    elif model == CNN_sequence_str:
        directory = CNN_sequence_dir
    elif model == GNN_str:
        directory = GNN_dir
    elif model == GNN2_str:
        directory = GNN2_dir
    elif model == TF2_str:
        directory = TF2_dir
    elif model == TF_seq2_str:
        directory = TF_seq2_dir
    else:
        raise ValueError('Model not found')
    with_uncertainty_add_str = '_with_uncertainty' if with_uncertainty else ''
    ordinal_str_add = '_' + ordinal_str if ordinal else ''
    early_timepoints_str = '_AT' if include_early_timepoints else ''
    if outcome_type != ms_sequence_str:
        outcome_type_str = '_{}'.format(outcome_type)
    else:
        outcome_type_str = ''
    if not clip_output:
        clip_output_str = 'no_clip_output'
    else:
        clip_output_str = ''
    return directory + with_uncertainty_add_str + ordinal_str_add + early_timepoints_str + outcome_type_str + clip_output_str


filesave_name = 'EMSCIpredRecovery'


def get_time_eval_plot_path(model, with_uncertainty=False, dataset=emsci_str):
    if dataset == sygen_str:
        dataset_str = '_sygen'
    else:
        dataset_str = ''
    if with_uncertainty:
        with_uncertainty_str = '_with_uncertainty'
    else:
        with_uncertainty_str = ''
    return f'plotting_data/{model}_time_eval{with_uncertainty_str}{dataset_str}.csv'


def get_performance_table_path(with_uncertainty=False, augmented_evaluation=False, dataset=emsci_str):
    filename = f'tables/rmse_table_with_uncertainty_{with_uncertainty}_augmented_evaluation_{augmented_evaluation}_{dataset}.csv'
    return filename


def get_AIS_table_path(model, with_uncertainty, augment_test_set=False, dataset=emsci_str, sum_of_residuals=False):
    if sum_of_residuals:
        sum_of_residuals_str = '_sum_of_residuals'
    else:
        sum_of_residuals_str = ''
    folder = f'{get_project_root()}/Tables/AIS_tables'
    Path(folder).mkdir(parents=True, exist_ok=True)
    filename = os.path.join(folder, 'AIS_bins_{}{}{}{}{}.csv'.format(
        model,
        '_with_uncertainty' if with_uncertainty else '',
        '_test_augmented' if augment_test_set else '',
        '_' + dataset,
        sum_of_residuals_str))
    return filename


def get_rmse_table_path(with_uncertainty, augmented_evaluation, dataset, times_interval_start=None,
                        sum_of_residuals=False, summation=False):
    if sum_of_residuals:
        sum_of_residuals_str = '_sum_of_residuals'
    else:
        sum_of_residuals_str = ''
    if summation:
        sum_str = '_sum'
    else:
        sum_str = ''
    folder = f'{get_project_root()}/Tables'
    Path(folder).mkdir(parents=True, exist_ok=True)
    filename = (f'{folder}/rmse_table_with_uncertainty_{with_uncertainty}_augmented_evaluation_{augmented_evaluation}_'
                f'{dataset}_time_interval{times_interval_start}{sum_of_residuals_str}{sum_str}.csv')
    return filename


def get_scim_conversion_path():
    return os.path.join(get_project_root(), 'data', 'SCIM_conversion.csv')
