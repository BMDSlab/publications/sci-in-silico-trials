import pickle

import numpy as np
import pandas as pd
from sklearn.metrics import mean_squared_error

from Miscellaneous.Misc.fields_to_match import *
from Miscellaneous.Misc.model_names import custom_model_names
from Miscellaneous.Misc.paths import *


def get_error_df(clf_choice, with_uncertainty=False, error_type=rmse_str, augmented_evaluation=False, dataset=emsci_str, debug=False):
    if clf_choice in basemodel_names:
        clf_choice_folder = clf_choice + '_sides_are_same'
        model_path = get_model_path(clf_choice_folder, debug=debug, with_uncertainty=with_uncertainty)
    elif clf_choice in custom_model_names:
        model_path = get_model_dir(model=clf_choice, with_uncertainty=with_uncertainty, ordinal=False)
    else:
        raise ValueError(f'clf_choice {clf_choice} not recognized')

    fold_index = None
    if dataset == sygen_str:
        fold_index = 0

    prediction_path = get_predictions_path(model_path, augmented_evaluation=augmented_evaluation, dataset=dataset, fold_index=fold_index)
    predictions = pd.read_csv(prediction_path, index_col=0)
    if error_type == lems_str:
        error_df = get_lems_from_predictions(predictions)
    elif error_type == rmse_str:
        error_df = get_error_df_from_predictions(predictions, dataset=dataset)
    else:
        raise ValueError(f'error_type {error_type} not recognized')
    return error_df


def get_error_df_from_predictions(predictions, dataset=emsci_str):
    data_sequence_unscaled = pickle.load(
        open(get_data_dict_splits_full_sequences_path(scaled=False, augmented=False, test_set_augmented=False, dataset=dataset),
             'rb'))
    x_use = data_sequence_unscaled[0][x_use_str]
    y_use = data_sequence_unscaled[0][y_use_str]

    ids = predictions.columns[1:]
    error_df = pd.DataFrame(index=ids, columns=[rmse_str, timeX_str, timeY_str])
    # add timeX and timeY to error_df
    for i, index in enumerate(ids):
        error_df.loc[index, timeX_str] = x_use.loc[index][timeX_str]
        error_df.loc[index, timeY_str] = x_use.loc[index][timeY_str]

    nli_indices = [dermatomes_withC1.index(i) for i in predictions.index.str[-2:]]
    for i, index in enumerate(ids):
        ind_NLI_patient = x_use.loc[str(index), ind_NLI_str]

        prediction = predictions.loc[:, index]
        nli_indices_to_use = nli_indices > ind_NLI_patient

        prediction_below_NLI = prediction[nli_indices_to_use]

        real_below_nli = y_use.loc[index][nli_indices_to_use]

        rmse_test_patient = np.sqrt(mean_squared_error(real_below_nli, prediction_below_NLI))

        error_df.loc[index, rmse_str] = rmse_test_patient

    return error_df


def get_lems_from_predictions(predictions):
    data_sequence_unscaled = pickle.load(
        open(get_data_dict_splits_full_sequences_path(scaled=False, augmented=False, test_set_augmented=False),
             'rb'))
    x_use = data_sequence_unscaled[0][x_use_str]
    y_use = data_sequence_unscaled[0][y_use_str]

    ids = predictions.columns[1:]
    error_df = pd.DataFrame(index=ids, columns=[lems_str, timeX_str, timeY_str])
    # add timeX and timeY to error_df
    for i, index in enumerate(ids):
        error_df.loc[index, timeX_str] = x_use.loc[index][timeX_str]
        error_df.loc[index, timeY_str] = x_use.loc[index][timeY_str]

    nli_indices = [dermatomes_withC1.index(i) for i in predictions.index.str[-2:]]
    for i, index in enumerate(ids):

        prediction = predictions.loc[:, index]
        lems_indices_to_use = np.array(nli_indices) >= dermatomes_withC1.index('L2')

        # prediction L2 and below
        lems_predicted = prediction[lems_indices_to_use]
        lems_real = y_use.loc[index][lems_indices_to_use]

        lems_test_patient = np.sum(np.abs(lems_real - lems_predicted))

        error_df.loc[index, lems_str] = lems_test_patient

    return error_df


