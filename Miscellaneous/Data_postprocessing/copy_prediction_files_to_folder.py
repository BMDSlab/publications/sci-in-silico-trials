
import pickle

import numpy as np
import pandas as pd

from Miscellaneous.Misc.fields_to_match import *
from Miscellaneous.Misc.model_names import custom_model_names
from Miscellaneous.Misc.paths import *
from Miscellaneous.Misc.columns import *
import shutil


def copy_prediction_files_to_folder(impute_5_nli_and_above=True, debug=False, augmented_evaluation=True, uncertainties=[True], datasets=[emsci_str]):
    if impute_5_nli_and_above:
        # load unscaled data
        path_unscaled = open(
            get_data_dict_splits_full_sequences_path(scaled=False, augmented=False,
                                                     test_set_augmented=False),
            'rb')
        sequences_unscaled = pickle.load(path_unscaled)
        X_unscaled = sequences_unscaled[0]['X_use']
        nli_emsci = X_unscaled[ind_NLI_str]

        # get nli sygen
        path_unscaled_sygen = open(
            get_data_dict_splits_full_sequences_path(scaled=False, augmented=False,
                                                     test_set_augmented=False, dataset=sygen_str),
            'rb')
        sequences_unscaled_sygen = pickle.load(path_unscaled_sygen)
        X_unscaled_sygen = sequences_unscaled_sygen[0]['X_use']
        nli_sygen = X_unscaled_sygen[ind_NLI_str]

    #uncertainties = [True, False]
    #uncertainties = [False]

    models_to_copy = basemodel_names + [RF_MS_same_str] + [CNN_str, ensemble_str, transformer_str, transformer_sequence_str,
                                        CNN_sequence_str]  # custom_model_names

    for dataset in datasets:
        for clf_choice in models_to_copy:
            for with_uncertainty in uncertainties:
                #try:
                    if clf_choice in basemodel_names:
                        clf_choice_folder = clf_choice + '_sides_are_same'
                        if clf_choice == RF_MS_same_str:
                            clf_choice_folder = 'RF_sides_are_same_MS_are_same'
                        model_path = get_model_path(clf_choice_folder, debug=debug, with_uncertainty=with_uncertainty)

                    elif clf_choice in custom_model_names:
                        model_path = get_model_dir(model=clf_choice, with_uncertainty=with_uncertainty, ordinal=False)

                    if dataset == emsci_str:
                        prediction_path = get_predictions_path(model_path, augmented_evaluation=False, dataset=dataset)
                        if augmented_evaluation:
                            prediction_augmented_path = get_predictions_path(model_path, augmented_evaluation=True)
                            prediction_augmented_lower_path = get_predictions_path(model_path, augmented_evaluation=True,
                                                                                   lower_conf=True)
                            prediction_augmented_upper_path = get_predictions_path(model_path, augmented_evaluation=True,
                                                                                   upper_conf=True)

                            prediction_paths = [prediction_path, prediction_augmented_path, prediction_augmented_lower_path,
                                                prediction_augmented_upper_path]
                        else:
                            prediction_paths = [prediction_path]
                    elif dataset == sygen_str:
                        prediction_paths = []
                        for i in range(5):
                            prediction_path = get_predictions_path(model_path, augmented_evaluation=False,
                                                                   dataset=dataset, fold_index=i)
                            prediction_paths.append(prediction_path)

                    if with_uncertainty:
                        with_uncertainty_str = 'trained_with_augmentation'
                    else:
                        with_uncertainty_str = 'trained_without_augmentation'

                    if dataset == emsci_str:
                        dataset_path = get_base_path()
                    elif dataset == sygen_str:
                        dataset_path = get_sygen_path()
                    else:
                        raise ValueError('Dataset not recognized')
                    print(dataset_path, temp_name, clf_choice, with_uncertainty_str)
                    dst = os.path.join(dataset_path, 'predictions', temp_name, clf_choice, with_uncertainty_str)

                    Path(dst).mkdir(parents=True, exist_ok=True)

                    for path in prediction_paths:
                        #print(path, dst)
                        shutil.copy(path, dst)
                        # print full path
                        print(os.path.join(dst, path))

                    if impute_5_nli_and_above:
                        if dataset == emsci_str:
                            nli = nli_emsci

                        elif dataset == sygen_str:
                            nli = nli_sygen

                        file_paths = prediction_paths
                        for file_path in file_paths:

                            filename = file_path.split('/')[-1]
                            new_file_path = os.path.join(dst, filename)
                            #print('New file path:', new_file_path)
                            prediction_csv_before_imputation = pd.read_csv(new_file_path)

                            # Convert 'nli' to motor score indices using the 'nli_dermatomes_double' dictionary and 'dermatomes_withC1'
                            nli_converted = nli.apply(lambda x: nli_dermatomes_double[dermatomes_withC1[x]])

                            # Create a copy of the DataFrame for imputation
                            prediction_csv_after_imputation = prediction_csv_before_imputation.copy()

                            if file_path == prediction_augmented_lower_path or file_path == prediction_augmented_upper_path:
                                # check if the second column contain 'Percentile'
                                if 'Percentile' in prediction_csv_after_imputation.columns[1]:
                                    # drop current row of column names and use the first row as header
                                    prediction_csv_after_imputation.columns = prediction_csv_after_imputation.iloc[0]
                                    prediction_csv_after_imputation = prediction_csv_after_imputation.drop(0)
                                    # set the index to the first column
                                    prediction_csv_after_imputation = prediction_csv_after_imputation.set_index(
                                        prediction_csv_after_imputation.columns[0])
                                    # drop 'motor score index' column
                                    prediction_csv_after_imputation = prediction_csv_after_imputation.drop(
                                        motor_score_index_str, axis=0)
                                    # reset the index
                                    prediction_csv_after_imputation = prediction_csv_after_imputation.reset_index()
                                    # change column name id_str to motor score index
                                    prediction_csv_after_imputation = prediction_csv_after_imputation.rename(
                                        columns={id_str: motor_score_index_str})

                            for column_name, start_row in nli_converted.items():

                                to_impute = np.where(
                                    prediction_csv_after_imputation.index <= start_row + 1,
                                    5,
                                    prediction_csv_after_imputation.loc[:, column_name]
                                )
                                prediction_csv_after_imputation.loc[:, column_name] = to_impute

                            # assert there are no nan values
                            assert prediction_csv_after_imputation.isna().sum().sum() == 0, 'There are NaN values in the imputed DataFrame'
                            # save the file
                            # drop unnamed column


                            # set motor score index to index
                            prediction_csv_after_imputation = prediction_csv_after_imputation.set_index(motor_score_index_str)

                            #print(prediction_csv_after_imputation.columns[:5])
                            prediction_csv_after_imputation.to_csv(new_file_path)

                    print('Successfully copied', clf_choice, 'with uncertainty', with_uncertainty)




