from Interpretability.shap_custom import store_shap_file
from Interpretability.shap_local_plot import plot_shap_figures
from ML_models.CNN_alt.CNN1D_alt import train_custom_model
from ML_models.CNN_alt.CNN_parameters import get_custom_model_parameters
from ML_models.CNN_alt.evaluate_CNNs import evaluate_custom_model_by_path
from Miscellaneous.Data_postprocessing.copy_prediction_files_to_folder import copy_prediction_files_to_folder
from Miscellaneous.Misc.model_names import *
from Miscellaneous.Misc.paths import hp_run_folder
from Miscellaneous.Plotting.AIS_eval_groups import run_AIS_eval
from Miscellaneous.Plotting.plot_time_evaluation import plot_time_eval_2
from Run_collection.Run_prediction.mainPrediction import run
from Run_collection.Run_files.full_performance_table import full_performance_table
from Run_collection.Run_files.run_eval_from_prediction_file import run_all_eval

if __name__ == '__main__':
    debug = True  # Change to False for full training
    with_uncertainty = True
    # Train custom models

    for model in custom_model_names:
        parameters = get_custom_model_parameters(debug=False)
        new_hp = True
        parameters[new_hp_str] = new_hp
        n_epochs = 5  # Change for full training
        parameters['n_epochs'] = n_epochs
        batch_size = 20  # Change for full training
        parameters['bs'] = batch_size
        parameters['with_uncertainty'] = True
        parameters[model] = True
        train_custom_model(parameters, debug=debug)

        evaluate_custom_model_by_path(model_type=model, parameters=parameters,
                                      run_folder=hp_run_folder,
                                      with_uncertainty=with_uncertainty, augmented=False, dataset=emsci_str,
                                      augmented_model=with_uncertainty)
    # # #
    # # # # Train benchmark models
    # # # # To run specific models, change the models_to_run parameter to a list of the models you want to run (e.g. ['RF', 'CNN'])
    # # # # augmented_evaluation=True will run the model on the augmented dataset
    # # #
    run(with_uncertainty=with_uncertainty, sides_are_same=True, debug=debug, run_model_training=True,
        models_to_run=basemodel_names, augmented_evaluation=False, MS_are_same=False)
    # # # # #
    # # # # # Train Random forest where the prediction model for MS0 and MS1-5 are the same
    run(with_uncertainty=with_uncertainty, sides_are_same=True, debug=debug, run_model_training=True,
        models_to_run=['RF'], augmented_evaluation=False, MS_are_same=True)
    #
    #
    # # Run evaluation script for all models (emsci and sygen)
    copy_prediction_files_to_folder(impute_5_nli_and_above=False, debug=True, augmented_evaluation=False)
    # #
    run_all_eval(augmented_evaluations=[False], with_uncertainties=[True], datasets=[emsci_str], times=[None])
    # Evaluation split into AIS groups

    run_AIS_eval(dataset=emsci_str, augmented_test_set=False)
    #
    # # Create table of results (RMSE)
    full_performance_table(sum_of_residuals=False, include_sygen_ais=False, datasets=[emsci_str], with_uncertainty=True,
                           augmented_evaluation=False, times=[None])
    #
    # # Create table of results (sum of residuals)
    full_performance_table(sum_of_residuals=True, datasets=[emsci_str], with_uncertainty=True, augmented_evaluation=False,
                           times=[None])
    
    # Create SHAP results
    for fold_index in range(5):
        print('Fold', fold_index)
        store_shap_file(num_examples=5000, dataset=emsci_str, dataset_part='test',
                        with_uncertainty=with_uncertainty, model_type=CNN_str, fold_index=fold_index)

    # Plot SHAP results
    plot_shap_figures(model_type=CNN_str, with_uncertainty=True, num_examples=5000, dataset=emsci_str,
                      skip_groups=True, skip_individual=True, skip_AIS=True, skip_ms_split=True, skip_ranking=False,
                      show=False, dataset_part='test')

    # Plot RMSE over time evaluation for CNN and RF
    plot_time_eval_2(CNN_str, augmented=True, window=7, read_plot_data=False, dataset=emsci_str, color='orange')
    plot_time_eval_2(RF_str, augmented=True, window=7, read_plot_data=False, dataset=emsci_str, color='grey', debug=debug)
