def clean_names(feature_names):
    # feature names dict, remove '_'
    feature_names = [f.replace('_', ' ') for f in feature_names]
    # change age to Age and VAC_Yes to VAC
    feature_names = [f.replace('age', 'Age') for f in feature_names]
    feature_names = [f.replace('VAC Yes', 'VAC') for f in feature_names]
    feature_names = [f.replace('DAP Yes', 'DAP') for f in feature_names]
    feature_names = [f.replace('timeX', 'Init time') for f in feature_names]
    feature_names = [f.replace('timeY', 'End time') for f in feature_names]
    feature_names = [f.replace('ind NLI', 'NLI') for f in feature_names]

    feature_names = [f.replace('S45', 'S4-5') for f in feature_names]

    return feature_names