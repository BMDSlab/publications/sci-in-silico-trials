import numpy as np
import pandas as pd
import shap
from matplotlib import pyplot as plt

from Miscellaneous.Misc.paths import get_shap_plot_path


def get_shap(X, model, clf_choice):
    # shap
    # pred = model.predict(X_train, output_margin=True)
    if clf_choice in ['XGB', 'RF']:
        explainer = shap.TreeExplainer(model)
    elif clf_choice == 'KNN':
        knn_n = 20
        if len(X) < knn_n:
            knn_n = len(X)

        X = shap.kmeans(X, knn_n).data
        explainer = shap.KernelExplainer(model.predict, X)
    elif clf_choice == 'MLP':
        explainer = shap.Explainer(model.predict, X)
    if clf_choice == 'CNN':
        explainer = shap.GradientExplainer(model, X)

    shap_values = explainer.shap_values(X)
    #if type(shap_values) == list:
    #    shap_values = np.array(shap_values)

    return shap_values, X


def plot_shap_figure(X, shap_values_folds, output, debug_str, ms, est_name):
    shap_values = shap_values_folds['shap']
    x_test = shap_values_folds['x']  # not sure if needed

    # bringing back variable names
    plt.figure()
    shap.summary_plot(np.vstack(shap_values), np.vstack(x_test), feature_names=X.columns, show=True)

    fig = plt.gcf()
    plt.tight_layout()
    file_path = get_shap_plot_path(output, ms, est_name, debug_str)
    fig.savefig(file_path, format='pdf')


def shuffle_along_axis(a, axis):
    idx = np.random.rand(*a.shape).argsort(axis=axis)
    return np.take_along_axis(a, idx, axis=axis)


def fill_shap_df(shap_values, X, shap_importance, ms, est_name):
    feature_names = X.columns
    shap_df = pd.DataFrame(shap_values, columns=feature_names)
    vals = np.abs(shap_df.values).mean(0)
    df_vals = pd.DataFrame(list(zip(feature_names, vals)),
                           columns=['col_name', 'feature_importance_vals'])
    df_vals.set_index('col_name', inplace=True)

    indices = shap_importance.index
    sc_x_value_list = []
    if est_name == 'MS0':
        indices = shap_importance.index.drop(["sc_X"])
        sc_x_value_list = [0]  # Should be- 1?

    shap_importance.loc[:, ms + '_class_'] = sc_x_value_list + df_vals.loc[
        indices, 'feature_importance_vals'].to_list()


def get_flattened_shap_values(shap_values_single_output):
    # divide light touch and pinprick (the last dimension of shap_values_single_output[1])
    light_touch_shap_values, pinprick_shap_values = np.split(shap_values_single_output[1], 2, axis=-1)
    output = [shap_values_single_output[0], light_touch_shap_values, pinprick_shap_values, shap_values_single_output[2]]

    flattened_shaps = np.concatenate([s.reshape(s.shape[0], -1) for s in output], axis=1)
    return flattened_shaps


def find_rows_and_submatrix_where_columns_are_value(matrix, column_indices, value_min=1, value_max=1):
    """
    Find row indices in a 2D matrix where specified columns' values are all 1 and return the submatrix of these rows.

    Parameters:
        matrix (np.ndarray): The input 2D matrix.
        column_indices (list): A list of column indices to check.

    Returns:
        tuple: A tuple containing an array of row indices and the submatrix where all specified columns' values are 1.
    """
    if any(col >= matrix.shape[1] for col in column_indices):
        raise ValueError("One or more column indices are out of the matrix's column bounds")

    # Step 1: Check if the matrix is a pandas DataFrame
    if type(matrix) != np.ndarray:
        matrix = matrix.values
    range_condition = np.logical_and(matrix[:, column_indices] >= value_min, matrix[:, column_indices] <= value_max)
    # Compute a boolean mask where any specified columns are 1

    # Step 5: Check if any row satisfies the condition
    condition = np.any(range_condition, axis=1)

    # Find indices where the condition is True
    row_indices = np.where(condition)[0]

    # Extract the submatrix using the found row indices

    submatrix = matrix[row_indices]

    return row_indices, submatrix


def find_rows_and_submatrix_where_all_columns_are_zero(matrix, column_indices):
    """
    Find row indices in a 2D matrix where specified columns' values are all 1 and return the submatrix of these rows.

    Parameters:
        matrix (np.ndarray): The input 2D matrix.
        column_indices (list): A list of column indices to check.

    Returns:
        tuple: A tuple containing an array of row indices and the submatrix where all specified columns' values are 1.
    """
    if any(col >= matrix.shape[1] for col in column_indices):
        raise ValueError("One or more column indices are out of the matrix's column bounds")

    # Compute a boolean mask where any specified columns are 1
    condition = np.all(matrix[:, column_indices] == 0, axis=1)

    # Find indices where the condition is True
    row_indices = np.where(condition)[0]

    # Extract the submatrix using the found row indices
    submatrix = matrix[row_indices]

    return row_indices, submatrix


def get_submatrix(shap_matrices, input_matrix, indices, all_zero=False, value_min=1, value_max=1):
    if indices is None or len(indices) == 0:
        return shap_matrices, input_matrix

    if not all_zero:
        subset_indices, _ = find_rows_and_submatrix_where_columns_are_value(input_matrix, indices, value_min=value_min,
                                                                            value_max=value_max)
    else:
        subset_indices, _ = find_rows_and_submatrix_where_all_columns_are_zero(input_matrix, indices)
    input_matrix_subset = input_matrix[subset_indices]
    if type(shap_matrices) == list:
        shap_values_subset = [s[subset_indices].T for s in shap_matrices]
        shap_matrices_subset = []
        for i in range(len(shap_values_subset)):
            shap_matrix = np.concatenate([s.reshape(s.shape[0], -1) for s in shap_values_subset[i]], axis=1)
            #shap_matrix = get_flattened_shap_values(shap_values_subset[i])

            shap_matrices_subset.append(shap_matrix)
    else:
        shap_matrices_subset = shap_matrices[subset_indices]

    return shap_matrices_subset, input_matrix_subset
