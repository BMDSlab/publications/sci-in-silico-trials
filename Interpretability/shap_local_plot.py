from Miscellaneous.Misc.paths import *

import pickle

from matplotlib.colors import ListedColormap, LinearSegmentedColormap
import matplotlib.colors as mcolors
import seaborn as sns


sns.set(font_scale=1.5)

from Interpretability.interpretability_misc import clean_names
from Interpretability.shap_funcs import *
from Miscellaneous.Misc.fields_to_match import *
from Miscellaneous.Misc.model_names import *


def get_main_shap_matrices(model_type, dataset=emsci_str, dataset_part='test', with_uncertainty=False, num_examples=5000,
                           motor_score=None, motor_score_set=None):
    if model_type in custom_model_names:
        shap_matrices = []
        input_matrix = []
        for fold_index in range(5):
            shap_file = shap_file_name(model_type, dataset=dataset, dataset_part=dataset_part,
                                       with_uncertainty=with_uncertainty, num_examples=num_examples,
                                       motor_score=motor_score, motor_score_set=motor_score_set, fold_index=fold_index)

            with open(shap_file, 'rb') as f:
                plot_dict = pickle.load(f)

            shap_matrices_fold = plot_dict['shap_values']
            if model_type in basemodel_names:
                shap_matrices_fold = [shap_matrices_fold]
            input_matrix_fold = plot_dict['input_data']

            shap_matrices.append(shap_matrices_fold)
            input_matrix.append(input_matrix_fold)
        # stack the shap values and input data
        shap_matrices = np.vstack(shap_matrices) #[np.vstack([shap_matrices[i][j] for i in range(5)]) for j in range(len(shap_matrices[0]))]
        input_matrix = np.vstack(input_matrix)

        feature_names = plot_dict['feature_names']

    elif model_type in basemodel_names:
        shap_matrices = {}
        input_matrix = {}

        for motor_score in ms_bothSide:
            for motor_score_set in ['MS0', 'MS1_5']:
                try:
                    shap_file = shap_file_name(model_type, dataset=dataset, dataset_part=dataset_part,
                                               with_uncertainty=with_uncertainty, num_examples=num_examples,
                                               motor_score=motor_score, motor_score_set=motor_score_set)
                    with open(shap_file, 'rb') as f:
                        plot_dict = pickle.load(f)

                    shap_matrices[motor_score_set + '_' + motor_score] = plot_dict['shap_values']
                    input_matrix[motor_score_set + '_' + motor_score] = plot_dict['input_data'].values
                except FileNotFoundError:
                    print(f'No file found for {motor_score} {motor_score_set}')

        feature_names = plot_dict['feature_names']
    else:
        raise ValueError('Model type not recognized')

    return shap_matrices, input_matrix, feature_names


def get_shap_matrices_AIS(model_type, dataset=emsci_str, dataset_part=False, with_uncertainty=False, num_examples=5000,
                          motor_score=None, motor_score_set=None):
    matrices = {}
    shap_matrices, input_matrix, feature_names = get_main_shap_matrices(model_type, dataset=dataset,
                                                                        dataset_part=dataset_part,
                                                                        with_uncertainty=with_uncertainty,
                                                                        num_examples=num_examples,
                                                                        motor_score=motor_score,
                                                                        motor_score_set=motor_score_set)
    # explainer = plot_dict['explainer']

    ais_b_index, ais_c_index, ais_d_index = (feature_names.index('AIS_B'), feature_names.index('AIS_C'),
                                             feature_names.index('AIS_D'))

    shap_matrices_A, input_matrix_A = get_submatrix(shap_matrices, input_matrix,
                                                    [ais_b_index, ais_c_index, ais_d_index],
                                                    all_zero=True)
    shap_matrices_B, input_matrix_B = get_submatrix(shap_matrices, input_matrix, [ais_b_index])
    shap_matrices_C, input_matrix_C = get_submatrix(shap_matrices, input_matrix, [ais_c_index])
    shap_matrices_B_C, input_matrix_B_C = get_submatrix(shap_matrices, input_matrix, [ais_b_index, ais_c_index])
    shap_matrices_D, input_matrix_D = get_submatrix(shap_matrices, input_matrix, [ais_d_index])

    # shap_matrices_ms = {}
    # for ms in ms_bothSide:
    #     for ms_set in ['MS0', 'MS1_5']:
    #         if ms_set == 'MS0':
    #             value_min = 0
    #             value_max = 0
    #         else:
    #             value_min = 1 / 5.000001
    #             value_max = 5 / 4.999999
    #         shap_matrix_ms = get_submatrix(shap_matrices, input_matrix, [feature_names.index(ms)], value_min=value_min,
    #                                        value_max=value_max)
    #         shap_matrices_ms[ms_set + '_' + ms] = shap_matrix_ms

    matrices['all'] = (shap_matrices, input_matrix)
    matrices['A'] = (shap_matrices_A, input_matrix_A)
    matrices['B'] = (shap_matrices_B, input_matrix_B)
    matrices['C'] = (shap_matrices_C, input_matrix_C)
    matrices['B_C'] = (shap_matrices_B_C, input_matrix_B_C)
    matrices['D'] = (shap_matrices_D, input_matrix_D)

    # matrices['shape_matrices_ms'] = shap_matrices_ms

    matrices['feature_names'] = feature_names

    matrices['AIS_B'] = ais_b_index
    matrices['AIS_C'] = ais_c_index
    matrices['AIS_D'] = ais_d_index

    return matrices


def get_shap_matrices_base(model_type, dataset=emsci_str, dataset_part=False, with_uncertainty=False, num_examples=5000,
                           motor_score=None, motor_score_set=None):
    matrices = {}
    shap_matrices, input_matrix, feature_names = get_main_shap_matrices(model_type, dataset=dataset,
                                                                        dataset_part=dataset_part,
                                                                        with_uncertainty=with_uncertainty,
                                                                        num_examples=num_examples,
                                                                        motor_score=motor_score,
                                                                        motor_score_set=motor_score_set)

    matrices['all'] = (shap_matrices, input_matrix)

    matrices['feature_names'] = feature_names

    return matrices


def get_shap_matrix_for_ms(model_type, ms_indices_and_values, dataset=emsci_str, training_set=False,
                           with_uncertainty=False, num_examples=5000):
    matrices = {}
    shap_file = shap_file_name(model_type, dataset=dataset, dataset_part=training_set,
                               with_uncertainty=with_uncertainty, num_examples=num_examples)
    # shap_file = "/Users/shaakansson/Documents/SCI_MLzoo/output_shap/SHAP_values_CNN_small.pkl"
    with open(shap_file, 'rb') as f:
        plot_dict = pickle.load(f)

    shap_matrices = plot_dict['shap_values']
    input_matrix = plot_dict['input_data']
    feature_names = plot_dict['feature_names']
    # explainer = plot_dict['explainer']
    for ms_name, value in ms_indices_and_values.items():
        ms_index = feature_names.index(ms_name)
        shap_matrices_ms, input_matrix_ms = get_submatrix(shap_matrices, input_matrix, [ms_index], value=value)
        matrices[ms_name] = (shap_matrices_ms, input_matrix_ms)

    matrices['feature_names'] = feature_names

    return matrices


def get_shap_ranking(shap_matrix, input_matrix, feature_names, class_names, show=False):
    feature_names = clean_names(feature_names)

    class_names = clean_names(class_names)

    shap_importance = pd.DataFrame({
        'feature': feature_names,
        'importance': np.mean(np.abs(shap_matrix), axis=0)
    })

    # Rank features by importance
    shap_importance = shap_importance.sort_values(by='importance', ascending=False)

    return shap_importance


def show_and_store_plot(shap_matrix, input_matrix, feature_names, class_names, title, filename, model_type,
                        with_uncertainty, subfolder=None, show=True, training_set=False, dataset=emsci_str,
                        num_examples=5000,
                        verbose=True, color='orange'):
    feature_names = clean_names(feature_names)
    # remove '_' from the title
    title = title.replace('_', ' ')

    # custom seaborn colormap with 20 colors starting with dark blue, going to white, and ending with the orange
    colors = ["darkblue", color]

    # Create a custom colormap
    n_bins = 20  # Number of colors in the colormap
    cmap_name = "custom_colormap"
    cm = LinearSegmentedColormap.from_list(cmap_name, colors, N=n_bins)

    # A diverging colormap with 20 colors starting with dark blue, going to white, and ending with the orange, 20 colors
    # cmap = LinearSegmentedColormap.from_list('mycmap', [(0, 'darkblue'), (0.5, 'white'), (1, 'darkorange')], N=20)
    # cmap = ListedColormap(sns.color_palette("Wistia", 20).as_hex())
    # cmap = cmap.reversed()

    class_names = clean_names(class_names)

    shap.summary_plot(shap_matrix, features=input_matrix, feature_names=feature_names,
                      class_names=class_names, show=False, cmap=cm)

    # set size of label for the colorbar

    # increase the font size of the plot
    font_size = 18

    plt.rcParams.update({'font.size': font_size})
    # increase the font size of the x-axis labels
    plt.xticks(fontsize=font_size)
    # increase the font size of the y-axis labels
    plt.yticks(fontsize=font_size)

    # increase the font size of the colorbar labels
    cbar = plt.gcf().axes[-1]  # Get the last axis object which should be the colorbar
    cbar.tick_params(labelsize=font_size - 2)  # Set the font size

    plt.title(title)
    # increase the size of the plot upwards
    # plt.subplots_adjust(top=0.95)
    plt.tight_layout()

    if with_uncertainty:
        with_uncertainty_str = 'with_uncertainty'
    else:
        with_uncertainty_str = 'without_uncertainty'

    # make subfolder with model_type
    if training_set:
        data_set_str = 'training_set'
    else:
        data_set_str = 'test_set'

    out = os.path.join(dataset + '_' + str(num_examples), model_type, with_uncertainty_str, data_set_str)
    if subfolder is not None:
        out = os.path.join(out, subfolder)
    Path(out).mkdir(parents=True, exist_ok=True)
    save_path = os.path.join(out, filename)
    if verbose:
        print('Saving to', save_path)
    plt.savefig(save_path, format='pdf')
    if show:
        plt.show()

    plt.close()
    plt.clf()


def plot_groups(matrices, model_type, with_uncertainty=False, training_set=False, dataset=emsci_str, num_examples=5000):
    shap_matrices, input_matrix = matrices['all']
    shap_matrices_A, input_matrix_A = matrices['A']
    shap_matrices_B, input_matrix_B = matrices['B']
    shap_matrices_C, input_matrix_C = matrices['C']
    shap_matrices_B_C, input_matrix_B_C = matrices['B_C']
    shap_matrices_D, input_matrix_D = matrices['D']

    feature_names = matrices['feature_names']

    # make loop for all AIS grades

    show_and_store_plot(shap_matrices, input_matrix, feature_names, ms_both_sides_fields_toMatch, 'All AIS',
                        'shap_all_AIS.pdf', model_type, with_uncertainty, training_set=training_set, dataset=dataset,
                        num_examples=num_examples)
    show_and_store_plot(shap_matrices_A, input_matrix_A, feature_names, ms_both_sides_fields_toMatch, 'AIS_A',
                        'shap_AIS_A.pdf', model_type, with_uncertainty, training_set=training_set, dataset=dataset,
                        num_examples=num_examples)
    show_and_store_plot(shap_matrices_B, input_matrix_B, feature_names, ms_both_sides_fields_toMatch, 'AIS_B',
                        'shap_AIS_B.pdf', model_type, with_uncertainty, training_set=training_set, dataset=dataset,
                        num_examples=num_examples)
    show_and_store_plot(shap_matrices_C, input_matrix_C, feature_names, ms_both_sides_fields_toMatch, 'AIS_C',
                        'shap_AIS_C.pdf', model_type, with_uncertainty, training_set=training_set, dataset=dataset,
                        num_examples=num_examples)
    show_and_store_plot(shap_matrices_B_C, input_matrix_B_C, feature_names, ms_both_sides_fields_toMatch,
                        'AIS_B and AIS_C',
                        'shap_AIS_B_C.pdf', model_type, with_uncertainty, training_set=training_set, dataset=dataset,
                        num_examples=num_examples)
    show_and_store_plot(shap_matrices_D, input_matrix_D, feature_names, ms_both_sides_fields_toMatch, 'AIS_D',
                        'shap_AIS_D.pdf', model_type, with_uncertainty, training_set=training_set, dataset=dataset,
                        num_examples=num_examples)


def plot_individual_ms(matrices, with_uncertainty=False, dataset=emsci_str, model_type=CNN_str, training_set=False,
                       show=False, debug=False, num_examples=5000, skip_AIS=False, skip_ms_split=False):
    shap_matrices, input_matrix = matrices['all']
    feature_names = matrices['feature_names']
    ais_b_index = matrices['AIS_B']
    ais_c_index = matrices['AIS_C']
    ais_d_index = matrices['AIS_D']

    ms_0_str = 'All_MS: 0'
    ms_1_5_str = 'All_MS: 1-5'

    group_dict = {}
    if not skip_AIS:
        ais_dict = {'A': ais_b_index, 'B': ais_c_index, 'C': ais_d_index, 'All': None}
        group_dict.update(ais_dict)
    if not skip_ms_split:
        ms_set_dict = {ms_0_str: None, ms_1_5_str: None}
        group_dict.update(ms_set_dict)

    for feature, feature_index in group_dict.items():
        print(feature)
        all_zeros = False
        value_min = None
        value_max = None
        if feature == 'A':
            feature_index = [ais_b_index, ais_c_index, ais_d_index]
            all_zeros = True
        elif feature in ['B', 'C', 'D']:
            feature_index = [feature_index]
            all_zeros = False
            value_min = 1
            value_max = 1
        elif feature == 'All':
            feature_index = None
        elif feature == ms_0_str:
            value_min = 0
            value_max = 0
            all_zeros = False
        elif feature == ms_1_5_str:
            value_min = 1 / 5.000001
            value_max = 5 / 4.999999
            all_zeros = False
        else:
            feature_index = [feature_index]
        for i, ms in enumerate(ms_both_sides_fields_toMatch):
            if feature in [ms_0_str, ms_1_5_str]:
                feature_index = [feature_names.index(ms)]

            inp_mat = input_matrix
            shap_mat = shap_matrices

            # subfolder = f'{ais_grade}_{ms}'
            shap_matrices_sub, input_matrix_sub = get_submatrix(shap_mat, inp_mat, feature_index,
                                                                all_zero=all_zeros,
                                                                value_min=value_min, value_max=value_max)
            ms_title = ms + ' ' + feature[4:]

            show_and_store_plot(shap_matrices_sub[i], input_matrix_sub, feature_names, ms_both_sides_fields_toMatch,
                                ms_title,
                                f'AIS_{feature}_{ms}.pdf', model_type, with_uncertainty, feature, show=show,
                                dataset=dataset, training_set=training_set, num_examples=num_examples)

            if debug:
                break


def plot_individual_ms_benchmark(matrices, with_uncertainty=False, dataset=emsci_str, model_type=CNN_str,
                                 training_set=False,
                                 show=False, debug=False, num_examples=5000):
    shap_matrices, input_matrix = matrices['all']
    feature_names = matrices['feature_names']

    feature_dict = {'All_MS0': None, 'All_MS1_5': None}

    for feature, feature_index in feature_dict.items():
        print(feature)

        for i, ms in enumerate(ms_bothSide):

            ms_set = feature[4:]
            ms_index_name = ms_set + '_' + ms
            try:
                inp_mat = input_matrix[ms_index_name]
                shap_mat = shap_matrices[ms_index_name]

                show_and_store_plot(shap_mat, inp_mat, feature_names, ms_both_sides_fields_toMatch, ms_index_name,
                                    f'AIS_{feature}_{ms}.pdf', model_type, with_uncertainty, feature, show=show,
                                    dataset=dataset, training_set=training_set, num_examples=num_examples)

                if debug:
                    break

            except KeyError:
                print(f'{ms} not found in {ms_set}')


def plot_grouped_ranking(matrices):
    shap_matrices, input_matrix = matrices['all']
    feature_names = matrices['feature_names']
    class_names = clean_names(ms_both_sides_fields_toMatch)
    # all rankings
    all_rankings = {}
    for i, ms in enumerate(ms_both_sides_fields_toMatch):
        feature_index = [feature_names.index(ms)]

        shap_matrices_sub, input_matrix_sub = get_submatrix(shap_matrices, input_matrix, feature_index,
                                                            all_zero=False,
                                                            value_min=0, value_max=5)

        ranking = get_shap_ranking(shap_matrices_sub[i], input_matrix_sub[i], feature_names, class_names, show=False)

        all_rankings[ms] = ranking

    all_feature_rankings = [all_rankings[ms].reset_index()['feature'] for ms in ms_both_sides_fields_toMatch]
    # concatenate all rankings
    all_rankings_df = pd.DataFrame(all_feature_rankings, index=ms_both_sides_fields_toMatch)
    # make total ranking
    # for feature in list(all_rankings_df.iloc[0].unique()):
    #     rank_sums[feature] = 0
    # Iterate over each column and row to update the sum of rankings for each feature
    all_rankings_df_transposed = all_rankings_df.transpose()
    # score_types = [['MS'], ['LT'], ['PP'], ['MS', 'LT', 'PP'], ['MS', 'LT', 'PP', 'global']]
    score_types = [['MS', 'LT', 'PP']]
    for score_type in score_types:
        rank_sums_collection = {}

        for column in all_rankings_df_transposed.columns:
            for rank, feature in enumerate(all_rankings_df_transposed[column], start=1):
                feature_output = column[4:]
                feature_check = feature[4:]
                feature_type = feature[1:3]
                output_side = column[0]
                check_side = feature[0]
                if feature_check == 'S4-5':
                    feature_check = 'S45'

                if feature_type not in ['MS', 'LT', 'PP']:
                    feature_type = 'non-distance feature'

                if score_type == ['MS']:
                    index_list = ms_bothSide
                elif score_type in [['LT'], ['PP']]:
                    index_list = dermatomes_withC1
                elif score_type == ['MS', 'LT', 'PP']:
                    index_list = dermatomes_withC1
                elif score_type == ['MS', 'LT', 'PP', 'global']:
                    index_list = dermatomes_withC1
                if feature_type == 'non-distance feature':
                    feature_group = feature
                else:
                    dist = index_list.index(feature_check) - index_list.index(feature_output)
                    same_side = output_side == check_side
                    feature_group = dist
                    feature_type = f'{feature_type} {"same" if same_side else "opposite"}'
                if feature_type not in rank_sums_collection:
                    rank_sums_collection[feature_type] = {}
                if feature_group not in rank_sums_collection[feature_type]:
                    rank_sums_collection[feature_type][feature_group] = []

                rank_sums_collection[feature_type][feature_group].append(rank)

        for feature_type, rank_sums in rank_sums_collection.items():
            # Calculate the mean rank for each feature
            for feature_group, ranks in rank_sums.items():
                rank_sums[feature_group] = np.mean(ranks), np.std(ranks)


        # create dataframe from rank_sums_collection
        temp_df = pd.concat({k: pd.Series(v) for k, v in rank_sums_collection.items()}, axis=1).reset_index()

        motor_sensory_scores_with_sides = ['MS same', 'MS opposite', 'LT same', 'LT opposite', 'PP same', 'PP opposite']
        value_vars = motor_sensory_scores_with_sides + ['non-distance feature']
        rank_sums_df = pd.melt(temp_df, id_vars=['index'], value_vars=value_vars, value_name='TotalRank',
                               var_name='Feature')
        # drop nans
        rank_sums_df = rank_sums_df.dropna()
        # rank_sums_df = pd.DataFrame(list(rank_sums.items()), columns=['Feature', 'TotalRank'])

        # Sort the DataFrame by the TotalRank column
        rank_sums_df_sorted_all = rank_sums_df.sort_values(by='TotalRank', ascending=True)
        vmin = 1
        vmax = len(rank_sums_df_sorted_all) + 1
        value_list = list(range(vmin, vmax))
        value_types = {'rank': value_list,
                       'value': rank_sums_df_sorted_all['TotalRank'].astype(str)}
        rank_sums_df_sorted_all['value'] = value_list
        rank_sums_df_sorted_all['annot_temp'] = rank_sums_df_sorted_all['TotalRank'].apply(lambda t: tuple(round(x, 2) for x in t)).astype(str)
        # remove parenthesis from annotation
        rank_sums_df_sorted_all['annotation'] = rank_sums_df_sorted_all['annot_temp'].str.replace('(', '')
        # add parenthesis to second value after comma
        rank_sums_df_sorted_all['annotation'] = rank_sums_df_sorted_all['annotation'].str.replace(', ', ' (', 1)

        rank_sums_df_sorted_all['annot_temp_extra'] = rank_sums_df_sorted_all['TotalRank'].apply(lambda t: round(t[0], 1)).astype(str)
        # remove parenthesis from annotation
        rank_sums_df_sorted_all['annot_extra'] = rank_sums_df_sorted_all['annot_temp_extra'].str.replace('(', '')
        # add parenthesis to second value after comma
        rank_sums_df_sorted_all['annot_extra'] = rank_sums_df_sorted_all['annot_extra'].str.replace(', ', ' (', 1)

        max_color = rank_sums_df_sorted_all['annot_temp_extra'].astype(float).max()
        norm = mcolors.Normalize(vmin=vmin, vmax=max_color)

        for value_type, v in value_types.items():
            annotation = True
            fmt = 'd'
            # split the dataframe by feature and keep index
            rank_sums_dfs = {}
            for feature in motor_sensory_scores_with_sides + ['non-distance feature']:
                rank_sums_dfs[feature] = rank_sums_df_sorted_all[rank_sums_df_sorted_all['Feature'] == feature]

            rank_sums_dfs['MS'] = rank_sums_dfs['MS same'].merge(rank_sums_dfs['MS opposite'], on='index')
            rank_sums_dfs['LT'] = rank_sums_dfs['LT same'].merge(rank_sums_dfs['LT opposite'], on='index')
            rank_sums_dfs['PP'] = rank_sums_dfs['PP same'].merge(rank_sums_dfs['PP opposite'], on='index')
            rank_sums_dfs['LT_PP'] = rank_sums_dfs['LT'].merge(rank_sums_dfs['PP'], on='index')

            rank_sums_dfs_merged = {'MS': rank_sums_dfs['MS'],
                                    'LT_PP': rank_sums_dfs['LT_PP'], 'non_distance': rank_sums_dfs['non-distance feature']}


            # dict with all dataframes

            for df_name, rank_sums_df_sorted in rank_sums_dfs_merged.items():

                rank_sums_df_resorted = rank_sums_df_sorted.sort_values(by='index', ascending=True)
                # all column names starting with "values"
                value_cols = [col for col in rank_sums_df_resorted.columns if 'annot_temp_extra' in col]
                annot_cols = [col for col in rank_sums_df_resorted.columns if 'annotation' in col]
                annot_extra_cols = [col for col in rank_sums_df_resorted.columns if 'annot_extra' in col]

                data = rank_sums_df_resorted[['index'] + value_cols + annot_cols + annot_extra_cols]
                data_sorted = data.sort_values('index')
                # if dimensionality is 1 then reshape the data
                if len(value_cols) == 1:
                    to_plot = data_sorted['annot_temp_extra'].values.reshape(1, -1).astype(float)
                    indices = data_sorted['index'].values.reshape(1, -1)
                    if value_type == 'value':
                        annotation = data_sorted[annot_cols].values
                        fmt = 's'
                    elif value_type == 'rank':
                        annotation = data_sorted[annot_extra_cols].values
                        fmt = 's'
                    yticks = indices.T
                    yticks = list(str(x[0]) for x in yticks)
                    to_plot_transposed = to_plot.T
                else:
                    to_plot = data_sorted[value_cols].values.astype(float)
                    indices = data_sorted['index'].values
                    if value_type == 'value':
                        annotation = data_sorted[annot_cols].values
                        fmt = 's'
                    elif value_type == 'rank':
                        annotation = data_sorted[annot_extra_cols].values
                        fmt = 's'
                    yticks = indices
                    to_plot_transposed = to_plot

                n_cols = len(value_cols)
                if df_name == 'non_distance':
                    n_cols -= 2
                    if value_type == 'value':
                        n_cols -= 1
                elif df_name == 'MS':
                    n_cols -= 1

                # Step 3: Create the heatmap
                length_increase = 1
                if value_type == 'rank':
                    plt.figure(figsize=(n_cols + 4, 12+length_increase))
                else:
                    plt.figure(figsize=(n_cols + 6, 14+length_increase))
                # cmap = 'YlGnBu'
                if score_type == ['MS']:
                    cmap = 'plasma_r'
                elif score_type in [['LT'], ['PP']]:
                    cmap = 'viridis_r'
                else:
                    cmap = 'plasma_r'

                if df_name == 'non_distance':
                    # change sex m to Sex
                    yticks = [x.replace('sex m', 'Sex') for x in yticks]
                    cbar = False
                else:
                    cbar = True
                same_label = 'Ipsilateral'
                opposite_label = 'Contralateral'
                if df_name == 'MS':
                    xticklabels = [same_label, opposite_label]
                elif df_name == 'LT_PP':
                    xticklabels = [same_label, opposite_label, same_label, opposite_label]
                else:
                    xticklabels = []

                ax = sns.heatmap(to_plot_transposed, annot=annotation, cmap=cmap, cbar=cbar, xticklabels=xticklabels,
                                 yticklabels=yticks,
                                 norm=norm, fmt=fmt)

                ax.xaxis.set_ticks_position('top')
                ax.xaxis.set_label_position('top')
                # decrease x-axis label font size
                #plt.xticks(fontsize=13)
                # rotate x-axis labels
                #plt.xticks(rotation=45)
                if df_name != 'non_distance':
                    rect = get_rect(to_plot_transposed)
                    ax.add_patch(rect)


                # plt.xticks(rotation=90)
                # set format of annotations to integer

                # set x-axis label orientation
                if df_name != 'non_distance':
                    ax.set_ylabel('Distance from MS')
                    # Invert the color bar and add a label
                    cbar = ax.collections[0].colorbar
                    cbar.ax.invert_yaxis()  # Invert the color bar
                    cbar.set_label('Rank (importance)', rotation=270, labelpad=15)  # Add label
                else:
                    ax.set_ylabel('Feature name')

                # ax.set_title(f'{df_name}')
                # show just every other y-tick label



                # ax.set_xticklabels(ax.get_xticklabels(), rotation=90)
                ax.set_yticklabels(ax.get_yticklabels(), rotation=0)
                # increase the font size of the y-axis labels
                plt.yticks(fontsize=20)
                if df_name != 'non_distance':
                    # make sure 0 is included in the x-axis

                    ytick_labels = ['' if int(ytick.get_text()) % 2 != 0 else ytick for ytick in ax.get_yticklabels()]
                    ax.set_yticklabels(ytick_labels)
                elif df_name == 'non_distance':
                    # change "Init time" to First time
                    ytick_labels = [ytick.get_text().replace('Init time', 'First time') for ytick in ax.get_yticklabels()]
                    ax.set_yticklabels(ytick_labels)
                # plt.title('One-Dimensional Heatmap of Rankings')
                plt.tight_layout()

                name = df_name
                # save plot
                Path.mkdir(Path('ranking_heatmap'), exist_ok=True)
                plt.savefig(f'ranking_heatmap/ranking_heatmap_{score_type}{name}{value_type}.pdf', format='pdf')
                plt.show()

def get_rect(data):
    nrows = data.shape[0]
    ncols = data.shape[1]
    middle_row = nrows // 2



    # Add dashed line around the middle row
    rect = plt.Rectangle((0, middle_row), ncols, 1,
                         fill=False, edgecolor='black', linestyle='--', linewidth=4)



    # Add the rectangle to the plot
    return rect


def plot_shap_figures(model_type=CNN_str, with_uncertainty=False, num_examples=5000, dataset='both', motor_score=None,
                      ms_set=None,
                      skip_groups=False, skip_individual=False, skip_AIS=False, skip_ms_split=False, skip_ranking=False,
                      show=True, dataset_part='test'):
    if dataset == 'both':
        datasets = [emsci_str, sygen_str]
    else:
        datasets = [dataset]
    for dataset in datasets:
        if model_type in custom_model_names:
            matrices = get_shap_matrices_AIS(model_type, dataset, dataset_part=dataset_part, with_uncertainty=with_uncertainty,
                                             num_examples=num_examples, motor_score=motor_score, motor_score_set=ms_set,
                                             )
        elif model_type in basemodel_names:
            matrices = get_shap_matrices_base(model_type, dataset, dataset_part=dataset_part,
                                              with_uncertainty=with_uncertainty,
                                              num_examples=num_examples, motor_score=motor_score,
                                              motor_score_set=ms_set,
                                              )
        else:
            raise ValueError('Model type not recognized')

        if not skip_ranking:
            plot_grouped_ranking(matrices)

        if not skip_groups:
            if model_type in custom_model_names:
                plot_groups(matrices, model_type=model_type, with_uncertainty=with_uncertainty, dataset=dataset,
                            num_examples=num_examples)
        if not skip_individual:
            if model_type in custom_model_names:
                plot_individual_ms(matrices, with_uncertainty=with_uncertainty, dataset=dataset, model_type=model_type,
                                   num_examples=num_examples, skip_AIS=skip_AIS, skip_ms_split=skip_ms_split, show=show)
            elif model_type in basemodel_names:
                plot_individual_ms_benchmark(matrices, with_uncertainty=with_uncertainty, dataset=dataset,
                                             model_type=model_type,
                                             num_examples=num_examples)
            else:
                raise ValueError('Model type not recognized')



def plot_ms_at_value(ms_name, value, model_type=CNN_str, with_uncertainty=False, dataset=emsci_str, training_set=False,
                     num_examples=5000):
    ms_index = ms_both_sides_fields_toMatch.index(ms_name)

    ms_indices_and_values = {ms_name: value}
    subfolder = ms_name + '_individual_' + str(value)

    matrices = get_shap_matrix_for_ms(model_type, ms_indices_and_values, dataset=dataset, training_set=training_set,
                                      with_uncertainty=with_uncertainty, num_examples=num_examples)
    shap_matrices_sub, input_matrix_sub = matrices[ms_name]
    feature_names = matrices['feature_names']

    show_and_store_plot(shap_matrices_sub[ms_index], input_matrix_sub, feature_names, ms_both_sides_fields_toMatch,
                        ms_name,
                        f'{ms_name}_{value}.pdf', model_type, with_uncertainty, subfolder=subfolder, show=show,
                        dataset=dataset, training_set=training_set, num_examples=num_examples)


show = True
model_type = CNN_str
with_uncertainty = False
training_set = False
dataset = emsci_str
num_examples = 5000
ms_name = 'LMS_L3'
value = 1


# plot_ms_at_value(ms_name, value, model_type=model_type, with_uncertainty=with_uncertainty, dataset=dataset, training_set=training_set, num_examples=num_examples)


#plot_shap_figures(model_type=CNN_str, with_uncertainty=True, num_examples=5000, dataset=emsci_str, show=False, dataset_part='test')
# plot_shap_figures(model_type=RF_str, with_uncertainty=False, num_examples=10, dataset=emsci_str, motor_score='L3')

# model_type = RF_str
# color = 'grey'
#
# num_examples = 5000
# motor_score = 'L3'
# shap_matrices, input_matrix, feature_names = get_main_shap_matrices(model_type, dataset=dataset,
#                                                                     training_set=training_set,
#                                                                     with_uncertainty=with_uncertainty,
#                                                                     num_examples=num_examples, motor_score=motor_score)
#
# ms = 'MS0 L3'
# show_and_store_plot(shap_matrices[0], input_matrix, feature_names, [ms], ms,
#                     f'MS0_{ms}.pdf', model_type, with_uncertainty, show=show,
#                     dataset=dataset, training_set=training_set, num_examples=num_examples, color=color)
def temp_cnn():
    model_type = CNN_str
    color = 'orange'

    num_examples = 5000
    motor_score = 'C7'
    matrices = get_shap_matrices_AIS(model_type, dataset, dataset_part=False, with_uncertainty=with_uncertainty,
                                     num_examples=num_examples, motor_score=motor_score)
    shap_matrices, input_matrix = matrices['all']
    feature_names = matrices['feature_names']
    ms = 'LMS_' + motor_score

    ms_feature_index = feature_names.index(ms)
    # shap_matrices_0, input_matrix_0 = get_submatrix(shap_matrices, input_matrix, [ms_feature_index], value=0)
    #
    # ms_index = ms_both_sides_fields_toMatch.index(ms)
    # show_and_store_plot(shap_matrices_0[ms_index], input_matrix_0, feature_names, [ms], ms,
    #                     f'MS0_{ms}.pdf', model_type, with_uncertainty, show=show,
    #                     dataset=dataset, training_set=training_set, num_examples=num_examples, color=color)
    #
    ms_index = ms_both_sides_fields_toMatch.index(ms)
    show_and_store_plot(shap_matrices[ms_index], input_matrix, feature_names, [ms], ms,
                        f'MS0_{ms}.pdf', model_type, with_uncertainty, show=show,
                        dataset=dataset, training_set=training_set, num_examples=num_examples, color=color)


# plot_shap_figures(model_type=CNN_str, with_uncertainty=True, num_examples=5000, dataset=emsci_str, skip_groups=False)
def all_bench_shap():
    for motor_score in ms_bothSide:
        for ms_set in ['MS0', 'MS1_5']:
            plot_shap_figures(model_type=RF_str, with_uncertainty=True, num_examples=5000, dataset=emsci_str,
                              motor_score=motor_score, ms_set=ms_set, skip_groups=True, skip_individual=False)


# all_bench_shap()
# plot_shap_figures(model_type=RF_str, with_uncertainty=True, num_examples=5000, dataset=emsci_str,
#                   skip_groups=True, skip_individual=False)



