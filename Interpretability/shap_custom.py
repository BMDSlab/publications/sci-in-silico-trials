import pickle

from tensorflow.keras.models import Model

from Interpretability.shap_funcs import *
from ML_models.CNN_alt.CNN_id_names import get_custom_model_name, get_model_ID_alt
from ML_models.CNN_alt.CNN_parameters import get_custom_model_parameters
from ML_models.CNN_alt.CNN_utils_model_path import load_custom_model_path
from ML_models.CNN_alt.custom_data_generator import get_data_generators
from Miscellaneous.Misc.fields_to_match import dermatomes_withC1, ms_bothSide
from Miscellaneous.Misc.model_names import custom_model_names
from Miscellaneous.Misc.paths import *

# Fixing backend issues
shap.explainers._deep.deep_tf.op_handlers["LeakyRelu"] = shap.explainers._deep.deep_tf.op_handlers["Relu"]
shap.explainers._deep.deep_tf.op_handlers["FusedBatchNormV3"] = shap.explainers._deep.deep_tf.op_handlers[
    "FusedBatchNorm"]
shap.explainers._deep.deep_tf.op_handlers["AddV2"] = shap.explainers._deep.deep_tf.op_handlers["Add"]


# Assuming you've defined a DataLoader that correctly formats your data for the model
# Replace `YourDataLoader` with the actual name of your class or function

def store_shap_file(num_examples=1000, dataset=emsci_str, dataset_part='all', with_uncertainty=False, model_type=CNN_str,
                    start_ms=None, fold_index=0):

    shap_models = {}

    if model_type in custom_model_names:
        parameters = get_custom_model_parameters(debug=False)
        dim = parameters['sample_shape'][1:]

        training_generator, validation_generator, test_generator = get_data_generators(parameters, dim, fold_index,
                                                                                       dataset=dataset)
        generator = test_generator
        if dataset_part == 'training':
            generator = training_generator
            get_data_func = generator.get_N_samples
        elif dataset_part == 'test':
            generator = test_generator
            get_data_func = generator.get_N_samples
        elif dataset_part == 'all':
            get_data_func = generator.get_N_samples_all
        (input_data, y) = get_data_func(num_examples)  # This function should return the prepared input data
        # convert input_data from tuple to list
        input_data = list(input_data)
        input_data = input_data[:-1]

        # print number of examples
        print(input_data[0].shape)

        output = get_model_dir(model=model_type, with_uncertainty=with_uncertainty)
        modelID = get_model_ID_alt(parameters)
        model_name = get_custom_model_name(modelID, fold_index, 'all')
        filepath_model = get_file_path_custom_model(output, model_name, hp_run_folder)
        print(filepath_model)
        model = load_custom_model_path(filepath_model)
        if model_type == ensemble_str:
            #output_layer = model.get_layer('dense_6')
            output_layer = model.get_layer('activation_1')
        else:
            output_layer = model.get_layer('main_output')

        inputs_without_dummy_output = model.inputs[:-1]
        model = Model(inputs=inputs_without_dummy_output, outputs=output_layer.output)

        predictions = model.predict(input_data)
        print(predictions)

        feature_names = training_generator.get_feature_names()

        shap_models[model_type] = (model, input_data, None, None)

    elif model_type in basemodel_names:

        data = {}
        data_path = open(get_data_dict_splits_path(fold_index, scaled=True, augmented=False), 'rb')
        data_fold = pickle.load(data_path)
        data[fold_index] = data_fold



        # concatenate test and train data
        #y = np.concatenate([train_Y, test_Y], axis=0)
        clf_choice_folder = model_type + '_sides_are_same'
        output_clf = get_model_path(clf_choice_folder, debug=False, with_uncertainty=with_uncertainty)
        model_folder = os.path.join(output_clf, 'models')
        motor_scores_estimators_path = get_motor_scores_estimators_path(model_folder, filesave_name, debug_str='', i=fold_index)
        motor_scores_estimators = pickle.load(open(motor_scores_estimators_path, 'rb'))

        #model = motor_scores_estimators['MS0'][motor_score]

        test_data_MS0 = data[fold_index]['X_test_MS0']
        train_data_MS0 = data[fold_index]['X_train_MS0']

        test_data_MS1_5 = data[fold_index]['X_test_MS1_5']
        train_data_MS1_5 = data[fold_index]['X_train_MS1_5']

        feature_names = list(test_data_MS0.columns)
        to_drop = ['Unnamed: 0', 'sc_X', 'level']
        feature_names = [f for f in feature_names if f not in to_drop]

        # merge train and test data
        train_test_data_MS0 = pd.concat([train_data_MS0, test_data_MS0])
        train_test_data_MS1_5 = pd.concat([train_data_MS1_5, test_data_MS1_5])

        data_MS = {'MS0': train_test_data_MS0, 'MS1_5': train_test_data_MS1_5}

        for motor_score in ms_bothSide:
            motor_score_index = ms_bothSide.index(motor_score)
            start_ms_index = ms_bothSide.index(start_ms)
            if start_ms and motor_score_index < start_ms_index:
                continue
            print('Storing SHAP values for', motor_score)
            for motor_score_set in ['MS0', 'MS1_5']:
                train_test_data = data_MS[motor_score_set]

                level_sel = dermatomes_withC1.index(motor_score)
                input_data_sel = train_test_data[(train_test_data['level'] == level_sel)]

                input_data_sel = input_data_sel.drop(to_drop, axis=1)
                input_data = input_data_sel

                model = motor_scores_estimators[motor_score_set][motor_score]
                shap_models[motor_score_set + motor_score] = (model, input_data, motor_score, motor_score_set)

    else:
        raise ValueError('Model type not recognized')

    # try to output predictions for the input data

    for key, (model, input_data, motor_score, motor_score_set) in shap_models.items():
        # Initialize the SHAP Explainer for your model and the prepared data
        if model_type in custom_model_names:
            explainer = shap.DeepExplainer(model, input_data)
        elif model_type in [RF_str, XGB_str]:
            explainer = shap.TreeExplainer(model)
        else:
            explainer = shap.KernelExplainer(model, input_data)
        #explainer = shap.DeepExplainer(model, input_data)

        # batch_size = 10
        # with tqdm(total=num_examples) as pbar:
        #     shap_values = []
        #     for i in range(0, num_examples, batch_size):
        #         batch_data = input_data[i:i + batch_size]
        #         batch_shap_values = explainer.shap_values(batch_data)
        #         shap_values.append(batch_shap_values)
        #         pbar.update(batch_data.shape[0])

        # Calculate SHAP values
        shap_values = explainer.shap_values(input_data)

        # Summarize the SHAP values in a plot

        if model_type in custom_model_names:
            #shap_matrices = []

                #shap_matrix = np.concatenate([s.reshape(s.shape[0], -1) for s in shap_values[i]], axis=1)
            shap_matrices = get_flattened_shap_values(shap_values)

            #shap_matrix = np.concatenate([s.reshape(s.shape[0], -1) for s in shap_values[0]], axis=1)

            #input_matrix = np.concatenate([s.reshape(s.shape[0], -1) for s in input_data], axis=1)
            input_matrix = get_flattened_shap_values(input_data)
        else:
            shap_matrices = shap_values
            input_matrix = input_data
        # store shap values, input data, and feature names together in a pickle file
        plot_dict = {'shap_values': shap_matrices,
                     'input_data': input_matrix,
                     'feature_names': feature_names,
                     #'explainer': explainer # AttributeError: Can't pickle local object 'Layer.add_loss.<locals>._tag_callable'
                     }
        Path(output_shap).mkdir(parents=True, exist_ok=True)
        output_path = shap_file_name(model_type, dataset=dataset, dataset_part=dataset_part,
                                     with_uncertainty=with_uncertainty, num_examples=num_examples,
                                     motor_score=motor_score, motor_score_set=motor_score_set, fold_index=fold_index)
        with open(output_path, 'wb') as f:
            pickle.dump(plot_dict, f)

        print('SHAP values saved to:', output_path)

